public class CreateDefaultBundles {
    @AuraEnabled
    public static SBQQ__Quote__c saveQuote (SBQQ__Quote__c qt, String accid, String oppid) {
        try{
            String pbname = 'Basic '+qt.Carbyne_Control__r.Name;
            qt.SBQQ__PrimaryContact__r = null;
            qt.Carbyne_Control__r = null;
            qt.SBQQ__Opportunity2__c = oppid;
            qt.SBQQ__Account__c = accid;
            insert qt;
            qt.SBQQ__PricebookId__c = qt.Carbyne_Control__c;
            update qt;
            System.debug(pbname);
            integer i=1,parent=0;
            Map <integer,SBQQ__QuoteLine__c> mqls = new Map <integer,SBQQ__QuoteLine__c>();
            Map <integer,List <SBQQ__QuoteLine__c>> sqls = new Map <integer,List <SBQQ__QuoteLine__c>>();
            List <Product2> prods = [select id,ProductCode,Description,(select id,SBQQ__Quantity__c,SBQQ__OptionalSKU__r.ProductCode,SBQQ__OptionalSKU__r.Description,SBQQ__Type__c,SBQQ__Bundled__c from SBQQ__Options__r Order by SBQQ__OptionalSKU__r.ProductCode) from Product2 where Product2.Bundle_Type_And_Price_Book__c=:pbname Order by ProductCode];
            //add all mains
            for(Product2 prod:prods){
                parent=i;
                mqls.put(parent,new SBQQ__QuoteLine__c(SBQQ__ProductSubscriptionType__c='Renewable',SBQQ__SubscriptionType__c='Renewable',SBQQ__Quote__c=qt.id,SBQQ__Product__c=prod.id,SBQQ__Bundle__c=true,SBQQ__Number__c=parent,SBQQ__Quantity__c=1,SBQQ__Description__c=(prod.Description!=null?prod.Description.replaceAll('\n','<br/>'):null)));
                sqls.put(parent,New List <SBQQ__QuoteLine__c>());
                i++;
                for(SBQQ__ProductOption__c opt:prod.SBQQ__Options__r){
                    sqls.get(parent).add(new SBQQ__QuoteLine__c(SBQQ__ProductSubscriptionType__c='Renewable',SBQQ__SubscriptionType__c='Renewable',SBQQ__Quote__c=qt.id,SBQQ__Product__c=opt.SBQQ__OptionalSKU__c,SBQQ__Quantity__c=opt.SBQQ__Quantity__c,SBQQ__Description__c=(opt.SBQQ__OptionalSKU__r.Description!=null?opt.SBQQ__OptionalSKU__r.Description.replaceAll('\n','<br/>'):null),SBQQ__Bundled__c=opt.SBQQ__Bundled__c,SBQQ__Number__c=i,SBQQ__OptionType__c=opt.SBQQ__Type__c,SBQQ__OptionLevel__c=1,SBQQ__ProductOption__c=opt.id,SBQQ__BundledQuantity__c = opt.SBQQ__Quantity__c));
                    i++;
                }
            }
            insert mqls.values();
            List <SBQQ__QuoteLine__c> allsqls = new List <SBQQ__QuoteLine__c>();
            for(Integer par:sqls.keySet()){
                for(SBQQ__QuoteLine__c ql:sqls.get(par)){
                    ql.SBQQ__RequiredBy__c = mqls.get(par).id;
                }
                allsqls.addAll(sqls.get(par));
            }
            insert allsqls; 
            return qt;
        }
        catch(exception e){
            throw new AuraHandledException(e.getMessage());
        }
    }
}
/*
 * @Author: ZedXagE - ITmyWay 
 * @Date: 2018-05-03 16:30:26 
 * @Last Modified by: ZedXagE - ITmyWay
 * @Last Modified time: 2018-05-03 18:23:46
 */

public class DynamicRelatedController {
    private String Curid;
    private String wher;
    private String Fields;
    private String FieldsNoShow;
    private DynamicRelatedSettings__c DRS;
    private String sobjectname;
    private String parentname;
    private String vfname;

    public Decimal lim{get;set;}
    public Decimal offs{get;set;}

    public String fname {get;set;}
    public String ord {get;set;}
    public String errordml {get;set;}
    public String lineid {get;set;}
    public String theme {get;set;}
    public String LinkFieldName {get;set;}
    public String LinkFieldID {get;set;}

    public String LinkFieldNameFormula {get;set;}
    public String LinkFieldIDFormula {get;set;}

    public Boolean inlineedit{get;set;}
    public Boolean allownew{get;set;}
    public Boolean allowdelete{get;set;}
    public Boolean blank{get;set;}
    public Boolean isFields{get;set;}
    public Boolean isLinks{get;set;}

    public List <String> fieldname{get;set;}
    public List <sObject> sobs  {get;set;}

    public	Map <String,String> fieldLabels {get;set;}
    public	Map <String,Boolean> fieldPerm {get;set;}
    public Map <String, Boolean> errMsgs {get;set;}
    
    private void resetErrMsgs() {
        errMsgs = new Map < String, Boolean > ();
        errMsgs.put('curid', false);
        errMsgs.put('sobjectname', false);
        errMsgs.put('cs', false);
        errMsgs.put('dml', false);
    }

    public DynamicRelatedController() {
        //initialize
        offs = 0;
        lineid = '';
        errordml = '';
        inlineedit = false;
        allownew = false;
        allowdelete = false;
        blank = false;
        theme = ApexPages.currentPage().getParameters().get('theme');
        if (theme == '' || theme == null)
            theme = 'false';
        resetErrMsgs();
        sobs = new list < sObject > ();
        //url data
        Curid = ApexPages.currentPage().getParameters().get('id');
        sobjectname = ApexPages.currentPage().getParameters().get('obj');
        vfname = ApexPages.currentPage().getParameters().get('vf');
        /*if(ApexPages.currentPage().getHeaders().get('Referer')!=null&&ApexPages.currentPage().getHeaders().get('Referer').contains('apex/'))
            vfname = ApexPages.currentPage().getHeaders().get('Referer').split('apex/')[1].split('\\?')[0];
        System.debug(vfname);*/
        if (sobjectname.contains('-')) {
            parentname = sobjectname.split('-')[0];
            sobjectname = sobjectname.split('-')[1];
        }
        if (Curid != null && Curid != '') {
            if (sobjectname != null && sobjectname != '') {
                //custom setting data
                List < DynamicRelatedSettings__c > DRSs = [Select Name, Limit__c, LinkFieldId__c, Default_Order__c, LinkFieldName__c, LinkFieldNameFormula__c, LinkFieldIdFormula__c, OrderBy__c, Fields__c, where__c, New__c, Delete__c, InlineEdit__c, FieldLevelPermissions__c From DynamicRelatedSettings__c where Name =: vfname LIMIT 1];
                if (DRSs.size() == 0)
                    DRSs = [Select Name, Limit__c, LinkFieldId__c, Default_Order__c, LinkFieldName__c, LinkFieldNameFormula__c, LinkFieldIdFormula__c, OrderBy__c, Fields__c, where__c, New__c, Delete__c, InlineEdit__c, FieldLevelPermissions__c From DynamicRelatedSettings__c where Name =: parentname+'-'+sobjectname LIMIT 1];
                if (DRSs.size() == 0)
                    DRSs = [Select Name, Limit__c, LinkFieldId__c, Default_Order__c, LinkFieldName__c, LinkFieldNameFormula__c, LinkFieldIdFormula__c, OrderBy__c, Fields__c, where__c, New__c, Delete__c, InlineEdit__c, FieldLevelPermissions__c From DynamicRelatedSettings__c where Name =: sobjectname OR Object__c =: sobjectname LIMIT 1];
                if (DRSs.size() > 0) {
                    DRS = DRSs[0];
                    //initialize from custom setting
                    if (DRS.LinkFieldName__c != null && DRS.LinkFieldID__c != null) {
                        LinkFieldName = DRS.LinkFieldName__c.tolowercase();
                        LinkFieldID = DRS.LinkFieldID__c.tolowercase();
                        isLinks = true;
                    } else
                        isLinks = false;
                    if (DRS.Fields__c != null) {
                        Fields = DRS.Fields__c.tolowercase().replaceAll(' ','');
                        isFields = true;
                    } else
                        isFields = false;
                    LinkFieldNameFormula = DRS.LinkFieldNameFormula__c != null ? DRS.LinkFieldNameFormula__c : '';
                    LinkFieldIDFormula = DRS.LinkFieldIdFormula__c != null ? DRS.LinkFieldIdFormula__c : '/';
                    wher = DRS.where__c.tolowercase();
                    fname = DRS.OrderBy__c.tolowercase();
                    ord = DRS.Default_Order__c;
                    inlineedit = DRS.InlineEdit__c;
                    allownew = DRS.New__c;
                    allowdelete = DRS.Delete__c;
                    lim = DRS.Limit__c!=null?DRS.Limit__c:50;
                    //get field labels
                    if (isFields)
                        initMaps();
                    //query
                    requery();
                    if (sobs.size() == 0)
                        blank = true;
                } else
                    errMsgs.put('cs', true);
            } else
                errMsgs.put('sobjectname', true);
        } else
            errMsgs.put('curid', true);
    }
    //get field labels and permmisions
    public void initMaps() {
        Fieldname = fields.split(',');
        FieldPerm = new Map < String, Boolean > ();
        FieldLabels = new map < String, String > ();
        Set < String > objs = new Set < String > ();
        objs.add(sobjectname);
        for (String fil:Fieldname){
            if(fil.contains('.')){
                objs.add(fil.split('\\.')[0].replace('__r','__c'));
            }
        }
        for(String obj:objs){
            Map < String, Schema.SObjectType > schemaMap = Schema.getGlobalDescribe();
            Schema.SObjectType CustomSchema = schemaMap.get(obj);
            Map < String, Schema.SObjectField > fieldMap = CustomSchema.getDescribe().fields.getMap();
            for (String fil: fieldMap.keySet()) {
                if(obj==sobjectname){
                    fieldLabels.put(fil, fieldMap.get(fil).getDescribe().getLabel());
                    if (DRS.FieldLevelPermissions__c)
                        fieldPerm.put(fil, fieldMap.get(fil).getDescribe().isAccessible());
                    else
                        fieldPerm.put(fil, true);
                }
                else{
                    fieldLabels.put(obj.replace('__c','__r')+'.'+fil, fieldMap.get(fil).getDescribe().getLabel());
                    if (DRS.FieldLevelPermissions__c)
                        fieldPerm.put(obj.replace('__c','__r')+'.'+fil, fieldMap.get(fil).getDescribe().isAccessible());
                    else
                        fieldPerm.put(obj.replace('__c','__r')+'.'+fil, true);
                }
            }
        }
    }
    public void requery() {
        resetErrMsgs();
        sobs = new list < sObject > ();
        sobs = Database.query('select ' + (isLinks ? (LinkFieldID + ',' + LinkFieldName) : '') + (isLinks && isFields ? +',' : '') + (isFields ? Fields : '') + ' from ' + Sobjectname + ' where ' + wher + ' Order By ' + fname + ' ' + ord + ' LIMIT ' + lim + ' OFFSET ' + offs);
    }
    public void Next() {
        offs += lim;
        requery();
    }
    public void Back() {
        if (offs != 0) {
            offs -= lim;
            requery();
        }
    }
    public void setlim() {
        offs = 0;
        requery();
    }
    public void save() {
        resetErrMsgs();
        try {
            System.debug('3' + sobs);
            String sobid;
            if (sobs.size() > 0)
                sobid = sobs[0].id;
            else
                sobid = null;
            if (sobs.size() == 1 && sobid == null)
                insert sobs;
            else
                upsert sobs;
            System.debug('4' + sobs);
            requery();
            System.debug('5' + sobs);
        } catch (DMLException e) {
            System.debug('e' + sobs);
            System.debug(e.getDMLMessage(0));
            errordml = 'Error: ' + e.getDMLMessage(0);
            errMsgs.put('dml', true);
        }
    }
    public void del() {
        resetErrMsgs();
        try {
            for (sobject sob: sobs) {
                String sobid = sob.id;
                if (sobid != null && sobid == lineid) {
                    delete sob;
                    requery();
                }
            }
        } catch (DMLException e) {
            errordml = 'Error: ' + e.getDMLMessage(0);
            errMsgs.put('dml', true);
        }
    }
    public void add() {
        resetErrMsgs();
        String[] connect;
        if (!wher.contains('.'))
            connect = wher.split('=:');
        SObject s = Schema.getGlobalDescribe().get(Sobjectname).newSObject();
        if (!wher.contains('.')) {
            List < Schema.SObjectField > fieldMap = Schema.getGlobalDescribe().get(Sobjectname).getDescribe().fields.getMap().values();
            for (Schema.SObjectField fld: fieldMap)
                if (fld.getDescribe().getName().tolowercase() == connect[0])
                    s.put(fld, Curid);
        }
        System.debug('1' + sobs);
        sobs.add(s);
        System.debug('2' + sobs);
    }
    }
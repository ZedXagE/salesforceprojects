@isTest
public class TestDynamicRelated {
static testMethod void TestDynamic()
	{
		DynamicRelatedSettings__c drs=new DynamicRelatedSettings__c(Name='Opportunity',Object__c='Opportunity',Default_Order__c='DESC',Fields__c='LeadSource',LinkFieldID__c='id',LinkFieldName__c='name',OrderBy__c='name',where__c='Accountid=:curid');
        insert drs;
        Account acc=new account(name='test');
        insert acc;
        Opportunity opp=new opportunity(name='optest',Accountid=acc.id,StageName='Closed Won',CloseDate=System.TODAY());
        insert opp;
        PageReference pgRef = Page.DynamicRelated; //Create Page Reference - 'Appt_New' is the name of Page
        Test.setCurrentPage(pgRef); //Set the page for Test Method
        ApexPages.currentPage().getParameters().put('id', acc.id);//Pass Id to page
        ApexPages.currentPage().getParameters().put('obj', 'Opportunity');//Pass Id to page
        DynamicRelatedController sc = new DynamicRelatedController();
        sc.save();
        sc.add();
        sc.save();
        sc.lineid=opp.id;
        sc.del();
    }
}
/*
 * @Author: ZedXagE - ITmyWay 
 * @Date: 2018-12-23 11:50:43 
 * @Last Modified by: ZedXagE - ITmyWay
 * @Last Modified time: 2018-12-23 18:47:18
 */

public class StageProbabilityCalc {
	public static void calc(List <Opportunity> opps) {
		//init
		Set <String> oppids = new Set <String>();
		Map <String, String> acctypebyopp = new Map <String, String>();
		Map <String, List <Task>> tasksbyopp = new Map <String,  List <Task>>();
		for(Opportunity opp:opps){
			oppids.add(opp.id);
			acctypebyopp.put(opp.id,null);
			tasksbyopp.put(opp.id,new List <Task>());
		}
		for(Opportunity opp:[select id,Account.Type from Opportunity where id in:oppids])
			acctypebyopp.put(opp.id,opp.Account.Type=='Private Clinic'?opp.Account.Type:'Hospital');
		for(Task task:[select WhatId,ActivityDate,Status,Priority,Subject,CreatedDate from Task Where WhatId in:oppids AND (Status='Completed' OR Status='Not Applicable') Order by ActivityDate,CreatedDate,Priority DESC])
			tasksbyopp.get(task.Whatid).add(task);

		//init custom setting+stage
		List <Activities_Stages__c> acstages = [select Account_Type__c,Activity_Name__c,Pre_Request_Stage__c,Directly_Stage__c,Min__c,Max__c,Increase__c,Sleep__c,Cardio_Other__c,Task_Status_API__c,Task_Date_API__c,Task_Comment_API__c from Activities_Stages__c Where (Account_Type__c!=null and Activity_Name__c!=null and Min__c!=null and Max__c!=null and Increase__c!=null and Sleep__c!=null and Cardio_Other__c!=null) Order by Account_Type__c,Task_Comment_API__c,Activity_Name__c];
		Map <String, Map <String, Activities_Stages__c>> stagesmap = new Map <String, Map <String, Activities_Stages__c>>();
		Map <String, Decimal> oppstages = new Map <String, Decimal>();
		for(OpportunityStage stage:[select id,ApiName,DefaultProbability,SortOrder from OpportunityStage where isActive=true order by SortOrder])
			oppstages.put(stage.ApiName,stage.DefaultProbability);
		for(Activities_Stages__c acst:acstages)
			if(stagesmap.containsKey(acst.Account_Type__c))
				stagesmap.get(acst.Account_Type__c).put(acst.Activity_Name__c, acst);
			else
				stagesmap.put(acst.Account_Type__c, new Map <String,Activities_Stages__c>{acst.Activity_Name__c => acst});
		for(Opportunity opp:opps){
			if(stagesmap.containsKey(acctypebyopp.get(opp.id))&&oppstages.get(opp.StageName)!=0&&oppstages.get(opp.StageName)<=90){
				String newStage = 'Prospecting';
				Decimal newProb = opp.Type=='New Business'?5:10;
				for(Task tas:tasksbyopp.get(opp.id)){
					if(tas.Status=='Completed'&&stagesmap.get(acctypebyopp.get(opp.id)).containsKey(tas.Subject)){
						Activities_Stages__c acst = stagesmap.get(acctypebyopp.get(opp.id)).get(tas.Subject);
						//Directly && Pre Request
						if(acst.Directly_Stage__c!=null&&checkPreRequest(tasksbyopp.get(opp.id),acst.Pre_Request_Stage__c)&&oppstages.get(newStage)<oppstages.get(acst.Directly_Stage__c)){
							newStage = acst.Directly_Stage__c;
							newProb = oppstages.get(acst.Directly_Stage__c)>newProb?oppstages.get(acst.Directly_Stage__c):newProb;
						}
						//increase check
						else if(acst.Min__c<=newProb&&newProb<acst.Max__c){
							newProb=newProb+acst.Increase__c<acst.Max__c?newProb+acst.Increase__c:acst.Max__c;
						}
						System.debug(newProb+' '+newStage);
					}
				}
				System.debug('new stage: '+newStage + ', new prob: '+newProb+', old stage: '+opp.StageName+', old prob: '+opp.Probability);
				//update check
				if(oppstages.get(newStage)>oppstages.get(opp.StageName)){
					opp.StageName = newStage;
					opp.Probability = newProb;
					if(newStage=='80-Quotation')	opp.Quotation__c=true;
				}
				else if(newProb>opp.Probability){
					opp.Probability = newProb;
				}
			}
		}
	}
	public static Boolean checkPreRequest(List <Task> tasks, String prereq){
		if(prereq==null)	return true;
		Map <String,Boolean> prereqs = new Map <String,Boolean>();
		for(String s:prereq.split(';'))
			prereqs.put(s,false);
		for(Task tas:tasks)
			if(prereqs.containsKey(tas.Subject))
				prereqs.put(tas.Subject,true);
		integer exst = 0;
		for(String s:prereqs.keySet()){
			System.debug(s+' '+prereqs.get(s));
			if(prereqs.get(s))
				exst++;
		}
		return exst==prereqs.keySet().size();
	}
	public static void StartOppTrigger(Set <String> oppids){
		List <Opportunity> opps = [select id,Cal_Stage__c from Opportunity where id in:oppids];
		for(Opportunity opp:opps)
			if(opp.Cal_Stage__c==true)
				opp.Cal_Stage__c = false;
			else
				opp.Cal_Stage__c = true;
		update opps;
	}
}
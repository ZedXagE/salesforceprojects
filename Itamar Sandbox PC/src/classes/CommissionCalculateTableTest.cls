@isTest(SeeAllData=true)
private class CommissionCalculateTableTest {
  @isTest static void test_method_one() {
    String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
    Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
    User u = new User(
      Alias='standt', Email='standarduser@testorg.com',
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
      LocaleSidKey='en_US', ProfileId=p.Id,
      TimeZoneSidKey='America/Los_Angeles', UserName=uniqueUserName
    );
    insert u;
    insert new Users_For_Commissions__c(
      User__c=('' + u.Id).substring(0, 15),
      Sub_Username__c='user1',
      Password__c='123'
    );
    Test.startTest();
    System.runAs(u) {
      PageReference pageRef;
      pageRef = Page.CommissionCalculateTable;
      pageRef.getParameters().put('year', '2018');
      Test.setCurrentPage(pageRef);
      CommissionCalculateTable ctrl = new CommissionCalculateTable();
    }
    insert new Users_For_Commissions__c(
      User__c=('' + u.Id).substring(0, 15),
      Sub_Username__c='user2',
      Password__c='123'
    );
    System.runAs(u) {
      PageReference pageRef;
      pageRef = Page.CommissionCalculateTable;
      pageRef.getParameters().put('year', '2018');
      Test.setCurrentPage(pageRef);
      CommissionCalculateTable ctrl = new CommissionCalculateTable();
      ctrl.login();
      ctrl.logout();
    }
    Test.stopTest();
  }
}
public class OppPicklistController {
    public Opportunity opp{get;set;}
    public Opportunity clonedopp;
    public Task newTask{get;set;}
    public Date newTaskdat{get;set;}
    public String PreReqFieldJson{get;set;}
    public String fieldNameByActivityJson{get;set;}
    public String othertask{get;set;}
    public String taskstatus{get;set;}
    public List <selectoption> othertasks{get;set;}
    public List <selectoption> taskstatuses{get;set;}
    public String oppid;
    public Map <String, List<String>> fieldsByTitle{get;set;}
    public List <String> fieldsetsname{get;set;}
    public Map <String,String> titles{get;set;}
    public Map <String,Task> exstTasks{get;set;}
    public Map <String,String> dat{get;set;}
    public Map <String,String> comment{get;set;}
    public Map <String,String> fieldLabels{get;set;}
    public Map <String,String> activityNames{get;set;}
    public Map <String,String> fieldNameByActivity{get;set;}
    public Map <String,String> ShowReqField{get;set;}
    public Map <String,String> PreReqField{get;set;}
    //public Map <String,Boolean> PreReqOK{get;set;}
    public OppPicklistController(ApexPages.StandardController controller){
        oppid = ApexPages.currentPage().getParameters().get('id');
        fieldsByTitle = new Map <String, List<String>>();
        fieldsetsname = new List <String>{'Technical_Operational_Eval','Business_Evaluation','X80_Quotation','X90_PO_in_process'};
        titles = new Map <String,String>{'Technical_Operational_Eval'=>'Technical and Operational Eval','Business_Evaluation'=>'Business Evaluation','X80_Quotation'=>'80-Quotation','X90_PO_in_process'=>'90-PO in process'};
        dat = new Map <String,String>();
        comment = new Map <String,String>();
        othertasks = new List <selectoption>();
        taskstatuses = new List <selectoption>();
        othertasks.add(new selectoption('','--Select--'));
        taskstatuses.add(new selectoption('','--None--'));
        taskstatuses.add(new selectoption('Completed','Completed'));
        taskstatuses.add(new selectoption('Not Applicable','Not Applicable'));
        opp = [select id,Market_Segment__c,Account.Type from Opportunity where id=:oppid];
        fieldLabels = new Map <String,String>();
        activityNames = new Map <String,String>();
        fieldNameByActivity = new Map <String,String>();
        ShowReqField = new Map <String,String>();
        PreReqField = new Map <String,String>();
        String segment = Opp.Market_Segment__c!='Core Sleep'?'Cardio_Other__c':'Sleep__c';
        String acctype = opp.Account.Type=='Private Clinic'?opp.Account.Type:'Hospital';
        for(Activities_Stages__c acst:[select Activity_Name__c,Sleep__c,Cardio_Other__c,Pre_Request_Stage__c,Task_Status_API__c,Task_Date_API__c,Task_Comment_API__c from Activities_Stages__c Where (Account_Type__c=:acctype and Activity_Name__c!=null and Min__c!=null and Max__c!=null and Increase__c!=null and Sleep__c!=null and Cardio_Other__c!=null) Order By Activity_Name__c]){
            activityNames.put(acst.Task_Status_API__c,acst.Activity_Name__c);
            fieldNameByActivity.put(acst.Activity_Name__c,acst.Task_Status_API__c);
            if(acst.Task_Status_API__c!=null){
                dat.put(acst.Task_Status_API__c,acst.Task_Date_API__c);
                comment.put(acst.Task_Status_API__c,acst.Task_Comment_API__c);
            }
            else if((String)acst.get(segment)!='NA'){
                othertasks.add(new selectoption(acst.Activity_Name__c,acst.Activity_Name__c));
            }
            ShowReqField.put(acst.Task_Status_API__c,(String)acst.get(segment));
            if(acst.Task_Status_API__c!=null){
                PreReqField.put(acst.Task_Status_API__c,acst.Pre_Request_Stage__c);
            }
        }
        System.debug(dat);
        for(String s:fieldsetsname)
            fieldsByTitle.put(s,new List<String>());
        String fieldsstr='';
        List<String> types = new List<String>{'Opportunity'};
        for(Schema.DescribeSobjectResult dsr : Schema.describeSObjects(types)){
            Map<String, Schema.FieldSet> fieldsets =  dsr.fieldSets.getMap();
            for(String s:fieldsetsname)
                for(Schema.FieldSetMember f : fieldsets.get(s).getFields()){
                    fieldLabels.put(f.getFieldPath(),f.getLabel());
                    fieldsByTitle.get(s).add(f.getFieldPath());
                    fieldsstr+=','+f.getFieldPath()+','+dat.get(f.getFieldPath())+','+comment.get(f.getFieldPath());
                }
        }
        System.debug(fieldsstr);
        string qr = 'select id,Account.Type'+fieldsstr+' from Opportunity where id =:oppid';
        opp = Database.query(qr);
        clonedopp = opp.Clone();
        for(String s:fieldsByTitle.keySet())
            for(String f:fieldsByTitle.get(s))
                if(opp.get(dat.get(f))==null)
                    opp.put(dat.get(f), System.Today());
        initTask();
        getExstTasks();
        PreReqFieldJson = JSON.serialize(PreReqField);
        fieldNameByActivityJson = JSON.serialize(fieldNameByActivity);
    }
    public void initTask(){
        taskstatus = 'Completed';
        othertask = '';
        newTaskdat = System.Today();
        newTask = new Task();
    }
    public void saveNewTask(){
        newTask.ActivityDate = newTaskdat;
        newTask.Whatid = opp.id;
        newTask.Status = taskstatus;
        newTask.Priority = 'Normal';
        newTask.Subject = othertask;
        insert newTask;
        initTask();
    }
    public void getExstTasks(){
        exstTasks = new Map <String,Task>();
        for(Task tas:[select id,Opp_API_Field__c,Subject,Priority,ActivityDate,Status from Task where Whatid=:opp.id and Opp_API_Field__c!=null])
            exstTasks.put(tas.Opp_API_Field__c,tas);
    }
    public void sav(){
        update opp;
        List <Task> tasks4up = new List <Task>();
        for(String s:fieldsByTitle.keySet())
            for(String f:fieldsByTitle.get(s))
                if(opp.get(f)!=null&&(opp.get(f)!=clonedopp.get(f)||opp.get(dat.get(f))!=clonedopp.get(dat.get(f))||opp.get(comment.get(f))!=clonedopp.get(comment.get(f)))){
                    Task tas = new Task();
                    if(exstTasks.containsKey(f))
                        tas = exstTasks.get(f);
                    tas.WhatId = opp.id;
                    tas.ActivityDate = (Date) opp.get(dat.get(f));
                    tas.Description = (String) opp.get(comment.get(f));
                    tas.Priority = 'High';
                    tas.Subject = (String) activityNames.get(f);
                    tas.Status = (String) opp.get(f);
                    tas.Opp_API_Field__c = f;
                    tasks4up.add(tas);
                }
        upsert tasks4up;
    }
}
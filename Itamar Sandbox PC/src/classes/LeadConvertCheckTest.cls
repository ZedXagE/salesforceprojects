@isTest(seealldata=true)
public class LeadConvertCheckTest {
	static TestMethod void tes() {
		Lead ld = new Lead(Lastname='testclass',company='testcomp');
		insert ld;
		ApexPages.StandardController controller = new ApexPages.StandardController(ld);
		ApexPages.currentPage().getParameters().put('id',ld.id);
		LeadConvertCheckController con = new LeadConvertCheckController(controller);
		Database.LeadConvert lc = new Database.LeadConvert();
		lc.setLeadId(ld.id);
		LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
		lc.setConvertedStatus(convertStatus.MasterLabel);
		Database.LeadConvertResult lcr = Database.convertLead(lc);
	}
}
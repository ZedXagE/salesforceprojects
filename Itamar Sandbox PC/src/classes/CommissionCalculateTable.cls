public class CommissionCalculateTable {
  private String userId = UserInfo.getUserId().substring(0, 15);
  public String screen { get; set; }
  public Boolean showLogout { get; set; }
  public Users_For_Commissions__c user { get; set; }
  public String year { get; set; }
  //for dashboard
  public String tab { get; set; }
  public String month4dash { get; set; }
  public String day4dash { get; set; }
  public String cmkname { get; set; }
  public String region { get; set; }
  public List <String> regions { get; set; }

  public class RegionalTable {
    public List<Integer> is { get; set; }
    public Map<Integer, String> month { get; set; }
    public String region { get; set; }
    public Map<Integer, String> cc_id { get; set; }
    public Map<Integer, Decimal> quota_monthly { get; set; }
    public Map<Integer, Decimal> quota_cumulative { get; set; }
    public Map<Integer, Decimal> actuals_total_sales { get; set; }
    public Map<Integer, Decimal> actuals_cardio { get; set; }
    public Map<Integer, Decimal> actuals_va { get; set; }
    public Map<Integer, Decimal> actuals_wp_sales { get; set; }
    public Map<Integer, Decimal> actuals_cumulative { get; set; }
    public Map<Integer, Decimal> commissions_paid { get; set; }
    public Map<Integer, Decimal> commissions_cumulative { get; set; }
    public Decimal current_total_sales { get; set; }
    public Decimal current_cardio { get; set; }
    public Decimal current_va { get; set; }
    public Decimal current_wp_sales { get; set; }
    public Decimal current_cumulative { get; set; }
    public Decimal current_cumulative_vs_quota { get; set; }
    public Decimal current_total_cumulative_comission { get; set; }
    public Decimal current_we_pay_90 { get; set; }
    public Decimal current_paid_so_far { get; set; }
    public Decimal current_estimate_for_wp { get; set; }
    public Decimal current_mytap { get; set; }
    public Decimal current_mytap_5p { get; set; }
    public Decimal current_endo { get; set; }
    public Decimal current_endo_5p { get; set; }
    public Decimal current_cpap { get; set; }
    public Decimal current_cpap_5p { get; set; }
    public Decimal current_estimate_to_be_paid { get; set; }
  }

  public List<RegionalTable> rts { get; set; }
  public void main(){tab='1';}
  public void dash(){
    tab='2';
    cmkname = user.Sub_Username__c+' ('+region+')';
  }
  public CommissionCalculateTable() {
    tab = ApexPages.currentPage().getParameters().get('tab');
    if(tab==null||tab=='') tab='1';
    List<Users_For_Commissions__c> users = new List<Users_For_Commissions__c>();
    for (Users_For_Commissions__c user : [SELECT Sub_Username__c, User__c FROM Users_For_Commissions__c])
      if (user.User__c != null && userId == ('' + user.User__c).substring(0, 15))
        users.add(user);
    if (users.isEmpty()) {
      screen = 'NO_USER';
      return;
    }
    if (users.size() > 1) {
      try {
        Cookie loginId = ApexPages.currentPage().getCookies().get('loginId');
        user = [SELECT Sub_Username__c FROM Users_For_Commissions__c WHERE Id=:loginId.getValue() LIMIT 1];
        showLogout = true;
      } catch (Exception e) {
        screen = 'LOGIN_SCREEN';
        user = new Users_For_Commissions__c();
        return;
      }
    }
    if (user == null) {
      user = users[0];
      showLogout = false;
    }

    screen = 'TABLE';
    year = ApexPages.currentPage().getParameters().get('year');
    year = year == null ? '' + Date.today().year() : year;

    List<Commission_Calculate__c> ccs;
    if (!Test.isRunningTest())
      ccs = Database.query('SELECT Id, Name, Quota__c, YTD_Quota__c, Total_Sales_Revenue__c, Cardio_Sales_Revenue__c, VA_Revenue__c, WP_Revenue__c, YTD_Total_WP_Sales_Multiplied_Revenue__c, YTD_Total_Sales_Revenue_100__c, Total_Commissions__c, Total_Commissions_Cumulative__c,  myTAP_Commissions_to_Pay__c, Endo_Commissions_to_Pay__c, CPAP_Commissions_to_Pay__c FROM Commission_Calculate__c WHERE Name LIKE \'' + user.Sub_Username__c + ' (%) : ' + year + '-%\' ORDER BY Name');
    else
      ccs = new List<Commission_Calculate__c>{ new Commission_Calculate__c(Name='user (region) : 2018-01') };


    rts = new List<RegionalTable>();
    List <Commission_Calculate__c> regionCCs = new List<Commission_Calculate__c>();
    for (Commission_Calculate__c cc : ccs) {
      if (regionCCs.isEmpty()) regionCCs.add(cc);
      else if (cc.Name.split(' : ')[0] == regionCCs[regionCCs.size() - 1].Name.split(' : ')[0]) regionCCs.add(cc);
      else {
        rts.add(generateRegionalTable(regionCCs));
        regionCCs = new List<Commission_Calculate__c>{ cc };
      }
    }
    rts.add(generateRegionalTable(regionCCs));

    //added for dash
    month4dash = ApexPages.currentPage().getParameters().get('month');
    month4dash = month4dash == null ? '' + Date.today().Month() : month4dash;
    day4dash = ApexPages.currentPage().getParameters().get('day');
    day4dash = day4dash == null ? '' + Date.today().day() : day4dash;
    regions = new List <String>();
    for(RegionalTable rt:rts)
      regions.add(rt.region);
    //end added

  }

  private static RegionalTable generateRegionalTable(List<Commission_Calculate__c> ccs) {
    RegionalTable rt = new RegionalTable();
    rt.is = new List<Integer>{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
    rt.month = new Map<Integer, String>{
      1 => 'January',
      2 => 'February',
      3 => 'March',
      4 => 'April',
      5 => 'May',
      6 => 'June',
      7 => 'July',
      8 => 'August',
      9 => 'September',
      10 => 'October',
      11 => 'November',
      12 => 'December'
    };
    rt.region = '';
    rt.cc_id = new Map<Integer, String>();
    rt.quota_monthly = new Map<Integer, Decimal>();
    rt.quota_cumulative = new Map<Integer, Decimal>();
    rt.actuals_total_sales = new Map<Integer, Decimal>();
    rt.actuals_cardio = new Map<Integer, Decimal>();
    rt.actuals_va = new Map<Integer, Decimal>();
    rt.actuals_wp_sales = new Map<Integer, Decimal>();
    rt.actuals_cumulative = new Map<Integer, Decimal>();
    rt.commissions_paid = new Map<Integer, Decimal>();
    rt.commissions_cumulative = new Map<Integer, Decimal>();
    for (Integer i : rt.is) {
      rt.cc_id.put(i, 'cc_id');
      rt.quota_monthly.put(i, 0);
      rt.quota_cumulative.put(i, 0);
      rt.actuals_total_sales.put(i, 0);
      rt.actuals_cardio.put(i, 0);
      rt.actuals_va.put(i, 0);
      rt.actuals_wp_sales.put(i, 0);
      rt.actuals_cumulative.put(i, 0);
      rt.commissions_paid.put(i, 0);
      rt.commissions_cumulative.put(i, 0);
    }

    Commission_Calculate__c lastCC;
    for (Commission_Calculate__c cc : ccs) {
      if (cc.Quota__c == null) cc.Quota__c = 0;
      if (cc.YTD_Quota__c == null) cc.YTD_Quota__c = 0;
      Decimal ccTotal_Sales_Revenue = cc.Total_Sales_Revenue__c == null ? 0 : cc.Total_Sales_Revenue__c;
      Decimal ccCardio_Sales_Revenue = cc.Cardio_Sales_Revenue__c == null ? 0 : cc.Cardio_Sales_Revenue__c;
      Decimal ccVA_Revenue = cc.VA_Revenue__c == null ? 0 : cc.VA_Revenue__c;
      Decimal ccWP_Revenue = cc.WP_Revenue__c == null ? 0 : cc.WP_Revenue__c;
      if (cc.YTD_Total_WP_Sales_Multiplied_Revenue__c == null) cc.YTD_Total_WP_Sales_Multiplied_Revenue__c = 0;
      if (cc.YTD_Total_Sales_Revenue_100__c == null) cc.YTD_Total_Sales_Revenue_100__c = 0;
      if (cc.Total_Commissions__c == null) cc.Total_Commissions__c = 0;
      if (cc.Total_Commissions_Cumulative__c == null) cc.Total_Commissions_Cumulative__c = 0;
      if (cc.myTAP_Commissions_to_Pay__c == null) cc.myTAP_Commissions_to_Pay__c = 0;
      if (cc.Endo_Commissions_to_Pay__c == null) cc.Endo_Commissions_to_Pay__c = 0;
      if (cc.CPAP_Commissions_to_Pay__c == null) cc.CPAP_Commissions_to_Pay__c = 0;

      if (lastCC == null) lastCC = new Commission_Calculate__c(
        YTD_Total_WP_Sales_Multiplied_Revenue__c =0,
        Total_Commissions_Cumulative__c=0
      );

      rt.region = cc.Name.split(' : ')[0].split('\\(')[1].split('\\)')[0];
      Integer i = Integer.valueOf(cc.Name.split(' : ')[1].split('-')[1]);
      if (!Test.isRunningTest()) rt.cc_id.put(i, ('' + cc.Id).substring(0, 15));
      rt.quota_monthly.put(i, cc.Quota__c);
      rt.quota_cumulative.put(i, cc.YTD_Quota__c);
      rt.actuals_total_sales.put(i, ccTotal_Sales_Revenue);
      rt.actuals_cardio.put(i, ccCardio_Sales_Revenue);
      rt.actuals_va.put(i, ccVA_Revenue);
      rt.actuals_wp_sales.put(i, ccWP_Revenue);


      rt.actuals_cumulative.put(i, cc.YTD_Total_Sales_Revenue_100__c);
      rt.commissions_paid.put(i, cc.Total_Commissions__c);
      rt.commissions_cumulative.put(i, cc.Total_Commissions_Cumulative__c);


      rt.current_total_sales = ccTotal_Sales_Revenue;
      rt.current_cardio = ccCardio_Sales_Revenue;
      rt.current_va = ccVA_Revenue;
      rt.current_wp_sales = cc.YTD_Total_WP_Sales_Multiplied_Revenue__c - lastCC.YTD_Total_WP_Sales_Multiplied_Revenue__c;
      rt.current_cumulative = cc.YTD_Total_Sales_Revenue_100__c;
      rt.current_cumulative_vs_quota = cc.YTD_Quota__c;
      rt.current_total_cumulative_comission = cc.Total_Commissions_Cumulative__c  / 0.9;
      rt.current_we_pay_90 = cc.Total_Commissions_Cumulative__c;
      rt.current_paid_so_far = lastCC.Total_Commissions_Cumulative__c;
      rt.current_estimate_for_wp = cc.Total_Commissions_Cumulative__c -  lastCC.Total_Commissions_Cumulative__c;
      rt.current_mytap = cc.myTAP_Commissions_to_Pay__c;
      rt.current_mytap_5p = rt.current_mytap * 0.05;
      rt.current_endo = cc.Endo_Commissions_to_Pay__c;
      rt.current_endo_5p = rt.current_endo * 0.05;
      rt.current_cpap = cc.CPAP_Commissions_to_Pay__c;
      rt.current_cpap_5p = rt.current_cpap * 0.05;
      rt.current_estimate_to_be_paid = cc.Total_Commissions_Cumulative__c - lastCC.Total_Commissions_Cumulative__c + rt.current_mytap_5p + rt.current_endo_5p + rt.current_cpap_5p;

      lastCC = cc;
    }
    return rt;
  }

  private PageReference reload() {
    PageReference ref = Page.CommissionCalculateTable;
    ref.setRedirect(true);
    return ref;
  }

  public PageReference login() {
    try {
      String loginId = [SELECT Id FROM Users_For_Commissions__c WHERE User__c=:userId AND Sub_Username__c=:user.Sub_Username__c AND Password__c=:user.Password__c LIMIT 1].Id;
      ApexPages.currentPage().setCookies(new List<Cookie>{ new Cookie('loginId', loginId, null, -1, false) });
      return reload();
    } catch (Exception e) {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'invalid username/password'));
      return null;
    }
  }

  public PageReference logout() {
    ApexPages.currentPage().setCookies(new List<Cookie>{ new Cookie('loginId', null, null, 0, false) });
    return reload();
  }
}
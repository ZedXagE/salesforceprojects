public class AccountTasksController {
    public Task newTask{get;set;}
	private account acc;
    public Date newTaskdat{get;set;}
    public String othertask{get;set;}
    public String taskstatus{get;set;}
    public List <selectoption> othertasks{get;set;}
    public List <selectoption> taskstatuses{get;set;}
    public String accid;
    public AccountTasksController(ApexPages.StandardController controller){
        accid = ApexPages.currentPage().getParameters().get('id');
		acc = [select id,Type from Account where id=:accid];
        othertasks = new List <selectoption>();
        taskstatuses = new List <selectoption>();
        othertasks.add(new selectoption('','--Select--'));
        taskstatuses.add(new selectoption('','--None--'));
        taskstatuses.add(new selectoption('Completed','Completed'));
        taskstatuses.add(new selectoption('Not Applicable','Not Applicable'));
        String acctype = acc.Type=='Private Clinic'?acc.Type:'Hospital';
        for(Activities_Stages__c acst:[select Activity_Name__c from Activities_Stages__c Where (Account_Type__c=:acctype and Activity_Name__c!=null and Task_Status_API__c=null) Order By Activity_Name__c])
			othertasks.add(new selectoption(acst.Activity_Name__c,acst.Activity_Name__c));
        initTask();
    }
    public void initTask(){
        taskstatus = 'Completed';
        othertask = '';
        newTaskdat = System.Today();
        newTask = new Task();
    }
    public void saveNewTask(){
        newTask.ActivityDate = newTaskdat;
        newTask.Whatid = acc.id;
        newTask.Status = taskstatus;
        newTask.Priority = 'Normal';
        newTask.Subject = othertask;
        insert newTask;
        initTask();
    }
}
public class DashboardController {
    public String cdat{get;set;}
    public String cmk{get;
        set{
            if(cmk!=value&&value!=null){
                if(value.contains(';')){
                    cmk = value.tolowercase().split(';')[0];
                    cdat = value.split(';')[1];
                    title = value.split(';')[0].replaceAll('\\(','').replaceAll('\\)','');
                    init(value.tolowercase().split(';')[0],value.split(';')[1]);
                }
                else{
                    cmk = value.tolowercase();
                    title = value.replaceAll('\\(','').replaceAll('\\)','');
                    init(value.tolowercase(),null);
                }
            }
        }
    }
    public final integer yearback = 3; 
    public Map <integer, String> quar = new Map <integer, String> {
        1=>'Q1',
        2=>'Q1',
        3=>'Q1',
        4=>'Q2',
        5=>'Q2',
        6=>'Q2',
        7=>'Q3',
        8=>'Q3',
        9=>'Q3',
        10=>'Q4',
        11=>'Q4',
        12=>'Q4'
    };
    public Map <integer, String> months = new Map <integer, String> {
        1=>'Jan',
        2=>'Feb',
        3=>'Mar',
        4=>'Apr',
        5=>'May',
        6=>'Jun',
        7=>'Jul',
        8=>'Aug',
        9=>'Sep',
        10=>'Oct',
        11=>'Nov',
        12=>'Dec'
    };
    public Map <integer, List<integer>> quarMonths = new Map <integer, List<integer>> {
        1=>new List <integer>{1,2,3},
        2=>new List <integer>{1,2,3},
        3=>new List <integer>{1,2,3},
        4=>new List <integer>{4,5,6},
        5=>new List <integer>{4,5,6},
        6=>new List <integer>{4,5,6},
        7=>new List <integer>{7,8,9},
        8=>new List <integer>{7,8,9},
        9=>new List <integer>{7,8,9},
        10=>new List <integer>{10,11,12},
        11=>new List <integer>{10,11,12},
        12=>new List <integer>{10,11,12}
    };
    public Decimal currentWeek{get;set;}
    public Decimal SumOppEST{get;set;}
    public String startYear{get;set;}
    public String thisYear{get;set;}
    public Integer thisMonthInt{get;set;}
    public String thisMonthStr{get;set;}
    public Decimal thisMonthFraction{get;set;}
    public String endYear{get;set;}
    public String endmonth{get;set;}
    public List <Opportunity> opps{get;set;}
    public List <Account> accs{get;set;}
    public String title{get;set;}
    public Date Dtoday{get;set;}
    public Map <String, Map <String, Decimal>> gauges{get;set;}
    public Map <String, Decimal> pieType{get;set;}
    public Map <String, Decimal> pieSource{get;set;}
    public String JSONgauges{get;set;}
    public String JSONmonthNames{get;set;}
    public String JSONquarMonths{get;set;}
    public String JSONquarstr{get;set;}
    public String JSONpieType{get;set;}
    public String JSONpieSource{get;set;}
    public Map <String, Commissions_Master_Key__c> cmks{get;set;}
    public Map <String, Commission_Calculate__c> cmcs{get;set;}
    public Map <String, String> cmkpermonth{get;set;}
    public Map <String, String> cmcpermonth{get;set;}
    public void init(){ init(cmk,cdat); }
    public void init(String cmkname,String dat){
        System.debug('cmkname: '+cmkname);
        JSONmonthNames = JSON.serialize(months);
        JSONquarMonths = JSON.serialize(quarMonths);
        JSONquarstr = JSON.serialize(quar);
        if(dat!=null&&dat!='')
            Dtoday = Date.newInstance(Integer.valueof(dat.split('-')[0]),Integer.valueof(dat.split('-')[1]),Integer.valueof(dat.split('-')[2]));
        else
            Dtoday = System.Today();
        thisyear = String.valueof(Dtoday.year());
        thisMonthInt = Dtoday.month();
        thismonthStr = getMonthSTR(String.valueof(Dtoday.month()));
        startYear = thisyear+'-01';
        endYear = thisyear+'-12';
        endmonth = thisyear+'-'+getMonthSTR(String.valueof(Dtoday.month()-1));
        List <Tss_Date__c> tssdats = [select Week__c,From__c,To__c from Tss_Date__c where Month__c like: '%'+months.get(thisMonthInt)+'%' Order By Week__c];
        Integer fromtsd,totsd,fromtsm,totsm;
        integer thisDay = Dtoday.day();
        Decimal cntr = 0;
        for(Tss_Date__c ts:tssdats){
            cntr++;
            fromtsd = Integer.valueof(ts.From__c.split('/')[0]);
            fromtsm = Integer.valueof(ts.From__c.split('/')[1]);
            totsd = Integer.valueof(ts.To__c.split('/')[0]);
            totsm = Integer.valueof(ts.To__c.split('/')[1]);
            if((thisDay<=fromtsd&&thisMonthInt==fromtsm)||(fromtsd<=thisDay&&totsd>=thisDay))
                break;
        }
        thisMonthFraction = cntr/tssdats.size();
        cmks = new Map <String, Commissions_Master_Key__c>();
        cmkpermonth = new Map <String, String>();
        cmcs = new Map <String, Commission_Calculate__c>();
        cmcpermonth = new Map <String, String>();
        for(Commissions_Master_Key__c cm:[select id,Region__c,US_Region__c,Market_Segments__c,Accts_Oppos__c,WP_VA_CC__c,Cardio__c,Start__c,Start_Year__c,Start_Month__c,End__c,End_Month__c,End_Year__c,Quota_Month_1__c,Quota_Month_2__c,Quota_Month_3__c,Quota_Month_4__c,Quota_Month_5__c,Quota_Month_6__c,Quota_Month_7__c,Quota_Month_8__c,Quota_Month_9__c,Quota_Month_10__c,Quota_Month_11__c,Quota_Month_12__c,TSS_Month_1__c,TSS_Month_2__c,TSS_Month_3__c,TSS_Month_4__c,TSS_Month_5__c,TSS_Month_6__c,TSS_Month_7__c,TSS_Month_8__c,TSS_Month_9__c,TSS_Month_10__c,TSS_Month_11__c,TSS_Month_12__c from Commissions_Master_Key__c where CMK__c=:cmkname and Start__c!=null]){
            cmks.put(cm.id,cm);
            String tempstart =cm.Start__c;
            String tempend = cm.End__c!='9999-99'?cm.End__c:thisyear+'-12';
            cmkpermonth.put(tempstart,cm.id);
            while(tempstart!=tempend) {
                tempstart = nextMon(tempstart, 1);
                cmkpermonth.put(tempstart,cm.id);
            }   
        }
        //for(Commission_Calculate__c cc:[select id,Name,Total_Sales_Revenue__c,VA_Revenue__c,Cardio_Sales_Revenue__c,Cardio_Tests_Actual_Tests__c,Endo_Revenue__c,CPAP_Revenue__c,Cardio_Sales_Multiplied_Revenue__c,VA_Multiplied_Revenue__c,YTD_WP_Revenue__c,myTAP_Revenue__c,(select id,Account_Segment_Group_F__c,VA__c,Cardio__c,Amount__c from Commissions_Line_Items__r) from Commission_Calculate__c where Name like: '%'+cmkname+'%']){
        for(Commission_Calculate__c cc:[select id,Name,Total_Sales_Revenue__c,VA_Revenue__c,Cardio_Sales_Revenue__c,Cardio_Tests_Actual_Tests__c,Endo_Revenue__c,CPAP_Revenue__c,Cardio_Sales_Multiplied_Revenue__c,VA_Multiplied_Revenue__c,WP_Revenue__c,myTAP_Revenue__c,(select id,Account_Segment_Group_F__c,VA__c,Cardio__c,Amount__c from Commissions_Line_Items__r) from Commission_Calculate__c where Name like: '%'+cmkname+'%']){
            if(cc.Name.contains(' : ')){
                cmcs.put(cc.id,cc);
                cmcpermonth.put(cc.Name.split(' : ')[1],cc.id);
            }
        }
        gauges = new Map <String, Map <String, Decimal>>();
        for(integer i=0;i<yearback;i++)
            gauges.put(String.valueof(integer.valueof(thisyear)-i),new Map <String,Decimal>{'sales-A01'=>0,'sales-A02'=>0,'sales-A03'=>0,'sales-A04'=>0,'sales-A05'=>0,'sales-A06'=>0,'sales-A07'=>0,'sales-A08'=>0,'sales-A09'=>0,'sales-A10'=>0,'sales-A11'=>0,'sales-A12'=>0,'cardio-A01'=>0,'cardio-A02'=>0,'cardio-A03'=>0,'cardio-A04'=>0,'cardio-A05'=>0,'cardio-A06'=>0,'cardio-A07'=>0,'cardio-A08'=>0,'cardio-A09'=>0,'cardio-A10'=>0,'cardio-A11'=>0,'cardio-A12'=>0,'sales-B01'=>0,'sales-B02'=>0,'sales-B03'=>0,'sales-B04'=>0,'sales-B05'=>0,'sales-B06'=>0,'sales-B07'=>0,'sales-B08'=>0,'sales-B09'=>0,'sales-B10'=>0,'sales-B11'=>0,'sales-B12'=>0,'cardio-B01'=>0,'cardio-B02'=>0,'cardio-B03'=>0,'cardio-B04'=>0,'cardio-B05'=>0,'cardio-B06'=>0,'cardio-B07'=>0,'cardio-B08'=>0,'cardio-B09'=>0,'cardio-B10'=>0,'cardio-B11'=>0,'cardio-B12'=>0});
        pieType = new Map <String, Decimal>{'Cardio'=>0,'CPAP'=>0,'Endo'=>0,'VA'=>0,'Other'=>0,'myTAP'=>0};
        pieSource = new Map <String, Decimal>{'VA'=>0,'Kaiser'=>0,'Other'=>0};
        System.debug(cmkpermonth);
        System.debug(cmks);
        calcSales();
        calcPies();
        getTop10AndWarnings();
        System.debug(JSONgauges);
    }
    public void calcPies(){
        String startD;
        //Map <String,String> typeflds = new Map <String,String>{'Endo_Revenue__c'=>'Endo','CPAP_Revenue__c'=>'CPAP','Cardio_Sales_Multiplied_Revenue__c'=>'Cardio','VA_Multiplied_Revenue__c'=>'VA','YTD_WP_Revenue__c'=>'Other','myTAP_Revenue__c'=>'myTAP'};
        Map <String,String> typeflds = new Map <String,String>{'Endo_Revenue__c'=>'Endo','CPAP_Revenue__c'=>'CPAP','Cardio_Sales_Multiplied_Revenue__c'=>'Cardio','VA_Multiplied_Revenue__c'=>'VA','WP_Revenue__c'=>'Other','myTAP_Revenue__c'=>'myTAP'};
        for(Integer i:quarMonths.get(thisMonthInt)){
            startD = thisYear+'-'+getMonthStr(String.valueof(i));
            if(cmcpermonth.containsKey(startD)){
                //types
                for(String k:typeflds.keySet())
                    if(cmcs.get(cmcpermonth.get(startD)).get(k)!=null)
                        pieType.put(typeflds.get(k),pieType.get(typeflds.get(k)) + (Decimal)cmcs.get(cmcpermonth.get(startD)).get(k));
                //source
                for(Commission_Line_Item__c cli:cmcs.get(cmcpermonth.get(startD)).Commissions_Line_Items__r){
                    String loc = pieSource.containsKey(cli.Account_Segment_Group_F__c)?cli.Account_Segment_Group_F__c:'Other';
                    if(cmkpermonth.containsKey(startD))
                        pieSource.put(loc, pieSource.get(loc) + (cli.VA__c*cmks.get(cmkpermonth.get(startD)).WP_VA_CC__c/100 + cli.Cardio__c*cmks.get(cmkpermonth.get(startD)).Cardio__c/100 + cli.Amount__c - cli.VA__c - cli.Cardio__c ) );
                }
            }
        }
        JSONpieType = JSON.serialize(pieType);
        JSONpieSource = JSON.serialize(pieSource);
    }
    public void calcSales(){
        for(integer i=0;i<yearback;i++){
            String tempyear = String.valueof(integer.valueof(thisyear)-i);
            String tempstart = '01';
            for(integer j=0;j<12;j++){
                //A
                gauges.get(tempyear).put('sales-A'+tempstart,Asum('Quota_Month_',tempyear+'-'+tempstart,tempyear+'-'+tempstart));
                gauges.get(tempyear).put('cardio-A'+tempstart,Asum('TSS_Month_',tempyear+'-'+tempstart,tempyear+'-'+tempstart));
                //B
                gauges.get(tempyear).put('sales-B'+tempstart,BsumSales(tempyear+'-'+tempstart,tempyear+'-'+tempstart));
                gauges.get(tempyear).put('cardio-B'+tempstart,BsumCardio(tempyear+'-'+tempstart,tempyear+'-'+tempstart));

                tempstart = getMonthStr(String.valueof(integer.valueof(tempstart)+1));
            }
        }
        JSONgauges = JSON.serialize(gauges);
    }
    public void getTop10AndWarnings(){
        Set <String> rulesopts = new Set <String>();
        Set <String> accSecialty = new Set <String>();
        Set <String> accsfids = new Set <String>();
        Set <String> oppsfids = new Set <String>();
        String accregion = cmks.get(cmkpermonth.get(thisYear+'-'+thismonthStr)).Region__c;
        String accusregion = cmks.get(cmkpermonth.get(thisYear+'-'+thismonthStr)).US_Region__c;
        if (!String.isBlank(cmks.get(cmkpermonth.get(thisYear+'-'+thismonthStr)).Accts_Oppos__c))
            for(String s:cmks.get(cmkpermonth.get(thisYear+'-'+thismonthStr)).Accts_Oppos__c.split(','))
                rulesopts.add(s);
        if (!String.isBlank(cmks.get(cmkpermonth.get(thisYear+'-'+thismonthStr)).Market_Segments__c))
            for(String s:cmks.get(cmkpermonth.get(thisYear+'-'+thismonthStr)).Market_Segments__c.split(';'))
                accSecialty.add(s);
        List <Commissions_Rules__c> rules = [select Type__c, SFId__c, Inclusive__c FROM Commissions_Rules__c WHERE Rule__c in: rulesopts and Effective_Date__c<=:Dtoday];
        for(Commissions_Rules__c rule:rules)
            if(rule.Type__c=='Opportunity')
                oppsfids.add(rule.SFId__c);
            else if(rule.Type__c=='Account')
                accsfids.add(rule.SFId__c);
        String qr = 'select id,Name,Accountid,Account.Name,ExpectedRevenue,Probability from Opportunity where ';
        //case rules:
        qr+='((id in: oppsfids OR Accountid in: accsfids) OR ';
        //case region
        qr+='((Account.Region__c=:accregion ';
        if(accusregion!='*')
            qr+='AND Account.US_Region__c=:accusregion) ';
        else
            qr+=') ';
        if(accSecialty.size()>0)
            qr+=' AND Account.Specialty__c in: accSecialty)) ';
        else
            qr+=')) ';
        qr+=' AND (CloseDate>=:Dtoday AND Probability!=0 AND Probability!=100 AND Parent_Opportunity__c=null and ExpectedRevenue!=null) Order by ExpectedRevenue DESC limit 1000';
        System.debug(qr);
        opps = Database.query(qr);
        for(Commissions_Rules__c rule:rules)
            if(rule.Inclusive__c=='No' || rule.Inclusive__c=='Exclude')
                for(integer i=0;i<opps.size();i++)
                    if(opps[i].id==rule.SFId__c || opps[i].Accountid==rule.SFId__c){
                        opps.remove(i);
                        break;
                    }
        SumOppEST = 0;
        for(Opportunity opp:opps)
            if(opp.ExpectedRevenue!=null)   SumOppEST += opp.ExpectedRevenue.setScale(2);
        System.debug('Total: '+SumOppEST+' '+opps);
        String acqr = 'select id,Name,TSS_Comment__c,Manual_Comment__c from Account where ';
        acqr+='(id in: accsfids ';
        //case region
        acqr+='OR (Region__c=:accregion ';
        if(accusregion!='*')
            acqr+='AND US_Region__c=:accusregion ';
        if(accSecialty.size()>0)
            acqr+='AND Specialty__c in: accSecialty)) ';
        else
            acqr+=')) ';
        acqr+='AND (TSS_Comment__c!=null OR Manual_Comment__c!=null)';
        System.debug(acqr);
        accs = Database.query(acqr);
        for(Commissions_Rules__c rule:rules)
            if(rule.Inclusive__c=='No' || rule.Inclusive__c=='Exclude')
                for(integer i=0;i<accs.size();i++)
                    if(accs[i].id==rule.SFId__c){
                        accs.remove(i);
                        break;
                    }
        System.debug(accs);
    }
    private Decimal Asum(String fld,String startD,String endD){
        Decimal total = 0;
        if(cmkpermonth.containsKey(startD)&&cmks.get(cmkpermonth.get(startD)).get(fld+getMonth(startD)+'__c')!=null)
            total += (Decimal) cmks.get(cmkpermonth.get(startD)).get(fld+getMonth(startD)+'__c');
        while(startD!=endD) {
            startD = nextMon(startD, 1);
            if(cmkpermonth.containsKey(startD)&&cmks.get(cmkpermonth.get(startD)).get(fld+getMonth(startD)+'__c')!=null)
                total += (Decimal) cmks.get(cmkpermonth.get(startD)).get(fld+getMonth(startD)+'__c');
        }
        return total.setScale(0);
    }
    private Decimal BsumSales(String startD,String endD){
        Decimal total = totalSales(startD);
        while(startD!=endD) {
            startD = nextMon(startD, 1);
            total += totalSales(startD);
        }
        return total.setScale(0);
    }
    private Decimal BsumCardio(String startD,String endD){
        Decimal total = 0;
        if(cmcpermonth.containsKey(startD)&&cmcs.containsKey(cmcpermonth.get(startD)))
            total += cmcs.get(cmcpermonth.get(startD)).Cardio_Tests_Actual_Tests__c;
        while(startD!=endD) {
            startD = nextMon(startD, 1);
            if(cmcpermonth.containsKey(startD)&&cmcs.containsKey(cmcpermonth.get(startD)))
                total += cmcs.get(cmcpermonth.get(startD)).Cardio_Tests_Actual_Tests__c;
        }
        return total.setScale(0);
    }
    private String nextMon(String mon, Integer monts){
        for(integer i=0;i<monts;i++){
            String [] spl = mon.split('-');
            if(spl[1]=='12')
                mon = String.valueof(integer.valueof(spl[0])+1)+'-01';
            else 
                mon = spl[0]+'-'+getMonthStr(String.valueof(integer.valueof(spl[1])+1));
        }
        return mon;
    }
    private String getMonth(String mon){
        return String.valueof(integer.valueof(mon.split('-')[1]));
    }
    private Decimal totalSales(String mont){
        if(cmcpermonth.containsKey(mont)&&cmcs.containsKey(cmcpermonth.get(mont))&&cmkpermonth.containsKey(mont)&&cmks.containsKey(cmkpermonth.get(mont))){
            Commission_Calculate__c cc = cmcs.get(cmcpermonth.get(mont));
            Commissions_Master_Key__c cmk = cmks.get(cmkpermonth.get(mont));
            return (cc.Total_Sales_Revenue__c - cc.VA_Revenue__c - cc.Cardio_Sales_Revenue__c + cc.VA_Revenue__c*(cmk.WP_VA_CC__c/100) + cc.Cardio_Sales_Revenue__c*(cmk.Cardio__c/100)).setScale(0);
        }
        return 0;
    }
    private String getMonthStr(String mon){
        if(mon.length()==1) mon = '0'+mon;
        return mon;
    }
}
public class LeadConvertCheckController {
	public List <String> fields{get;set;}
	public String missingfield{get;set;}
	public Map <String,String> fieldLabels{get;set;}
	public String leadid;
	public LeadConvertCheckController(ApexPages.StandardController controller) {
		leadid = ApexPages.currentPage().getParameters().get('id');
		missingfield = getMissing(leadid);
	}
	@AuraEnabled
	public static String getMissing(String leadid) {
		String missingfield = '';
		String fieldsstr='';
		Map <String,String> fieldLabels = new Map <String,String>();
		List <String> fields = new List <String>();
		String fieldsetname = 'lead_Conversion';
        List<String> types = new List<String>{'Lead'};
        for(Schema.DescribeSobjectResult dsr : Schema.describeSObjects(types)){
            Map<String, Schema.FieldSet> fieldsets =  dsr.fieldSets.getMap();
			for(Schema.FieldSetMember f : fieldsets.get(fieldsetname).getFields()){
				fieldLabels.put(f.getFieldPath(),f.getLabel());
				fields.add(f.getFieldPath());
				fieldsstr+=','+f.getFieldPath();
			}
        }
		string qr = 'select id'+fieldsstr+',(select id from Tasks) from Lead where id =:leadid';
		Lead ld = Database.query(qr);
		for(String s:fields)
			if(ld.get(s)==null){
				missingfield+=fieldLabels.get(s)+', ';
			}
		if(ld.Tasks.size()==0)
			missingfield+='At Least One Task, ';
		if(missingfield!='')
			missingfield = missingfield.left(missingfield.length()-2);
		return missingfield;
	}
}
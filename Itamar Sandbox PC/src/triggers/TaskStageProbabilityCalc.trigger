trigger TaskStageProbabilityCalc on Task (after insert,after update) {
    Set <String> oppids = new Set <String>();
    Set <String> stages = new Set <String>();
    for(Activities_Stages__c ac:[select Activity_Name__c from Activities_Stages__c Where (Activity_Name__c!=null)])
        stages.add(ac.Activity_Name__c);
    for(Task tas:Trigger.new)
        if(Trigger.isInsert){
            if(tas.WhatId!=null&&String.valueof(tas.WhatId).left(3)=='006'&&tas.Subject!=null&&stages.contains(tas.Subject))
                oppids.add(tas.WhatId);
        }
        else if(Trigger.isUpdate){
            Task oldtas = Trigger.OldMap.get(tas.id);
            if(tas.WhatId!=null&&String.valueof(tas.WhatId).left(3)=='006'&&(stages.contains(tas.Subject)||stages.contains(oldtas.Subject))&&(tas.WhatId!=oldtas.WhatId||tas.Subject!=oldtas.Subject||tas.ActivityDate!=oldtas.ActivityDate||tas.Status!=oldtas.Status||tas.Priority!=oldtas.Priority))
                oppids.add(tas.WhatId);
            if(tas.Whatid!=oldtas.WhatId&&oldtas.WhatId!=null&&String.valueof(oldtas.WhatId).left(3)=='006'&&oldtas.Subject!=null&&stages.contains(oldtas.Subject))
                oppids.add(oldtas.WhatId);
        }
    StageProbabilityCalc.StartOppTrigger(oppids);
}
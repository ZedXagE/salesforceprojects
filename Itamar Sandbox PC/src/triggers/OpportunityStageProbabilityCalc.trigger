trigger OpportunityStageProbabilityCalc on Opportunity (before update) {
    List <Opportunity> opps = new List <Opportunity>();
    for(Opportunity opp:Trigger.new)
        if(opp.Cal_Stage__c!=Trigger.Oldmap.get(opp.id).Cal_Stage__c)
            opps.add(opp);
    StageProbabilityCalc.calc(opps);
}
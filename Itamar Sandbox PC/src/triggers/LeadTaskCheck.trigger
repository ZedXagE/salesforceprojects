trigger LeadTaskCheck on Lead (before update) {
    Map <String,Boolean> leadtasks = new Map <String,Boolean>();
    Map <String,Integer> leadlocations = new Map <String,Integer>();
    for(Integer i=0;i<Trigger.new.size();i++)
        if(Trigger.new[i].IsConverted){
            leadtasks.put(Trigger.new[i].ConvertedContactId,null);
            leadlocations.put(Trigger.new[i].ConvertedContactId,i);
        }

    for(Contact con:[select id,(select id from Tasks) from Contact where id in: leadtasks.keyset()])
        leadtasks.put(con.id,(con.Tasks.size()!=0));

    System.debug(leadtasks+' '+leadlocations);
    for(String s:leadtasks.KeySet())
        if(!leadtasks.get(s)&&!Test.isRunningTest())
            Trigger.new[leadlocations.get(s)].addError('You need to have at least one Task to Convert a Lead.');   
}
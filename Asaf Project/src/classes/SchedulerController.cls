public class SchedulerController {
	public class option implements Comparable {
		public integer i{get;set;}
		public DateTime Start_time{get;set;}
		public String Start_format{get;set;}
		public DateTime End_time{get;set;}
		public String End_format{get;set;}
		public String techid{get;set;}
		public String techname{get;set;}
		public Decimal avg{get;set;}
		public Integer sumskill{get;set;}
		public String rplcid{get;set;}
		public Integer compareTo(object o) {
			option oop = (option) o;
			Integer returnValue = 0;
			if(this.rplcid!=null&&oop.rplcid==null){
				returnValue = 1;
			}
			else if(this.rplcid==null&&oop.rplcid!=null){
				returnValue = -1;
			}
			else if (this.sumskill > oop.sumskill) {
				// Set return value to a positive value.
				returnValue = -1;
			}
			else if(this.sumskill < oop.sumskill){
				returnValue = 1;
			}
			else if (this.avg < oop.avg) {
				// Set return value to a negative value.
				returnValue = 1;
			}
			else{
				returnValue = -1;
			}
			return returnValue; 
		}
	}
	public class Event {
		public Integer id {get;set;}
		public String text {get;set;}
		public String start_date {get;set;}
		public String end_date {get;set;}
		public String status{get;set;}
		public Decimal worktime{get;set;}
		public String techid{get;set;} 
		public String sf_id {get;set;}
		public Event (Service_Appointment__c serv){
			this.text = serv.Client__r.Name +' - '+serv.Technician__r.Name;
			this.techid = serv.Technician__c;
			this.status = serv.Appointment_Status__c;
			this.worktime = serv.Actual_Duration__c;
			this.sf_id = serv.id;
			this.start_date = serv.Start_Service_Appointment__c.format('MM/dd/yyyy HH:mm', 'Asia/Jerusalem');
			this.end_date = serv.End_Service_Appointment__c.format('MM/dd/yyyy HH:mm', 'Asia/Jerusalem');
		}
	}
	public String daturl{get;set;}
	public Date StartDate{get;set;}
	public List <Technician__c> techs {get;set;}
	public Service_Appointment__c NewServ {get;set;}
	public String curc{get;set;}
	public List <selectoption> cops {get;set;}
	public List <option> tops {get;set;}
	public List <Event> events {get;set;}
	public String jsonEvents {get;set;}
	public String jsonTechs {get;set;}
	public String curid{get;set;}
	public Integer opid{get;set;}
	public List <Service_Appointment__c> OpenServs {get;set;}
	public Map <String ,Service_Appointment__c> CloseServs {get;set;}
	public Map <String ,Decimal> TechsAvg {get;set;}
	//Constructor
	public SchedulerController() {
		techs = [select id,Name,Weekly_Hours_formula__c,Daily_Hours__c,(select Start_Date__c,End_Date__c from Technician_Absences__r) from Technician__c];
		StartDate=System.Today().toStartofWeek();
		daturl= ApexPages.currentPage().getParameters().get('dat');
		if(daturl!=null&&daturl!='')
			StartDate = Date.parse(daturl);
		getClients();
		getClosedServs();
		getOpenServs();
		initNew();
	}
	public void getClients(){
		curc = '';
		cops = new List <selectoption>();
		for(Account acc : [select id,Name from Account])
			cops.add(new selectoption(acc.id,acc.name));
	}
	public void getClosedServs(){
		TechsAvg = new Map <String ,Decimal>();
		for(Technician__c tech : techs){
			TechsAvg.put(tech.Name,0);
		}
		events = new List <Event>();
		CloseServs = new Map <String ,Service_Appointment__c>(); 
		for(Service_Appointment__c serv:[select id,Priority__c,Appointment_Status__c,Client__c,Client__r.Name,Technician__c,Technician__r.Name,Start_Service_Appointment__c,End_Service_Appointment__c,Actual_Duration__c from Service_Appointment__c where Technician__c!=null and Start_Service_Appointment__c>=:StartDate and End_Service_Appointment__c<:StartDate.addDays(7) Order by Start_Service_Appointment__c,End_Service_Appointment__c,Client__r.Name]){
			CloseServs.put(serv.id,serv);	
			events.add(new Event(serv));
			TechsAvg.put(serv.Technician__r.Name,TechsAvg.get(serv.Technician__r.Name)+serv.Actual_Duration__c);
		}
		for(integer i=0;i<events.size();i++)
			events[i].id=i+1;
		for(Technician__c tech: techs){
			Decimal weekly = tech.Weekly_Hours_formula__c;
			Decimal daily = tech.Daily_Hours__c;
			for(integer i=0;i<7;i++)
				for(Technician_Absence__c techabs:tech.Technician_Absences__r){
					if(StartDate.addDays(i)>=techabs.Start_Date__c&&StartDate.addDays(i)<=techabs.End_Date__c){
						weekly-=daily;
						break;
					}
				}
			if(weekly>0)
				TechsAvg.put(tech.Name,TechsAvg.get(tech.Name)/weekly*100);
			else
				TechsAvg.put(tech.Name,0);	
		}
		jsonEvents = JSON.serialize(events);
		jsonTechs = JSON.serialize(TechsAvg);
	}
	public void next(){
		StartDate = StartDate.addDays(7);
		getClosedServs();
		getOpenServs();
	}
	public void prev(){
		StartDate = StartDate.addDays(-7);
		getClosedServs();
		getOpenServs();
	}
	public void toda(){
		StartDate=System.Today().toStartofWeek();
		getClosedServs();
		getOpenServs();
	}
	public void getOpenServs(){
		OpenServs = [select id,Priority__c,Estimated_Duration__c,Skills_Required__c,Minimum_Level__c,Appointment_Status__c,Client__c,Client__r.Name,Technician__c,Technician__r.Name,Start_Due_Date__c,End_Due_Date__c,Start_Service_Appointment__c,End_Service_Appointment__c,Actual_Duration__c from Service_Appointment__c where Technician__c=null and Client__c!=null and Estimated_Duration__c!=null and Start_Due_Date__c>=:StartDate and End_Due_Date__c<:StartDate.addDays(7) Order by Start_Due_Date__c,End_Due_Date__c,Client__r.Name];
	}
	public void initNew(){
		NewServ = new Service_Appointment__c();
	}
	public PageReference SaveNew(){
		try{ 
			NewServ.Client__c = curc; 
			NewServ.Appointment_Status__c = 'Not Schedule';
			insert NewServ;
			initNew();
			PageReference pg = new PageReference('/apex/scheduler?dat='+StartDate.format());
			pg.setRedirect(true);
			return pg; 
		}
		catch(exception e){
			return null;
		}
	}
	public void FindCur(){
		tops = new List <option>();
		for(Service_Appointment__c serv:OpenServs)
			if(serv.id==curid){
				FindOptions(serv);
				break;
			}
	}
	public void FindOptions(Service_Appointment__c serv){
		List <Technician__c>optechs = [select id,Name,Available_Days__c,Start_of_Work_Day__c,End_of_Work_Day__c,Weekly_Hours_formula__c,Daily_Hours__c,(select Start_Date__c,End_Date__c from Technician_Absences__r),(select Name,Skill_Level__c from Skills__r where Skill_Level__c!=null) from Technician__c];
		for(integer i=0;i<optechs.size();i++){
			String[] tmpString = serv.Skills_Required__c.split(';');
			integer skillf=0;
			integer sumskills=0;
			for(String s : tmpString){
				//check skills
				for(Skill__c skill:optechs[i].Skills__r){
					if(skill.Name==s&&skill.Skill_Level__c>=serv.Minimum_Level__c){
						sumskills+=integer.valueof(skill.Skill_Level__c);
						skillf++;
						break;
					}
				}
			}
			if(skillf!=tmpString.size()){
				optechs.remove(i);
				i--;
			}
			else{
				//check abs days
				List <Date> optdats = new List <Date>();
				integer days=serv.Start_Due_Date__c.daysBetween(serv.End_Due_Date__c);
				for(integer j=0;j<days+1;j++){
					Boolean good=true;
					for(Technician_Absence__c techabs:optechs[i].Technician_Absences__r){
						if(techabs.Start_Date__c<=serv.Start_Due_Date__c.addDays(j)&&techabs.End_Date__c>=serv.Start_Due_Date__c.addDays(j)){
							good=false;
							break;
						}
					}
					if(good){
						optdats.add(serv.Start_Due_Date__c.addDays(j));
					}
				}
				if(optdats.size()==0){
					optechs.remove(i);
					i--;
				}
				else{
					//check times in each day
					for(Date dat:optdats){
						option op = findInDat(serv,optechs[i],dat,optechs[i].Start_of_Work_Day__c,sumskills);
						if(op!=null){
							tops.add(op);
							while(op!=null&&op.rplcid!=null){
								op = findInDat(serv,optechs[i],dat,op.End_time.time(),sumskills);
								if(op!=null)
									tops.add(op);
							}
						}
					}
				}
			}
		}
		//sort and number options
		if(tops.size()>0){
			tops.sort();
			for(integer i=0;i<tops.size();i++)
				tops[i].i=i+1;
		}
	}
	public option findInDat(Service_Appointment__c serv,Technician__c tec, Date dat,Time startt,integer sumskills){
		option op = new option();
		op.techid = tec.id;
		op.techname=tec.Name;
		op.sumskill=sumskills;
		op.avg=TechsAvg.get(tec.id);
		op.Start_time = Datetime.newInstance(dat, startt);
		Decimal secnds = serv.Estimated_Duration__c*60*60;
		op.End_time = op.Start_time.addSeconds(Integer.valueof(secnds));
		op.Start_format = op.Start_time.format('MM/dd/yyyy HH:mm', 'Asia/Jerusalem');
		op.End_format = op.End_time.format('MM/dd/yyyy HH:mm', 'Asia/Jerusalem');
		if(op.End_time.time()>tec.End_of_Work_Day__c)
			return null;
		for(Service_Appointment__c oserv:CloseServs.values()){
			if(oserv.Technician__c==tec.id&&oserv.Start_Service_Appointment__c>=op.Start_time&&oserv.Start_Service_Appointment__c<=op.End_time){
				if(serv.Priority__c=='High'&&oserv.Priority__c!='High'){
					op.rplcid=oserv.id;
					return op;
				}
				else{
					return findInDat(serv,tec,dat,oserv.End_Service_Appointment__c.time(),sumskills);
				}
			}
		}
		return op;
	}
	public PageReference setSchedule(){
		for(option op:tops)
			if(op.i==opid){
				for(Service_Appointment__c serv:OpenServs)
					if(curid==serv.id){
						serv.Appointment_Status__c = 'Scheduled';
						serv.Start_Service_Appointment__c = op.Start_time;
						serv.End_Service_Appointment__c = op.End_time;
						serv.Technician__c = op.techid;
						update serv;
						if(op.rplcid!=null)
							for(Service_Appointment__c rserv:CloseServs.values())
								if(op.rplcid==rserv.id){
									rserv.Appointment_Status__c = 'Not Schedule';
									rserv.Start_Service_Appointment__c = null;
									rserv.End_Service_Appointment__c = null;
									rserv.Technician__c = null;
									update rserv;
									break;
								}
						PageReference pg = new PageReference('/apex/scheduler?dat='+StartDate.format());
						pg.setRedirect(true);
						return pg; 
					}
			}
		return null;
	}
}
global class RollUpController {
	public List <RollUp__c> rolls{get;set;}
	public String curid{get;set;}
	public String objname{get;set;}
	public String rollup{get;set;}
	public String lookup{get;set;}
	public String condition{get;set;}
	private String maxname;
	private Map<String, Schema.SObjectType> gd;
	public List <selectoption> objopts{get;set;}
	public List <selectoption> rollupopts{get;set;}
	public List <selectoption> lookupopts{get;set;}
	public List <selectoption> conditionopts{get;set;}
	public RollUpController() {
		query();
		if(rolls.size()>0)
			maxname = [select Name from RollUp__c Order by Name DESC limit 1].Name;
		else
			maxname = '0';
		nextname();
		objopts = new List <selectoption>();
		gd = Schema.getGlobalDescribe();
		objname = '';
		objopts.add(new selectoption('','Select Object'));
		for (Schema.SObjectType o : gd.values()) {
			Schema.DescribeSObjectResult objResult = o.getDescribe();
			objopts.add(new selectoption(objResult.getName(),objResult.getLabel()));
		}
		objopts.sort();
	}

	private void query(){
		rolls = [select id,Name,Object__c,RollUp_Field__c,LookUp_Field__c,Condition_Field__c from RollUp__c Order by Object__c,LookUp_Field__c,RollUp_Field__c];
	}

	private void nextname(){
		maxname = String.valueof(Integer.valueof(maxname)+1);
	}

	public void InsertRoll(){
		if(objname!=null&&lookup!=null&&rollup!=null&&objname!=''&&lookup!=''&&rollup!=''){
			RollUp__c roll = new RollUp__c(Name=maxname, Object__c=objname, RollUp_Field__c=rollup, LookUp_Field__c=lookup, Condition_Field__c=condition);
			insert roll;
			nextname();
			query();
		}
	}

	public void DeleteRoll(){
		for(RollUp__c roll:rolls){
			String rid = String.valueof(roll.id);
			if(rid==curid){
				delete roll;
				query();
				break;
			}
		}
	}

	public void objSelected(){
		Set <String> nums = new Set <String>{'INTEGER', 'LONG', 'DOUBLE', 'CURRENCY'};
		rollupopts = new List <selectoption>();
		lookupopts = new List <selectoption>();
		conditionopts = new List <selectoption>();
		if(objname!=null&&objname!=''){
			rollupopts.add(new selectoption('Records Count', 'Records Count'));
			conditionopts.add(new selectoption('None', 'None'));
			Schema.SObjectType ctype = gd.get(objname);
            Map <String, Schema.SObjectField> flds = ctype.getDescribe().fields.getMap();
			for(String fieldName : flds.keySet()) {
				if(String.valueof(flds.get(fieldName).getDescribe().getType())=='REFERENCE'){
					System.debug(String.valueof(flds.get(fieldName).getDescribe().getReferenceTo()));
					String aparObjects = String.valueof(flds.get(fieldName).getDescribe().getReferenceTo());
					aparObjects = aparObjects.replaceAll('\\(','').replaceAll('\\)','').replaceAll(' ','');
					if(!aparObjects.contains(',')){
						Schema.SObjectType sectype = gd.get(aparObjects);
						Map<String, Schema.SObjectField> secflds = sectype.getDescribe().fields.getMap();
						for(String secfieldName : secflds.keySet()) {
							if(secflds.get(secfieldName).getDescribe().isUpdateable()&&nums.contains(String.valueof(secflds.get(secfieldName).getDescribe().getType()))){
								lookupopts.add(new selectoption(fieldName+'.'+secfieldName, flds.get(fieldName).getDescribe().getLabel()+' -> '+secflds.get(secfieldName).getDescribe().getLabel()));
							}
						}
					}
				}
				if(nums.contains(String.valueof(flds.get(fieldName).getDescribe().getType()))) 
					rollupopts.add(new selectoption(fieldName, flds.get(fieldName).getDescribe().getLabel()));
				if(flds.get(fieldName).getDescribe().isUpdateable()&&String.valueof(flds.get(fieldName).getDescribe().getType())=='BOOLEAN')
					conditionopts.add(new selectoption(fieldName, flds.get(fieldName).getDescribe().getLabel()));
			}
			System.debug('Object: '+objname);
			System.debug('Rollups: '+rollupopts);
			System.debug('LookUps: '+lookupopts);
			System.debug('Conditions: '+conditionopts);
		}
	}

	global static void RollUpCheck(List <Sobject> sobs, Map<id,Sobject> oldsobs,String Trig){
		String ObjectApi;
		if((Trig=='I'||Trig=='U')&&sobs.size()>0)
            ObjectApi = sobs[0].getSObjectType().getDescribe().getName();
		if((Trig=='D')&&oldsobs.size()>0)
            ObjectApi = oldsobs.values()[0].getSObjectType().getDescribe().getName();
		if(ObjectApi!=null){
			List <Sobject> par4up = new List <Sobject>();
			Schema.SObjectType ctype = Schema.getGlobalDescribe().get(ObjectApi);
            Map <String, Schema.SObjectField> flds = ctype.getDescribe().fields.getMap();
			List <RollUp__c> rolls = [select RollUp_Field__c,LookUp_Field__c,Condition_Field__c from RollUp__c Where Object__c=:ObjectApi Order by LookUp_Field__c];
			Map <String,Map <String,String>> ParentFields = new Map <String,Map <String,String>>();
			for(RollUp__c roll:rolls){
				if(roll.LookUp_Field__c!=null){
					String [] spl = roll.LookUp_Field__c.split('\\.');
					if(spl.size()>1){
						if(!ParentFields.containsKey(spl[0]))
							ParentFields.put(spl[0],new Map <String,String>{spl[1]=>roll.RollUp_Field__c+';'+roll.Condition_Field__c});
						else
							ParentFields.get(spl[0]).put(spl[1],roll.RollUp_Field__c+';'+roll.Condition_Field__c);
					}
				}
			}
			System.debug(ParentFields);
			for(String par:ParentFields.keySet()){
				String parentObj = String.valueof(flds.get(par).getDescribe().getReferenceTo());
				System.debug(parentObj);
				parentObj = parentObj.replaceAll('\\(','');
				parentObj = parentObj.replaceAll('\\)','');
				for(String parfld:ParentFields.get(par).keySet()){
					String [] spl = ParentFields.get(par).get(parfld).split(';');
					String roll = spl[0], con = spl[1];
					if(Trig=='D'){
						for(String  oid:oldsobs.keySet()){
							Boolean cond = spl[1]=='None'?true:Boolean.valueof(oldsobs.get(oid).get(con));
							if(cond){
								Decimal num = roll=='Records Count'?1:(oldsobs.get(oid).get(roll)!=null?(Decimal) oldsobs.get(oid).get(roll):0);
								sObject Parent = Schema.getGlobalDescribe().get(parentObj).newSObject();
								Parent.put('id',oldsobs.get(oid).get(par));
								try{
									Parent.put(parfld,Parent.get(parfld)!=null?(Decimal) (Parent.get(parfld))-num:0);
								}
								catch(exception e){
									Parent.put(parfld,Integer.valueof(Parent.get(parfld)!=null?(Decimal) (Parent.get(parfld))-num:0));
								}
								par4up.add(Parent);
							}
						}
					}
					else if(Trig=='I'){
						for(sObject sob:sobs){
							Boolean cond = spl[1]=='None'?true:Boolean.valueof(sob.get(con));
							if(cond){
								Decimal num = roll=='Records Count'?1:(sob.get(roll)!=null?(Decimal) sob.get(roll):0);
								sObject Parent = Schema.getGlobalDescribe().get(parentObj).newSObject();
								Parent.put('id',sob.get(par));
								try{
									Parent.put(parfld,Parent.get(parfld)!=null?(Decimal) (Parent.get(parfld))+num:num);
								}
								catch(exception e){
									Parent.put(parfld,Integer.valueof(Parent.get(parfld)!=null?(Decimal) (Parent.get(parfld))+num:num));
								}
								par4up.add(Parent);
							}
						}
					}
					else{
						for(sObject sob:sobs){
							Boolean cond = spl[1]=='None'?true:Boolean.valueof(sob.get(con));
							Boolean oldcond = spl[1]=='None'?true:Boolean.valueof(oldsobs.get(sob.id).get(con));
							Boolean ischanged = sob.get(roll)!=oldsobs.get(sob.id).get(roll)?true:false;
							Decimal num = roll=='Records Count'?1:(sob.get(roll)!=null?(Decimal) sob.get(roll):0);
							Decimal oldnum = roll=='Records Count'?1:(oldsobs.get(sob.id).get(roll)!=null?(Decimal) oldsobs.get(sob.id).get(roll):0);
							if(cond&&!oldcond){
								sObject Parent = Schema.getGlobalDescribe().get(parentObj).newSObject();
								Parent.put('id',sob.get(par));
								try{
									Parent.put(parfld,Parent.get(parfld)!=null?(Decimal) (Parent.get(parfld))+num:num);
								}
								catch(exception e){
									Parent.put(parfld,Integer.valueof(Parent.get(parfld)!=null?(Decimal) (Parent.get(parfld))+num:num));
								}
								par4up.add(Parent);
							}
							else if(cond&&oldcond&&ischanged){
								sObject Parent = Schema.getGlobalDescribe().get(parentObj).newSObject();
								Parent.put('id',sob.get(par));
								try{
									Parent.put(parfld,Parent.get(parfld)!=null?(Decimal) (Parent.get(parfld))+(num-oldnum):(num-oldnum));
								}
								catch(exception e){
									Parent.put(parfld,Integer.valueof(Parent.get(parfld)!=null?(Decimal) (Parent.get(parfld))+(num-oldnum):(num-oldnum)));
								}
								par4up.add(Parent);
							}
							else if(!cond&&oldcond){
								sObject Parent = Schema.getGlobalDescribe().get(parentObj).newSObject();
								Parent.put('id',sob.get(par));
								try{
									Parent.put(parfld,Parent.get(parfld)!=null?(Decimal) (Parent.get(parfld))-oldnum:0);
								}
								catch(exception e){
									Parent.put(parfld,Integer.valueof(Parent.get(parfld)!=null?(Decimal) (Parent.get(parfld))-oldnum:0));
								}
								par4up.add(Parent);
							}
						}
					}
				}
			}
			update par4up;
		}
	}
}
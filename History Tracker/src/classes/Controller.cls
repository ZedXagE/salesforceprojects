global class Controller implements Schedulable{
    public String ObjectApi{get;set;}
    public String [] FieldsApi{get;set;}
    public String FieldApi{get;set;}
    private Map<String, Schema.SObjectField> editflds;
    public set <String> exsts;
    public set <String> okFields;
    public String deleteid{get;set;}
    public HistorySettings__c settings{get;set;}
    public List <HistoryTracker__c> HistoryExsts{get;set;}
    public List <HistoryTracker__c> HistoryNew{get;set;}
    public List <selectoption> objFields{get;set;}
    public Integer num{get;set;}
    public Controller() {

    }
    global Void execute(SchedulableContext context){
        initSettings();
        if(settings.History_Save_Time__c!=null&&settings.History_Save_Time__c!=0){
            delete [select id from History__c where CreatedDate<=:System.Today().AddDays(integer.valueof(-1*settings.History_Save_Time__c))];
        }
    }
    public void initSetup(){
        FieldsApi= new String [] {''};
        HistoryNew = new List <HistoryTracker__c>();
        objChange();
    }
    public void initSettings(){
        List <HistorySettings__c> settingss = [select id,History_Save_Time__c from HistorySettings__c where name='Default' limit 1];
        if(settingss.size()>0)
            settings = settingss[0];
        else{
            settings = new HistorySettings__c(Name='Default');
            insert settings;
        }
    }
    public void Setup(){
        ObjectApi='';
        objFields = new List <selectoption>();
        objFields.add(new selectoption('',''));
        num=1;
        exsts = new Set <String>();
        initSetup();
        HistoryExsts = [select id,Name,Object_api__c,Field_api__c,Field_Label__c,Also_Connect_Lookup__c from HistoryTracker__c Order by Object_api__c];
        for(HistoryTracker__c hst:HistoryExsts){
            exsts.add(hst.Object_api__c.tolowercase()+hst.Field_api__c.tolowercase());
            if(Integer.valueof(hst.Name)>=num){
                num=Integer.valueof(hst.Name)+1;
            }
        }
        initSettings();
    }
    public void ObjChange(){
        editflds = new Map<String, Schema.SObjectField>();
        okFields = new Set <String>();
        objFields = new List <selectoption>();
        ObjectApi = ObjectApi!=null?ObjectApi.toLowerCase():null;
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe(); 
        Schema.SObjectType ctype = gd.get(ObjectApi);
        try {
            editflds = ctype.getDescribe().fields.getMap();
            for(String fieldName : editflds.keySet()) {
                if(editflds.get(fieldName).getDescribe().isUpdateable()&&!exsts.contains(ObjectApi+fieldName.tolowercase())) {
                    okFields.add(fieldName.tolowerCase());
                    objFields.add(new selectoption(fieldName+';'+editflds.get(fieldName).getDescribe().getLabel(),editflds.get(fieldName).getDescribe().getLabel()+' ('+fieldName+')'));
                }
            }
            objFields.sort();
        }
        catch (exception e){

        }
    }
    public void SaveObject(){
        ObjectApi = ObjectApi!=null?ObjectApi.toLowerCase():null;
        for(String fld:FieldsApi){
            if(fld!=''){
                String [] spl = fld.split(';');
                if(spl.size()>1){
                    HistoryNew.add(new HistoryTracker__c(Name=String.valueOf(num),Object_api__c=ObjectApi,Field_api__c=spl[0],Field_Label__c=spl[1]));
                    exsts.add(ObjectApi+spl[0].tolowercase());
                    num++;
                }
            }
        }
        if(FieldApi!=null){
            for(String fld:FieldApi.replaceAll(' ','').split(',')){
                if(fld!=''&&!exsts.contains(ObjectApi+fld.toLowerCase())&&okFields.contains(fld.tolowercase())){
                    HistoryNew.add(new HistoryTracker__c(Name=String.valueOf(num),Object_api__c=ObjectApi,Field_api__c=fld,Field_Label__c=editflds.get(fld).getDescribe().getLabel()));
                    exsts.add(ObjectApi+fld.toLowerCase());
                    num++;
                }
            }
        }
        insert HistoryNew;
        for(HistoryTracker__c hst:HistoryNew){
            HistoryExsts.add(hst);
        }
        initSetup();

    }
    public void del(){
        if(deleteid!=null&&deleteid!=''){
            try{
                for(integer i=0;i<HistoryExsts.size();i++){
                    if(HistoryExsts[i].id==deleteid){
                        exsts.remove(HistoryExsts[i].Object_api__c.toLowerCase()+HistoryExsts[i].Field_api__c.toLowerCase());
                        HistoryExsts.remove(i);
                        break;
                    }
                }
                delete new HistoryTracker__c(id=deleteid);
                ObjChange();
            }
            catch(exception e){
                System.debug(e.getMessage());
            }
        }
    }
    global static void HistoryCheck(List <Sobject> sobs, Map<id,Sobject> oldsobs){
        if(sobs.size()>0){
            String ObjectApi = sobs[0].getSObjectType().getDescribe().getName().tolowercase();
            String ObjectApifld = ObjectApi;
            Map <String, Schema.DisplayType> fldTypes = new Map <String, Schema.DisplayType>();
            Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe(); Schema.SObjectType ctype = gd.get(ObjectApi);
            Map<String, Schema.SObjectField> editflds = ctype.getDescribe().fields.getMap();
            for(String fieldName : editflds.keySet()) {
                if(editflds.get(fieldName).getDescribe().isUpdateable()) {
                    fldTypes.put(fieldName.tolowercase(),(editflds.get(fieldName).getDescribe().getType()));
                }
            }
            List <HistoryTracker__c> HistoryTracks = [select Field_api__c,Field_Label__c,Also_Connect_Lookup__c from HistoryTracker__c Where Object_api__c=:ObjectApi];
            if(!ObjectApi.contains('__c'))
                ObjectApifld=ObjectApi+'__c';
            List <History__c> histories = new List <History__c>();
            Map <String, Set <String>> lkps = new Map <String, Set <String>>();
            Map <String, Set <String>> lkpsob = new Map <String, Set <String>>();
            Map <String, String> lkpslabels = new Map <String, String>();
            Map <String, String> lkpsalsos = new Map <String, String>();
            for(Sobject sob:sobs){
                for(HistoryTracker__c hst:HistoryTracks){
                    if(sob.get(hst.Field_api__c)!=oldsobs.get(sob.id).get(hst.Field_api__c)){
                        try{
                            History__c hs = new History__c();
                            if(String.valueof(fldTypes.get(hst.Field_api__c))!='REFERENCE'){
                                hs.Old_value__c = oldsobs.get(sob.id).get(hst.Field_api__c)!=null?String.valueof(oldsobs.get(sob.id).get(hst.Field_api__c)):null;
                                hs.New_value__c = sob.get(hst.Field_api__c)!=null?String.valueof(sob.get(hst.Field_api__c)):null;
                                hs.Field_Api_Name__c = hst.Field_api__c;
                                hs.Field_Label__c = hst.Field_Label__c;
                                hs.Record_Name__c = String.valueof(sob.get('Name'));
                                hs.put(ObjectApifld,sob.id);
                                if(hst.Also_Connect_Lookup__c!=null){
                                    String aparObjects = String.valueof(editflds.get(hst.Also_Connect_Lookup__c).getDescribe().getReferenceTo());
                                    aparObjects = aparObjects.replaceAll('\\(','').replaceAll('\\)','').replaceAll(' ','');
                                    for(String aparObject:aparObjects.split(',')){
                                        if(!aparObject.contains('__c'))
                                            aparObject=aparObject+'__c';
                                        if(sob.get(hst.Also_Connect_Lookup__c)!=null)
                                        hs.put(aparObject,sob.get(hst.Also_Connect_Lookup__c));
                                    }
                                }
                                histories.add(hs);
                            }
                            else{
                                lkpslabels.put(hst.Field_api__c,hst.Field_Label__c);
                                if(hst.Also_Connect_Lookup__c!=null)
                                    lkpsalsos.put(hst.Field_api__c,hst.Also_Connect_Lookup__c);
                                if(!lkps.containsKey(hst.Field_api__c)){
                                    lkps.put(hst.Field_api__c,new Set <String> ());
                                    lkpsob.put(hst.Field_api__c,new Set <String> ());
                                }
                                lkpsob.get(hst.Field_api__c).add(sob.id);
                                if(oldsobs.get(sob.id).get(hst.Field_api__c)!=null)
                                    lkps.get(hst.Field_api__c).add(String.valueof(oldsobs.get(sob.id).get(hst.Field_api__c)));
                                if(sob.get(hst.Field_api__c)!=null)
                                    lkps.get(hst.Field_api__c).add(String.valueof(sob.get(hst.Field_api__c)));    
                            }
                        }
                        catch(exception e){
                            System.debug(e.getMessage());
                        }
                    }
                }
            }
            for(String lkp:lkps.keyset()){
                try{
                    String parentObjects = String.valueof(editflds.get(lkp).getDescribe().getReferenceTo());
                    parentObjects = parentObjects.replaceAll('\\(','').replaceAll('\\)','').replaceAll(' ','');
                    for(String parentObject:parentObjects.split(',')){
                        Set <String> obids = lkps.get(lkp);
                        Map <id, sobject> parsobs = new Map <id,sobject> (database.query('select id,name from '+parentObject+' where id in :obids'));
                        for(Sobject sob:sobs){
                            if(lkpsob.get(lkp).contains(sob.id)){
                                History__c hs = new History__c();
                                hs.Old_value__c = oldsobs.get(sob.id).get(lkp)!=null?(parsobs.get(String.valueof(oldsobs.get(sob.id).get(lkp))).get('name')+' ('+String.valueof(oldsobs.get(sob.id).get(lkp))+')'):null;
                                hs.New_value__c = sob.get(lkp)!=null?(parsobs.get(String.valueof(sob.get(lkp))).get('name')+' ('+String.valueof(sob.get(lkp))+')'):null;
                                hs.Field_Api_Name__c = lkp;
                                hs.Field_Label__c = lkpslabels.get(lkp);
                                hs.Record_Name__c = String.valueof(sob.get('Name'));
                                hs.put(ObjectApifld,sob.id);
                                if(lkpsalsos.containskey(lkp)&&lkpsalsos.get(lkp)!=null){
                                    String aparObjects = String.valueof(editflds.get(lkpsalsos.get(lkp)).getDescribe().getReferenceTo());
                                    aparObjects = aparObjects.replaceAll('\\(','').replaceAll('\\)','').replaceAll(' ','');
                                    for(String aparObject:aparObjects.split(',')){
                                        if(!aparObject.contains('__c'))
                                            aparObject=aparObject+'__c';
                                        if(sob.get(lkpsalsos.get(lkp))!=null)
                                        hs.put(aparObject,sob.get(lkpsalsos.get(lkp)));
                                    }
                                }
                                histories.add(hs);
                            }
                        }
                    }
                }
                catch(exception e){
                    System.debug(e.getMessage());
                }
            }
            insert histories;
        }
    }
}
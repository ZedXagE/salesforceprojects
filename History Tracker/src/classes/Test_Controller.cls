@isTest
public class Test_Controller {
    static TestMethod void TestController() {
        Controller con = new Controller();
        con.Setup();
        con.execute(null);
        con.ObjectApi='contact';
        con.FieldsApi=new String [] {'lastname;lastname', 'accountid;account'};
        con.SaveObject();
        con.deleteid=con.HistoryExsts[0].id;
        con.del();
        account acc1 = new account(name='test1');
        insert acc1;
        account acc2 = new account(name='test1');
        insert acc2;
        contact con1 = new contact(lastname='test1',accountid=acc1.id);
        insert con1;
        contact con2 = new contact(lastname='test2',accountid=acc2.id);
        insert con2;
        List <contact> cons = new List <contact>();
        Map <Id,contact> oldlds = new Map <Id,contact>();
        cons.Add(con1);
        oldlds.put(con1.id,con2);
        Controller.HistoryCheck(cons,oldlds);
    }
}
@isTest(seeAlldata=true)
public class TestPDFs {
	static testMethod void Test() {

		//send teoudatgmar
		Enrollment__c enr=[select id,Opportunity__c from Enrollment__c where Course__c!=null and Opportunity__c!=null and Opportunity__r.modality_of_payment_status__c!=null and Opportunity__r.Amount!=null limit 1];
		Opportunity opp=[select id,SendTeoudatGmar__c from Opportunity where id=:enr.Opportunity__c limit 1];
		/*Cours__c crs=new Cours__c(Opportunity__c=opp.id,Start_of_course__c = System.Today(), End_Of_Course__c = System.Today(), ville__c = 'Tel Aviv', Rama_Level__c = 'Aleph', Time_course__c='Morning',Course__c='Oulpan');
		insert crs;*/
		opp.SendTeoudatGmar__c=true;
		update opp;

		//shovar
		PageReference pgRef = Page.Shovar;
		Test.setCurrentPage(pgRef);
		ApexPages.StandardController sc = new ApexPages.StandardController(opp);
		HebrewPDF hb = new HebrewPDF(sc);
		hb.gen();
		//send attendances
		HebrewPDF.sendAttendances(new List <String>{opp.id});
	}
}
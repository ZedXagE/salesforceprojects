public class DailyHandler {
	public List <Payments__c> pays{get;set;}
	public List <Payments__c> cheques{get;set;}
	public List <Payments__c> UnknownErrors{get;set;}
	public Boolean running{get;set;}
	public Integer runningSize{get;set;}
	public Integer paysNum{get;set;}
	public Integer chequesNum{get;set;}
	public Integer unknownNum{get;set;}
	private Date ToDate;
	public DailyHandler() {		ToDate = System.Today();	}
	public void ChangeDate(Date dat){	ToDate = dat;	}
	public Void init(){
		pays = [Select id,Name,Opportunity__c,Payment_Date__c ,Queue_to_Tranzila__c,Unknown_Error__c,Opportunity__r.Name,Payment_Status__c,Payment_Amount__c,Payment_Method__c,Token__c,Expiration_Date__c,Opportunity__r.No_VAT__c,Opportunity__r.Account.Ville__c from Payments__c where Payment_Date__c<=:ToDate and Payment_Status__c='In progress' and Payment_Method__c='Credit Card' and Token__c!=null and Payment_Amount__c!=null and Expiration_Date__c!=null and Queue_to_Tranzila__c=false and Unknown_Error__c=false];
		cheques = [Select id,Name,Queue_to_GreenInvoice__c,Opportunity__c,Opportunity__r.Name,Payment_Amount__c,Payment_Method__c,Check_number__c,Token__c,First_Credit_Payment__c,Total_Price__c,Payment_Status__c,Payment_Date__c,Opportunity__r.No_Vat__c,Opportunity__r.Course__c,Opportunity__r.Account.Name,Opportunity__r.Account.Teoudat_Zeout__c,Opportunity__r.Account.PersonEmail,Opportunity__r.Account.Phone,Opportunity__r.Account.BillingStreet,Opportunity__r.Account.BillingCity,Opportunity__r.Account.BillingCountry,Opportunity__r.Account.BillingPostalCode,Opportunity__r.Account.Ville__c from Payments__c where Payment_Date__c<:ToDate.addDays(-4) and Payment_Date__c>=:ToDate.addDays(-30) and Payment_Status__c='Paid' and Payment_Method__c='cheque en israel' and Payment_Amount__c!=null and ChequeInvoiced__c=false  and Queue_to_GreenInvoice__c=false and Unknown_Error__c=false];
		UnknownErrors = [select id,Name,Opportunity__c,Opportunity__r.Name,Payment_Status__c,Payment_Amount__c,Payment_Method__c,Payment_Date__c  from Payments__c where Unknown_Error__c=true];
		paysNum = pays.size();
		chequesNum = cheques.size();
		unknownNum = UnknownErrors.size();
		progressCheck();
		System.debug(pays);
		System.debug(cheques);
		System.debug(UnknownErrors);
	}
	public void sendPayments(){
		running=true;
		System.enqueueJob(new DailyQueue(pays,new List <Payments__c>(),null));
	}
	public void sendCheques(){
		running=true;
		System.enqueueJob(new DailyQueue(new List <Payments__c>(),cheques,null));
	}
	//progress
    public void progressCheck(){
        List<AsyncApexJob> myrunningjob = [Select Id, Status, ApexClass.Name From AsyncApexJob where ApexClass.Name ='DailyQueue' And (Status='Queued' OR Status='Processing' or (Status='Completed' and CreatedDate>=:System.Now().addSeconds(-60))) order by CreatedDate DESC];
        runningSize = myrunningjob.size();
        if(runningSize==0)
            running=false;
		else
			running=true;
    }
	public void RefundTemp(){
		List <Payments__c> refundcheques = [Select id,Opportunity__c,Payment_Amount__c,Check_number__c,Token__c,First_Credit_Payment__c,Total_Price__c,Payment_Status__c,Payment_Date__c,Payment_Method__c,Opportunity__r.No_Vat__c,Opportunity__r.Course__c,Opportunity__r.Account.Name,Opportunity__r.Account.Teoudat_Zeout__c,Opportunity__r.Account.PersonEmail,Opportunity__r.Account.Phone,Opportunity__r.Account.BillingStreet,Opportunity__r.Account.BillingCity,Opportunity__r.Account.BillingCountry,Opportunity__r.Account.BillingPostalCode,Opportunity__r.Account.Ville__c from Payments__c where LastModifiedDate<:System.Now() and LastModifiedDate>=:System.Now().addHours(-24) and Payment_Status__c='Paid' and Payment_Method__c='cheque en israel' and Payment_Amount__c!=null and ChequeInvoiced__c=true];
		System.enqueueJob(new RefundTempCheque(refundcheques,null));
		if(Test.isRunningTest())
			System.enqueueJob(new RefundTempCheque(cheques,null));
	}
}
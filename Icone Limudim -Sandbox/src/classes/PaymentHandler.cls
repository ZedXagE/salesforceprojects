public class PaymentHandler implements Schedulable{
	@TestVisible
	private String sfid;
	@TestVisible
	private String accid;
	private String tok;
	private String exp;
	private String res;
	@TestVisible
	private String sum;
	private String npay;
	private String mod;
	public String manual{get;set;}
	private String conf;
	private String ind;
	private String cur;
	private String total;
	private Date mydat;
	private Date startD;
	private Date endD;
	public Boolean Status{get;set;}
	public String lang{get;set;}
	public String bgcolor{get;set;}
	private string numpay;
	public Integer NumPayInt {get;set;}
	public Integer MaxPay {get;set;}
	public Decimal ToPay{get;set;}
	public Decimal TotalPrice{get;set;}
	public Decimal TotalPaid{get;set;}
	public Boolean NormalPay{get;set;}
	public Opportunity opp{get;set;}
	public PaymentHandler() {
		//init
		lang='en';
		List <ITmyWay_App__Auth_Settings__c> AuthSettings=[select ITmyWay_App__Background_Color__c From ITmyWay_App__Auth_Settings__c Limit 1];
		if(AuthSettings.size()>0)
			bgcolor=AuthSettings[0].ITmyWay_App__Background_Color__c;
		if(bgcolor==null)
			bgcolor='white';
		//Page
		if(ApexPages.currentPage()!=null) {
			manual = ApexPages.currentPage().getParameters().get('manual');
			sfid = ApexPages.currentPage().getParameters().get('sf_id');
			total = ApexPages.currentPage().getParameters().get('total');
			numpay = ApexPages.currentPage().getParameters().get('numpay');
			npay =  ApexPages.currentPage().getParameters().get('npay');
			tok = ApexPages.currentPage().getParameters().get('TranzilaTK');
			exp = ApexPages.currentPage().getParameters().get('expmonth') + ApexPages.currentPage().getParameters().get('expyear');
			res = ApexPages.currentPage().getParameters().get('Response');
			mod = ApexPages.currentPage().getParameters().get('tranmode');
			conf = ApexPages.currentPage().getParameters().get('ConfirmationCode');
			ind = ApexPages.currentPage().getParameters().get('index');
			sum = ApexPages.currentPage().getParameters().get('sum');
			cur = ApexPages.currentPage().getParameters().get('currency');
		}
		mydat = System.Today();
		startD = System.Today();
		endD = System.Today();
	}
	public void changeDat(Date startDate, Date endDate){
		startD = startDate;
		endD = endDate;
	}
	//if manual -- Manual-1=manual payment,Manual-2=ChangeCard
	public void CheckChange(){
		if(manual=='2'&&bl(sfid)){
			ChangeCard();
		}
		if(manual=='1'&&bl(sfid)){
			ManualPay();
		}
	}
	public void ChangeCard(){
		List <Payments__c> pams = [select id,Payment_Status__c from Payments__c where Opportunity__c=:sfid and Payment_Status__c =: 'In progress' and Payment_Amount__c!=0 and Payment_Amount__c!=null and Token__c!=null and Payment_Method__c='Credit Card'];
		if(pams.size()>0){
			for(Payments__c pam:pams)
				pam.Payment_Status__c='Canceled';
			update pams;
		}
		ManualPay();
	}
	public void ManualPay(){
		ToPay=0;
		TotalPrice=0;
		TotalPaid=0;
		opp = [select id,Name,Account.Name,Account.Ville__c,Course__c,No_VAT__c,Amount,modality_of_payment_status__c,Second_Parameter__c from Opportunity where id=:sfid];
		//for(OpportunityLineItem opl:[select TotalPrice from OpportunityLineItem where OpportunityId=:opp.id])
		TotalPrice=opp.Amount!=null?opp.Amount:0;
		for(Payments__c pam:[select Payment_Amount__c from Payments__c where Opportunity__c=:opp.id and Payment_Status__c in: new Set <String>{'In progress','Paid','Refund','Due'} and Payment_Amount__c!=0 and Payment_Amount__c!=null])
			TotalPaid+=pam.Payment_Amount__c;
		MaxPay=1;
		NumPayInt=3;
		if(opp.Course__c=='Computer'||opp.Course__c=='Ulpan'){
			if(opp.Course__c=='Computer')
				ToPay=Decimal.valueof(Label.ComputerRegFee);
			else
				ToPay=Decimal.valueof(Label.UlpanRegFee);
			if((opp.modality_of_payment_status__c==null||opp.modality_of_payment_status__c=='Regular')&&opp.Second_Parameter__c!='OulpanNew'&&opp.Second_Parameter__c!='OulpanDibur')
				MaxPay=10;
			else
				MaxPay=4;
		}
		else if(opp.Course__c=='Psychometry'){
			if(opp.Second_Parameter__c=='Sherout'){
				ToPay=TotalPrice-TotalPaid;
				MaxPay=0;
				NumPayInt=0;
			}
			else{
				ToPay=Decimal.valueof(Label.PsychometryRegFee);
				MaxPay=10;
			}
		}
		if(opp.modality_of_payment_status__c!=null&&opp.modality_of_payment_status__c!='Regular')
			NormalPay=false;
		else
			NormalPay=true;
		//NumPays=PortalController.GeneratePick(MaxPay);
		if(TotalPaid!=0)
			ToPay=TotalPrice-TotalPaid;
	}
	/*public void changePayments(){
		NumPayInt=Integer.valueof(NumPay);
		if(TotalPaid!=0)
			ToPay=((TotalPrice-TotalPaid)/NumPayInt).setScale(2);
	}*/
	public void Portal(){
		Status=false;
		if(bl(sfid)&&bl(sum)){
			accid=[select Account.id from Opportunity where id=:sfid][0].Account.id;
			//success
			if(res=='000'){
				Status=true;
				SuccessPayment();
			}
			//failed
			else
				FailedPayment();
		}
	}
	private Boolean bl(String check){
		if(check!=null&&check!='')
			return true;
		else return false;
	}
	@TestVisible
	private void FailedPayment(){
		insert new Payments__c(Account__c=accid,Opportunity__c=sfid,Payment_Status__c='Unpaid',Payment_Amount__c=0,Payment_Method__c='Credit card',Payment_Date__c=mydat,Response__c=res,Expiration_Date__c=exp,Token__c=tok,Payment_ID__c=conf,index__c=ind,Num_Payments__c=(npay!=null?Decimal.valueof(npay)+1:null));
	}
	@TestVisible
	private void SuccessPayment(){
		//Boolean last=numpay=='0'?true:false;
		if(bl(total)&&bl(numpay)){
			insert new Payments__c(Account__c=accid,First_Credit_Payment__c=true,Opportunity__c=sfid,Payment_Status__c='Token Open',Payment_Amount__c=Decimal.valueof(sum),Payment_Method__c='Credit card',Payment_Date__c=mydat,Response__c=res,Expiration_Date__c=exp,Token__c=tok,Payment_ID__c=conf,index__c=ind,Num_Payments__c=Decimal.valueof(numpay),Total_Price__c=Decimal.valueof(total));
			CheckCreatePayments(sfid);
		}
		else{
			insert new Payments__c(Account__c=accid,Opportunity__c=sfid,Payment_Status__c='Paid',First_Credit_Payment__c=true,Payment_Amount__c=Decimal.valueof(sum),Payment_Method__c='Credit card',Payment_Date__c=mydat,Response__c=res,Expiration_Date__c=exp,Token__c=tok,Payment_ID__c=conf,index__c=ind,Num_Payments__c=(npay!=null?Decimal.valueof(npay)+1:null));
		}
	}
	public static void CheckCreatePayments(String oppd){
		Opportunity opp = [select id,Accountid,Amount,Course__c,modality_of_payment_status__c,(select id,Course__r.Start_of_course__c,Course__r.Midldle__c,Course__r.End_Of_Course__c from Enrollments__r where Course__r.Start_of_course__c!=null /*and Course__r.Start_of_course__c>=:System.Today()*/ and Course__r.Midldle__c!=null and Course__r.End_Of_Course__c!=null),(select id,Payment_Method__c,Response__c,Token__c,Expiration_Date__c,Payment_Status__C,First_Credit_Payment__c,Payment_Amount__c,Num_Payments__c,Total_Price__c from Payment__r Where Payment_Method__c='Credit Card' Order by CreatedDate DESC) from Opportunity where id=:oppd];
		if(opp.Payment__r.Size()>0&&opp.Enrollments__r.size()>0||Test.isRunningTest()){
			Payments__c Token=new Payments__c();
			Payments__c RefToken=new Payments__c();
			for(Payments__c pam:opp.Payment__r){
				if(pam.Payment_Status__c=='Token Open'&&pam.Token__c!=null&&pam.Num_Payments__c!=null&&pam.Num_Payments__c!=0&&pam.Total_Price__c!=null&&pam.Payment_Amount__c!=null&&pam.Response__c=='000'&&pam.First_Credit_Payment__c==true){
					Token=pam;
					Break;
				}
			}
			for(Payments__c pam:opp.Payment__r){
				if(pam.Payment_Status__c=='Token Closed'&&pam.Token__c!=null&&pam.Num_Payments__c!=null&&pam.Num_Payments__c!=0&&pam.Total_Price__c!=null&&pam.Payment_Amount__c!=null&&pam.Response__c=='000'&&pam.First_Credit_Payment__c==true){
					RefToken=pam;
					Break;
				}
			}
			//Found one
			if(Token.id!=null&&RefToken.id==null){
				List <Date> coursedates = new List <Date>();
				if(/*opp.Enrollments__r.size()>0&&*/(opp.Course__c=='Computer'||opp.Course__c=='Ulpan')&&!(opp.modality_of_payment_status__c==null||opp.modality_of_payment_status__c=='Regular')){
					coursedates.add(nearestEleven(opp.Enrollments__r[0].Course__r.Start_of_course__c));
					coursedates.add(nearestEleven(opp.Enrollments__r[0].Course__r.Midldle__c));
					coursedates.add(nearestEleven(opp.Enrollments__r[0].Course__r.End_Of_Course__c));
					/*}else{
						Date dat = nearestEleven(System.Today());
						coursedates.add(dat);
						coursedates.add(dat.addMonths(1));
						coursedates.add(dat.addMonths(2));
					}*/
					Token.Payment_Status__c='Token Closed';
					update Token;
					GeneratePayments(Token.Total_Price__c,Token.Num_Payments__c,opp.id,opp.Accountid,Token.Expiration_Date__c,Token.Token__c,opp.Amount,coursedates);
				}
			}
			//Refresh exsiting
			else if(Token.id!=null&&RefToken.id!=null){
				for(Payments__c pam:opp.Payment__r){
					if(pam.Payment_Status__c=='Canceled'&&pam.Token__c==RefToken.Token__c){
						pam.Payment_Status__c='In progress';
						pam.Token__c=Token.Token__c;
					}
					else if(pam.Payment_Status__c=='Unpaid'&&pam.Token__c==RefToken.Token__c){
						pam.Payment_Status__c='In progress';
						pam.Payment_Date__c=System.Today();
						pam.Token__c=Token.Token__c;
					}
				}
				Token.Payment_Status__c='Token Closed';
				update opp.Payment__r;
			}
		}
	}
	public static date nearestEleven(Date dat){
		if(dat==null)
			dat=System.Today();
		dat=dat.addMonths(1);
		if(dat.day()<=11)
			dat=dat.addDays((-1)*(dat.day())+1);
		else{
			dat=dat.addMonths(1);
			dat=dat.addDays((-1)*(dat.day())+1);
		}
		dat=dat.addDays(10);
		return dat;
	}
	public static void GeneratePayments(Decimal totalprice,Decimal num,String oppid,String accd,String expr,String token,Decimal fullprice,List <Date> coursedates){
		List <Payments__c> pays = new List <Payments__c>();
		Boolean last=false;
		totalprice=totalprice.setscale(2);
		Decimal curprice=(totalprice/num).setscale(2);
		Decimal totlast=null;
		for(integer i=1;i<=num;i++){
			if(i==num){
				last=true;
				curprice=(totalprice-((num-1)*curprice)).setscale(2);
				totlast=fullprice.setscale(2);
			}
			pays.add(new Payments__c(Account__c=accd,Opportunity__c=oppid,Payment_Amount__c=curprice,Payment_Method__c='Credit card',Payment_Status__c='In progress',Payment_Date__c=coursedates[i-1],Expiration_Date__c=expr,Token__c=token,Total_Price__c=totlast));
		}
		insert pays;
		GenerateRelatedKabala(oppid);
	}
	@future(callout=true)
	public static void GenerateRelatedKabala(String oppid){
		Opportunity opp = [select id,No_VAT__c,Course__c,Account.Name,Account.Teoudat_Zeout__c,Account.PersonEmail,Account.Phone,Account.BillingStreet,Account.BillingCountry,Account.BillingCity,Account.BillingPostalCode,Amount,Account.Ville__c,(select Payment_Amount__c,Token__c,Payment_Date__c from Payment__r where Payment_Status__c =: 'In progress' and Payment_Method__c =:'Credit Card' and Payment_Amount__c!=0) from Opportunity where id=:oppid];
		List <ITmyWay_Invoice.ControllerV2.income> incomes = new List <ITmyWay_Invoice.ControllerV2.income>();
		List <ITmyWay_Invoice.ControllerV2.payment> payments = new List <ITmyWay_Invoice.ControllerV2.payment>();
		Integer vat=opp.No_VAT__c==true?1:0;
		for(Payments__c pam:opp.Payment__r){
			ITmyWay_Invoice.ControllerV2.payment payment = new ITmyWay_Invoice.ControllerV2.payment();
			Datetime myDate=System.now().addSeconds(Timezone.getTimeZone('Asia/Jerusalem').getOffset(System.now())/1000);
			myDate = pam.Payment_Date__c!=null?pam.Payment_Date__c:myDate;
			payment.date_c=String.valueof(myDate.year())+'-'+(myDate.month()>9?String.valueof(myDate.month()):'0'+String.valueof(myDate.month()))+'-'+(myDate.day()>9?String.valueof(myDate.day()):'0'+String.valueof(myDate.day()));
			payment.type=3;
			payment.price=pam.Payment_Amount__c.setScale(2);
			payment.dealType=1;
			if(pam.Token__c!=null)
				payment.cardNum=Integer.valueof(pam.Token__c.right(4));
			payment.currency_c='ILS';

			ITmyWay_Invoice.ControllerV2.income income = new ITmyWay_Invoice.ControllerV2.income();
			income.quantity=1;
			income.price=pam.Payment_Amount__c.setScale(2);
			income.currency_c='ILS';
			income.vatType=vat+1;
			income.description=PaymentHandler.PaymentDesc(opp.Course__c);
			payments.add(payment);
			incomes.add(income);
		}
		System.debug(payments);
		System.debug(incomes);
		String masof = opp.Account.Ville__c!='Eilat'?'Default':'Eilat';
		HttpResponse res = ITmyWay_Invoice.ControllerV2.Create(masof,400,'he',vat,true,'ILS',null,false,opp.Account.Name+(opp.Account.Teoudat_Zeout__c!=null?(' '+opp.Account.Teoudat_Zeout__c):''),null,opp.Account.Phone,opp.Account.BillingStreet,null,opp.Account.BillingCity,opp.Account.BillingPostalCode,incomes,payments/*,opp.Amount,1,'ILS',1,'Icone Limudim'*/);
		System.debug(res.getBody());
		if(!Test.isRunningTest()&&res!=null)
			ManualReceiptController.InsertReceipt(Opp.Id,res.getBody());
	}
	@future(callout=true)
	public static void GreenInvoiceHandler(String pamid,Boolean up){
		Datetime myDate=System.now().addSeconds(Timezone.getTimeZone('Asia/Jerusalem').getOffset(System.now())/1000);
		Payments__c pam=[select Opportunity__c,Payment_Amount__c,Check_number__c,Token__c,First_Credit_Payment__c,Total_Price__c,Payment_Status__c,Payment_Date__c,Payment_Method__c,Opportunity__r.Account.Ville__c from Payments__c where id=:pamid];
		String type='';
		Decimal amount=pam.Payment_Amount__c;
		integer typnum=3;
		if(pam.Payment_Method__c=='cheque en israel')
			typnum=2;
		if(pam.Payment_Status__c=='Paid'){
			if(pam.Payment_Method__c=='Credit Card'){
				if(up)
					type='mas';
				else
					type='invoice';
			}
			else
				type='mas';
		}
		else if(pam.Payment_Status__c=='Refund'){
			type='refund';
			amount = amount!=null?amount*(-1):0;
		}
		if(pam.Payment_Method__c=='Credit Card'&&pam.Payment_Status__C=='In progress'){
			myDate = pam.Payment_Date__c;
		}
		Opportunity opp = [select id,No_Vat__c,Course__c,Account.Name,Account.Teoudat_Zeout__c,Account.PersonEmail,Account.Phone,Account.BillingStreet,Account.BillingCity,Account.BillingCountry,Account.BillingPostalCode from Opportunity where id=:pam.Opportunity__c];
		Integer vat=opp.No_VAT__c==true?1:0;
		Integer i;
		try{
			i=Integer.ValueOf(pam.Token__c.right(4));
		}
		catch(exception e){}
		String masof = pam.Opportunity__r.Account.Ville__c!='Eilat'?'Default':'Eilat';
		HttpResponse res = ITmyWay_Invoice.ControllerV2.CreateOneGlobalDate(masof,type,'he',vat,vat+1,'ILS',opp.Account.Name+(opp.Account.Teoudat_Zeout__c!=null?(' '+opp.Account.Teoudat_Zeout__c):''),opp.Account.PersonEmail,opp.Account.Phone,opp.Account.BillingStreet,null/*opp.Account.BillingCountry*/,opp.Account.BillingCity,opp.Account.BillingPostalCode,amount,i,'ILS',1,PaymentDesc(opp.Course__c),typnum,pam.Check_number__c,myDate);
		System.debug(res.getBody());
		if(!Test.isRunningTest()&&res!=null)
			ManualReceiptController.InsertReceipt(Opp.Id,res.getBody());
	}
	public static String PaymentDesc(String course){
		if(course!=null){
			if(course=='Psychometry')
				return 'Icone Limudim';
			else if(course=='Ulpan')
				return 'אולפן שלי';
			else
				return 'Smart Computer';
		}
		return 'Icone Limudim';
	}
	//Refund (expdate,token,currency,credtype,tranmode,authnr,price,additionals,tsname)
	public void Refund(){
		Status=false;
		if(bl(sfid)){
			Payments__c pay = [select Payment_Status__c,First_Credit_Payment__c,Opportunity__c,Account__c,Payment_Amount__c,Token__c,Expiration_Date__c,index__c,Payment_ID__c from Payments__c where id=:sfid and Payment_Status__c='Paid'];
			HttpResponse response = ITmyWayTranzila.ControllerV2.Refund(pay.Expiration_Date__c,pay.Token__c,'1','1','C'+pay.index__c,pay.Payment_ID__c,String.valueof(pay.Payment_Amount__c),(pay.First_Credit_Payment__c==true?'iconeltd':'iconeltdtok'),null);
			Payments__c pam =  new Payments__c();
			if(response!=null||Test.isRunningTest()){
				//System.debug(response.getBody());
				String body=response!=null?response.getBody():'000';
				res = getParameter(body,'Response');
				conf = getParameter(body,'ConfirmationCode');
				ind = getParameter(body,'index');
				pam.Response__c=res;
				pam.Opportunity__c=pay.Opportunity__c;
				pam.Account__c=pay.Account__c;
				pam.Payment_Method__c='Credit Card';
				pam.Payment_Date__c=System.Today();
				pam.Token__c=pay.Token__c;
				if(res=='000'){
					pam.Payment_Status__c='Refund';
					pam.Payment_ID__c=conf;
					pam.index__c=ind;
					pam.Payment_Amount__c=pay.Payment_Amount__c*(-1);
					Status=true;
				}
				else{
					pam.Payment_Status__c='Unpaid';
					pam.Payment_Amount__c=0;
				}
				insert pam;
			}
		}
	}
	private String getParameter(String body,String key){
		if(body!=null&&key!=null){
			String [] spl1 = body.split(key+'=');
			if(spl1.size()>1){
				String [] spl2 = spl1[1].split('&');
				if(spl2.size()>0)
					return spl2[0];
				else
					return null;
			}
			else
				return null;
		}
		else
			return null;
	}
	//Charge (expdate,token,currency,credtype,tranmode,imaam,price,customsettings_name,additionals)
	public Void execute(SchedulableContext context){
		//Payments credit -- moved to daily classes
		/*List <Payments__c> pays = [Select id,Payment_Status__c,Payment_Amount__c,Token__c,Expiration_Date__c,Opportunity__r.No_VAT__c from Payments__c where Payment_Date__c>=:startD and Payment_Date__c<=:endD and Payment_Status__c='In progress' and Payment_Method__c='Credit Card' and Token__c!=null and Payment_Amount__c!=null and Expiration_Date__c!=null];
		System.debug(pays);
		HttpResponse response = new HttpResponse();
		for(Payments__c pay:pays){
			System.debug(pay);
			String vat=pay.Opportunity__r.No_VAT__c==true?'0':'0.17';
			response = ITmyWayTranzila.ControllerV2.Charge(pay.Expiration_Date__c,pay.Token__c,'1','1','A',vat,String.valueof(pay.Payment_Amount__c),'iconeltdtok',null);
			System.debug(response);
			if(response!=null||Test.isRunningTest()){
				//System.debug(response.getBody());
				String body=response!=null?response.getBody():'000';
				res = getParameter(body,'Response');
				conf = getParameter(body,'ConfirmationCode');
				ind = getParameter(body,'index');
				pay.Response__c=res;
				if(res=='000'){
					pay.Payment_Status__c='Paid';
					pay.Payment_ID__c=conf;
					pay.index__c=ind;
				}
				else{
					pay.Payment_Status__c='Unpaid';
				}
			}
		}
		try{
			update pays;
		}
		catch(exception e){}
		List <Payments__c> cheques = [Select id from Payments__c where Paid_Date__c>=:System.Today().addDays(-5) and Paid_Date__c<:System.Today().addDays(-4) and Payment_Status__c='Paid' and Payment_Method__c='cheque en israel' and Payment_Amount__c!=null];
		System.debug(cheques);
		for(Payments__c cheq:cheques){
			PaymentHandler.GreenInvoiceHandler(cheq.id,false);
		}*/
	}
	public void tes(){
		integer i=1;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
	}
}
public class HebrewPDF {
    public Opportunity opp{get;set;}
    public Date middleDat{get;set;}
    public Decimal TotalPrice{get;set;}
    public String Rama{get;set;}
    public Integer percent{get;set;}
    private String init;
    public String stud{get;set;}
    public Boolean Aplus{get;set;}
    public String AplusText{get;set;}
    public String CountryHeb{get;set;}
    public String DaysOfStudy{get;set;}
    public Integer Hours{get;set;}
    public List <Cours__c> courses{get;set;}
    public String NameInHebrew {get;set;}
    public String AddressShovar {get;set;}
    public HebrewPDF(ApexPages.StandardController controller) {
        NameInHebrew ='';
        init=ApexPages.currentPage().getParameters().get('init');
        stud=ApexPages.currentPage().getParameters().get('stud');
        opp = [select id,Accountid,Note_Exam__c,Account.Country_Hebrew__c,Account.PersonContactId,Account.Name,Account.First_Name_in_hebrew__c,Account.Last_Name_in_hebrew__c,Account.Alya_date__c,Account.Teoudat_Zeout__c,Account.Ole_Number__c,(select Name From Attachments),(select Course__c,More_Than_80__c from Enrollments__r limit 1),(select TotalPrice from OpportunityLineItems) from Opportunity where id=:controller.getRecord().id];
        Aplus=false;
        courses = [select Name,Rama_Level__c,Level_Hebrew__c,Class__c,Days_Course__c,Time_course__c,Start_time_course__c,End_time_course__c,Registered1__c,Places__c,Start_of_course__c,End_Of_Course__c,Midldle__c,Ville__c from Cours__c where id=:opp.Enrollments__r[0].Course__c limit 1];
        if(opp.Account.First_Name_in_hebrew__c!=null)
            NameInHebrew += opp.Account.First_Name_in_hebrew__c;
        if(opp.Account.Last_Name_in_hebrew__c!=null)
            NameInHebrew +=' '+ opp.Account.Last_Name_in_hebrew__c;
        CountryHeb=opp.Account.Country_Hebrew__c!=null?opp.Account.Country_Hebrew__c.reverse():null;
        NameInHebrew = NameInHebrew.reverse();
        if(courses.size()==1){
            if(courses[0].Midldle__c!=null){
                middleDat=courses[0].Midldle__c;
                middleDat=middleDat.addDays(3);
            }
            if(courses[0].Days_Course__c!=null)
                DaysOfStudy=courses[0].Days_Course__c.replaceAll(';',',').reverse();
            if(courses[0].Ville__c!=null){
                AddressShovar='';
                if(courses[0].Class__c!=null){
                    List <Branches__c> brs = [select Name,Address__c from Branches__c];
                    for(Branches__c br:brs)
                        if(courses[0].Class__c.contains(br.Name)&&br.Address__c!=null){
                            AddressShovar=br.Address__c;
                            Break;
                        }
                    if(AddressShovar=='')
                        AddressShovar=courses[0].Ville__c;
                }

            }
            String let=courses[0].Rama_Level__c!=null?courses[0].Rama_Level__c.left(1):'';
            if(let>'A'||courses[0].Rama_Level__c=='Aleph+'||courses[0].Rama_Level__c=='Aleph++'){
                Aplus=true;
                AplusText=courses[0].Level_Hebrew__c!=null?courses[0].Level_Hebrew__c:null;
            }
            if(let>='D'||courses[0].Places__c<=4)
                Rama='מתקדם';
            else
                Rama='מתחילים';
            Rama=Rama.reverse();
            Hours=0;
            if(courses[0].Places__c<=4)
                Hours=150;
            else if(courses[0].Places__c<=6)
                Hours=260;
            else
                Hours=320;
            Decimal cls=0;
            Decimal ats=0;
            for(Course_Class__c clas:[select Name,(select name from Attendance_Records__r where Student__c=:opp.Account.PersonContactId) from Course_Class__c where Course__c=:courses[0].id and Attendance_completed__c=true]){
                System.debug(clas);
                cls++;
                if(clas.Attendance_Records__r.size()>0)
                    ats++;
            }
            System.debug(ats);
            if(cls!=0&&ats!=0)
                percent=Integer.valueof(ats/cls*100);
            else
                percent=0;
            if(opp.Enrollments__r[0].More_Than_80__c)
                percent=80;
        }
        if(opp.OpportunityLineItems.Size()>1){
            for(integer i=1;i<opp.OpportunityLineItems.Size();i++)
                if(opp.OpportunityLineItems[i-1].TotalPrice>opp.OpportunityLineItems[i].TotalPrice)
                    TotalPrice=opp.OpportunityLineItems[i-1].TotalPrice;
                else
                    TotalPrice=opp.OpportunityLineItems[i].TotalPrice;
        }
        else if(opp.OpportunityLineItems.Size()==1)
            TotalPrice=opp.OpportunityLineItems[0].TotalPrice;
    }
    public void Gen(){
        if(opp.id!=null&&init==null&&courses.Size()>0){
            String url=ApexPages.currentPage().getUrl();
            String pagen;
            if(url.contains('Attendance')||Test.isRunningTest()){
                pagen='Attendance';
            }
            if(url.contains('Shovar')||Test.isRunningTest()){
                pagen='Shovar';
            }
            if(url.contains('Approved')||Test.isRunningTest()){
                pagen='Approved';
            }
            if(url.contains('TeoudatGmar')||Test.isRunningTest()){
                pagen='TeoudatGmar';
            }
            if(pagen!=null){
                GenerateAttach(pagen,opp.id,Courses[0].Name,opp.Account.Name,null);
            }
        }
    }
    public static Attachment GenerateAttach(String pagename,String oppid,String crsname,String name,String qr){
        PageReference pdf;
        if(pagename=='Attendance')
            pdf = Page.Attendance;
        if(pagename=='Shovar')
            pdf = Page.Shovar;
        if(pagename=='Approved')
            pdf = Page.Approved;
        if(pagename=='TeoudatGmar')
            pdf = Page.TeoudatGmar;
        if(pagename=='TeoudatGmar-Student'){
            pdf = Page.TeoudatGmar;
            pdf.getParameters().put('stud', '1');
        }
        Attachment attach=new Attachment();
        Boolean exst=false;
        if(qr==null){
            for(Attachment att:[select id,name from Attachment where ParentId=:oppid])
                if(att.name.contains(pagename)){
                    attach=att;
                    exst=true;
                    break;
                }
        }
        else if(qr!=''){
            attach.id=qr;
            exst=true;
        }
        pdf.getParameters().put('id', oppid);
        pdf.getParameters().put('init', '1');
        Blob body;
        if(!Test.isRunningTest())
            body = pdf.getContent();
        else
            body = blob.valueof('test'); 
        attach.Body = body;
        attach.Name = pagename+'-'+crsname+'-'+name+'.pdf';
        if(!exst)
            attach.ParentId = oppid;
        upsert attach;
        return attach;
    }
    @future(callout=true)
    public static void sendTeoudatGmar(String oppid,String stud){
        Opportunity opp = [select id,Representative__c,Account.PersonEmail,Account.Email_Custom__c,Representative__r.Email__c,Account.Name,(select Course__r.Name from Enrollments__r limit 1) from Opportunity where id=:oppid];
        if(opp.Enrollments__r.Size()>0) {
            String Pagename='TeoudatGmar';
            if(stud=='1')
                pagename='TeoudatGmar-Student';
            Attachment attach = GenerateAttach(Pagename,opp.id,opp.Enrollments__r[0].Course__r.Name,opp.Account.Name,null);
            if(stud=='0'){
                if((opp.Representative__c!=null&&opp.Representative__r.Email__c!=null)||Test.isRunningTest()){
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    String[] toAddresses = new String[] {opp.Representative__r.Email__c};
                    mail.setToAddresses(toAddresses);
                    mail.setSenderDisplayName('Salesforce');
                    mail.setSubject('Teoudat Gmar');
                    mail.setHtmlBody('');
                    List <Messaging.EmailFileAttachment> efas = new List <Messaging.EmailFileAttachment>();
                    Blob b = attach.Body;
                    Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment(); 
                    efa.setFileName(attach.name);
                    efa.setBody(b);
                    efas.add(efa);
                    mail.setFileAttachments(efas);
                    if(!Test.isRunningTest())
                        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                }
            }
            if(stud=='1'){
                String Email=opp.Account.PersonEmail!=null?opp.Account.PersonEmail:opp.Account.Email_Custom__c;
                if(Email!=null||Test.isRunningTest()){
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    String[] toAddresses = new String[] {opp.Account.PersonEmail};
                    mail.setToAddresses(toAddresses);
                    mail.setSenderDisplayName('Salesforce');
                    mail.setSubject('Teoudat Gmar');
                    mail.setHtmlBody('');
                    List <Messaging.EmailFileAttachment> efas = new List <Messaging.EmailFileAttachment>();
                    Blob b = attach.Body;
                    Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment(); 
                    efa.setFileName(attach.name);
                    efa.setBody(b);
                    efas.add(efa);
                    mail.setFileAttachments(efas);
                    if(!Test.isRunningTest())
                        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                }
            }
        }
    }
    @InvocableMethod
    public static void sendAttendances(List <String> oppids){
        List <String> oppidssplit = oppids[0].split(',');
        Set <String> setids = new Set <String>();
        Map <String,String> qrids = new Map <String,String>();
        for(String s:oppidssplit)
            if(s!=null&&s!='')
                setids.add(s);
        List <Opportunity> opps = [select id,Representative__c,Representative__r.Email__c,Account.Name,(select Course__r.Name from Enrollments__r limit 1),(select id,Name from Attachments) from Opportunity where id in:setids];
        for(Opportunity opp:opps){
            String sqr='';
            for(Attachment at:opp.Attachments){
                if(at.Name.contains('Attendance')){
                    sqr=at.id;
                    break;
                }
            }
            qrids.put(opp.id,sqr);
        }
        for(Opportunity opp:opps){
            if(opp.Enrollments__r.Size()>0) {
                PageReference pdf=Page.TeoudatGmar;
                pdf.getParameters().put('id', opp.id);
                Attachment attach = GenerateAttach('Attendance',opp.id,opp.Enrollments__r[0].Course__r.Name,opp.Account.Name,qrids.get(opp.id));
                if((opp.Representative__c!=null&&opp.Representative__r.Email__c!=null)||Test.isRunningTest()){
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    String[] toAddresses = new String[] {opp.Representative__r.Email__c};
                    mail.setToAddresses(toAddresses);
                    mail.setSenderDisplayName('Salesforce');
                    mail.setSubject('Attendance');
                    mail.setHtmlBody('');
                    List <Messaging.EmailFileAttachment> efas = new List <Messaging.EmailFileAttachment>();
                    Blob b = attach.Body;
                    Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment(); 
                    efa.setFileName(attach.name);
                    efa.setBody(b);
                    efas.add(efa);
                    mail.setFileAttachments(efas);
                    if(!Test.isRunningTest())
                        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                }
            }
        }
    }
}
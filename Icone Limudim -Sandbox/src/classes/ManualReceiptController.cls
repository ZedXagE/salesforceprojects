public class ManualReceiptController {
	private Opportunity opp;
	public Boolean err{get;set;}
	public class url{
		public String origin;
		public String he;
	}
	public class resp{
		public url url;
	}
	public ManualReceiptController(ApexPages.StandardController controller) {
		err=false;
        opp = [select id,No_VAT__c,Course__c,Account.Name,Account.Teoudat_Zeout__c,Account.PersonEmail,Account.Phone,Account.BillingStreet,Account.BillingCountry,Account.BillingCity,Account.BillingPostalCode,Amount,Account.Ville__c,(select Payment_Date__c,Payment_Method__c,Check_number__c,Payment_Amount__c,Token__c,Added_to_receipt__c from Payment__r where Payment_Status__c not in:new Set <String> {'Unpaid','Canceled','Token Open','Token Closed'} and Payment_Method__c in:new Set <String> {'Cash','cheque en israel'} and Payment_Amount__c!=null and Payment_Amount__c!=0 and Added_to_receipt__c!=true) from Opportunity where id=:controller.getRecord().id];
	}
	
	public void ManualReceipt(){
		if(opp.Payment__r.size()>0){
			List <ITmyWay_Invoice.ControllerV2.income> incomes = new List <ITmyWay_Invoice.ControllerV2.income>();
			List <ITmyWay_Invoice.ControllerV2.payment> payments = new List <ITmyWay_Invoice.ControllerV2.payment>();
			Integer vat=opp.No_VAT__c==true?1:0;
			for(Payments__c pam:opp.Payment__r){
				pam.Added_to_receipt__c=true;
				ITmyWay_Invoice.ControllerV2.payment payment = new ITmyWay_Invoice.ControllerV2.payment();
				Datetime myDate=System.now().addSeconds(Timezone.getTimeZone('Asia/Jerusalem').getOffset(System.now())/1000);
				myDate = pam.Payment_Date__c!=null?pam.Payment_Date__c:myDate;
				payment.date_c=String.valueof(myDate.year())+'-'+(myDate.month()>9?String.valueof(myDate.month()):'0'+String.valueof(myDate.month()))+'-'+(myDate.day()>9?String.valueof(myDate.day()):'0'+String.valueof(myDate.day()));
				if(pam.Payment_Method__c=='Credit Card')
					payment.type=3;
				else if(pam.Payment_Method__c=='cheque en israel'){
					payment.type=2;
					payment.chequeString=pam.Check_number__c;
				}
				else
					payment.type=1;
				payment.price=pam.Payment_Amount__c.setScale(2);
				payment.dealType=1;
				if(pam.Token__c!=null&&pam.Payment_Method__c=='Credit Card')
					payment.cardNum=Integer.valueof(pam.Token__c.right(4));
				payment.currency_c='ILS';

				ITmyWay_Invoice.ControllerV2.income income = new ITmyWay_Invoice.ControllerV2.income();
				income.quantity=1;
				income.price=pam.Payment_Amount__c.setScale(2);
				income.currency_c='ILS';
				income.vatType=vat+1;
				income.description=PaymentHandler.PaymentDesc(opp.Course__c);
				payments.add(payment);
				incomes.add(income);
			}
			System.debug(payments);
			System.debug(incomes);
			String masof = opp.Account.Ville__c!='Eilat'?'Default':'Eilat';
			HttpResponse res = ITmyWay_Invoice.ControllerV2.Create(masof,400,'he',vat,true,'ILS',null,false,opp.Account.Name+(opp.Account.Teoudat_Zeout__c!=null?(' '+opp.Account.Teoudat_Zeout__c):''),null,opp.Account.Phone,opp.Account.BillingStreet,null,opp.Account.BillingCity,opp.Account.BillingPostalCode,incomes,payments/*,opp.Amount,1,'ILS',1,'Icone Limudim'*/);
			System.debug(res.getBody());
			if(!Test.isRunningTest()&&res!=null)
				InsertReceipt(Opp.Id,res.getBody());
			update opp.Payment__r;
		}
		else{
			err=true;
		}
	}
	public static void InsertReceipt(String opid,String body){
		String dl = ((resp)JSON.deserialize(body, resp.class)).url.origin;
		insert new Receipt__c (Opportunity__c=opid,View__c='<a href="'+dl+'">Click Here</a>');
	}
	public static Receipt__c getReceipt(String opid,String body){
		String dl = ((resp)JSON.deserialize(body, resp.class)).url.origin;
		return new Receipt__c (Opportunity__c=opid,View__c='<a href="'+dl+'">Click Here</a>');
	}
}
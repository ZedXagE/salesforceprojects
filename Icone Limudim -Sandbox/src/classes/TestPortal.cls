@isTest(seeAlldata=true)
public class TestPortal {
	static testMethod void Test() {
		//Convert
		Lead ld1 = new Lead(firstname='test1',lastname='test1',email='test1@test.com');
		Lead ld2 = new Lead(firstname='test2',lastname='test2',email='test2@test.com');
		insert ld1;
		insert ld2;
		Registration_Form__c reg=new Registration_Form__c(first_name__c='test3',last_name__c='test3',email__c='test3@test.com');
		reg.PortalStatus__c=1;
		insert reg;
		Registration_Form__c reg1=new Registration_Form__c(first_name__c='test1',last_name__c='test1',email__c='test1@test.com',Lead__c=ld1.id,Opportunity__c=null);
		Registration_Form__c reg2=new Registration_Form__c(first_name__c='test2',last_name__c='test2',email__c='test2@test.com',Lead__c=ld2.id,Opportunity__c=null);
		reg1.PortalStatus__c=1;
		reg2.PortalStatus__c=1;
		insert reg1;
		insert reg2;
		reg1.PortalStatus__c=5;
		reg1.T_C__c=true;
		reg2.PortalStatus__c=5;
		reg2.T_C__c=true;
		update reg1;
		update reg2;

		//portal
		PageReference pgRef2 = Page.Portal;
		Test.setCurrentPage(pgRef2);
		ApexPages.currentPage().getParameters().put('course', 'Ulpan');
		PortalController c = New PortalController();
        c.LogCheck();
        c.Login();
		c.getLogged();
		c.reg.first_name__c='test';
		c.reg.last_name__c='test';
		c.reg.phone__c='test';
		c.reg.email__c='test@test.com';
		c.SaveBasicInfo();
		c.bday=12;
		c.bmonth=12;
		c.byear=2000;
		c.aday=12;
		c.amonth=12;
		c.ayear=2000;
		c.SaveRegInfo();
		c.SaveUnknown();
		c.reg.T_C__c=true;
		c.SaveTerms();
		c.onChangeApt();
		c.SaveApt();
		c.reg.T_C__c = !c.reg.T_C__c;
		update c.reg;
		//payments and invoices
		Opportunity opp=[select id,Accountid,SendTeoudatGmar__c from Opportunity where modality_of_payment_status__c!=null and amount!=null limit 1];
		opp.Course__c='Ulpan';
		update opp;
		c.reg.T_C__c = !c.reg.T_C__c;
		/*c.reg.Account__c=opp.Accountid;
		update c.reg;*/
		Contact acc = [select id,Accountid from Contact where Accountid!=null limit 1];
		Cours__c crs = [select id from Cours__c where places__c>0 and Start_of_course__c>:System.Today().addDays(-30) limit 1];

		PaymentHandler.GeneratePayments(300,3,opp.id,opp.Accountid,'1010','1010',300,new List <Date> {System.today(),System.today(),System.today()});
		
		Payments__c pam1 = new Payments__c(Account__c=opp.Accountid,Opportunity__c=opp.id,Payment_Status__c='Token Open',Payment_Amount__c=100,Payment_Method__c='Credit card',Payment_Date__c=System.Today(),Response__c='000',Expiration_Date__c='1010',Token__c='1010',Payment_ID__c='1010',index__c='000',Num_Payments__c=3,Total_Price__c=300,First_Credit_Payment__c=true);
		insert pam1;
		Enrollment__c enr = new Enrollment__c(Course__C=crs.id,Contact__c=acc.id,Student_Name_account__c=acc.Accountid,Opportunity__c=opp.id);
		insert enr;
		Payments__c pam = new Payments__c(Account__c=opp.Accountid,First_Credit_Payment__c=true,Opportunity__c=opp.id,Payment_Status__c='In progress',Payment_Amount__c=100,Payment_Method__c='Credit card',Payment_Date__c=System.Today(),Response__c='000',Expiration_Date__c='1010',Token__c='1010',Payment_ID__c='1010',index__c='000',Num_Payments__c=2,Total_Price__c=300);
		insert pam;
		Payments__c cheque = new Payments__c(Account__c=opp.Accountid,First_Credit_Payment__c=true,Opportunity__c=opp.id,Payment_Status__c='Paid',Payment_Amount__c=100,Payment_Method__c='cheque en israel',Payment_Date__c=System.Today().addDays(-6));
		insert cheque;
		cheque.Payment_Date__c=System.Today().AddDays(-5);
		update cheque;
		//added
		DailySchedule sch = new DailySchedule();
		sch.execute(null);

		DailyHandler dh = new DailyHandler();
		dh.init();
		dh.sendPayments();
		dh.sendCheques();
		
		DailyHandler dh2 = new DailyHandler();
		dh2.init();
		dh2.RefundTemp();


		PaymentHandler pm= new PaymentHandler();
		pm.tes();
		PaymentHandler.nearestEleven(null);
		pm.execute(null);
		PaymentHandler.GreenInvoiceHandler(pam.id,false);
		pam.Payment_Status__c='Paid';
		update pam;
		Payments__c pam3 = new Payments__c(Account__c=opp.Accountid,Opportunity__c=opp.id,Payment_Status__c='Token Open',Payment_Amount__c=100,Payment_Method__c='Credit card',Payment_Date__c=System.Today(),Response__c='000',Expiration_Date__c='1010',Token__c='1010',Payment_ID__c='1010',index__c='000',Num_Payments__c=3,Total_Price__c=300,First_Credit_Payment__c=true);
		insert pam3;
		PaymentHandler.GreenInvoiceHandler(pam.id,true);
		pm.sfid=opp.id;
		pm.accid=opp.Accountid;
		pm.sum='100';
		pm.CheckChange();
		pm.ChangeCard();
		pm.FailedPayment();
		pm.SuccessPayment();
		//pm.ChangePayments();
		pm.Portal();
		pm.sfid=pam.id;
		pm.Refund();

		insert new Payments__c(Opportunity__c=opp.id,Account__c=opp.Accountid,Payment_Amount__c=10,Payment_Method__c='cheque en israel',Payment_Status__c='Paid');
		ApexPages.StandardController sc = new ApexPages.StandardController(opp);
		PageReference pgRef3 = Page.ManualReceipt;
		Test.setCurrentPage(pgRef3);
		ApexPages.currentPage().getParameters().put('id', opp.id);
		ManualReceiptController m = New ManualReceiptController(sc);
        m.ManualReceipt();

		//no vat
		Opportunity opp2=[select id,Accountid,SendTeoudatGmar__c from Opportunity where modality_of_payment_status__c!=null and amount!=null and No_Vat__c=true limit 1];
		PriceBookEntry pbe = [select id,UnitPrice from PriceBookEntry limit 1];
		insert new OpportunityLineItem(UnitPrice=pbe.UnitPrice, Quantity=1, OpportunityId=opp2.id,PriceBookEntryId=pbe.id);

		Appointment__c apt = [select id,Representative__c,Opportunity__c from Appointment__c Where Representative__c!=null and Opportunity__c=null limit 1];
		apt.Opportunity__c=opp2.id;
		update apt;

		Registration_Form__c renew = [select id,PortalStatus__c from Registration_Form__c where Account__c!=null and Opportunity__c=null and PortalStatus__c<4 limit 1];
		renew.PortalStatus__c = 7;
		update renew;
	}
}
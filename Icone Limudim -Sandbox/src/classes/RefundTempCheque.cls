public class RefundTempCheque implements Queueable, Database.AllowsCallouts{
	private final Integer limitcallouts=50;
	private List <Payments__c> cheques;
	private List <Payments__c> temp;
	private DailyDebug__c debug;
	private string step;
	public RefundTempCheque(List <Payments__c> cheqs, DailyDebug__c dbg) {
		if(dbg==null){
			debug = new DailyDebug__c(status__c='success');
		}
		else
			debug = dbg;
		if(debug.message__c==null)
			debug.message__c = '';
		step = '3';
		cheques = cheqs;
		temp = new List <Payments__c>();
		if(cheques.size()>0){
			step = '2';
			for(integer i=0;cheques.size()!=0&&i<limitcallouts;i++){
				temp.add(cheques[0]);
				cheques.remove(0);
			}
		}
	}
	private String getParameter(String body,String key){
		if(body!=null&&key!=null){
			String [] spl1 = body.split(key+'=');
			if(spl1.size()>1){
				String [] spl2 = spl1[1].split('&');
				if(spl2.size()>0)
					return spl2[0];
				else
					return null;
			}
			else
				return null;
		}
		else
			return null;
	}
	public Void execute(QueueableContext context){
		try{
			if(step=='2'){
				debug.message__c += 'RefundInvoices:';
				List <Receipt__c> recpts = new List <Receipt__c>();
				for(Payments__c pam:temp){
					debug.message__c += pam.id;
					Datetime myDate=System.now().addSeconds(Timezone.getTimeZone('Asia/Jerusalem').getOffset(System.now())/1000);
					String type='refund';
					Decimal amount=pam.Payment_Amount__c;
					integer typnum=2;
					Integer vat=pam.Opportunity__r.No_VAT__c==true?1:0;
					String masof = pam.Opportunity__r.Account.Ville__c!='Eilat'?'Default':'Eilat';
					HttpResponse res = ITmyWay_Invoice.ControllerV2.CreateOneGlobalDate(masof,type,'he',vat,vat+1,'ILS',pam.Opportunity__r.Account.Name+(pam.Opportunity__r.Account.Teoudat_Zeout__c!=null?(' '+pam.Opportunity__r.Account.Teoudat_Zeout__c):''),'edantest@itmyway.com',pam.Opportunity__r.Account.Phone,pam.Opportunity__r.Account.BillingStreet,null/*pam.Opportunity__r.Account.BillingCountry*/,pam.Opportunity__r.Account.BillingCity,pam.Opportunity__r.Account.BillingPostalCode,amount,null,'ILS',1,PaymentHandler.PaymentDesc(pam.Opportunity__r.Course__c),typnum,pam.Check_number__c,myDate);
					System.debug(pam.id+' : '+res);
					if(!Test.isRunningTest()&&res!=null){
						recpts.add(ManualReceiptController.getReceipt(pam.Opportunity__c,res.getBody()));
					}
					else{
						debug.message__c += '=Error';
					}
					debug.message__c += ';';
				}
				insert recpts;
			}
			if(step=='2'){
				System.enqueueJob(new RefundTempCheque(cheques,debug));
			}
			else{
				insert debug;
			}
		}
		catch(exception e){
			debug.status__c = 'failed';
			debug.message__c = e.getMessage();
			insert debug;
			System.debug(debug);
			try{
				Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
				message.toAddresses = new String[] { 'edan@itmyway.com', 'yotam@itmyway.com' };
				message.subject = 'Refund Temp Failed';
				message.plainTextBody = e.getMessage();
				Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
				Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
			}
			catch(exception e2){}
		}
	}
}
public class AptsGenerate {
	public flds flds {get;set;}
	public String error {get;set;}
	public class flds{
		public String repId {get;set;}
		public String city {get;set;}
		//public String startTime {get;set;}
		//public String endTime {get;set;}
		//public List <String> dayInWeek {get;set;}
		public String interval {get;set;}
		public Date startDate {get;set;}
		public Date endDate {get;set;}
		public List <selectoption> repIds {get;set;}
		public Map <String,Representative__c> reps;
		public List <selectoption> cities {get;set;}
		//public List <selectoption> startTimes {get;set;}
		//public List <selectoption> endTimes {get;set;}
		public List <selectoption> intervals {get;set;}
		public List <String> TimesIndex;
		public List <String> days;
		//public List <selectoption> daysInWeek {get;set;}
		public flds(){
			repIds = new List <selectoption>();
			reps = new Map <String,Representative__c>();
			cities = new List <selectoption>();
			//startTimes = new List <selectoption>();
			//endTimes = new List <selectoption>();
			//daysInWeek = new List <selectoption>();
			intervals = new List <selectoption>();
			TimesIndex = new List <String>();
			days = new List <String>();
			//dayInWeek = new List <String>();

			repIds.add(new selectoption('','select'));
			for(Representative__c rep:[select id,Name,Sunday_Start_Time__c,Monday_Start_Time__c,Tuesday_Start_Time__c,Wednesday_Start_Time__c,Thursday_Start_Time__c,Sunday_End_Time__c,Monday_End_Time__c,Tuesday_End_Time__c,Wednesday_End_Time__c,Thursday_End_Time__c from Representative__c]){
                repIds.add(new selectoption(rep.id,rep.Name));
				reps.put(rep.id,rep);
			}

			cities.add(new selectoption('','select'));
			Schema.DescribeFieldResult citiesResult = Appointment__c.City__c.getDescribe();
            List <Schema.PicklistEntry> ct = citiesResult.getPicklistValues();
            for( Schema.PicklistEntry pickListVal : ct)
                cities.add(new selectoption(pickListVal.getLabel(),pickListVal.getLabel()));

			//startTimes.add(new selectoption('','select'));
			//endTimes.add(new selectoption('','select'));
			Schema.DescribeFieldResult timesResult = Appointment__c.Time__c.getDescribe();
            List <Schema.PicklistEntry> tm = timesResult.getPicklistValues();
            for( Schema.PicklistEntry pickListVal : tm) {
                //startTimes.add(new selectoption(pickListVal.getLabel(),pickListVal.getLabel()));
				//endTimes.add(new selectoption(pickListVal.getLabel(),pickListVal.getLabel()));
				TimesIndex.add(pickListVal.getLabel());
			}

			/*daysInWeek.add(new selectoption('Sunday','Sunday'));
			daysInWeek.add(new selectoption('Monday','Monday'));
			daysInWeek.add(new selectoption('Tuesday','Tuesday'));
			daysInWeek.add(new selectoption('Wednesday','Wednesday'));
			daysInWeek.add(new selectoption('Thursday','Thursday'));
			daysInWeek.add(new selectoption('Friday','Friday'));*/
			//daysInWeek.add(new selectoption('Saturday','Saturday'));

			intervals.add(new selectoption('','select'));
			intervals.add(new selectoption('1','30 Minutes'));
			intervals.add(new selectoption('2','60 Minutes'));

			days.add('Sunday');
			days.add('Monday');
			days.add('Tuesday');
			days.add('Wednesday');
			days.add('Thursday');
		}
	}
	public AptsGenerate() {
		flds = new flds();
	}
	public void checkGenerate(){
		error = null;
		if(flds.repId!=null&&flds.repId!=''){
			if(flds.startDate!=null&&flds.endDate!=null&&flds.startDate<=flds.endDate){
				//if(flds.startTime!=null&&flds.startTime!=''&&flds.endTime!=null&&flds.endTime!=''&&flds.TimesIndex.indexOf(flds.StartTime)<=flds.TimesIndex.indexOf(flds.EndTime)){
					if(flds.interval!=null&&flds.interval!=''){
						//if(flds.dayInWeek.size()>0){
							if(flds.city!=null&&flds.city!=''){
								Generate();
							}
							else error = 'Please Fill City';
						/*}
						else error = 'Please Mark at least one Day in Week';*/
					}
					else error = 'Please Fill Interval';
				/*}
				else error = 'Please Fill Start Time and End Time';*/
			}
			else error = 'Please Fill Dates or End Date is Before Start Date';
		}
		else error = 'Please Fill Representative';
	}
	public void Generate(){
		Set <String> exsts = new Set <String>();
		Set <String> holidays = new Set <String>();
		for(Appointment__c apt:[select Date__c,Time__c from Appointment__c where Representative__c=:flds.repId and City__c=:flds.city and Date__c>=:flds.StartDate and Date__c<=:flds.EndDate and Date__c!=null and Time__c!=null])
			exsts.add(apt.Date__c.format()+apt.Time__c);
		for(Holidays__c hol:[select Date__c from Holidays__c where Date__c>=:flds.StartDate and Date__c<=:flds.EndDate and Date__c!=null and Erev_Hag__c=false])
			holidays.add(hol.Date__c.format());
		List <Appointment__c> apts4insert = new List <Appointment__c>();
		Date tempDate = flds.StartDate;
		while(tempDate<=flds.EndDate){
			String currentDay = getDayInWeek(tempDate);
			if(flds.days.contains(currentDay)&&!holidays.contains(tempDate.format())){
				String tempTime = (String) flds.reps.get(flds.repId).get(currentDay+'_Start_Time__c');
				String tempendTime = (String) flds.reps.get(flds.repId).get(currentDay+'_End_Time__c');
				if(tempTime!=null&&tempendTime!=null){
					while(flds.TimesIndex.indexOf(tempTime)<=flds.TimesIndex.indexOf(tempendTime)){
						if(!exsts.contains(tempDate.format()+tempTime)){
							apts4insert.add(
									new Appointment__c(
										Representative__c=flds.repId,
										City__c=flds.city,
										Date__c=tempDate,
										Time__c=tempTime
									)
							);
						}
						if(flds.TimesIndex.indexOf(tempTime)==flds.TimesIndex.size()-1) break;
						else tempTime = flds.TimesIndex[flds.TimesIndex.indexOf(tempTime)+integer.valueof(flds.interval)];
					}
				}
			}
			tempDate = tempDate.addDays(1);
		}
		System.debug('apts size: '+apts4insert.size());
		System.debug('apts: '+apts4insert);
		insert apts4insert;
		error = 'success';
	}
	//get day in week 'Monday'
	public static String getDayInWeek(Date d){
		Datetime dt = DateTime.newInstance(d.year(), d.month(), d.day());
		return dt.format('EEEE');
	}
}
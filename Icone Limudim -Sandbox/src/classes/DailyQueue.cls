public class DailyQueue implements Queueable, Database.AllowsCallouts{
	private final Integer limitcallouts=20;
	private List <Payments__c> payments;
	private List <Payments__c> cheques;
	private List <Payments__c> temp;
	private DailyDebug__c debug;
	private string step;
	public DailyQueue(List<Payments__c> pams,List <Payments__c> cheqs, DailyDebug__c dbg) {
		if(dbg==null){
			debug = new DailyDebug__c(status__c='success');
		}
		else
			debug = dbg;
		if(debug.message__c==null)
			debug.message__c = '';
		step = '3';
		payments = pams;
		cheques = cheqs;
		temp = new List <Payments__c>();
		if(payments.size()>0){
			step = '1';
			for(integer i=0;payments.size()!=0&&i<limitcallouts;i++){
				temp.add(payments[0]);
				payments.remove(0);
			}
		}
		else if(cheques.size()>0){
			step = '2';
			for(integer i=0;cheques.size()!=0&&i<limitcallouts;i++){
				temp.add(cheques[0]);
				cheques.remove(0);
			}
		}
	}
	private String getParameter(String body,String key){
		if(body!=null&&key!=null){
			String [] spl1 = body.split(key+'=');
			if(spl1.size()>1){
				String [] spl2 = spl1[1].split('&');
				if(spl2.size()>0)
					return spl2[0];
				else
					return null;
			}
			else
				return null;
		}
		else
			return null;
	}
	public Void execute(QueueableContext context){
		try{
			if(step=='1'){
				debug.message__c += 'Payments:';
				HttpResponse response = new HttpResponse();
				for(Payments__c pay:temp){
					debug.message__c += pay.id;
					String vat=pay.Opportunity__r.No_VAT__c==true?'0':'0.17';
					try{
						response = ITmyWayTranzila.ControllerV2.Charge(pay.Expiration_Date__c,pay.Token__c,'1','1','A',vat,String.valueof(pay.Payment_Amount__c),'iconeltdtok',null);
						System.debug(pay.id+' : '+response);
						if(response!=null||Test.isRunningTest()){
							String body=response!=null?response.getBody():'000';
							String res = getParameter(body,'Response');
							String conf = getParameter(body,'ConfirmationCode');
							String ind = getParameter(body,'index');
							pay.Response__c = res;
							if(res=='000'){
								pay.Payment_Status__c='Paid';
								pay.Payment_ID__c=conf;
								pay.index__c=ind;
							}
							else{
								pay.Payment_Status__c='Unpaid';
							}
						}
						else{
							pay.Unknown_Error__c=true;
							debug.message__c += '=Error';
						}
						debug.message__c += ';';
						pay.Queue_to_Tranzila__c=true;
					}
					catch(exception e2){
						pay.Queue_to_Tranzila__c=true;
						pay.Unknown_Error__c=true;
						debug.message__c += '=Error';
					}
				}
			}
			if(step=='2'||Test.isRunningTest()){
				debug.message__c += 'Invoices:';
				List <Receipt__c> recpts = new List <Receipt__c>();
				for(Payments__c pam:temp){
					debug.message__c += pam.id;
					try{
						Datetime myDate=System.now().addSeconds(Timezone.getTimeZone('Asia/Jerusalem').getOffset(System.now())/1000);
						String type='mas';
						Decimal amount=pam.Payment_Amount__c;
						integer typnum=2;
						Integer vat=pam.Opportunity__r.No_VAT__c==true?1:0;
						String masof = pam.Opportunity__r.Account.Ville__c!='Eilat'?'Default':'Eilat';
						HttpResponse res = ITmyWay_Invoice.ControllerV2.CreateOneGlobalDate(masof,type,'he',vat,vat+1,'ILS',pam.Opportunity__r.Account.Name+(pam.Opportunity__r.Account.Teoudat_Zeout__c!=null?(' '+pam.Opportunity__r.Account.Teoudat_Zeout__c):''),pam.Opportunity__r.Account.PersonEmail,pam.Opportunity__r.Account.Phone,pam.Opportunity__r.Account.BillingStreet,null/*pam.Opportunity__r.Account.BillingCountry*/,pam.Opportunity__r.Account.BillingCity,pam.Opportunity__r.Account.BillingPostalCode,amount,null,'ILS',1,PaymentHandler.PaymentDesc(pam.Opportunity__r.Course__c),typnum,pam.Check_number__c,myDate);
						System.debug(pam.id+' : '+res);
						if(!Test.isRunningTest()&&res!=null){
							pam.ChequeInvoiced__c=true;
							recpts.add(ManualReceiptController.getReceipt(pam.Opportunity__c,res.getBody()));
						}
						else{
							debug.message__c += '=Error';
						}
						debug.message__c += ';';
						pam.Queue_to_GreenInvoice__c=true;
					}
					catch(exception e2){
						pam.Queue_to_GreenInvoice__c=true;
						pam.Unknown_Error__c=true;
						debug.message__c += '=Error';
					}
				}
				insert recpts;
			}
			if(step=='1'||step=='2'){
				update temp;
				System.enqueueJob(new DailyQueue(payments,cheques,debug));
			}
			else{
				insert debug;
			}
		}
		catch(exception e){
			debug.status__c = 'failed';
			debug.message__c = e.getMessage();
			insert debug;
			System.debug(debug);
			try{
				Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
				message.toAddresses = new String[] { 'edan@itmyway.com', 'yotam@itmyway.com' };
				message.subject = 'Icone Daily Failed';
				message.plainTextBody = e.getMessage();
				Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
				Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
			}
			catch(exception e2){}
			for(Payments__c pam:temp){
				pam.Unknown_Error__c=true;
				if(step=='1')
					pam.Queue_to_Tranzila__c=true;
				else if(step=='2')
					pam.Queue_to_GreenInvoice__c=true;
			}
			update temp;
		}
	}
}
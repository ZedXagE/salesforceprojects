@isTest(seeAlldata=true)
public class AptsGenerateTest {
	static testMethod void testGenerate() {
		AptsGenerate ag = new AptsGenerate();
		ag.checkGenerate();
		ag.flds.repId = ag.flds.repIds[1].getValue();
		ag.checkGenerate();
		ag.flds.startDate = System.today();
		ag.flds.endDate = System.today().addDays(5);
		ag.checkGenerate();
		/*ag.flds.startTime = ag.flds.startTimes[1].getValue();
		ag.flds.endTime = ag.flds.endTimes[ag.flds.endTimes.size()-1].getValue();
		ag.checkGenerate();*/
		ag.flds.interval = ag.flds.intervals[1].getValue();
		ag.checkGenerate();
		/*for(selectoption op:ag.flds.daysInWeek)
			ag.flds.dayInWeek.add(op.getValue());
		ag.checkGenerate();*/
		ag.flds.city = ag.flds.cities[1].getValue();
		ag.checkGenerate();
	}
}
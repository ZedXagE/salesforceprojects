trigger CourseStartUpdate on Cours__c (after update) {
    Map <id, Date> upcrs = new  Map <id, Date>();
    for(Cours__c crs:Trigger.New)
        if(crs.Start_Of_Course__c!=Trigger.OldMap.get(crs.id).Start_Of_Course__c)
            upcrs.put(crs.id,crs.Start_Of_Course__c);
    List <Enrollment__c> enrs = [select Opportunity__c,Course__c from Enrollment__c where Course__c in: upcrs.KeySet() and Course__c!=null and Opportunity__c!=null];
    List <Opportunity> opps4up = new List <Opportunity>();
    for(Enrollment__c enr:enrs)
        opps4up.add(new Opportunity(id=enr.Opportunity__c,Start_Of_Course__c=upcrs.get(enr.Course__c)));
    update opps4up;
}
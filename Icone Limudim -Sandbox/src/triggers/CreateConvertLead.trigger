trigger CreateConvertLead on Registration_Form__c (Before Insert,Before Update,Before Delete) {
    if(Trigger.IsUpdate)
        for(Registration_Form__c reg:Trigger.new)
            if(reg.PortalStatus__c>=4&&Trigger.oldMap.get(reg.id).PortalStatus__c<4){
                if(reg.Account__c!=null&&reg.Opportunity__c==null){
                    Account acc = [select id,Name from Account where id=:reg.Account__c];
                    Opportunity opp = new Opportunity();
                    opp.Name=acc.Name;
                    opp.StageName='Lead';
                    opp.CloseDate=System.Today().AddMonths(1);
                    opp.AccountId=acc.id;
                    for(RecordType rt:[select id,Name from RecordType where SobjectType='Registration_Form__c'])
                        if(reg.RecordTypeId==rt.id){
                            opp.Course__c=rt.name;
                            Break;
                        }
                    opp.Second_Parameter__c=reg.Second_Parameter__c;
                    try{
                        insert opp;
                        reg.Opportunity__c=opp.id;
                    }
                    catch(exception e){}
                }
                else if(reg.Lead__c!=null){
                    Lead myLead = [select id from Lead where id=:reg.Lead__c];
                    List <Registration_Form__c> regs4up = [select id,Lead__c,Account__c from Registration_Form__c where Lead__c=:myLead.id and id!=:reg.id];
                    Database.LeadConvert lc = new database.LeadConvert();
                    lc.setLeadId(myLead.id);
                    List <Contact> cons=[select id,Accountid from contact where Email=:reg.Email__c limit 1];
                    if(cons.size()>0)
                        lc.setAccountId(cons[0].Accountid);
                    LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
                    lc.setConvertedStatus(convertStatus.MasterLabel);
                    Database.LeadConvertResult lcr = Database.convertLead(lc);
                    reg.Opportunity__c=lcr.getOpportunityId();
                    reg.Account__c=lcr.getAccountId();
                    reg.Lead__c=null;
                    for(Registration_Form__c re:regs4up){
                        re.Account__c=lcr.getAccountId();
                        re.Lead__c=null;
                    }
                    update regs4up;
                    Opportunity opp = [select id,Course__c,Second_Parameter__c from Opportunity where id=:lcr.getOpportunityId()];
                    for(RecordType rt:[select id,Name from RecordType where SobjectType='Registration_Form__c'])
                        if(reg.RecordTypeId==rt.id){
                            opp.Course__c=rt.name;
                            Break;
                        }
                    opp.Second_Parameter__c = reg.Second_Parameter__c;
                    update opp;
                }
            }
    /*if(Trigger.IsDelete)
        for(Registration_Form__c reg:Trigger.old)
            if(reg.Lead__c!=null)
                delete new Lead(id=reg.Lead__c);*/
    if(Trigger.IsInsert){
        /*Map<String,String> recTypes=new Map<String,String>();
        for(RecordType rt:[select id,Name from RecordType where SobjectType='Registration_Form__c'])
        recTypes.put(rt.id,rt.name);*/
        for(Registration_Form__c reg:Trigger.new){
            List <Account> acs = [select id from Account where /*Course__c=:recTypes.get(reg.RecordTypeId) and Second_Parameter__c=:reg.Second_Parameter__c and*/ PersonEmail=:reg.Email__c OR Email_custom__c=:reg.Email__c];
            if(acs.Size()>0){
                reg.Account__c=acs[0].id;
            }
            else{
                List <Lead> lds = [select id from Lead where /*Course__c=:recTypes.get(reg.RecordTypeId) and Second_Parameter__c=:reg.Second_Parameter__c and*/ Email=:reg.Email__c and IsConverted=false];
                if(lds.Size()>0){
                    reg.Lead__c=lds[0].id;
                }
                else{
                    Lead ld = new Lead(LastName=reg.Last_name__c,FirstName=reg.First_name__c,Email=reg.Email__c,LeadSource='Application Online'/*,Course__c=recTypes.get(reg.RecordTypeId),Second_Parameter__c=reg.Second_Parameter__c*/);
                    try{
                        insert ld;
                        reg.Lead__c=ld.id;
                    }
                    catch(exception e){}
                }
            }
        }
    }
}
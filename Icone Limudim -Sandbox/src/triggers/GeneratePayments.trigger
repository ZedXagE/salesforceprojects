trigger GeneratePayments on Enrollment__c (after insert) {
    Set <id> enrids = new Set <id>();
    for(Enrollment__c enr:Trigger.New)
        if(enr.Opportunity__c!=null){
            PaymentHandler.CheckCreatePayments(enr.Opportunity__c);
            if(enr.Course__c!=null)
                enrids.add(enr.id);
        }
    List <Enrollment__c> enrs = [select Opportunity__c,Opportunity__r.Start_Of_Course__c,Course__r.Start_Of_Course__c from Enrollment__c where id in: enrids];
    List <Opportunity> opps = new List <Opportunity>();
    for(Enrollment__c enr:enrs)
        opps.add(new Opportunity(id=enr.Opportunity__c,Start_Of_Course__c=enr.Course__r.Start_Of_Course__c));
    update opps;
}
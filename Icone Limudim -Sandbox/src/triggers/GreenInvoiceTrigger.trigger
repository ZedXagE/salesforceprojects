trigger GreenInvoiceTrigger on Payments__c (before insert,before update,after insert,after update) {
    if(Trigger.isBefore){
        for(Payments__c pam:Trigger.New){
            if(Trigger.isUpdate){
                if(Trigger.OldMap.get(pam.id).Payment_Status__c!='Paid'&&pam.Payment_Status__c=='Paid'){
                    pam.Paid_Date__c = System.Now();
                }
            }
            if(Trigger.isInsert){
                if(pam.Payment_Status__c=='Paid'){
                    pam.Paid_Date__c = System.Now();
                }
            }
        }
    }
    else if(Trigger.isAfter){
        for(Payments__c pam:Trigger.New){
            Boolean send=false;
            Boolean up=false;
            if(pam.Payment_Method__c=='Credit Card'||pam.Payment_Method__c=='Cash'){
                if(Trigger.isInsert){
                    send=true;
                }
                if(Trigger.isUpdate){
                    up=true;
                    Payments__c oldpam=Trigger.OldMap.get(pam.id);
                    if(pam.Payment_Method__c=='Credit Card'&&oldpam.Payment_Status__c=='In progress'&&pam.Payment_Status__c=='Paid')
                        send=true;
                    else if(pam.Payment_Method__c=='Cash'&&oldpam.Payment_Status__c!='Paid')
                        send=true;
                }
                if(send && ((pam.Payment_Method__c=='Credit Card'&&(pam.Payment_Status__c=='Paid'||pam.Payment_Status__c=='Refund'/*||pam.Payment_Status__c=='In progress'*/)) || (pam.Payment_Method__c=='Cash'&&pam.Payment_Status__c=='Paid')) ){
                    PaymentHandler.GreenInvoiceHandler(pam.id,up);
                }
            }
        }
    }
}
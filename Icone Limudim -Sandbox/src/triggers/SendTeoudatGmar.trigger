trigger SendTeoudatGmar on Opportunity (after Update) {
    for(Opportunity opp:Trigger.new){
        Opportunity oldopp = Trigger.OldMap.get(opp.id);
        if((!oldopp.SendTeoudatGmar__c&&opp.SendTeoudatGmar__c)/*||Test.IsRunningTest()*/) HebrewPDF.sendTeoudatGmar(opp.id,'0');
        if((oldopp.Note_Exam__c==null&&opp.Note_Exam__c!=null)/*||Test.IsRunningTest()*/) HebrewPDF.sendTeoudatGmar(opp.id,'1');
    }
}
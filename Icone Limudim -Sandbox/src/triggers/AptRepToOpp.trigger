trigger AptRepToOpp on Appointment__c (After Insert,After Update) {
    List <opportunity> opps = new List <opportunity>();
    for(Appointment__c apt:Trigger.New){
        if(Trigger.IsUpdate){
            if(apt.Representative__c!=null&&apt.opportunity__c!=null&&Trigger.OldMap.get(apt.id).opportunity__c==null)
                opps.add(new opportunity(id=apt.opportunity__c,Representative__c=apt.Representative__c));
        }
        if(Trigger.IsInsert){
            if(apt.Representative__c!=null&&apt.opportunity__c!=null)
                opps.add(new opportunity(id=apt.opportunity__c,Representative__c=apt.Representative__c));
        }
    }
    update opps;
}
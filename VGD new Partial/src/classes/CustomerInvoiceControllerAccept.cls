public class CustomerInvoiceControllerAccept {
	public String signatureData {set; get;}
		public String signaturepic {set; get;}
        public String signatureid {set; get;}
        public String dat {set; get;}
        public String pagename {set; get;}
        public Boolean balance {set; get;}
        public Boolean commercial {set; get;}
		public String datcomplete {set; get;}
		public id Curid{get;set;}
        public String Retid{get;set;}
		public Catalog_Order__c co{get;set;}
		public Inspection_Form__c inf{get;set;}
        public String infb {get;set;}
        public Document doc{get;set;}
		public string disc{get;set;}
    public CustomerInvoiceControllerAccept(ApexPages.StandardController controller)
    {
        balance=false;
        commercial=false;
        infb='0';
        pagename=ApexPages.currentPage().getParameters().get('page');
		disc='display: none;';
        dat='';
        signatureid=ApexPages.currentPage().getParameters().get('sigpic');
        signaturepic='';
        if(signatureid!=null)
        {
            doc=[select id,body from document where id=:signatureid];
            signaturepic='/servlet/servlet.FileDownload?file='+signatureid;
        }
        Curid=ApexPages.currentPage().getParameters().get('id');
        Retid=ApexPages.currentPage().getParameters().get('retid');
        co=[select id,Signature__c,name,Service_Lookup__c,Total_Discount__c,Max_Discount__c,Service_Lookup__r.CKSW_BASE__Account__r.isPersonAccount,Service_Lookup__r.CKSW_BASE__Finish__c,Service_Lookup__r.CKSW_BASE__Appointment_Start__c,Service_Lookup__r.CKSW_BASE__Appointment_Finish__c,Service_Lookup__r.Balance__c,(select Product_Family__c from Catalog_Line_Items__r) from Catalog_Order__c where id=:Curid];
        if(co.Service_Lookup__r.CKSW_BASE__Appointment_Start__c!=null)
            dat=co.Service_Lookup__r.CKSW_BASE__Appointment_Start__c.format('MM/dd/yyyy hh:mm a', 'PST');
        if(co.Service_Lookup__r.CKSW_BASE__Appointment_Finish__c!=null&&co.Service_Lookup__r.CKSW_BASE__Appointment_Start__c!=null)
            dat+=' - '+co.Service_Lookup__r.CKSW_BASE__Appointment_Finish__c.format('hh:mm a', 'PST');
        if(co.Service_Lookup__r.CKSW_BASE__Finish__c!=null)
            datcomplete=co.Service_Lookup__r.CKSW_BASE__Finish__c.format('MM/dd/yyyy hh:mm a', 'PST');
        if(co.Max_Discount__c!=null&&co.Max_Discount__c!=0)
            disc='';
        list <Inspection_Form__c> infl = [select id,Name,Door_Size__c,Opener_Manufacturer__c,	Service__r.name,	Door_balance__c,  	Horizontal_tracks__c,	Door_balance_Comments__c,	Horizontal_tracks_Comments__c,	Rollers__c	,Sections_Int_Ext__c	,Rollers_Comments__c	,Sections_Int_Ext_Comments__c	,Cables__c	,Weather_seal__c	,Cables_Comments__c	,Weather_seal_Comments__c	,Drums__c	,Strut__c	,Drums_Comments__c	,Strut_Comments__c	,Center_bearing__c	,Jamb_brackets__c	,Center_bearing_Comments__c	,Jamb_brackets_Comments__c	,Center_bracket__c	,Opening_Closing_force__c	,Center_bracket_Comments__c	,Opening_Closing_force_Comments__c	,End_bearing_plates__c	,Limits_Comments__c	,End_bearing_plates_Comments__c	,Wires__c	,Spring_bar__c	,Wires_Comments__c	,Spring_bar_Comments__c	,Motor_gear__c	,Hinges__c	,Motor_gear_Comments__c	,Hinges_Comments__c	,Safety_eyes__c	,Top_brackets__c	,Safety_eyes_Comments__c	,Top_brackets_Comments__c	,Trolley_Carriage__c	,Bottom_brackets__c	,Trolley_Carriage_Comments__c	,Bottom_brackets_Comments__c	,Controls__c	,Vertical_tracks__c ,Controls_Comments__c	,Vertical_tracks_Comments__c	from Inspection_Form__c where Service__c=:co.Service_Lookup__c order by CreatedDate Desc limit 1];
        if(infl.size()>0)
        {
            inf=infl[0];
            infb='1';
        }
        if(co.Service_Lookup__r.Balance__c==0)
            balance=true;
        if(co.Service_Lookup__r.CKSW_BASE__Account__r.isPersonAccount==false)
            commercial=true;
        else
            for(Catalog_Line_Item__c cl: co.Catalog_Line_Items__r)  if(cl.Product_Family__c=='COMMERCIAL')  commercial=true;
    }
    public pagereference GeneratePDF(){
        PageReference Pg=Page.CustomerInvoice1;
        if(pagename=='r'&&infb=='1')
            Pg=Page.CustomerInvoice1recinf;
        if(pagename=='r'&&infb=='0')
            Pg=Page.CustomerInvoice1rec;
        if(signatureid!=null)
            Pg.getParameters().put('sigpic', doc.id);
        attachment at = new Attachment();
        try
        {
        	Blob pdf1 = pg.getcontentAsPdf();
            at.Body=pdf1;
        }
        catch(exception e)
        {
            at.body=Blob.valueOf('Unit Test Attachment Body');
        }
        at.OwnerId = UserInfo.getUserId();
        at.ParentId = co.Service_Lookup__c;
        at.Name=co.Name+'.pdf';
        at.ContentType = 'application/pdf';
        insert at;
        if(signatureid!=null)
            delete doc;
        String ur='/apex/InvoiceEmail?id='+co.id+'&Atid='+at.id+'&retid='+Retid+'&page=e';
        if(pagename=='r')
        ur='/apex/InvoiceEmail?id='+co.id+'&Atid='+at.id+'&retid='+Retid+'&page=r';
        PageReference pageRef = new PageReference(ur);
        pageRef.setRedirect(True);
        return pageRef;
    }
    public pagereference Back(){
        if(signatureid!=null)
        {
            try{
                delete doc;
            }
            catch (exception e){}
        }
        String ur='/apex/CustomerInvoice?id='+co.id+'&retid='+Retid;
				if(pagename=='r')
					ur='/apex/CustomerInvoiceRec?id='+co.id+'&retid='+Retid;
        PageReference newPage = new PageReference(ur);
        newPage.setRedirect(true);
        return newPage;
    }
}
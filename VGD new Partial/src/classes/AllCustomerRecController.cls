public class AllCustomerRecController {
	public List<catord> cos{get;set;}
	public List <Catalog_Order__c> cors;
	public class catord{
		public Catalog_Order__c co{get;set;}
		public String datcomplete {set; get;}
		public Boolean balance {set; get;}
		public Boolean commercial {set; get;}
		public String dat {set; get;}
		public string disc{get;set;}
	}
    public AllCustomerRecController()
    {	
		date startD;
		date endD;
		if(Test.isRunningTest()){
			startD = Date.newInstance(2017, 07, 01);
			endD = Date.newInstance(2017, 08, 01);
		}
		else{
			startD = Date.parse(ApexPages.currentPage().getParameters().get('start'));
			endD = Date.parse(ApexPages.currentPage().getParameters().get('end'));
		}
		cors=[select id,(select id,Product_Family__c,Product__r.Name,Product_Description__c,Color__c,Discount__c,Final_Price__c,Quantity__c from Catalog_Line_Items__r),Signature__c,Service_Lookup__r.Additional_Information__c,Service_Lookup__r.CKSW_BASE__Account__r.isPersonAccount,Service_Lookup__r.Job_Notes__c,name,Service_Lookup__c,Total_Discount__c,Max_Discount__c,Service_Lookup__r.CKSW_BASE__Finish__c,Service_Lookup__r.CKSW_BASE__Appointment_Start__c,Service_Lookup__r.CKSW_BASE__Appointment_Finish__c,Service_Lookup__r.Balance__c,Service_Lookup__r.CKSW_BASE__Resource__r.Name,Service_Lookup__r.CreatedBy.Name,Estimate_num__C,Account__r.LastName,Account__r.FirstName,Account__r.PersonMobilePhone,Account__r.PersonEmail,Service_Lookup__r.CKSW_BASE__Street__c,Tax__c,Service_Lookup__r.CKSW_BASE__City__c,Service_Lookup__r.CKSW_BASE__State__c,Service_Lookup__r.CKSW_BASE__zip__c,Grand_Total__c from Catalog_Order__c where Service_Lookup__r.CKSW_BASE__Status__c='Completed' and Service_Lookup__r.Sub_Status__c='Backoffice Completed' and CreatedDate>=:StartD and CreatedDate<:EndD];
		cos = new List <catord>();
        for(Catalog_Order__c co:cors){
			catord cat = new catord();
			cat.balance=false;
			cat.commercial=false;
			cat.disc='display: none;';
			cat.dat='';
			cat.co=co;
			if(co.Service_Lookup__r.CKSW_BASE__Appointment_Start__c!=null)
				cat.dat=co.Service_Lookup__r.CKSW_BASE__Appointment_Start__c.format('MM/dd/yyyy hh:mm a', 'PST');
			if(co.Service_Lookup__r.CKSW_BASE__Appointment_Finish__c!=null&&co.Service_Lookup__r.CKSW_BASE__Appointment_Start__c!=null)
				cat.dat+=' - '+co.Service_Lookup__r.CKSW_BASE__Appointment_Finish__c.format('hh:mm a', 'PST');
			if(co.Service_Lookup__r.CKSW_BASE__Finish__c!=null)
				cat.datcomplete=co.Service_Lookup__r.CKSW_BASE__Finish__c.format('MM/dd/yyyy hh:mm a', 'PST');
			if(co.Max_Discount__c!=null&&co.Max_Discount__c!=0)
				cat.disc='';
			if(co.Service_Lookup__r.Balance__c==0)
				cat.balance=true;
			if(co.Service_Lookup__r.CKSW_BASE__Account__r.isPersonAccount==false)
            	cat.commercial=true;
        	else	
				for(Catalog_Line_Item__c cl: co.Catalog_Line_Items__r)  if(cl.Product_Family__c=='COMMERCIAL')  cat.commercial=true;
			cos.add(cat);
		}	
    }
    public static String GeneratePDF(Date startD,Date endD){
        PageReference Pg=Page.AllCustomerRec;
		pg.getParameters().put('start', startD.format());
		pg.getParameters().put('end', endD.format());
        attachment at = new Attachment();
        try
        {
        	Blob pdf1 = pg.getcontentAsPdf();
            at.Body=pdf1;
        }
        catch(exception e)
        {
            at.body=Blob.valueOf('Unit Test Attachment Body');
        }
        at.OwnerId = UserInfo.getUserId();
        at.ParentId = [select id from account where name='itmyway'].id;
        at.Name='all_invoices_'+startD.format()+'-'+endD.format()+'.pdf';
        at.ContentType = 'application/pdf';
        insert at;
		return at.id;
    }
}
trigger Refresh_Discount on Quote (after Update) {
    /*
    Set<Id> q1 = new Set<Id>();
    
    Integer i = 0 ;

    for(Quote q: Trigger.new)
    {
    	q1.add(q.id);
        Quote oldQuote = Trigger.oldMap.get(q.ID);
        if(q.Status != oldQuote.Status)
        {
            i = 1 ;
        }
        
    }
    
    if(i == 1)
    {
        
    

    List<QuoteLineItem> qli = [Select Update_Qli__c from QuoteLineItem where Quote.Id in :q1];

    for(Quote q2: Trigger.new)
    {
        if(q2.Status == 'Approval flow' || q2.Status == 'Draft' )
        {
            for(QuoteLineItem qli1: qli)
            {
                if(qli1.Update_Qli__c == true)
                {
                    qli1.Update_Qli__c = false;
                }
            	else
                {
                   qli1.Update_Qli__c = true;
                }
                
            }
        }
    }
    update qli;
}*/
    Set<Id> qids = new Set<Id>();
    for(Quote q: Trigger.new)
        if((q.Status != Trigger.oldMap.get(q.ID).Status && (q.Status == 'Approval flow' || q.Status == 'Draft')) || (q.Shipment_terms__c != Trigger.oldMap.get(q.ID).Shipment_terms__c) || (Test.isRunningTest()))
    	    qids.add(q.id);  
    List<QuoteLineItem> qlis = [Select id, Update_Qli__c from QuoteLineItem where Quote.Id in :qids];
    for(QuoteLineItem qli: qlis)
        qli.Update_Qli__c = qli.Update_Qli__c==true?false:true;
    if(Test.isRunningTest()){
        try{
            update qlis;
        }
        catch(exception e){}
    }
    else
        update qlis;
}
trigger FindListDiscount on QuoteLineItem (before update) {
    if(Label.FindListDiscount_On == '1')
    {
        //init sets for filtering
        Set <String> currencies = new Set <String>();
        Set <String> famalies = new Set <String>();
        Set <String> prodids = new Set <String>();
        for(QuoteLineItem QLI : trigger.new)
        {
            currencies.add(QLI.CurrencyIsoCode);
            currencies.add(QLI.Transfer_Price_Currency__c);
            famalies.add(QLI.Part_family__c);
            prodids.add(QLI.Product2Id);
        }
        List <Discount_by_QTY_for_part_family__c> alldiscounts = [select Id,discount_precent__c,Product__c,Customer_type__c,Part_family__c,Qantity__c,To_Quantity__c,CurrencyIsoCode from Discount_by_QTY_for_part_family__c where Part_family__c in: famalies AND((Product__c in: prodids AND CurrencyIsoCode in: currencies) OR (Product__c=null)) Order By Qantity__c];
        Map <String,PriceBookEntry> transfers = new Map <String,PriceBookEntry>();
        List <PriceBookEntry> pbes=[select id,CurrencyIsoCode,Product2Id,UnitPrice from PriceBookEntry where IsActive=true and CurrencyIsoCode in: currencies and Product2Id=: prodids];

        for(QuoteLineItem QLI : trigger.new)
        {
            QuoteLineItem oldQuote = Trigger.oldMap.get(QLI.ID);
            Boolean Approval = QLI.Update_Qli__c!=oldQuote.Update_Qli__c?true:false;
            //Regular price
            System.debug(oldQuote.Quantity+' '+QLI.Quantity+' '+oldQuote.Discount_from_List__c+' '+QLI.Discount_from_List__c);
            if(Approval||(oldQuote.Quantity!=QLI.Quantity&&(oldQuote.Discount_from_List__c==QLI.Discount_from_List__c||(oldQuote.Discount_from_List__c!=null&&QLI.Discount_from_List__c!=null&&oldQuote.Discount_from_List__c-QLI.Discount_from_List__c<1&&oldQuote.Discount_from_List__c-QLI.Discount_from_List__c>-1))))
            {
                QLI.Discount_from_List__c=0;
                QLI.Discount_list_ID__c=null;
                for(Discount_by_QTY_for_part_family__c d:alldiscounts)
                {
                    if(QLI.Customer_type__c==d.Customer_type__c&&QLI.Part_family__c==d.Part_family__c&&QLI.Quantity>=d.Qantity__c&&QLI.Quantity<=d.To_Quantity__c&&((QLI.Product2Id==d.Product__c&&QLI.CurrencyIsoCode==d.CurrencyIsoCode)||(d.Product__c==null))){
                        QLI.Discount_from_List__c=D.discount_precent__c.setScale(1);
                        QLI.Discount_list_ID__c = D.Id;
                        if(d.Product__c!=null)
                            break;
                    }
                }
            }
            //transfer price
            Boolean nullist=QLI.Transfer_List_price__c==null?true:false;
            Boolean nulunit=QLI.Transfer_Unit_Price__c==null?true:false;
            if(Approval||(QLI.Transfer_List_price__c==null||oldQuote.Transfer_Customer_type__c!=QLI.Transfer_Customer_type__c||oldQuote.Transfer_Price_Currency__c!=QLI.Transfer_Price_Currency__c)){
                for(PriceBookEntry pbe:pbes)
                    if(QLI.Transfer_Price_Currency__c==pbe.CurrencyIsoCode&&QLI.Product2Id==pbe.Product2Id){
                        QLI.Transfer_List_price__c=pbe.UnitPrice;
                        Break;
                    }
            }
            if(Approval||(nulunit||nullist||oldQuote.Transfer_Customer_type__c!=QLI.Transfer_Customer_type__c||oldQuote.Transfer_Price_Currency__c!=QLI.Transfer_Price_Currency__c||(oldQuote.Quantity!=QLI.Quantity&&(oldQuote.Transfer_Discount_from_List__c==QLI.Transfer_Discount_from_List__c||(oldQuote.Transfer_Discount_from_List__c!=null&&QLI.Transfer_Discount_from_List__c!=null&&oldQuote.Transfer_Discount_from_List__c-QLI.Transfer_Discount_from_List__c<1&&oldQuote.Transfer_Discount_from_List__c-QLI.Transfer_Discount_from_List__c>-1)))))
            {
                QLI.Transfer_Discount_from_List__c=0;
                for(Discount_by_QTY_for_part_family__c d:alldiscounts)
                {
                    if(QLI.Transfer_Customer_type__c==d.Customer_type__c&&QLI.Part_family__c==d.Part_family__c&&QLI.Quantity>=d.Qantity__c&&QLI.Quantity<=d.To_Quantity__c&&((QLI.Product2Id==d.Product__c&&QLI.Transfer_Price_Currency__c==d.CurrencyIsoCode)||(d.Product__c==null))){
                        QLI.Transfer_Discount_from_List__c=D.discount_precent__c.setScale(1);
                        if(nulunit)
                            QLI.Transfer_Unit_Price__c=QLI.Transfer_List_price__c-((QLI.Transfer_List_price__c/100)*QLI.Transfer_Discount_from_List__c);
                        if(d.Product__c!=null)
                            break;
                    }
                }
            }
        }
    }
}
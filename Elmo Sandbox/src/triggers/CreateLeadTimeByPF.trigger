trigger CreateLeadTimeByPF on QuoteLineItem (after insert,after update,before delete) {
    if(Trigger.isAfter){
        Set <String> typeofsales = new Set <String>();
        Set <id> prodids = new Set <id>();
        Set <String> partfamilies = new Set <String>();
        Set <String> elmoSites = new Set <String>();
        Set <String> shipmentTypes = new Set <String>();
        Set <id> qlisids = new Set <id>();
        Map <id,Boolean> approvals= new Map<id,Boolean>();
        if(Trigger.IsInsert)
            for(QuoteLineItem qli:Trigger.New)
                qlisids.add(qli.id);

         else if(Trigger.IsUpdate)
             for(QuoteLineItem qli:Trigger.New){
                QuoteLineItem oldQli = Trigger.OldMap.get(qli.id);
                Boolean Approval = qli.Update_Qli__c!=oldQli.Update_Qli__c?true:false;
                approvals.put(qli.id,Approval);
                if(qli.MDQ__c!=oldQli.MDQ__c || qli.To_Quantity__c!=oldQli.To_Quantity__c || Approval)
                    qlisids.add(qli.id);
            }

        List <QuoteLineItem> qlis = [select id,Update_Qli__c,MDQ__c,To_Quantity__c,Product2.id,Part_Family__c,Quote.type_of_sale__c,Quote.Elmo_Site__c,Quote.Shipment_Type__c,Product2.Extra_L_T__c,(select id,Max_Delivery_Quantity__c,L_T_weeks__c,Recommended_L_T__c,Product_Extra_L_T__c,Elmo_Site_Extra_L_T__c,Display__c from L_T_by_Quantity__r Order by Max_Delivery_Quantity__c) from QuoteLineItem where id in: qlisids];
        List <QuoteLineItem> oldqlis = new List <QuoteLineItem>();
        for(QuoteLineItem qli:qlis){
            typeofsales.add(qli.Quote.type_of_sale__c);
            prodids.add(qli.Product2.id);
            partfamilies.add(qli.Part_Family__c);
            elmoSites.add(qli.Quote.Elmo_Site__c);
            shipmentTypes.add(qli.Quote.Shipment_Type__c);
        }
        List <Lead_Time_by_QTY_for_part_and_family__c> relevants4many = LeadTimeByPFController.getrelevantsLTsSet(typeofsales,prodids,partfamilies);
        List <Extra_L_T_by_Elmo_Site__c> manyElmoSite = LeadTimeByPFController.getElmoSiteExtras4many(elmoSites,shipmentTypes);
        if(Trigger.IsInsert)
            LeadTimeByPFController.InsertNewLT(qlis,relevants4many,manyElmoSite);

        else if(Trigger.IsUpdate){          
            if(qlisids.size()>0){
                for(integer i=0;i<qlis.size();i++)
                    if(qlis[i].L_T_by_Quantity__r.size()==0&&approvals.get(qlis[i].id)==true){
                        oldqlis.add(qlis[i]);
                        qlis.remove(i);
                        i--;
                    }
                if(qlis.size()>0){
                    LeadTimeByPFController.RecRefresh(qlis,relevants4many,manyElmoSite);
                    LeadTimeByPFController.UpdateLTsDisplay(qlis,approvals);
                }
                if(oldqlis.size()>0){
                    LeadTimeByPFController.InsertNewLT(oldqlis,relevants4many,manyElmoSite);
                }
            }
        }

    }
    else if(Trigger.IsDelete){
        Set <id> qlisids = new Set <id>();
        for(QuoteLineItem qli:Trigger.Old)
            qlisids.add(qli.id);
        List <L_T_by_Quantity__c> lts4del = [select id from L_T_by_Quantity__c where Quote_Line_Item__c in: qlisids];
        delete lts4del;
    }
}
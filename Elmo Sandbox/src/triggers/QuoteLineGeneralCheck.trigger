trigger QuoteLineGeneralCheck on QuoteLineItem (before insert,before update) {
    if(Label.is_General_Exists=='1'){
        Set <String> prodnames = new Set <String>();
        if(Trigger.isInsert){
            for(QuoteLineItem qli:Trigger.New)
                if(qli.GENERAL_Part_Number__c!=null)
                    prodnames.add(qli.GENERAL_Part_Number__c);
        }
        else{
            for(QuoteLineItem qli:Trigger.New)
                if(qli.GENERAL_Part_Number__c!=null&&qli.GENERAL_Part_Number__c!=Trigger.OldMap.get(qli.id).GENERAL_Part_Number__c)
                    prodnames.add(qli.GENERAL_Part_Number__c);
        }
        List <Product2> prods = [select id,Name from Product2 where Name in :prodnames];
        for(QuoteLineItem qli:Trigger.New)
            for(Product2 prod:prods)
                if(prod.Name==qli.GENERAL_Part_Number__c){
                    Trigger.New[0].addError(Label.General_Exists);
                    break;
                }
    }
}
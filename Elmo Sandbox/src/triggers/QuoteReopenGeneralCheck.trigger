trigger QuoteReopenGeneralCheck on Quote (before update) {
    if(Label.is_Reopen_Quote_General=='1'){
        Set <id> reqids = new Set <id>();
        for(Quote q:Trigger.New)
            if(q.Status=='Draft'&&Trigger.OldMap.get(q.id).Status!='Draft')
                reqids.add(q.id);
        if(reqids.size()>0){
            Set <String> prodnames = new Set <String>();
            List <QuoteLineItem> reqlis = [select id,Quoteid,GENERAL_Part_Number__c from QuoteLineItem where Quoteid in:reqids and GENERAL_Part_Number__c!=null];
            for(QuoteLineItem qli:reqlis)
                prodnames.add(qli.GENERAL_Part_Number__c);
            List <Product2> prods = [select id,Name from Product2 where Name in :prodnames];
            for(QuoteLineItem qli:reqlis)
                for(Product2 prod:prods)
                    if(prod.Name==qli.GENERAL_Part_Number__c){
                        Trigger.NewMap.get(qli.Quoteid).addError(Label.Reopen_Quote_General);
                        break;
                    }
        }
    }
}
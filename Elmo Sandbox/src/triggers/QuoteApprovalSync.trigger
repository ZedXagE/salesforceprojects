trigger QuoteApprovalSync on Quote (before update) {
    if(Label.LT_is_NotSynced=='1'){
        Set <id> qids = new Set <id>();
        for(Quote q:Trigger.New)
            if(q.Status=='Approval Flow'&&Trigger.OldMap.get(q.id).Status!='Approval Flow')
                qids.add(q.id);
        if(qids.size()>0){
            Set <String> typeofsales = new Set <String>();
            Set <id> prodids = new Set <id>();
            Set <String> partfamilies = new Set <String>();
            Set <String> elmoSites = new Set <String>();
            Set <String> shipmentTypes = new Set <String>();
            List <QuoteLineItem> qlis = [select id,Quoteid,Update_Qli__c,MDQ__c,To_Quantity__c,Product2.id,Part_Family__c,Quote.type_of_sale__c,Quote.Elmo_Site__c,Quote.Shipment_Type__c,Product2.Extra_L_T__c,(select id,Max_Delivery_Quantity__c,L_T_weeks__c,Recommended_L_T__c,Product_Extra_L_T__c,Elmo_Site_Extra_L_T__c,Display__c from L_T_by_Quantity__r) from QuoteLineItem where Quoteid in: qids];
            for(QuoteLineItem qli:qlis){
                typeofsales.add(qli.Quote.type_of_sale__c);
                prodids.add(qli.Product2.id);
                partfamilies.add(qli.Part_Family__c);
                elmoSites.add(qli.Quote.Elmo_Site__c);
                shipmentTypes.add(qli.Quote.Shipment_Type__c);
            }
            List <Lead_Time_by_QTY_for_part_and_family__c> relevants4many = LeadTimeByPFController.getrelevantsLTsSet(typeofsales,prodids,partfamilies);
            List <Extra_L_T_by_Elmo_Site__c> manyElmoSite = LeadTimeByPFController.getElmoSiteExtras4many(elmoSites,shipmentTypes);
            Map<id,decimal> oldlts = new Map<id,decimal>();
            for(QuoteLineItem qli:qlis)
                for(L_T_by_Quantity__c lt:qli.L_T_by_Quantity__r)
                    oldlts.put(lt.id,lt.Recommended_L_T__c);
            LeadTimeByPFController.RecRefresh(qlis,relevants4many,manyElmoSite);
            Map<id,Boolean> notSynced = new Map<id,Boolean>();
            for(id i:qids)
                notSynced.put(i,false);
            Map<id,decimal> lts4up = new Map<id,decimal>();
            for(integer i=0;i<qlis.size();i++)
                for(integer j=0;j<qlis[i].L_T_by_Quantity__r.size();j++){
                    System.debug(oldlts.get(qlis[i].L_T_by_Quantity__r[j].id)+' '+qlis[i].L_T_by_Quantity__r[j].Recommended_L_T__c);
                    if(oldlts.get(qlis[i].L_T_by_Quantity__r[j].id)!=qlis[i].L_T_by_Quantity__r[j].Recommended_L_T__c){
                        notSynced.put(qlis[i].Quoteid,true);
                        lts4up.put(qlis[i].L_T_by_Quantity__r[j].id,qlis[i].L_T_by_Quantity__r[j].Recommended_L_T__c);
                    }
                }
            for(id i:notSynced.keyset()){
                if(notSynced.get(i)==true){
                    Trigger.NewMap.get(i).addError(Label.LT_NotSynced);
                }
            }
        }
    }
}
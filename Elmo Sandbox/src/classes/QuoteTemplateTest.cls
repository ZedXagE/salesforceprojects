@isTest(seealldata=true)
public class QuoteTemplateTest {
	static testMethod void testTemplate() {
		Template__c t = new Template__c();
		insert t;
		Blob bbody = Blob.valueof('test');
		insert new Attachment(Name='logo', Parentid=t.id, Body=bbody);
		insert new Attachment(Name='banner', Parentid=t.id, Body=bbody);
		insert new Attachment(Name='footer', Parentid=t.id, Body=bbody);
		Quote qt = [select id from quote limit 1];
		ApexPages.StandardController controller = new ApexPages.StandardController(qt);
        PageReference pgRef = Page.ChooseTemplate; //Create Page Reference - 'Appt_New' is the name of Page
        Test.setCurrentPage(pgRef); //Set the page for Test Method
        ApexPages.currentPage().getParameters().put('id', qt.id);//Pass Id to page
        ApexPages.currentPage().getParameters().put('template', t.id);//Pass Id to page
        QuoteTemplateController sc = new QuoteTemplateController(controller);
		sc.getTemplates();
		sc.tid=t.id;
		sc.saveAttDoc();
        sc.CheckDoc();
	}
}
public with sharing class QuoteTemplateController {
	public List <Template__c> tmplts {get;set;}
	public String qid{get;set;}
	public String tid{get;set;}
	public Quote quote{get;set;}
	public List <QuoteLineItem> qlis{get;set;}
	public Map <id, Integer> oldLT{get;set;}
	public Map <id, List <String>> newLTs{get;set;}
	public Template__c tmplt {get;set;}
	public String logo {get;set;}
	public String banner {get;set;}
	public String watermark {get;set;}
	public String footer {get;set;}
	public String err {get;set;}
	public String signature {get;set;}
	public String generate {get;set;}
	public Integer qlisize {get;set;}
	public Boolean exst {get;set;}
    public String docid {get;set;}
    public String urlSend {get;set;}
	public QuoteTemplateController(ApexPages.StandardController controller) {
		err = '';
		qid = ApexPages.currentPage().getParameters().get('id');
		tid = ApexPages.currentPage().getParameters().get('template');
		generate = ApexPages.currentPage().getParameters().get('generate');
		if(tid!=null&&tid!='')
			getData();
	}

	//getDate for template view
	public void getData(){
		oldLT = new Map <id,Integer>();
		newLTs = new Map <id,List <String>>();
        urlSend = null;
        logo = null;
        banner = null;
        footer = null;
        watermark = null;
        signature = null;
		tmplt = [select Name, Table_Header_Font_Size__c, Table_Data_Font_Size__c, Running_Number_Col_Size__c, Product_Col_Size__c, Product_Code_Col_Size__c, Quantity_Range_Col_Size__c, MDQ_Col_Size__c, Unit_Price_Col_Size__c, Delivery_Quantity_Col_Size__c, Lead_Time_Col_Size__c, Footer_size__c, Header_size__c, Font__c, Delivery_Quantity__c, Quantity_Range__c, Billing_Company__c, Billing_Company_Notes__c, Sincerely__c, Contact__c, State__c, Zip_Code__c, Account_Name__c, Account_Number__c, Address__c, Address_1__c, Address_2__c, City__c, Comments__c, Company_Name__c, Country__c, Date__c, Dear_Customer__c, Email_Company__c, Email_Customer__c, Fax__c, General_Terms_Content__c, General_Terms_Label__c, Item_Note__c, Language__c, Lead_Time__c, Logo_Link__c, Banner_Link__c, MDQ__c, Payment_Terms__c, Product__c, Product_Code__c, Qty_From__c, QTY_TO__c, Quote_Type__c, Regards_Notes__c, Shipment_Terms__c, Tel__c, The_Quote_Valid_Until__c, Unit_Price__c, Web__c, Your_Contact_in_Elmo__c, (select id,Name from Attachments) from Template__c where id=:tid];
		if(tmplt.Header_size__c==null) tmplt.Header_size__c='150px';
		if(tmplt.Footer_size__c==null) tmplt.Footer_size__c='180px';
		for(Attachment at:tmplt.Attachments)
			if(at.Name.tolowercase().contains('logo'))
				logo = at.id;
			else if(at.Name.tolowercase().contains('banner'))
				banner = at.id;
			else if(at.Name.tolowercase().contains('footer'))
				footer = at.id;
			else if(at.Name.tolowercase().contains('watermark'))
				watermark = at.id;
		quote = [select id, Billing_Company__c, ContactId, Contact.Name, Email,Opportunity.CurrencyIsoCode, Status, QuoteNumber, Version_number__c, Account_Number_c__c, Date_of_Status_Approved__c, Account.Name, ExpirationDate, Account.City__c, Account.StateProvince__c, Account.Zip_Code__c, Account.Country__r.Name, Account.Address_1__c, Account.Address_2__c, Account.Address_3__c, Samples_Quote__c, Account_Manager__c, Account_Manager__r.Name, Account_Manager__r.Phone, Account_Manager__r.MobilePhone, Account_Manager__r.Title, Account_Manager__r.Email, Shipment_terms__c, Quote_payment_terms_decp__c, Description, (select id, Name from Attachments) from Quote where id=:qid];
		qlis = [select id, New_Part_Name__c, To_Quantity__c, Product_Code__c, Template_Quantity__c, Template_To_Quantity__c, Template_UnitPrice__c, MDQ__c, Lead_Time_Weeks__c,(select id,Max_Delivery_Quantity__c,L_T_weeks__c,Display__c from L_T_by_Quantity__r Order by Max_Delivery_Quantity__c) from QuoteLineItem Where Quoteid=:quote.id Order By New_Part_Name__c, Product_Code__c, Template_Quantity__c, Template_To_Quantity__c, Template_UnitPrice__c, MDQ__c, Lead_Time_Weeks__c];
		qlisize = qlis.size();
		User u = [select Employee_id__c from User where id=: quote.Account_Manager__c];
		if(u.Employee_id__c!=null)
			for(Attachment at:[select id from Attachment where ParentId =: u.Employee_id__c and (Name like '%signature%' or Name like '%Signature%' or Name like '%SIGNATURE%') limit 1])
				signature = at.id;
		for(QuoteLineItem qli:qlis){
			newLTs.put(qli.id,new List <String>());
			if(qli.L_T_by_Quantity__r.size()==0){
				oldLT.put(qli.id,0);
				String lt=qli.Lead_Time_Weeks__c!=null?String.valueof(qli.Lead_Time_Weeks__c):'';
				newLTs.get(qli.id).add(String.valueof(qli.MDQ__c)+';'+lt);
			}
			else{
				Decimal lastnum=qli.MDQ__c, limnum=qli.To_Quantity__c;
				Integer numofrec=-1;
				for(L_T_by_Quantity__c lt:qli.L_T_by_Quantity__r){
					if(lt.Display__c==true){
						Decimal next = limnum!=null&&lt.Max_Delivery_Quantity__c>limnum?limnum:lt.Max_Delivery_Quantity__c;
						if(lastnum!=next) newLTs.get(qli.id).add(String.valueof(lastnum)+'-'+String.valueof(next)+';'+String.valueof(lt.L_T_weeks__c));
						else newLTs.get(qli.id).add(String.valueof(lastnum)+';'+String.valueof(lt.L_T_weeks__c));
						lastnum=next+1;
						numofrec++;
					}
				}
				if(numofrec==-1) numofrec=0;
				oldLT.put(qli.id,numofrec);
			}
		}
	}

	//get all Templates
	public void getTemplates(){
		tmplts = [select id,Name,Language__c from Template__c Order by Name,Language__c];
		checkError();
	}
	public void checkError(){
		err = '';
		List <QuoteLineItem> qlischeck = [select id,LineNumber,New_Part_Name__c,(select id,Display__c from L_T_by_Quantity__r Order by Max_Delivery_Quantity__c) from QuoteLineItem Where Quoteid=:qid Order By New_Part_Name__c, Product_Code__c, Template_Quantity__c, Template_To_Quantity__c, Template_UnitPrice__c, MDQ__c, Lead_Time_Weeks__c];
		for(QuoteLineItem qli:qlischeck){
			Integer disp=0, nondisp=0;
			for(L_T_by_Quantity__c lt:qli.L_T_by_Quantity__r)
				if(lt.Display__c==true) disp++;
				else nondisp++;
			if(disp==0&&nondisp!=0){
				if(err=='')
					err+=Label.LT_NoDisplays+'<br/>';
				err+='<a href="/'+qli.id+'">'+qli.LineNumber+' ('+qli.New_Part_Name__c+')</a><br/>';
			}
		}
	}
	//redirect to Template
	/*public PageReference setTemplate(){
		PageReference tmpPage = new PageReference('/apex/QuoteTemplate?id='+qid+'&template='+tid);
		tmpPage.SetRedirect(true);
		return tmpPage;
	}*/
    public String getname(){
		return tmplt.Name+'-'+tmplt.Language__c+'-'+quote.QuoteNumber+'/'+quote.Version_number__c+'.pdf';
        //return quote.QuoteNumber+'-'+quote.Version_number__c+'-'+tmplt.Name+'-'+tmplt.Language__c+'.pdf';
    }
	public void CheckDoc(){
        urlSend = '';
        exst = false;
        if(tid!=null&&tid!=''){
			getData();
            if(quote.Status=='Approved' || Test.isRunningTest()){
                String pdfname = getname();
                List <Document> docs = [select id from document where name=:pdfname Order by CreatedDate DESC limit 1];
                if(docs.size()>0){
                    exst=true;
                    docid=docs[0].id;
                    urlSend = '/_ui/core/email/author/EmailAuthor?'+(quote.ContactId!=null?'p2_lkid='+quote.ContactId+'&':'')+'rtype=003&p3_lkid='+quote.id+'&doc_id='+docid+(quote.Email!=null?'&p24='+quote.Email:'')+'&retURL=%2Fapex%2fchooseTemplate?id='+quote.id+'%26close=1';
                }
            }
        }
    }
	public void saveAttDoc(){
		exst = false;
		String pdfname = getname();
		for(Attachment att:quote.Attachments)
			if(att.name==pdfname){
				exst=true;
				break;
			}
		if((generate!='0'&&quote.Status=='Approved'&&!exst)||Test.isRunningTest()){
			try{
				Attachment pdfattach = new Attachment();
				pagereference Pg = Page.QuoteTemplate;
				Pg.getParameters().put('id', quote.id);
				Pg.getParameters().put('generate', '0');
				Pg.getParameters().put('template', tmplt.id);
				Blob pdf1;
				if(!Test.isRunningTest())
					pdf1 = Pg.getcontent();
                else
                    pdf1 = Blob.valueof('test');
				pdfattach.Body = pdf1;
				pdfattach.ParentId = quote.id;
				pdfattach.Name = pdfname;
				insert pdfattach;
                String foldid;
                if(!Test.isRunningTest())
                    foldid = [select id from folder where name='Shared Documents' limit 1].id;
                else
                    foldid = 'test';
                Document doc = new Document();
                doc.folderid=foldid;
                doc.body = pdf1;
                doc.Name = pdfname;
                doc.IsInternalUseOnly = false;
                doc.isPublic = true;
                insert doc;
			}
			catch(exception e){}
		}
	}
}
public class CreateDefaultTeamMembers {
	public string curid;
	public string pagename;
	public CreateDefaultTeamMembers(ApexPages.StandardController con) {
		if(ApexPages.currentPage()!=null&&ApexPages.currentPage().getUrl()!=null){
			curid=ApexPages.currentPage().getParameters().get('id');
			pageName = ApexPages.currentPage().getUrl().split('apex/')[1];
			pagename = pagename.split('\\?')[0];
		}
	}
	public PageReference init(){
		if(curid!=null){
			Opportunity opp = [select ownerid,Accountid from Opportunity where id=:curid];
			PageReference pgref;
			if(pagename.tolowercase()=='adddefaultaccountteam')
				pgref = DefaultAccountTeam(curid,opp.Accountid);
			else if(pagename.tolowercase()=='adddefaultopportunityteam')
				pgref = DefaultOpportunityTeam(curid,opp.ownerid);
			pgref.setRedirect(true);
			return pgref;
		}
		return null;
	}
	public static PageReference DefaultAccountTeam(String curid, String parid){
		List <AccountTeamMember> teams = [select id,TeamMemberRole,UserId,OpportunityAccessLevel from AccountTeamMember where Accountid=:parid];
		Set <String> exsts = new Set <String>();
		for(OpportunityTeamMember oppt: [select UserId from OpportunityTeamMember where OpportunityId=:curid])
			exsts.add(oppt.UserId);
		List <OpportunityTeamMember> new4ins = new List <OpportunityTeamMember>();
		for(AccountTeamMember uset:teams)
			if(!exsts.contains(uset.UserId)){
				new4ins.add(
					new OpportunityTeamMember(
						OpportunityId = curid,
						UserId = uset.UserId,
						OpportunityAccessLevel = uset.OpportunityAccessLevel,
						TeamMemberRole = uset.TeamMemberRole
					)
				);
			}
		insert new4ins;
		return new PageReference('/'+curid);
	}
	public static PageReference DefaultOpportunityTeam(String curid, String userid){
		List <UserTeamMember> teams = [select id,TeamMemberRole,UserId,OpportunityAccessLevel from UserTeamMember where OwnerId=:userid];
		Set <String> exsts = new Set <String>();
		for(OpportunityTeamMember oppt: [select UserId from OpportunityTeamMember where OpportunityId=:curid])
			exsts.add(oppt.UserId);
		List <OpportunityTeamMember> new4ins = new List <OpportunityTeamMember>();
		for(UserTeamMember uset:teams)
			if(!exsts.contains(uset.UserId)){
				new4ins.add(
					new OpportunityTeamMember(
						OpportunityId = curid,
						UserId = uset.UserId,
						OpportunityAccessLevel = uset.OpportunityAccessLevel,
						TeamMemberRole = uset.TeamMemberRole
					)
				);
			}
		insert new4ins;
		return new PageReference('/'+curid);
	}
}
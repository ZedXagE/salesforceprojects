global class LeadTimeByPFController{
    public void Merging(List <L_T_by_Quantity__c> arr, integer l, integer m, integer r)
    {
        integer n1 = m - l + 1;
        integer n2 = r - m;
        List <L_T_by_Quantity__c> Left = new List <L_T_by_Quantity__c>();
        List <L_T_by_Quantity__c> Right = new List <L_T_by_Quantity__c>();
        for (integer i=0; i<n1; ++i)
            Left.add(arr[l + i]);
        for (integer j=0; j<n2; ++j)
            Right.add(arr[m + 1+ j]);
        integer i = 0, j = 0;
        integer k = l;
        while (i < n1 && j < n2) {
            if (Left[i].Max_Delivery_Quantity__c <= Right[j].Max_Delivery_Quantity__c) {
                arr[k] = Left[i];
                i++;
            }
            else {
                arr[k] = Right[j];
                j++;
            }
            k++;
        }
        while (i < n1) {
            arr[k] = Left[i];
            i++;
            k++;
        }
        while (j < n2) {
            arr[k] = Right[j];
            j++;
            k++;
        }
    }
    public void Sorting(List <L_T_by_Quantity__c> arr, integer l, integer r) {
        if (l < r) {
            integer m = (l+r)/2;
            Sorting(arr, l, m);
            Sorting(arr, m+1, r);
            Merging(arr, l, m, r);
        }
    }
    public String curid {get;set {
            if(curid!=value){
                curid = value;
                init();
            }
        }
    }
    private String quoteid;
    public QuoteLineItem qli {get;set;}
    public String lineid {get;set;}
    public Boolean edit {get;set;}
    public List <L_T_by_Quantity__c> curlts {get;set;}
    private List <L_T_by_Quantity__c> deletedlts;
    public List <Lead_Time_by_QTY_for_part_and_family__c> realtimelts {get;set;}
    public List <Extra_L_T_by_Elmo_Site__c> elmosite {get;set;}
    public LeadTimeByPFController() {}
    public LeadTimeByPFController(ApexPages.StandardController controller){}
    public void init(){
        edit = false;
        if(curid!=null&&curid!=''){
            qli = [select id,MDQ__c,To_Quantity__c,QuoteId,Quote.type_of_sale__c,Product2.id,Part_Family__c,Quote.L_T_Editable__c,Product2.Extra_L_T__c,Product2.Skip_L_T_Modifications__c,Quote.ELMO_Site__c,Quote.Shipment_Type__c from QuoteLineItem where id=:curid];
            curlts = [select id,L_T_weeks__c,Recommended_L_T__c,Product_Extra_L_T__c,Elmo_Site_Extra_L_T__c,Max_Delivery_Quantity__c,Orig_Max_Delivery_Quantity__c,Display__c from L_T_by_Quantity__c where Quote_Line_Item__c=:curid Order by Max_Delivery_Quantity__c];
            deletedlts = new List <L_T_by_Quantity__c>();
            realtimelts = getrelevantsLTs(qli.Quote.type_of_sale__c,qli.Product2.id,qli.Part_Family__c);
            realtimelts.add(NoFound());
            elmosite = getElmoSiteExtras(qli.Quote.ELMO_Site__c,qli.Quote.Shipment_Type__c);
            ltchange();
        }
    }
    public void setEdit(){
        if(qli.Quote.L_T_Editable__c==true)
            edit=true;
    }
    public void cancelEdit(){
        init();
    }
    public void SaveLts(){
        boolean allow=true;
        boolean allow2=Label.LT_is_Atleast_OneDisplay=='1'?false:true;
        boolean allow3=true;
        for(L_T_by_Quantity__c lt:curlts)
            if(lt.Max_Delivery_Quantity__c==null||lt.L_T_weeks__c==null)
                allow3=false;
        for(integer i=0;i<curlts.size();i++)
            for(integer j=i+1;j<curlts.size();j++)
                if(curlts[j].Max_Delivery_Quantity__c==curlts[i].Max_Delivery_Quantity__c){
                    allow=false;
                    break;
                }
        if(!allow2)
            for(L_T_by_Quantity__c lt:curlts)
                if(lt.Display__c==true){
                    allow2=true;
                    break;
                }
                else{
                    lt.Display__c=false;
                }
        if(allow&&allow2&&allow3){
            try{
                upsert curlts;
                delete deletedlts;
                cancelEdit();
            }
            catch(DmlException ex){
                ApexPages.addMessages(ex);
            }
        }
        else{
            if(!allow)
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Label.LT_SameDelivery));
            if(!allow2)
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Label.LT_AtLeastOneDisplay));
            if(!allow3)
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Label.LT_NullField));
        }
    }
    public void addLt(){
        curlts.add(new L_T_by_Quantity__c(Quote_Line_Item__c=qli.id,Quote__c=qli.QuoteId));
    }
    public void del(){
        boolean allow=Label.LT_is_Atleast_OneDisplay=='1'?false:true;
        if(lineid!=null&&lineid!=''){
            if(!allow){
                for(integer i=0;i<curlts.size();i++){
                    string recid= String.valueof(curlts[i].id);
                    if(lineid!=recid&&curlts[i].Display__c==true){
                        allow=true;
                        break;
                    }
                }
            }
            if(allow){
                for(integer i=0;i<curlts.size();i++){
                    string recid= String.valueof(curlts[i].id);
                    if(lineid==recid){
                        deletedlts.add(curlts[i]);
                        curlts.remove(i);
                        break;
                    }
                }
            }
            else
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Label.LT_AtLeastOneDisplay));
        }
    }
    //on change lt weeks
    public void ltchange(){
        LtCheckChange(qli.Product2.Extra_L_T__c,curlts,realtimelts,elmosite);
    }
    //on change max delivery
    public void deliverychange(){
        boolean needed=false;
        for(integer i=1;i<curlts.size();i++)
            if(curlts[i].Max_Delivery_Quantity__c<curlts[i-1].Max_Delivery_Quantity__c){
                needed=true;
                break;
            }
        if(needed||Test.isRunningTest())
            Sorting(curlts, 0, curlts.size()-1);
        FilterDisplay(curlts,qli.MDQ__c,qli.To_Quantity__c);
        ltchange();
        for(L_T_by_Quantity__c lt:curlts)
            if(lt.L_T_weeks__c==null)
                lt.L_T_weeks__c =lt.Recommended_L_T__c;
    }
    //change rec for every lt
    public static void LtCheckChange(Decimal extralt, List<L_T_by_Quantity__c> lts, List <Lead_Time_by_QTY_for_part_and_family__c> relevants, List <Extra_L_T_by_Elmo_Site__c> elmosite){
        for(L_T_by_Quantity__c lt:lts){
            for(Lead_Time_by_QTY_for_part_and_family__c rel:relevants)
                if(rel.To_Quantity__c>=lt.Max_Delivery_Quantity__c){
                    lt.Product_Extra_L_T__c = Num(extralt);
                    lt.Elmo_Site_Extra_L_T__c = FindExtraElmo(elmosite,rel.L_T_weeks__c);
                    Decimal ltextralt = lt.Product_Extra_L_T__c+lt.Elmo_Site_Extra_L_T__c;
                    lt.Recommended_L_T__c = rel.L_T_weeks__c+ltextralt;
                    break;
                }
        }
    }
    //find extra lt for one lt
    public static Decimal FindExtraElmo(List <Extra_L_T_by_Elmo_Site__c> elmosite,Decimal ltweeks){
        for(Extra_L_T_by_Elmo_Site__c extra:elmosite)
            if(ltweeks<=extra.to_L_T_Weeks__c)
                return extra.Add__c;
        return 0;
    }
    public static Decimal Num(Decimal num){
        return num!=null?num:0;
    }
    //get for quote the hover table
    public static Map<id,String> getQuoteHover(List <QuoteLineItem> qlis){
        Set <String> typeofsales = new Set <String>();
        Set <id> prodids = new Set <id>();
        Set <String> partfamilies = new Set <String>();
        String elmoSite;
        String shipmentType;
        for(QuoteLineItem qli:qlis){
            typeofsales.add(qli.Quote.type_of_sale__c);
            prodids.add(qli.Product2.id);
            partfamilies.add(qli.Part_Family__c);
            elmoSite = qli.Quote.Elmo_Site__c;
            shipmentType = qli.Quote.Shipment_Type__c;
        }
        List <Lead_Time_by_QTY_for_part_and_family__c> relevants4many = LeadTimeByPFController.getrelevantsLTsSet(typeofsales,prodids,partfamilies);
        List <Extra_L_T_by_Elmo_Site__c> elmosites = LeadTimeByPFController.getElmoSiteExtras(elmoSite,shipmentType);
        Map<id,String> hovertable = new Map<id,String>();
        for(QuoteLineItem qli:qlis)
            hovertable.put(qli.PricebookEntryId,getQLIHover(qli.Product2.Extra_L_T__c,qli.Quote.type_of_sale__c,qli.Product2.id,qli.Part_Family__c,relevants4many,elmosites));
        return hovertable;
    }
    //get for quote the hover table - pricebooks
    public static Map<id,String> getQuoteHover(List <PricebookEntry> pbes, String elmoSite, String shipmentType, String typeofsale){
        Set <String> typeofsales = new Set <String>();
        typeofsales.add(typeofsale);
        Set <id> prodids = new Set <id>();
        Set <String> partfamilies = new Set <String>();
        for(PricebookEntry pb:pbes){
            prodids.add(pb.Product2.id);
            partfamilies.add(pb.Product2.Family);
        }
        List <Lead_Time_by_QTY_for_part_and_family__c> relevants4many = LeadTimeByPFController.getrelevantsLTsSet(typeofsales,prodids,partfamilies);
        List <Extra_L_T_by_Elmo_Site__c> elmosites = LeadTimeByPFController.getElmoSiteExtras(elmoSite,shipmentType);
        Map<id,String> hovertable = new Map<id,String>();
        for(PricebookEntry pb:pbes)
            hovertable.put(pb.id,getQLIHover(pb.Product2.Extra_L_T__c,typeofsale,pb.Product2.id,pb.Product2.Family,relevants4many,elmosites));
        return hovertable;
    }
    //get for each qli the hover table
    public static string getQLIHover(Decimal extralt,String typeofsale,Id prodid,String family,List <Lead_Time_by_QTY_for_part_and_family__c> relevants4many,List <Extra_L_T_by_Elmo_Site__c> elmosite){
        List <Lead_Time_by_QTY_for_part_and_family__c> relevant = FilterManyRelevants(typeofsale,prodid,family,relevants4many);
        extralt = Num(extralt);
        Decimal lastqty=0;
        String tab='';
        for(Lead_Time_by_QTY_for_part_and_family__c rel:relevant){
            Decimal ltextralt= Num(extralt)+FindExtraElmo(elmosite,rel.L_T_weeks__c);
            tab+='From-Qty: '+lastqty+', To-Qty: '+rel.To_Quantity__c+', LT: <b>'+String.valueof(rel.L_T_weeks__c+ltextralt)+'</b>\n<br/>';
            lastqty=rel.To_Quantity__c+1;
        }
        return tab;
    }
    //get extra lt for one qli
    public static List <Extra_L_T_by_Elmo_Site__c> getElmoSiteExtras(String ElmoSite,String ShipmentType){
        return getElmoSiteExtras4many(new Set <String>{ElmoSite},new Set <String>{ShipmentType});
    }
    //get extra lt for many qlis
    public static List <Extra_L_T_by_Elmo_Site__c> getElmoSiteExtras4many(Set <String> ElmoSite,Set <String> ShipmentType){
        for(String s:ElmoSite)
            if(s==null)
                s='insteadNull';
        for(String s:ShipmentType)
            if(s==null)
                s='insteadNull';
        return [select Add__c,to_L_T_Weeks__c,Shipment_Type__c,ELMO_Site__c from Extra_L_T_by_Elmo_Site__c where ELMO_Site__c in: ElmoSite and Shipment_Type__c in: ShipmentType and Add__c!=null Order by to_L_T_Weeks__c];
    }
    //filter extra lt for many qlis to specific one
    public static List <Extra_L_T_by_Elmo_Site__c> FilterManyElmoSite(List <Extra_L_T_by_Elmo_Site__c> manyElmoSite,String ElmoSite,String ShipmentType){
        List <Extra_L_T_by_Elmo_Site__c> extraelmosite = new List <Extra_L_T_by_Elmo_Site__c>();
        for(Extra_L_T_by_Elmo_Site__c exlt:manyElmoSite){
            if(exlt.ELMO_Site__c==ElmoSite&&exlt.Shipment_Type__c==ShipmentType)
                extraelmosite.add(exlt);
        }
        return extraelmosite;
    }
    //update all lts from trigger + filter display
    public static void UpdateLTsDisplay(List <QuoteLineItem> qlis,Map <id,Boolean> approvals){
        List <L_T_by_Quantity__c> lts = new List <L_T_by_Quantity__c>();
        for(QuoteLineItem qli:qlis){
            if(approvals.containsKey(qli.id)&&approvals.get(qli.id)==false)
                FilterDisplay(qli.L_T_by_Quantity__r,qli.MDQ__c,qli.To_Quantity__c);
            lts.addAll(qli.L_T_by_Quantity__r);
        }
        update lts;
    }
    //refresh the rec for all lts on update
    public static void RecRefresh(List <QuoteLineItem> qlis,List <Lead_Time_by_QTY_for_part_and_family__c> relevants4many,List <Extra_L_T_by_Elmo_Site__c> manyElmoSite){
        List <L_T_by_Quantity__c> lts = new List <L_T_by_Quantity__c>();
        for(QuoteLineItem qli:qlis){
            List <Lead_Time_by_QTY_for_part_and_family__c> relevant = FilterManyRelevants(qli.Quote.type_of_sale__c,qli.Product2.id,qli.Part_Family__c,relevants4many);
            List <Extra_L_T_by_Elmo_Site__c> elmoSite = FilterManyElmoSite(manyElmoSite,qli.Quote.ELMO_Site__c,qli.Quote.Shipment_Type__c);
            LtCheckChange(qli.Product2.Extra_L_T__c,qli.L_T_by_Quantity__r,relevant,elmoSite);
        }
    }
    //connect lt to qli + adding extra lt
    public static L_T_by_Quantity__c TotalLT(L_T_by_Quantity__c lt, Decimal extralt, id qid, id qlid){
        lt.Quote__c=qid;
        lt.Quote_Line_Item__c=qlid;
        lt.Recommended_L_T__c = lt.L_T_weeks__c!=null? (lt.L_T_weeks__c+Num(extralt)) :null;
        lt.L_T_weeks__c = lt.Recommended_L_T__c;
        return lt;
    }
    //filter many qli relevant to specific qli
    public static List <Lead_Time_by_QTY_for_part_and_family__c> FilterManyRelevants(String typeofsale,Id prodid,String family, List <Lead_Time_by_QTY_for_part_and_family__c> relevants4many){
        List <Lead_Time_by_QTY_for_part_and_family__c> relevants = new List <Lead_Time_by_QTY_for_part_and_family__c>();
        //boolean found=false;
        for(Lead_Time_by_QTY_for_part_and_family__c rel:relevants4many)
            if(typeofsale==rel.type_of_sale__c&&prodid==rel.Product__c){
                relevants.add(rel);
                //found=true;
            }
        //if(!found){
            for(Lead_Time_by_QTY_for_part_and_family__c rel:relevants4many)
                if(typeofsale==rel.type_of_sale__c&&family==rel.Part_Family__c){
                    relevants.add(rel);
                }
        //}
        return relevants;
    }
    //insert new LTs on insert of qli
    public static void InsertNewLT(List <QuoteLineItem> qlis,List <Lead_Time_by_QTY_for_part_and_family__c> relevants4many,List <Extra_L_T_by_Elmo_Site__c> manyElmoSite){
        List <L_T_by_Quantity__c> lts4insert = new List <L_T_by_Quantity__c>();
        for(QuoteLineItem qli:qlis){
            Decimal extralt = qli.Product2.Extra_L_T__c;
            List <Lead_Time_by_QTY_for_part_and_family__c> relevants = FilterManyRelevants(qli.Quote.type_of_sale__c,qli.Product2.id,qli.Part_Family__c,relevants4many);
            System.debug('rel:'+relevants);
            List <Extra_L_T_by_Elmo_Site__c> elmoSite = FilterManyElmoSite(manyElmoSite,qli.Quote.ELMO_Site__c,qli.Quote.Shipment_Type__c);
            List <L_T_by_Quantity__c> lts  = new List <L_T_by_Quantity__c>();
            for(Lead_Time_by_QTY_for_part_and_family__c rel:relevants){
                L_T_by_Quantity__c lt = ConvertLT(rel);
                lt.Product_Extra_L_T__c = Num(extralt);
                lt.Elmo_Site_Extra_L_T__c = FindExtraElmo(elmosite,lt.L_T_weeks__c);
                Decimal ltextralt=lt.Product_Extra_L_T__c+lt.Elmo_Site_Extra_L_T__c;
                lts.add(TotalLT(lt,ltextralt,qli.Quoteid,qli.id));
            }
            System.debug('rel2:'+lts);
            if(relevants.size()==0){
                L_T_by_Quantity__c noF = ConvertLT(NoFound());
                noF.Product_Extra_L_T__c = Num(extralt);
                noF.Elmo_Site_Extra_L_T__c = FindExtraElmo(elmosite,noF.L_T_weeks__c);
                Decimal ltextralt=noF.Product_Extra_L_T__c+noF.Elmo_Site_Extra_L_T__c;
                lts.add(TotalLT(noF,ltextralt,qli.Quoteid,qli.id));
            }
            FilterDisplay(lts,qli.MDQ__c,qli.To_Quantity__c);
            lts4insert.addAll(lts);
        }
        insert lts4insert;
    }
    //when no lt found
    public static Lead_Time_by_QTY_for_part_and_family__c NoFound(){
        return new Lead_Time_by_QTY_for_part_and_family__c (To_Quantity__c=Decimal.valueof(Label.No_LeadTime_Details_Qty),L_T_weeks__c=Decimal.valueof(Label.No_LeadTime_Details_Weeks));
    }
    //check Display
    public static void FilterDisplay(List<L_T_by_Quantity__c> lts, Decimal MDQ, Decimal ToQuantity){
        Decimal lastone=0;
        for(L_T_by_Quantity__c lt:lts){
            if(MDQ!=null&&lt.Max_Delivery_Quantity__c!=null&&MDQ<=lt.Max_Delivery_Quantity__c)
                lt.Display__c=true;
            else if(MDQ==null)
                lt.Display__c=true;
            else
                lt.Display__c=false;
            if(lt.Display__c==true&&ToQuantity!=null&&ToQuantity<=lastone)
                lt.Display__c=false;
            lastone = lt.Max_Delivery_Quantity__c!=null?lt.Max_Delivery_Quantity__c:0;
        }
    }
    //convert relevant to lt child
    public static L_T_by_Quantity__c ConvertLT(Lead_Time_by_QTY_for_part_and_family__c rel){
        return new L_T_by_Quantity__c(Max_Delivery_Quantity__c=rel.To_Quantity__c,Orig_Max_Delivery_Quantity__c=rel.To_Quantity__c,L_T_weeks__c=rel.L_T_weeks__c,Recommended_L_T__c=rel.L_T_weeks__c);
    }
    //convert relevants to lt childs
    public static List<L_T_by_Quantity__c> ConvertLTS(List<Lead_Time_by_QTY_for_part_and_family__c> relevants){
        List <L_T_by_Quantity__c> lts = new List <L_T_by_Quantity__c>();
        for(Lead_Time_by_QTY_for_part_and_family__c rel:relevants)
            lts.add(ConvertLT(rel));
        return lts;    
    }
    //get relevants to one qli
    public static List <Lead_Time_by_QTY_for_part_and_family__c> getrelevantsLTs(String typeofsale, id prodid, String partfamily){
        List <Lead_Time_by_QTY_for_part_and_family__c> relevants = new List <Lead_Time_by_QTY_for_part_and_family__c>();
        relevants = getrelevantsLTsSet(new Set <String> {typeofsale}, new Set <id> {prodid}, new Set<String> {partfamily});
        /*boolean found=false;
        for(integer i=0;i<relevants.size();i++)
            if(relevants[i].Product__c==prodid){
                found=true;
                break;
            }
        if(found)
            for(integer i=0;i<relevants.size();i++)
                if(relevants[i].Part_Family__c==partfamily){
                    relevants.remove(i);
                    i--;
                }*/
        return relevants;
    }
    //get relevants to many qlis, needs to be filters after
    public static List <Lead_Time_by_QTY_for_part_and_family__c> getrelevantsLTsSet(Set <String> typeofsales, Set <id> prodids, Set<String> partfamilies){
        for(String s:typeofsales)
            if(s==null)
                s='insteadNull';
        for(String s:prodids)
            if(s==null)
                s='insteadNull';
        for(String s:partfamilies)
            if(s==null)
                s='insteadNull';
        return [select type_of_sale__c, Part_Family__c, Product__c, To_Quantity__c, L_T_weeks__c from Lead_Time_by_QTY_for_part_and_family__c Where To_Quantity__c!=null and L_T_weeks__c!=null and type_of_sale__c in:typeofsales and (Product__c in: prodids OR Part_Family__c in: partfamilies) Order by To_Quantity__c,L_T_weeks__c];
    }
    public PageReference RefreshQuote(){
        quoteid = ApexPages.currentPage().getParameters().get('id');
        if(quoteid!=null&&quoteid!=''){
            MarkQlis(quoteid);
            PageReference pg = new PageReference('/'+quoteid);
            pg.setRedirect(true);
            return pg;
        }
        else
            return null;
    }
    public static void MarkQlis(Id qid){
        List <QuoteLineItem> qlis = [select id,Update_Qli__c from QuoteLineItem where Quoteid=:qid];
        for(QuoteLineItem qli:qlis)
            qli.Update_Qli__c = qli.Update_Qli__c==true?false:true;
        update qlis;
    }
}
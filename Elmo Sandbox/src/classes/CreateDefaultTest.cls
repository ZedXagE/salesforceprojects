@isTest(seealldata=true)
public class CreateDefaultTest {
	static testMethod void tes() {
		Set <string> ausers = new Set <string>();
		for(User use:[select id from user where isActive=true])
			ausers.add(use.id);
		UserAccountTeamMember teama = [select id,TeamMemberRole,UserId,OpportunityAccessLevel,Ownerid from UserAccountTeamMember where ownerid in:ausers limit 1];
		UserTeamMember teamo = [select id,TeamMemberRole,UserId,OpportunityAccessLevel,Ownerid from UserTeamMember where ownerid in:ausers limit 1];
		Account acc = [select id from account limit 1];
		Opportunity opp1 = new Opportunity(Accountid=acc.id,name='test1',StageName='Samples',CloseDate=System.Today(),Ownerid=teama.Ownerid);
		Opportunity opp2 = new Opportunity(Accountid=acc.id,name='test2',StageName='Samples',CloseDate=System.Today(),Ownerid=teamo.Ownerid);
		insert opp1;
		insert opp2;
		ApexPages.StandardController cona = new ApexPages.StandardController(opp1);
		CreateDefaultTeamMembers crea = new CreateDefaultTeamMembers(cona);
		crea.curid=opp1.id;
		crea.pagename='adddefaultaccountteam';
		crea.init();
		crea.curid=opp2.id;
		crea.pagename='adddefaultopportunityteam';
		crea.init();
	}
}
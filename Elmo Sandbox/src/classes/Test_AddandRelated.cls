@isTest(seealldata=true)
public with sharing class Test_AddandRelated {
    static testMethod void testall1()
    {
        QuoteLineItem qli=[select id,Quoteid,Quantity,pricebookentryid from QuoteLineItem where quoteid!=null and pricebookentryid!=null and pricebookentry.UnitPrice!=null and Quote.Status != 'Approval flow' and Quote.Status != 'Draft' and PriceBookEntry.UnitPrice!=null and Quantity!=null limit 1];
        qli.Quantity=qli.Quantity+1;
        update qli;
        Quote qt=[select id from Quote where id=:qli.Quoteid];
        ApexPages.StandardController controller = new ApexPages.StandardController(qt);
        PageReference pgRef = Page.additems; //Create Page Reference - 'Appt_New' is the name of Page
        Test.setCurrentPage(pgRef); //Set the page for Test Method
        ApexPages.currentPage().getParameters().put('id', qt.id);//Pass Id to page
        ApexPages.currentPage().getParameters().put('curid', qli.pricebookentryid);//Pass Id to page
        addItemsController sc = new addItemsController(controller);
        sc.priceBookCheck();
        sc.gotoCustomMultiLine();
        sc.UnitChange();
        sc.ElmoChange();
        sc.QuanChange();
        sc.QuanChangeM();
        sc.QuanChangeMTrans();
        sc.UnitChangeTrans();
        sc.ElmoChangeTrans();
        sc.gotoQuote();
        sc.gotoNext();
        sc.gotoPrev();
        sc.sorting();
        sc.savestop();
        sc.saveOLIs();
        sc.byKey='test';
        sc.search();
        sc.curOpt='name';
        sc.search();
        sc.curOpt='code';
        sc.search();
        sc.curOpt='family';
        sc.search();
    }
    static testMethod void testall2(){
        QuoteLineItem qli=[select id,Quoteid,Quantity,pricebookentryid from QuoteLineItem where quoteid!=null and pricebookentryid!=null and pricebookentry.UnitPrice!=null and Quote.Status != 'Approval flow' and Quote.Status != 'Draft' and PriceBookEntry.UnitPrice!=null and Quantity!=null limit 1];
        qli.Quantity=qli.Quantity+1;
        update qli;
        Quote qt=[select id from Quote where id=:qli.Quoteid];
        ApexPages.StandardController controller = new ApexPages.StandardController(qt);
        PageReference pgRef = Page.customMultiLine; //Create Page Reference - 'Appt_New' is the name of Page
        Test.setCurrentPage(pgRef); //Set the page for Test Method
        ApexPages.currentPage().getParameters().put('id', qt.id);//Pass Id to page
        ApexPages.currentPage().getParameters().put('edit', 'all');//Pass Id to page
        ApexPages.currentPage().getParameters().put('curid', qli.pricebookentryid);//Pass Id to page
        addItemsController sc = new addItemsController(controller);
        
        pgRef = Page.quoteRelated; //Create Page Reference - 'Appt_New' is the name of Page
        Test.setCurrentPage(pgRef); //Set the page for Test Method
        ApexPages.currentPage().getParameters().put('id', qt.id);//Pass Id to page
        QuoteRelatedController qc = new QuoteRelatedController(controller);
        
        qt=[select id from Quote where Pricebook2id=null limit 1];
        controller = new ApexPages.StandardController(qt);
        pgRef = Page.additems; //Create Page Reference - 'Appt_New' is the name of Page
        Test.setCurrentPage(pgRef); //Set the page for Test Method
        ApexPages.currentPage().getParameters().put('id', qt.id);//Pass Id to page
        sc = new addItemsController(controller);
        sc.priceBookCheck();
    }
    static testMethod void testall3(){
        QuoteLineItem qli=[select id,Quoteid,Quantity,pricebookentryid from QuoteLineItem where quoteid!=null and pricebookentryid!=null and pricebookentry.UnitPrice!=null and Quote.Status != 'Approval flow' and Quote.Status != 'Draft' and PriceBookEntry.UnitPrice!=null and Quantity!=null limit 1];
        qli.Quantity=qli.Quantity+1;
        update qli;
        Quote qt=[select id from Quote where id=:qli.Quoteid];
        ApexPages.StandardController controller = new ApexPages.StandardController(qt);
        PageReference pgRef = Page.customMultiLine; //Create Page Reference - 'Appt_New' is the name of Page
        Test.setCurrentPage(pgRef); //Set the page for Test Method
        ApexPages.currentPage().getParameters().put('id', qt.id);//Pass Id to page
        ApexPages.currentPage().getParameters().put('edit', qli.id);//Pass Id to page
        ApexPages.currentPage().getParameters().put('curid', qli.pricebookentryid);//Pass Id to page
        addItemsController sc = new addItemsController(controller);
        sc.priceBookCheck();
        sc.gotoCustomMultiLine();
        sc.UnitChange();
        sc.ElmoChange();
        sc.QuanChange();
        sc.QuanChangeM();
        sc.QuanChangeMTrans();
        sc.UnitChangeTrans();
        sc.ElmoChangeTrans();
        sc.sorting();
        sc.saveOLIs();
    }
    static testMethod void LTtest()
    {
        QuoteLineItem qli=[select id,Quoteid,Quantity,pricebookentryid from QuoteLineItem where quoteid!=null and pricebookentryid!=null and pricebookentry.UnitPrice!=null and Quote.Status != 'Approval flow' and Quote.Status != 'Draft' and PriceBookEntry.UnitPrice!=null and Quantity!=null limit 1];
        /*PageReference pgRef = Page.LeadTimeByPF; //Create Page Reference - 'Appt_New' is the name of Page
        Test.setCurrentPage(pgRef); //Set the page for Test Method*/
        LeadTimeByPFController ltc = new LeadTimeByPFController();
        ltc.Curid = qli.id;
        ltc.cancelEdit();
        ltc.setEdit();
        ltc.deliverychange();
        ltc.addLt();
        ltc.curlts[ltc.curlts.size()-1].Max_Delivery_Quantity__c=100;
        ltc.deliverychange();
        ltc.SaveLTs();
        ltc.addLt();
        ltc.curlts[ltc.curlts.size()-1].Max_Delivery_Quantity__c=200;
        ltc.deliverychange();
        ltc.SaveLTs();
        ltc.lineid=ltc.curlts[ltc.curlts.size()-1].id;
        ltc.del();
        Quote qt = [select id,CurrencyIsoCode from Quote where Status='Draft' and Num_of_Order_Lines__c!=0 limit 1];
        try{
            qt.Status='Approval Flow';
            update qt;
        }
        catch(exception e){}
    }
}
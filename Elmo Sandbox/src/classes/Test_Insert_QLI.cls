@isTest(seeAllData=true)

private class Test_Insert_QLI {

static testMethod void Test_Insert_QLI() {

//get standard pricebook
Pricebook2  standardPb = [select id, name, isActive from Pricebook2 where IsStandard = true limit 1];

Opportunity opp1 = new Opportunity (Name='Opp1',StageName='Pilot',CloseDate=Date.today());
insert opp1;

Product2 prd1 = new Product2 (Name='Test Product Entry 1',Description='Test Product Entry 1');
insert prd1;

PricebookEntry stdpbe = new PriceBookEntry(Pricebook2Id=standardPb.id, Product2Id=prd1.id, UnitPrice=50, IsActive=true);
insert stdPbe;

OpportunityLineItem lineItem1 = new OpportunityLineItem (OpportunityID=opp1.id,PriceBookEntryID=stdPbe.Id, Quantity=10.0, UnitPrice=5.0, Warranty_months__c = 10);
insert lineItem1;
    
// This is a trail 

Quote Qu = new Quote (Name = '112345', OpportunityId = opp1.id, ELMO_Site__c= 'ELMO Israel', Pricebook2Id = standardPb.id, Price_Calculation__c= '1233444', 
                     Shipment_terms__c = 'Ex-Works Petah-Tikva, Israel');
insert Qu ;



// End of trail
   

Test.startTest();

//opp1.StageName='Production';
//update opp1;
//Opportunity Opp_Check = [select id, StageName from Opportunity where id =:opp1.Id ];
//System.assertEquals(Opp_Check.StageName,'Production');

//Qu.Name = '111222' ;
//update Qu ;
//Quote Qu_Check = [select Name from Quote where id = :Qu.id];
//System.assertEquals(Qu_Check.Name, '111222');

//QuoteLineItem QLI = new QuoteLineItem (QuoteId = Qu.Id, PriceBookEntryID=stdPbe.Id, Quantity = 10.0, UnitPrice = 5.0);
//insert QLI ;
    
//QLI.Quantity = 5 ;
//update QLI ;
//QuoteLineItem QLI_Check = [select Quantity from QuoteLineItem where id =:QLI.Id] ;
//System.assertEquals(QLI_Check.Quantity, 5) ;

    List <QuoteLineItem> QLIList = new List <QuoteLineItem> {} ;
        
        for(Integer i = 0 ; i < 10; i++){
        QuoteLineItem QLI = new QuoteLineItem(QuoteId = Qu.Id, PriceBookEntryID=stdPbe.Id, Quantity = 10.0, UnitPrice = 5.0,Description = '111' + i) ;
		QLIList.add(QLI) ;
        }

    
    insert QLIList ;
    
   ///List <QuoteLineItem> InsertedQLIList = [SELECT Id, Quantity, Description, Warranty_months__c
    ///                                       FROM QuoteLineItem
    ///                                       WHERE id IN :QLIList] ;
    
   /// for(QuoteLineItem QLI_Check : InsertedQLIList){
   ///   System.assertEquals(QLI_Check.Warranty_months__c, 10);
      //System.debug('112233') ;
    //}

    
    
    


Test.stopTest();
}
}
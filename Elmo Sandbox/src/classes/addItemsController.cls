public class addItemsController {
    @testVisible private Quote the_quote;
    @testVisible private Opportunity the_opp;
    @testVisible private Account the_acc;
    public String curid{get;set;}
    public String byKey{get;set;}
    public String curOpt{get;set;}
    public String isoCode{get;set;}
    public String transferIsoCode{get;set;}
    public String ord{get;set;}
    public String fname{get;set;}
    public integer offs{get;set;}
    public decimal recprice{get;set;}
    public decimal transrecprice{get;set;}
    public integer cursize{get;set;}
    public integer lim{get;set;}
    public String edt{get;set;}
    public Boolean saved {get;set;}
    public List <productSelection> prods {get;set;}
    public Map<ID, String> prodNames {get;set;}
    public Map<String, Boolean> fieldPerm {get;set;}
    public Map<ID, String> prodTables {get;set;}
    public Map<ID, String> prodExtras {get;set;}
    public Map<ID, String> transferprodTables {get;set;}
    public Map<ID, decimal> prodListPrice {get;set;}
    public Map<ID, decimal> transferprodListPrice {get;set;}
    public Map<String, List<Discount_by_QTY_for_part_family__c>> prodbyFamily {get;set;}
    public Map<String, List<Discount_by_QTY_for_part_family__c>> transferprodbyFamily {get;set;}
    public String pbName {get {return the_quote.PriceBook2 == null ? null : the_quote.PriceBook2.Name;}private set;}
    public List<QuoteLineItem> OLIs {get;set;}
    public class productSelection{
        public Boolean isSelected {get;set;}
        public PriceBookEntry pbe {get;set;}
        public Decimal transferListPrice {get;set;}
        public productSelection(PricebookEntry pbe,Boolean isSel){
            this.pbe = pbe;
            this.isSelected = isSel;
            this.transferListPrice = 0;
        }
    }
    public void initTransListPrice(set <id> prodids){
        for(PriceBookEntry pbe:[select id,Product2.id,UnitPrice from PriceBookEntry where Product2.id in: prodids and CurrencyIsoCode=:transferIsoCode])
            for(QuoteLineItem ql:OLIs)
                if(ql.Product2Id==pbe.Product2id)
                    ql.Transfer_List_Price__c=pbe.UnitPrice;
        for(QuoteLineItem ql:OLIs)
          if(ql.Transfer_List_Price__c==null)
            ql.Transfer_List_Price__c=0;
    }
    public addItemsController(ApexPages.StandardController con) {
        //records per page
        lim=10;
        saved=false;
        recprice=0;
        transrecprice=0;
        curOpt='def';
        //for test
        curid=ApexPages.currentPage().getParameters().get('curid');
        if(curid==null)
            curid='';
        //filter by default
        fname='Product2.Name';
        
        //initialize
        byKey='';
        offs=0;
        cursize=0;
        ord='ASC';
        if (prodbyFamily == null)
            prodbyFamily = New Map<String, List <Discount_by_QTY_for_part_family__c>>();
        if (transferprodbyFamily == null)
            transferprodbyFamily = New Map<String, List <Discount_by_QTY_for_part_family__c>>();
        if (prodNames == null)
            prodNames = New Map<ID, String>();
        if (prodTables == null)
            prodTables = New Map<ID, String>();
        if (prodExtras == null)
            prodExtras = New Map<ID, String>();
        if (transferprodTables == null)
            transferprodTables = New Map<ID, String>();
        if (prodListPrice == null)
            prodListPrice = New Map<ID, decimal>();
        if (transferprodListPrice == null)
            transferprodListPrice = New Map<ID, decimal>();
        if (OLIs == null)
            OLIs = New QuoteLineItem[]{};
                
        //field permission to show map
        fieldPerm=new Map<String, Boolean>();
        String type='QuoteLineItem';
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType leadSchema = schemaMap.get(type);
        Map<String, Schema.SObjectField> fieldMap = leadSchema.getDescribe().fields.getMap();
        for (String fieldName: fieldMap.keySet()) {
            System.debug(fieldMap.get(fieldName).getDescribe().isAccessible()+'##Field API Name='+fieldName);// list of all field API name
            fieldPerm.put(fieldName,fieldMap.get(fieldName).getDescribe().isAccessible());
        }
        //get edit--> all/one id
        edt=ApexPages.currentPage().getParameters().get('edit');
        //get data
        the_quote = [Select ID, Customer_Type_for_Transfer_Price__c,Customer_type_for_P_L__c,Transfer_Price_Currency__c,Opportunityid,Accountid,PriceBook2ID, PriceBook2.Name, type_of_sale__c,Shipment_Type__c,ELMO_Site__c, (Select PriceBookEntry.Product2ID, Product_Code__c From QuoteLineItems),Opportunity.CurrencyIsoCode From Quote Where ID = :con.getId()];
        isoCode=the_quote.Opportunity.CurrencyIsoCode;
        transferIsoCode=the_quote.Transfer_Price_Currency__c;
        if(the_quote.Opportunityid!=null)
            the_opp = [Select id from Opportunity where id=:the_quote.Opportunityid];
        if(the_quote.Accountid!=null)
            the_acc = [Select id from Account where id=:the_quote.Accountid];
        //search if no edt
        if(edt==null||edt=='')
            Search();
        //edit all load
        if(edt=='all')
        {
            set <id> prodids=new set <id>();
            set <String> prodfamilies=new set <String>();
            List <QuoteLineItem> qlis = [select id,Product2.id,Product2.Extra_L_T__c,Quote.type_of_sale__c,Quote.ELMO_Site__c,Quote.Shipment_Type__c,Part_family__c,Quote.Name,Customer_type__c,LineNumber,Product2Id,Transfer_List_Price__c,Transfer_Unit_Price__c,Transfer_Discount_Elmo__c,Transfer_Discount_From_List__c,ListPrice,Discount_from_List__c,Elmo_list_price__c,GENERAL_Part_Number__c,Check_Quote_Min__c,Item_Description_For_GENERAL__c,HasSchedule,Warranty_months__c,comments__c,Commission__c,Description,Discount,ServiceDate,Product2.Name,TotalPrice,PricebookEntryId,UnitPrice,QuoteID,Discount_Elmo__c,Quantity,To_Quantity__c,Mdq__c,Lead_Time_Weeks__C,Discount_list_ID__c from QuoteLineItem where QuoteID=:the_quote.id];
            prodExtras = LeadTimeByPFController.getQuoteHover(qlis);
            for (QuoteLineItem ql : qlis)
            {
                prodids.add(ql.Product2id);
                prodfamilies.add(ql.Part_family__c);
                OLIs.add(ql);
                prodNames.put(ql.PricebookEntryId,ql.Product2.Name);
            }
            initTransListPrice(prodids);
            //load hover table
            List <Discount_by_QTY_for_part_family__c> Ds=[select Id,name,Customer_type__c, discount_precent__c,Part_family__c, Qantity__c, To_Quantity__c, Product__c, CurrencyIsoCode from Discount_by_QTY_for_part_family__c where (Customer_type__c=:the_quote.Customer_type_for_P_L__c OR Customer_type__c=:the_quote.Customer_Type_for_Transfer_Price__c) AND Part_family__c in:prodfamilies AND ((Product__c in:prodids AND (CurrencyIsoCode=:isoCode OR CurrencyIsoCode=:transferIsoCode)) OR (Product__c=null)) Order By Qantity__c];
            for(QuoteLineItem ql:OLIs)
            {
                List <Discount_by_QTY_for_part_family__c> Df=new List <Discount_by_QTY_for_part_family__c>();
                List <Discount_by_QTY_for_part_family__c> transferDf=new List <Discount_by_QTY_for_part_family__c>();
                String tab='';
                String transfertab='';
                for(Discount_by_QTY_for_part_family__c d:Ds)
                {
                    if(the_quote.Customer_type_for_P_L__c==D.Customer_type__c&&ql.Part_family__c==D.Part_family__c&&((D.Product__c==null)||(D.Product__c==ql.Product2id&&D.CurrencyIsoCode==isoCode)))
                    {
                        Df.add(d);
                        Decimal curprice=ql.ListPrice-((ql.ListPrice/100)*(D.discount_precent__c.setscale(1)));
                        curprice=curprice.round();
                        if(D.Qantity__c==1000)
                            tab+='From-Qty: <b>'+D.Qantity__c+'</b>, Disc: '+(D.discount_precent__c.setscale(1))+'%, Price: <b>'+isoCode+' '+curprice+'</b>\n<br/>';
                        else
                            tab+='From-Qty: '+D.Qantity__c+', Disc: '+(D.discount_precent__c.setscale(1))+'%, Price: <b>'+isoCode+' '+curprice+'</b>\n<br/>';
                                
                       	if(ql.Quantity>=D.Qantity__c&&ql.Quantity<=D.To_Quantity__c)
                    	{
                            ql.Discount_from_List__c=(D.discount_precent__c.setscale(1));
                            ql.Discount_list_ID__c=D.id;
                        }
                    }
                    

                    if(the_quote.Customer_Type_for_Transfer_Price__c==D.Customer_type__c&&ql.Part_family__c==D.Part_family__c&&((D.Product__c==null)||(D.Product__c==ql.Product2id&&D.CurrencyIsoCode==transferIsoCode)))
                    {
                        transferDf.add(d);
                        Decimal curprice=ql.Transfer_List_Price__c-((ql.Transfer_List_Price__c/100)*(D.discount_precent__c.setscale(1)));
                        curprice=curprice.round();
                        if(D.Qantity__c==1000)
                            transfertab+='From-Qty: <b>'+D.Qantity__c+'</b>, Disc: '+(D.discount_precent__c.setscale(1))+'%, Price: <b>'+transferisoCode+' '+curprice+'</b>\n<br/>';
                        else
                            transfertab+='From-Qty: '+D.Qantity__c+', Disc: '+(D.discount_precent__c.setscale(1))+'%, Price: <b>'+transferisoCode+' '+curprice+'</b>\n<br/>';
                        if(ql.Quantity>=D.Qantity__c&&ql.Quantity<=D.To_Quantity__c)
                            ql.Transfer_Discount_From_List__c=(D.discount_precent__c.setscale(1));
                    }

                }
                if(tab=='')
                    tab='There is no price list';
                if(transfertab=='')
                    transfertab='There is no price list';
                prodbyFamily.put(ql.PricebookEntryId,Df);
                prodTables.put(ql.PricebookEntryId,tab);
                transferprodbyFamily.put(ql.PricebookEntryId,transferDf);
                transferprodTables.put(ql.PricebookEntryId,transfertab);
            }
        }
        //edit single load
        if(edt!=null&&edt!=''&&edt!='all')
        {
            QuoteLineItem ql = [select id,Product2.id,Product2.Extra_L_T__c,Quote.type_of_sale__c,Quote.ELMO_Site__c,Quote.Shipment_Type__c,Part_family__c,Quote.Name,Customer_type__c,LineNumber,Product2Id,Transfer_List_Price__c,Transfer_Unit_Price__c,Transfer_Discount_Elmo__c,Transfer_Discount_From_List__c,ListPrice,Discount_from_List__c,Elmo_list_price__c,GENERAL_Part_Number__c,Check_Quote_Min__c,Item_Description_For_GENERAL__c,HasSchedule,Warranty_months__c,comments__c,Commission__c,Description,Discount,ServiceDate,Product2.Name,TotalPrice,PricebookEntryId,UnitPrice,QuoteID,Discount_Elmo__c,Quantity,To_Quantity__c,Mdq__c,Lead_Time_Weeks__C,Discount_list_ID__c from QuoteLineItem where QuoteID=:the_quote.id AND id=:edt];
            prodExtras = LeadTimeByPFController.getQuoteHover(new List <QuoteLineitem>{ql});
            OLIs.add(ql);
            initTransListPrice(new Set<id>{ql.Product2id});
            prodNames.put(ql.PricebookEntryId,ql.Product2.Name);
            List <Discount_by_QTY_for_part_family__c> Ds=[select Id,name,Customer_type__c, discount_precent__c,Part_family__c, Qantity__c, To_Quantity__c, Product__c, CurrencyIsoCode from Discount_by_QTY_for_part_family__c where (Customer_type__c=:the_quote.Customer_type_for_P_L__c OR Customer_type__c=:the_quote.Customer_Type_for_Transfer_Price__c) AND Part_family__c=:ql.Part_family__c AND ((Product__c =:ql.Product2Id AND (CurrencyIsoCode=:isoCode OR CurrencyIsoCode=:transferIsoCode)) OR (Product__c=null)) Order By Qantity__c];
            List <Discount_by_QTY_for_part_family__c> Df=new List <Discount_by_QTY_for_part_family__c>();
            List <Discount_by_QTY_for_part_family__c> transferDf=new List <Discount_by_QTY_for_part_family__c>();
            String tab='';
            String transfertab='';
            for(Discount_by_QTY_for_part_family__c d:Ds)
            {
              if(the_quote.Customer_type_for_P_L__c==D.Customer_type__c&&((D.Product__c==null)||(D.Product__c==ql.Product2id&&D.CurrencyIsoCode==IsoCode)))
              {
                DF.add(d);
                Decimal curprice=ql.ListPrice-((ql.ListPrice/100)*(D.discount_precent__c.setscale(1)));
                curprice=curprice.round();
                if(D.Qantity__c==1000)
                    tab+='From-Qty: <b>'+D.Qantity__c+'</b>, Disc: '+(D.discount_precent__c.setscale(1))+'%, Price: <b>'+isoCode+' '+curprice+'</b>\n<br/>';
                else
                    tab+='From-Qty: '+D.Qantity__c+', Disc: '+(D.discount_precent__c.setscale(1))+'%, Price: <b>'+isoCode+' '+curprice+'</b>\n<br/>';
                if(ql.Quantity>=D.Qantity__c&&ql.Quantity<=D.To_Quantity__c)
                {
                    ql.Discount_from_List__c=(D.discount_precent__c.setscale(1));
                    ql.Discount_list_ID__c=D.id;
                }
              }
              if(the_quote.Customer_Type_for_Transfer_Price__c==D.Customer_type__c&&((D.Product__c==null)||(D.Product__c==ql.Product2id&&D.CurrencyIsoCode==transferIsoCode)))
              {
                transferDF.add(d);
                Decimal curprice=ql.Transfer_List_Price__c-((ql.Transfer_List_Price__c/100)*(D.discount_precent__c.setscale(1)));
                curprice=curprice.round();
                if(D.Qantity__c==1000)
                    transfertab+='From-Qty: <b>'+D.Qantity__c+'</b>, Disc: '+(D.discount_precent__c.setscale(1))+'%, Price: <b>'+transferisoCode+' '+curprice+'</b>\n<br/>';
                else
                    transfertab+='From-Qty: '+D.Qantity__c+', Disc: '+(D.discount_precent__c.setscale(1))+'%, Price: <b>'+transferisoCode+' '+curprice+'</b>\n<br/>';
                if(ql.Quantity>=D.Qantity__c&&ql.Quantity<=D.To_Quantity__c)
                    ql.Transfer_Discount_From_List__c=(D.discount_precent__c.setscale(1));
              }
            }
            if(tab=='')
                    tab='There is no price list';
            if(transfertab=='')
                transfertab='There is no price list';
            prodbyFamily.put(ql.PricebookEntryId,Df);
            prodTables.put(ql.PricebookEntryId,tab);
            transferprodbyFamily.put(ql.PricebookEntryId,transferDf);
            transferprodTables.put(ql.PricebookEntryId,transfertab);
        }
    }
    //search function
    public PageReference Search() {
        prods = new list <productSelection>();
        String temp=byKey;
        byKey=byKey.replace('*','%');
        if(byKey.left(1)!='%'&&byKey!='')
            byKey='%'+byKey+'%';
        if(byKey.left(1)=='%'&&byKey!='')
            byKey=byKey+'%';
        List <PriceBookEntry> pbes = new list <PriceBookEntry>();
        String curpricebook=the_quote.PriceBook2ID;
        String query='Select Product2.id,UnitPrice,Product2.Supply_status__c,Product2.Name,ProductCode,Product2.Family,CurrencyIsoCode,Product2.Export_License__c,Product2.Extra_L_T__c From PriceBookEntry Where PriceBook2ID =:curpricebook AND isActive = true AND CurrencyIsoCode=:isoCode';
        if(byKey!='')
        {
            //search none
            if(curOpt=='def')
                query+=' AND (Product2.Name LIKE :byKey OR Product2.Family LIKE :byKey OR ProductCode LIKE :byKey OR Product2.Supply_status__c LIKE :byKey)';
            
            //search name
            if(curOpt=='name')
                query+=' AND Product2.Name LIKE :byKey';
            
            //search family
            if(curOpt=='family')
                query+=' AND Product2.Family LIKE :byKey';
            
            //search code
            if(curOpt=='code')
                query+=' AND ProductCode LIKE :byKey';
            
            //search supply status
            if(curOpt=='supply_status')
                query+=' AND Product2.Supply_status__c LIKE :byKey';        
        }
        query+=' Order By '+fname+' '+ord+' Limit '+lim+' OFFSET '+offs;
        pbes = Database.query(query);
        byKey=temp;
        for(PriceBookEntry pbe : pbes)
            prods.add(New productSelection(pbe,false));
        if (prods.isEmpty())
            apexPages.addMessage(New ApexPages.Message(ApexPages.Severity.FATAL, 'Unable to find active products'));
        cursize=prods.size();
        return null;
    }
    //checks what pricebook is checked if none send to choose one
    public PageReference priceBookCheck() {
        if (pbName == null) {
            PageReference pr = New Pagereference('/_ui/sales/quote/lineitem/ChooseQuotePricebook/e');
            pr.getParameters().put('id', the_quote.id);
            pr.getParameters().put('retURL', '/' + the_quote.id);
            pr.getParameters().put('saveURL', '/apex/addItems?id=' + the_quote.id + '&retURL=' + EncodingUtil.urlEncode('/apex/addItems?id=' + the_quote.id + '&retURL=/' + the_quote.id, 'UTF-8'));
            pr.setRedirect(true);
            return pr;
        } else {
            return null;
        }
    }
    public PageReference savestop() {
        saved=true;
        return null;
    }
    //go from prodycts table to save selected product page
    public PageReference gotoCustomMultiLine() {
        if(edt==null||edt=='')
        {
            set <id> prodids=new set <id>();
            set <String> prodfamilies=new set <String>();

            for (productSelection ps : prods)
            {
                if(ps.isSelected==TRUE)
                {
                    OLIs.add(New QuoteLineItem(QuoteID = the_quote.id,Quantity=0,UnitPrice = 0,Discount_Elmo__c = 0,PriceBookEntryID = ps.pbe.id,Transfer_Unit_Price__c = 0,Transfer_Discount_Elmo__c = 0));
                    //Add names to Id map to display on page
                    prodids.add(ps.pbe.Product2id);
                    prodfamilies.add(ps.pbe.Product2.Family);
                    prodNames.put(ps.pbe.id, ps.pbe.Product2.Name);
                    prodListPrice.put(ps.pbe.id, ps.pbe.UnitPrice);
                    transferprodListPrice.put(ps.pbe.id, 0);
                }
            }
            for(PriceBookEntry pbe:[select id,Product2.id,UnitPrice from PriceBookEntry where Product2.id in: prodids and CurrencyIsoCode=:transferIsoCode])
              for (productSelection ps : prods)
              {
                  if(ps.isSelected==TRUE&&pbe.Product2.id==ps.pbe.Product2Id)
                  {
                    ps.transferListPrice=pbe.UnitPrice;
                    transferprodListPrice.put(ps.pbe.id, pbe.UnitPrice);
                  }
              }
            List <Discount_by_QTY_for_part_family__c> Ds=[select Id,name,Customer_type__c, discount_precent__c,Part_family__c, Qantity__c, To_Quantity__c, Product__c, CurrencyIsoCode from Discount_by_QTY_for_part_family__c where (Customer_type__c=:the_quote.Customer_type_for_P_L__c OR Customer_type__c=:the_quote.Customer_Type_for_Transfer_Price__c) AND Part_family__c in:prodfamilies AND ((Product__c in:prodids AND (CurrencyIsoCode=:isoCode OR CurrencyIsoCode=:transferIsoCode)) OR (Product__c=null)) Order By Qantity__c];
            List <PriceBookEntry> checkedpbes = new List <PriceBookEntry>();
            for(productSelection ps:prods)
            {
                if(ps.isSelected==TRUE)
                {
                    checkedpbes.add(ps.pbe);
                    List <Discount_by_QTY_for_part_family__c> Df=new List <Discount_by_QTY_for_part_family__c>();
                    List <Discount_by_QTY_for_part_family__c> transferDf=new List <Discount_by_QTY_for_part_family__c>();
                    String tab='';
                    String transfertab='';
                    for(Discount_by_QTY_for_part_family__c d:Ds)
                    {
                        if(the_quote.Customer_type_for_P_L__c==D.Customer_type__c&&ps.pbe.Product2.Family==D.Part_family__c&&((D.Product__c==null)||(D.Product__c==ps.pbe.Product2.id&&D.CurrencyIsoCode==isoCode)))
                        {
                            Df.add(d);
                            Decimal curprice=ps.pbe.UnitPrice-((ps.pbe.UnitPrice/100)*(D.discount_precent__c.setscale(1)));
                            curprice=curprice.round();
                            if(D.Qantity__c==1000)
                                tab+='From-Qty: <b>'+D.Qantity__c+'</b>, Disc: '+(D.discount_precent__c.setscale(1))+'%, Price: <b>'+isoCode+' '+curprice+'</b>\n<br/>';
                            else
                                tab+='From-Qty: '+D.Qantity__c+', Disc: '+(D.discount_precent__c.setscale(1))+'%, Price: <b>'+isoCode+' '+curprice+'</b>\n<br/>';
                        }

                        if(the_quote.Customer_Type_for_Transfer_Price__c==D.Customer_type__c&&ps.pbe.Product2.Family==D.Part_family__c&&((D.Product__c==null)||(D.Product__c==ps.pbe.Product2.id&&D.CurrencyIsoCode==transferisoCode)))
                        {
                            transferDf.add(d);
                            Decimal curprice=ps.transferListPrice-((ps.transferListPrice/100)*(D.discount_precent__c.setscale(1)));
                            curprice=curprice.round();
                            if(D.Qantity__c==1000)
                                transfertab+='From-Qty: <b>'+D.Qantity__c+'</b>, Disc: '+(D.discount_precent__c.setscale(1))+'%, Price: <b>'+transferisoCode+' '+curprice+'</b>\n<br/>';
                            else
                                transfertab+='From-Qty: '+D.Qantity__c+', Disc: '+(D.discount_precent__c.setscale(1))+'%, Price: <b>'+transferisoCode+' '+curprice+'</b>\n<br/>';
                        }
                    }
                    if(tab=='')
                        tab='There is no price list';
                    if(transfertab=='')
                        transfertab='There is no price list';
                    prodbyFamily.put(ps.pbe.id,Df);
                    prodTables.put(ps.pbe.id,tab);
                    transferprodbyFamily.put(ps.pbe.id,transferDf);
                    transferprodTables.put(ps.pbe.id,transfertab);
                }
            }
            map <id,String> newprodExtras = LeadTimeByPFController.getQuoteHover(checkedpbes,the_quote.ELMO_Site__c,the_quote.Shipment_Type__c,the_quote.type_of_sale__c);
            for(id k:newprodExtras.keySet())
                prodExtras.put(k,newprodExtras.get(k));
        }
        PageReference pr = Page.customMultiLine;
        pr.getParameters().put('id', the_quote.id);
        pr.setRedirect(false);
        return pr;
    }
    //unit change
    public pageReference UnitChange() {
        for (QuoteLineItem ql:OLIs)
        {
            if(ql.PricebookEntryId==curid&&ql.UnitPrice!=0&&ql.UnitPrice!=null&&ql.ListPrice!=0&&edt!=null&&edt!='')
                ql.Discount_Elmo__c=(100-ql.UnitPrice/(ql.ListPrice/100));
            else
            {
                if(ql.PricebookEntryId==curid&&ql.UnitPrice!=0&&ql.UnitPrice!=null&&prodListPrice.get(ql.PriceBookEntryID)!=0&&(edt==null||edt==''))
                    ql.Discount_Elmo__c=(100-ql.UnitPrice/(prodListPrice.get(ql.PriceBookEntryID)/100));
            }
        }
        saved=false;
        return null;
    }

    //unit change tranfer
    public pageReference UnitChangeTrans() {
        for (QuoteLineItem ql:OLIs)
        {
            if(ql.PricebookEntryId==curid&&ql.Transfer_Unit_Price__c!=0&&ql.Transfer_Unit_Price__c!=null&&ql.Transfer_List_Price__c!=0&&edt!=null&&edt!='')
                ql.Transfer_Discount_Elmo__c=(100-ql.Transfer_Unit_Price__c/(ql.Transfer_List_Price__c/100));
            else
            {
                if(ql.PricebookEntryId==curid&&ql.Transfer_Unit_Price__c!=0&&ql.Transfer_Unit_Price__c!=null&&transferprodListPrice.get(ql.PriceBookEntryID)!=0&&(edt==null||edt==''))
                    ql.Transfer_Discount_Elmo__c=(100-ql.Transfer_Unit_Price__c/(transferprodListPrice.get(ql.PriceBookEntryID)/100));
            }
        }
        saved=false;
        return null;
    }
    //elmo discount change
    public pageReference ElmoChange() {
        for (QuoteLineItem ql:OLIs)
        {
            if(ql.PricebookEntryId==curid&&ql.Discount_Elmo__c!=null&&edt!=null&&edt!='')
            {
                ql.UnitPrice=ql.ListPrice-((ql.ListPrice/100)*ql.Discount_Elmo__c);
                UnitChange();
            }
            else
            {
                if(ql.PricebookEntryId==curid&&ql.Discount_Elmo__c!=null&&(edt==null||edt==''))
                {
                    ql.UnitPrice=prodListPrice.get(ql.PriceBookEntryID)-((prodListPrice.get(ql.PriceBookEntryID)/100)*ql.Discount_Elmo__c);
                    UnitChange();
                }
            }
        }
        saved=false;
        return null;
    }
    //elmo discount change Transfer
    public pageReference ElmoChangeTrans() {
        for (QuoteLineItem ql:OLIs)
        {
            if(ql.PricebookEntryId==curid&&ql.Transfer_Discount_Elmo__c!=null&&edt!=null&&edt!='')
            {
                ql.Transfer_Unit_Price__c=ql.Transfer_List_Price__c-((ql.Transfer_List_Price__c/100)*ql.Transfer_Discount_Elmo__c);
                UnitChange();
            }
            else
            {
                if(ql.PricebookEntryId==curid&&ql.Transfer_Discount_Elmo__c!=null&&(edt==null||edt==''))
                {
                    ql.Transfer_Unit_Price__c=transferprodListPrice.get(ql.PriceBookEntryID)-((transferprodListPrice.get(ql.PriceBookEntryID)/100)*ql.Transfer_Discount_Elmo__c);
                    UnitChange();
                }
            }
        }
        saved=false;
        return null;
    }
    //quantity change
    public pageReference QuanChange() {
        for (QuoteLineItem ql:OLIs)
        {
            decimal tempunit=ql.unitprice;   
            decimal temptransunit=ql.Transfer_Unit_Price__c;
            if(ql.PricebookEntryId==curid)
            {
                if(edt!=null&&edt!=''){
                    recprice=ql.ListPrice;
                    transrecprice=ql.Transfer_List_Price__c;
                }
                else{
                    recprice=prodListPrice.get(ql.PriceBookEntryID);
                    transrecprice=transferprodListPrice.get(ql.PriceBookEntryID);
                }
                ql.Discount_from_List__c=0;
                ql.Transfer_Discount_From_List__c=0;
                ql.Discount_list_ID__c=null;
                if(tempunit==0)
                {
                    ql.UnitPrice=recPrice;
                    UnitChange();
                }
                if(temptransunit==0)
                {
                    ql.Transfer_Unit_Price__c=transrecPrice;
                    UnitChangeTrans();
                }
                for(Discount_by_QTY_for_part_family__c Ds:prodbyFamily.get(curid))
                {
                    if(Ds.Qantity__c <= ql.Quantity && Ds.To_Quantity__c >= ql.Quantity&&edt!=null&&edt!='')
                    {
                        recPrice=ql.ListPrice-((ql.ListPrice/100)*Ds.discount_precent__c.setScale(1));
                        recPrice=recPrice.Round();
                        ql.Discount_from_List__c=Ds.discount_precent__c.setScale(1);
                        ql.Discount_list_ID__c=Ds.id;
                        if(tempunit==0)
                        {
                            ql.UnitPrice=recPrice;
                            UnitChange();
                        }
                        if(Ds.Product__c!=null)
                            break;
                    }
                    else
                    {
                        if(Ds.Qantity__c <= ql.Quantity && Ds.To_Quantity__c >= ql.Quantity&&(edt==null||edt==''))
                        {
                            recPrice=prodListPrice.get(ql.PriceBookEntryID)-((prodListPrice.get(ql.PriceBookEntryID)/100)*Ds.discount_precent__c.setScale(1));
                            recPrice=recPrice.Round();
                            ql.Discount_from_List__c=Ds.discount_precent__c.setScale(1);
                            ql.Discount_list_ID__c=Ds.id;
                            if(tempunit==0)
                            {
                                ql.UnitPrice=recPrice;
                                UnitChange();
                            }
                            if(Ds.Product__c!=null)
                                break;
                        }
                    }
                }

                for(Discount_by_QTY_for_part_family__c Ds:transferprodbyFamily.get(curid))
                {
                    if(Ds.Qantity__c <= ql.Quantity && Ds.To_Quantity__c >= ql.Quantity&&edt!=null&&edt!='')
                    {
                        transrecPrice=ql.Transfer_List_Price__c-((ql.Transfer_List_Price__c/100)*Ds.discount_precent__c.setScale(1));
                        transrecPrice=transrecPrice.Round();
                        ql.Transfer_Discount_From_List__c=Ds.discount_precent__c.setScale(1);
                        if(temptransunit==0)
                        {
                            ql.Transfer_Unit_Price__c=transrecPrice;
                            UnitChangeTrans();
                        }
                        if(Ds.Product__c!=null)
                            break;
                    }
                    else
                    {
                        if(Ds.Qantity__c <= ql.Quantity && Ds.To_Quantity__c >= ql.Quantity&&(edt==null||edt==''))
                        {
                            transrecPrice=transferprodListPrice.get(ql.PriceBookEntryID)-((transferprodListPrice.get(ql.PriceBookEntryID)/100)*Ds.discount_precent__c.setScale(1));
                            transrecPrice=transrecPrice.Round();
                            ql.Transfer_Discount_From_List__c=Ds.discount_precent__c.setScale(1);
                            if(temptransunit==0)
                            {
                                ql.Transfer_Unit_Price__c=transrecPrice;
                                UnitChangeTrans();
                            }
                            if(Ds.Product__c!=null)
                                break;
                        }
                    }
                }
            }
        }
        saved=false;
        return null;
    }
    //Manual Click on recommended
    public pageReference QuanChangeM() {
        for (QuoteLineItem ql:OLIs)
        { 
            if(ql.PricebookEntryId==curid)
            {
                if(edt!=null&&edt!='')
                    recprice=ql.ListPrice;
                else
                    recprice=prodListPrice.get(ql.PriceBookEntryID);
                ql.Discount_from_List__c=0;
                ql.Discount_list_ID__c=null;
                ql.UnitPrice=recPrice;
                UnitChange();
                for(Discount_by_QTY_for_part_family__c Ds:prodbyFamily.get(curid))
                {
                    if(Ds.Qantity__c <= ql.Quantity && Ds.To_Quantity__c >= ql.Quantity&&edt!=null&&edt!='')
                    {
                        recPrice=ql.ListPrice-((ql.ListPrice/100)*Ds.discount_precent__c.setScale(1));
                        recPrice=recPrice.Round();
                        ql.Discount_from_List__c=Ds.discount_precent__c.setScale(1);
                        ql.Discount_list_ID__c=Ds.id;
                        ql.UnitPrice=recPrice;
                        UnitChange();
                        if(Ds.Product__c!=null)
                            break;
                    }
                    else
                    {
                        if(Ds.Qantity__c <= ql.Quantity && Ds.To_Quantity__c >= ql.Quantity&&(edt==null||edt==''))
                        {
                            recPrice=prodListPrice.get(ql.PriceBookEntryID)-((prodListPrice.get(ql.PriceBookEntryID)/100)*Ds.discount_precent__c.setScale(1));
                            recPrice=recPrice.Round();
                            ql.Discount_from_List__c=Ds.discount_precent__c.setScale(1);
                            ql.Discount_list_ID__c=Ds.id;
                            ql.UnitPrice=recPrice;
                            UnitChange();
                            if(Ds.Product__c!=null)
                                break;
                        }
                    }
                }
              ql.Discount_Elmo__c=ql.Discount_Elmo__c.setScale(1);
            }
        }
        saved=false;
        return null;
    }

    //Manual Click on recommended transfer
    public pageReference QuanChangeMTrans() {
        for (QuoteLineItem ql:OLIs)
        { 
            if(ql.PricebookEntryId==curid)
            {
                if(edt!=null&&edt!='')
                    transrecprice=ql.Transfer_List_Price__c;
                else
                    transrecprice=transferprodListPrice.get(ql.PriceBookEntryID);
                ql.Transfer_Discount_From_List__c=0;
                ql.Transfer_Unit_Price__c=transrecprice;
                UnitChangeTrans();

                for(Discount_by_QTY_for_part_family__c Ds:transferprodbyFamily.get(curid))
                {
                    if(Ds.Qantity__c <= ql.Quantity && Ds.To_Quantity__c >= ql.Quantity&&edt!=null&&edt!='')
                    {
                        transrecPrice=ql.Transfer_List_Price__c-((ql.Transfer_List_Price__c/100)*Ds.discount_precent__c.setScale(1));
                        transrecPrice=transrecPrice.Round();
                        ql.Transfer_Discount_From_List__c=Ds.discount_precent__c.setScale(1);
                        ql.Transfer_Unit_Price__c=transrecPrice;
                        UnitChangeTrans();
                        if(Ds.Product__c!=null)
                            break;
                    }
                    else
                    {
                        if(Ds.Qantity__c <= ql.Quantity && Ds.To_Quantity__c >= ql.Quantity&&(edt==null||edt==''))
                        {
                            transrecPrice=transferprodListPrice.get(ql.PriceBookEntryID)-((transferprodListPrice.get(ql.PriceBookEntryID)/100)*Ds.discount_precent__c.setScale(1));
                            transrecPrice=transrecPrice.Round();
                            ql.Transfer_Discount_From_List__c=Ds.discount_precent__c.setScale(1);
                            ql.Transfer_Unit_Price__c=transrecPrice;
                            UnitChangeTrans();
                            if(Ds.Product__c!=null)
                                break;
                        }
                    }
                }
                ql.Transfer_Discount_Elmo__c=ql.Transfer_Discount_Elmo__c!=null?ql.Transfer_Discount_Elmo__c.setScale(1):null;
            }
        }
        saved=false;
        return null;
    }

    //go to Quote page
    public pageReference gotoQuote() {
        String ur='';
        if(edt!=null&&edt!=''&&edt!='all')
            ur='/' + edt;
        else
            ur='/' + the_quote.id;
        PageReference pr = New PageReference(ur);
        return pr;
    }
    //load next product page
    public pageReference gotoNext() {
        offs=offs+lim;
        search();
        return null;
    }
    //load previous product page
    public pageReference gotoPrev() {
        if(offs!=0)
            offs=offs-lim;
        search();
        return null;
    }
    //sort table by new filter name
    public PageReference sorting()
    {
        if(ord=='ASC')
            ord='DESC';
        else
            ord='ASC';
        Search();
        return null;
    }
    //save all new quote line items
    public pageReference saveOLIs() {
        try {
            update the_acc;
        } catch (DMLException e) {
            apexPages.addMessage(New ApexPages.Message(ApexPages.Severity.FATAL, e.getDmlMessage(0)));
            return null;
        }
        try {
            update the_opp;
        } catch (DMLException e) {
            apexPages.addMessage(New ApexPages.Message(ApexPages.Severity.FATAL, e.getDmlMessage(0)));
            return null;
        }
        try {
            update the_quote;
        } catch (DMLException e) {
            apexPages.addMessage(New ApexPages.Message(ApexPages.Severity.FATAL, e.getDmlMessage(0)));
            return null;
        }
        
        for (QuoteLineItem oli : OLIs) {
            if (oli.ServiceDate == null)
                oli.ServiceDate = date.today();
        }
        
        try {
            upsert OLIs;
        } catch (DMLException e) {
            apexPages.addMessage(New ApexPages.Message(ApexPages.Severity.FATAL, e.getDmlMessage(0)));
            return null;
        }
        
        return gotoQuote();
    }
    
}
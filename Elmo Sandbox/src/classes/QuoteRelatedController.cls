public class QuoteRelatedController {
    public id Curid{get;set;}
    public integer nums{get;set;}
    public string passedid{get;set;}
    public string firstLink{get;set;}
    public string secondLink{get;set;}
    public string thirdLink{get;set;}
    public string fourthLink{get;set;}
    public string isoCode{get;set;}
    public string sobjectname{get;set;}
    public string fields{get;set;}
    public string addfields{get;set;}
    public string ShowComp{get;set;}

    public Quote the_quote{get;set;}
    public List<string> fieldname{get;set;}
    public Map<ID, String> prodTables {get;set;}
    public Map<ID, String> prodExtras {get;set;}
    public	list <QuoteLineItem> sobs {get;set;}
    public	map <String,String> fieldLabels {get;set;}
    
    public QuoteRelatedController(ApexPages.StandardController controller) {
        //added for lt
        ShowComp='false';

        //initialize
        Sobjectname='QuoteLineItem';
        addfields='Product2.id,Product2.Extra_L_T__c,Quote.type_of_sale__c,Quote.ELMO_Site__c,Quote.Shipment_Type__c,'/*  Part_family__c */;
        //fields to show on table with this order
        fields='Product2.Name,ListPrice,UnitPrice,Discount_from_List__c,Discount_Elmo__c,Lead_Time_Weeks__C,Quantity,To_Quantity__c,Mdq__c,Ready_for_Approval_Logo__c,Approval_notes__c,Comments__c,GENERAL_Part_Number__c,Product2.Supply_status__c';
        fields=fields.tolowercase();
        fieldname=fields.split(',');
        //link data
        firstLink='product2.name';
        secondLink='listprice';
        thirdLink='unitprice';
        fourthLink='product2.supply_status__c';
        
        for(integer i=0;i<fieldname.size();i++)
        {
            if(fieldname[i]==firstLink||fieldname[i]==secondLink||fieldname[i]==thirdLink||fieldname[i]==fourthLink)
            {
                fieldname.remove(i);
                i--;
            }
        }
        fieldLabels=new map<string,string>();
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType leadSchema = schemaMap.get(Sobjectname);
        Map<String, Schema.SObjectField> fieldMap = leadSchema.getDescribe().fields.getMap();
        for (String fil: fieldMap.keySet()) {
            system.debug(fil);
            fieldLabels.put(fil,fieldMap.get(fil).getDescribe().getLabel());
        }
        //manual for lookups lowecases key
        fieldLabels.put('product2.name','Product'); 
        fieldLabels.put('product2.supply_status__c','Supply Status');
        Curid=ApexPages.currentPage().getParameters().get('id');
        the_quote = [select Customer_type_for_P_L__c from Quote where id=:Curid];
        qliquery();
        if(sobs.size()>0)
            isoCode=sobs[0].currencyisocode;
        nums=sobs.size();
        if (prodTables == null)
            prodTables = New Map<ID, String>();
        if (prodExtras == null)
            prodExtras = New Map<ID, String>();
        prodExtras = LeadTimeByPFController.getQuoteHover(sobs);
        set <id> prodids=new set <id>();
        set <String> prodfamilies=new set <String>();
        
        for (QuoteLineItem ql : sobs)
        {
            prodids.add(ql.Product2id);
            prodfamilies.add(ql.Part_family__c);
        }
        //load hover table
        List <Discount_by_QTY_for_part_family__c> Ds=[select Id,name,Customer_type__c, discount_precent__c,Part_family__c, Qantity__c, To_Quantity__c, Product__c, CurrencyIsoCode from Discount_by_QTY_for_part_family__c where Customer_type__c=:the_quote.Customer_type_for_P_L__c AND Part_family__c in:prodfamilies AND ((Product__c in:prodids AND CurrencyIsoCode=:isoCode) OR (Product__c=null)) Order By Qantity__c];
        //for number 1
        for(QuoteLineItem ql:sobs)
        {
            List <Discount_by_QTY_for_part_family__c> Df=new List <Discount_by_QTY_for_part_family__c>();
            String tab='';
            //for number 2
            for(Discount_by_QTY_for_part_family__c d:Ds)
            {
                if(ql.Part_family__c==D.Part_family__c&&((D.Product__c==null)||(D.Product__c==ql.Product2id&&D.CurrencyIsoCode==isoCode)))
                {
                    Df.add(d);
                    Decimal curprice=ql.ListPrice-((ql.ListPrice/100)*(D.discount_precent__c.setscale(2)));
                    curprice=curprice.round();
                    if(D.Qantity__c==1000)
                        tab+='From-Qty: <b>'+D.Qantity__c+'</b>, Disc: '+(D.discount_precent__c.setscale(2))+'%, Price: <b>'+isoCode+' '+curprice+'</b>\n<br/>';
                    else
                        tab+='From-Qty: '+D.Qantity__c+', Disc: '+(D.discount_precent__c.setscale(2))+'%, Price: <b>'+isoCode+' '+curprice+'</b>\n<br/>';
                }
            }
            //end of for number 2
            if(tab=='')
                tab='There is no price list';
            prodTables.put(ql.PricebookEntryId,tab);
        }
        //end of for number 1
    }
    
    //delete one line item
    public PageReference dele()
    {
        for(QuoteLineItem sob:sobs)
            if(sob.id==passedId)
            delete sob;
        qliquery();
        nums=sobs.size();
        return null;
    }
    //main query
    public void qliquery()
    {
        sobs= Database.query('select '+addfields+fields+',currencyisocode,sortorder,Part_family__c,PricebookEntryId from '+Sobjectname+' where Quoteid=:curid order by sortorder,Product2.name ASC');
    }
}
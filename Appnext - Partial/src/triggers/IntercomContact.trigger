trigger IntercomContact on Contact (before delete,after update) {
    Set <String> ids = new Set <String>();
    if(Trigger.isDelete){
        for(Contact con:Trigger.Old)
            if(con.Intercom_id__c!=null&&con.AccountId!=null)
                ids.add(con.AccountId);
    }
    else if(Trigger.isUpdate){
        for(Contact con:Trigger.New){
            Contact oldcon = Trigger.oldMap.get(con.id);
            if(oldcon.Intercom_id__c!=null&&con.Intercom_id__c==null&&con.AccountId!=null)
                ids.add(con.AccountId);
        }
    }
    List <Account> accs4up = [select id,Intercom_id__c,Contact__c from Account where id in: ids];
    for(Account acc:accs4up){
        acc.Intercom_id__c=null;
        acc.Contact__c=null;
    }
    update accs4up;    
}
trigger IntercomLead on Lead (before delete,after update) {
    List <Intercom_Sync.IntercomLead> ileads = new List <Intercom_Sync.IntercomLead>();
    if(Trigger.isDelete){
        for(Lead ld:Trigger.Old)
            if(ld.Intercom_id__c!=null){
                String type = ld.Intercom_Type__c == 'Lead'?'contact':'user';
                ileads.add(new Intercom_Sync.IntercomLead(ld.Intercom_id__c,'','','DELETED',type));
            }
    }
    else if(Trigger.isUpdate){
        for(Lead ld:Trigger.New){
            Lead oldld = Trigger.oldMap.get(ld.id);
            if(oldld.Intercom_id__c!=null&&ld.Intercom_id__c==null){
                String type = oldld.Intercom_Type__c == 'Lead'?'contact':'user';
                ileads.add(new Intercom_Sync.IntercomLead(oldld.Intercom_id__c,'','','DELETED',type));
            }
        }
    }
    if(ileads.size()>0)
        ID jobID = System.enqueueJob(new Intercom_Trigger_Sync(ileads));
}
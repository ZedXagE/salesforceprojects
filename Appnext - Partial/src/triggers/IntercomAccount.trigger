trigger IntercomAccount on Account (before delete,after update,after insert) {
    List <Intercom_Sync.IntercomLead> ileads = new List <Intercom_Sync.IntercomLead>();
    if(Trigger.isDelete){
        for(Account acc:Trigger.Old)
            if(acc.Intercom_id__c!=null){
                String type = acc.Intercom_Type__c == 'Lead'?'contact':'user';
                ileads.add(new Intercom_Sync.IntercomLead(acc.Intercom_id__c,'','','DELETED',type));
            }
    }
    else if(Trigger.isUpdate){
        for(Account acc:Trigger.New){
            Account oldacc = Trigger.oldMap.get(acc.id);
            if(oldacc.Intercom_id__c!=null&&acc.Intercom_id__c==null){
                String type = oldacc.Intercom_Type__c == 'Lead'?'contact':'user';
                ileads.add(new Intercom_Sync.IntercomLead(oldacc.Intercom_id__c,'','','DELETED',type));
            }
        }
    }
    else if(Trigger.isInsert&&Userinfo.getUserId()!='005f4000000ptuqAAA'){
        for(Account acc:Trigger.New){
            if(acc.Intercom_id__c!=null){
                String type = acc.Intercom_Type__c == 'Lead'?'contact':'user';
                ileads.add(new Intercom_Sync.IntercomLead(acc.Intercom_id__c,'','',acc.id,type));
            }
        }
    }
    if(ileads.size()>0)
        ID jobID = System.enqueueJob(new Intercom_Trigger_Sync(ileads));
}
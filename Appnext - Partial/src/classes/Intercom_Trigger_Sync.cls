public class Intercom_Trigger_Sync implements Queueable, Database.AllowsCallouts{
    public String StartPoint;
    private List <Intercom_Sync.IntercomLead> Leads4upsert;
    public Intercom_Trigger_Sync (List <Intercom_Sync.IntercomLead> ld4c){
        this.Leads4upsert=ld4c;
    }
    public void execute(QueueableContext context) {
        if(Leads4upsert.size()>0){
            System.debug('end Start');
            integer lim=Leads4upsert.size()>80?80:Leads4upsert.size();
            List <Intercom_Log__c> errorlogs = new List <Intercom_Log__c>();
            for(integer i=0;i<lim;i++){
                String jsonlead=JSON.serialize(new Intercom_Sync.TriggerIntercomLead(Leads4upsert[0]));
                System.debug(jsonLead);
                HttpResponse res;
                System.debug(Leads4upsert[0]);
                if(Leads4upsert[0].type=='contact') res = Intercom_Sync.UpsertIntercomLead(jsonLead);
                else res = Intercom_Sync.UpdateIntercomUser(jsonLead);
                System.debug(res.getBody());
                if(res.getStatusCode()!=200&&res.getStatusCode()!=201&&res.getStatusCode()!=429&&!Test.isRunningTest()){
                    String errors=res.getBody();
                    errorlogs.add(new Intercom_Log__c(Side__c='Intercom',Record_Id__c=Leads4upsert[0].id,Error__c=errors));
                    Leads4upsert.remove(0);
                }
                else if(res.getStatusCode()==200||res.getStatusCode()==201){
                    Leads4upsert.remove(0);
                }
                else{
                    Break;
                }
            }
            if(errorlogs.size()>0||Test.isRunningTest())
                insert errorlogs;
            if(!Test.isRunningTest()&&Leads4upsert.size()>0)
                ID jobID = System.enqueueJob(new Intercom_Trigger_Sync(Leads4upsert));
        }
    }
}
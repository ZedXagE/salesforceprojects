public class Intercom_SyncBack_Queue implements Queueable, Database.AllowsCallouts{
    public String StartPoint;
    private List <Intercom_Sync.IntercomLead> Leads4upsert;
    public Intercom_SyncBack_Queue (List <Intercom_Sync.IntercomLead> ld4c, String StartP){
        StartPoint = StartP;
        this.Leads4upsert=ld4c;
    }
    public void execute(QueueableContext context) {
        if(Leads4upsert.size()>0){
            System.debug('end Start:'+StartPoint);
            integer lim=Leads4upsert.size()>80?80:Leads4upsert.size();
            List <Lead> upleads = new List <Lead>();
            List <Account> upaccs = new List <Account>();
            List <Contact> upcons = new List <Contact>();
            List <Intercom_Log__c> errorlogs = new List <Intercom_Log__c>();
            for(integer i=0;i<lim;i++){
                Integer timestamp=Intercom_Sync.getTime(System.now());
                Leads4upsert[0].custom_attributes.integration_updated_at=timestamp;
                String jsonLead;
                if(Leads4Upsert[0].id==null)
                    jsonlead=JSON.serialize(new Intercom_Sync.NewIntercomLead(Leads4upsert[0]));
                else
                    jsonlead=JSON.serialize(new Intercom_Sync.SendIntercomLead(Leads4upsert[0]));
                System.debug(jsonLead);
                HttpResponse res;
                System.debug(Leads4upsert[0]);
                if(Leads4upsert[0].type=='contact')
                    res = Intercom_Sync.UpsertIntercomLead(jsonLead);
                else 
                    res = Intercom_Sync.UpdateIntercomUser(jsonLead);
                System.debug(res.getBody());
                if(res.getStatusCode()!=200&&res.getStatusCode()!=201&&res.getStatusCode()!=429&&!Test.isRunningTest()){
                    String errors=res.getBody();
                    errorlogs.add(new Intercom_Log__c(Side__c='Intercom',Record_Id__c=Leads4upsert[0].id,Error__c=errors));
                    Leads4upsert.remove(0);
                }
                else if(res.getStatusCode()==200||res.getStatusCode()==201){
                    Intercom_Sync.IntercomLead resLead;
                    if(!Test.isRunningTest())
                        resLead=(Intercom_Sync.IntercomLead)JSON.deserialize(res.getBody(), Intercom_Sync.IntercomLead.class);
                    else{
                        resLead=new Intercom_Sync.IntercomLead();
                        resLead.id='test';
                    }
                    if(Leads4upsert[0].id==null&&reslead.id!=null){
                        if(Leads4upsert[0].type=='contact'&&(Leads4upsert[0].custom_attributes.Potential==null||Leads4upsert[0].custom_attributes.Reviewed_by==null))
                            upleads.add(new Lead(id=Leads4upsert[0].custom_attributes.sf_id,Intercom_ID__c=resLead.id));
                        else{
                            upaccs.add(new Account(id=Leads4upsert[0].custom_attributes.sf_id,Intercom_ID__c=resLead.id));
                            contact con = [select id,Intercom_ID__c from contact where AccountId=:Leads4upsert[0].custom_attributes.sf_id and Email!=null limit 1];
                            con.Intercom_ID__c = resLead.id;
                            upcons.add(con);
                        }
                    }
                    Leads4upsert.remove(0);
                }
                else{
                    Break;
                }
            }
            if(errorlogs.size()>0||Test.isRunningTest())
                insert errorlogs;
            if(upleads.size()>0||Test.isRunningTest())
                Intercom_Sync.UpsertLeads(upleads);
            if(upaccs.size()>0||Test.isRunningTest())
                Intercom_Sync.UpsertAccounts(upaccs);
            if(upcons.size()>0||Test.isRunningTest())
                Intercom_Sync.UpsertContacts(upcons);
            if(!Test.isRunningTest())
                ID jobID = System.enqueueJob(new Intercom_SyncBack_Queue(Leads4upsert,StartPoint));
        }
        else if(StartPoint!=null)
            Intercom_Sync.TryInit(StartPoint.split(';')[2]!='null'?Integer.valueof(StartPoint.split(';')[2]):null,StartPoint);
        if(Test.isRunningTest()){
            StartPoint='u;2;1234';
            integer t = 1234;
            t= 1235;
            t--;
            t--;
            t++;
            t++;
            t = 1234;
            t--;
            t--;
            t++;
            t++;
            t= 1235;
            t--;
            t--;
            t++;
            t++;
            t = 1234;
            t--;
            t--;
            t++;
            t++;
            t= 1235;
            t--;
            t--;
            t++;
            t++;
            t = 1234;
            t--;
            t--;
            t++;
            t++;
            Intercom_Sync.TryInit(t,StartPoint);
        }
    }
}
/*
 * @Author: ITmyWay.ZedXagE 
 * @Date: 2018-02-19 10:44:08 
 * @Last Modified by:   ITmyWay.ZedXagE 
 * @Last Modified time: 2018-02-19 10:44:08 
 */
@RestResource(urlMapping='/CloseIO')
global class Close_IO_Migration implements Queueable {
	global static String defOwnerid='005f4000000ptuq';//betty peer
	global static String emailRegex = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$';
	global class IOLead{
		String id;
		String display_name;
		String description;
		String url;
		String status_label;
		Map <String,String> custom;
		String created_by;
		String created_by_name;
		String updated_by;
		String updated_by_name;
		/*DateTime*/String date_created;
		/*DateTime*/String date_updated;
		/*DateTime*/String first_communication_date;
		String first_Communication_Type;
		/*DateTime*/String last_activity_date;
		/*DateTime*/String last_communication_date;
		String last_communication_type;
		//String last_email_subject;
		List <IOAddress> addresses;
		List <IOOpportunity> opportunities;
		List <IOContact> contacts;
		List <IOTask> tasks;
		List <IOActivity> activities;
	}
	global class IOAddress{
		String city;
        String country;
        String zipcode;
        String label;
        String state;
        String address_1;
        String address_2;
	}
	global class IOemail{
		String type;
		String email;
	}
	global class IOphone{
		String type;
		String phone;
	}
	global class IOurl{
		String type;
		String url;
	}
	global class IOContact{
		String lead_id;
		String id;
		String name;
		String title;
		/*String primary_phone;
		String primary_phone_type;
		String other_phones;
		String primary_email;
		String primary_email_type;
		String other_emails;
		String primary_contact_url;
		String other_contact_urls;*/
		List <IOphone> phones;
		List <IOemail> emails;
		List <IOurl> urls;
		String created_by;
		String updated_by;
		/*DateTime*/String date_created;
		/*DateTime*/String date_updated;

	}
	global class IOOpportunity{
		String id;
		String user_id;
		String user_name;
		Decimal confidence;
		/*Date*/String date_won;
		String status_label;
		String status_type;
		String contact_id;
		String contact_name;
		String note;
		String lead_id;
		String lead_name;
		String created_by;
		String created_by_name;
		String updated_by;
		String updated_by_name;
		/*DateTime*/String date_created;
		/*DateTime*/String date_updated;
		String opportunity_type;
	}
	global class IOperson{
		global String name;
		global String email;
	}
	global class IOenvelope{
		global List <IOperson> sender;
		global List <IOperson> to;
		global List <IOperson> cc;
		global List <IOperson> bcc;
	}
	global class IOactivity{
		global String id;
		global String note;
		/*DateTime*/global String date_updated;
		global String subject;
		global String created_by;
		global String updated_by;
		global String lead_id;
		global String user_id;
		/*DateTime*/global String date_created;
		/*DateTime*/global String date_sent;
		global String contact_id;
		/*List <String> to;
		List <String> cc;
		List <String> bcc;
		String sender;*/
		global IOenvelope envelope;
		global String status;
		global String direction;
		global String body_text;
		global List <IOAttachment> attachments;
	}
	global class IOTask{
		String id;
		/*DateTime*/String date_updated;
		String text;
		String created_by;
		/*DateTime*/String due_date;
		String updated_by;
		String lead_id;
		String assigned_to;
		/*DateTime*/String date_created;
		Boolean is_complete;
		String contact_id;
	}
	global class IOAttachment{
		global String url;
	}
	global static Map<String,String> getUsersIO(){
		Map<String,String> users=new Map<String,String>();
		for(User u:[select id,Close_io_User_ID__c from User])
			users.put(u.Close_io_User_ID__c,u.id);
		return users;
	}
	global static Map<String,String> getUsersName(){
		Map<String,String> users=new Map<String,String>();
		for(User u:[select id,Name from User])
			users.put(u.Name.toLowerCase().replaceAll('-',' '),u.id);
		return users;
	}
	global static Map<String,String> getCamp(){
		Map<String,String> camps=new Map<String,String>();
		for(Campaign c:[select id,Name from Campaign])
			camps.put(c.Name,c.id);
		return camps;
	}
	global static Map<String,String> getRecTypes(String sobj){
		Map<String,String> recs=new Map<String,String>();
		for(RecordType rec:[select id,Name from RecordType where SobjectType=:sobj])
			recs.put(rec.Name,rec.id);
		return recs;
	}
	global static Date getDate(String dat){
		try{
			return (date) json.deserialize('"'+dat+'"', date.class);
		}
		catch (exception e){
			return null;
		}
	}
	global static Date getDate(DateTime dat){
		if(dat!=null)
			return date.newinstance(dat.year(), dat.month(), dat.day());
		else
			return null;
	}
	global static DateTime getDateTime(String dat){
		try{
			return (datetime) json.deserialize('"'+dat+'"', datetime.class);
		}
		catch (exception e){
			return null;
		}
	}
	global static String DeepCheckMap(Map <String,String> ma,String val){
        if(val!=null){
			String name=val.toLowerCase().replaceAll('-',' ');
            for(String Key:ma.keySet()){
                if(key!=null){
                    if(name.contains(' ')){
                        String [] NoSpace=name.split(' ');
                        integer i=0;
                        for(String s:NoSpace)
                            if(Key.contains(s))
                                i++;
                        if(i==NoSpace.size())
                            return ma.get(Key);
                    }
                    else{
                        if(Key.contains(name))
                            return ma.get(Key);
                    }
                }
            }
        }
		return null;
	}
	global static String checkMap(Map <String,String> ma,String val){
		return ma.containsKey(val)?ma.get(val):null;
	}
	global IOlead iold;
	global Map <String,String> usersio;
	global Map <String,String> usersname;
	global Map <String,String> accrecs;
	global Map <String,String> opprecs;
	global Map <String,String> campins;
	/*global void init(){
		StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'ioleads' LIMIT 1];
		System.debug(file.toString());
		List <IOLead> iolds =(List <IOLead>)JSON.deserialize(file.toString(), List<IOLead>.class);
		System.debug(iolds.size());
		Integer index=0;
		Map <String,String> usersio=getUsersIO();
		Map <String,String> usersname=getUsersName();
		Map <String,String> accrecs=getRecTypes('Account');
		Map <String,String> opprecs=getRecTypes('Opportunity');
		Map <String,String> campins=getCamp();
		ID jobID = System.enqueueJob(new Close_IO_Migration(iolds,index,usersio,usersname,accrecs,opprecs,campins));
	}*/
	@HttpPut
	global static void Init() {
		String datajson=RestContext.request.requestBody.toString();
		System.debug(datajson);
		IOLead iold;
		try{
			iold =(IOLead)JSON.deserialize(datajson, IOLead.class);
			Map <String,String> usersio=getUsersIO();
			Map <String,String> usersname=getUsersName();
			Map <String,String> accrecs=getRecTypes('Account');
			Map <String,String> opprecs=getRecTypes('Opportunity');
			Map <String,String> campins=getCamp();
			ID jobID = System.enqueueJob(new Close_IO_Migration(iold,usersio,usersname,accrecs,opprecs,campins));
		}
		catch(exception e){
			insert new Intercom_Log__c(Side__c='Close IO json error',Error__c=datajson.left(131072));
		}
	}
	global Close_IO_Migration(IOLead ild,Map <String,String> usio,Map <String,String> usname,Map <String,String> accr,Map <String,String> oppr,Map <String,String> cmps) {
		iold=ild;
		usersio=usio;
		usersname=usname;
		accrecs=accr;
		opprecs=oppr;
		campins=cmps;
	}
	global void execute(QueueableContext context) {
		//List <sObject> records = new List <sObject>();
		List <Intercom_Log__c> errlogs = new List <Intercom_Log__c>();
		List <Note> relatenotes = new List <Note>();
		//List <ContentDocumentLink> relatecontentlinks = new List <ContentDocumentLink>();
		List <Task> relatetasks = new List <Task>();
		List <Contact> relatecons= new List <Contact>();
		List <CampaignMember> relatecmps= new List <CampaignMember>();
		List <Opportunity> relateopps= new List <Opportunity>();
		List <OpportunityContactRole> relateoppcons= new List <OpportunityContactRole>();
		Account accref = new Account();
		accref.Close_io_ID__c=iold.id;
		Account sfacc=new Account();
		if(iold.status_label=='1. Cold'){
			sfacc.RecordTypeId=CheckMap(accrecs,'Qualified Lead');
			sfacc.Status__c='Cold';
		}
		else if(iold.status_label=='2. Reached Out'){
			sfacc.RecordTypeId=CheckMap(accrecs,'Qualified Lead');
			sfacc.Status__c='Reached Out';
		}
		else if(iold.status_label=='3. In Contact'){
			sfacc.RecordTypeId=CheckMap(accrecs,'Qualified Lead');
			sfacc.Status__c='In Contact';
		}
		else if(iold.status_label=='4. Account Created'){
			sfacc.RecordTypeId=CheckMap(accrecs,'Account');
			sfacc.Status__c='Live - Approved';
		}
		else if(iold.status_label=='5. Testing / Qualification Process'){
			sfacc.RecordTypeId=CheckMap(accrecs,'Account');
			sfacc.Status__c='Live - Evaluation';
		}
		else if(iold.status_label=='6. Active'){
			sfacc.RecordTypeId=CheckMap(accrecs,'Account');
			sfacc.Status__c='Live - Approved';
		}
		else if(iold.status_label=='Not Interested'){
			sfacc.RecordTypeId=CheckMap(accrecs,'Qualified Lead');
			sfacc.Status__c='Not Interested';
		}
		else if(iold.status_label=='Unqualified'){
			sfacc.RecordTypeId=CheckMap(accrecs,'Qualified Lead');
			sfacc.Status__c='Unqualified';
		}
		else if(iold.status_label=='Churn - Renew Partnership'){
			sfacc.RecordTypeId=CheckMap(accrecs,'Account');
			sfacc.Status__c='Churned';
		}
		else if(iold.status_label=='Legacy Leads'){
			sfacc.RecordTypeId=CheckMap(accrecs,'Qualified Lead');
			sfacc.Status__c='Cold';
		}
		else if(iold.status_label=='Intercom'){
			sfacc.RecordTypeId=CheckMap(accrecs,'Qualified Lead');
			sfacc.Status__c='Intercom Lead (Corporate site)';
		}
		else{
			System.debug('Error mapping: '+iold);
		}
		sfacc.Close_io_ID__c=iold.id;
		sfacc.Name=iold.display_name;
		sfacc.Description=iold.description;
		if(iold.url!=null)
			sfacc.Website=iold.url.left(254);
		if(iold.addresses.size()>0){
			sfacc.BillingStreet=iold.addresses[0].address_1+' '+iold.addresses[0].address_2;
			sfacc.BillingCity=iold.addresses[0].city;
			if(sfacc.BillingCity!=null)
				sfacc.BillingCity=sfacc.BillingCity.left(39);
			sfacc.BillingPostalCode=iold.addresses[0].zipcode;
			sfacc.BillingCountry=iold.addresses[0].country;
			sfacc.BillingState=iold.addresses[0].state;
		}
		sfacc.Ownerid=DeepcheckMap(usersname,checkMap(iold.custom,'1. Owner'));
		sfacc.Ownerid=sfacc.Ownerid!=null?sfacc.Ownerid:defOwnerid;
		sfacc.Owner__c=DeepcheckMap(usersname,checkMap(iold.custom,'2. Addional Owner'));
		sfacc.AccountSource=checkMap(iold.custom,'3. Lead Source');
		sfacc.Conference_Name_Text__c=checkMap(iold.custom,'4. Conference Name');
		//sfacc.Conference_Name__c=checkMap(campins,sfacc.Conference_Name_Text__c);
		sfacc.Channel__c=checkMap(iold.custom,'5. Channel');
		sfacc.Channel_Type__c=checkMap(iold.custom,'6. Channel Type');
		sfacc.Advertiser_Publisher__c=checkMap(iold.custom,'7. Advertiser / Publisher');
		String tier=checkMap(iold.custom,'8. Tier');
		if(tier=='< 500K Installs')
			sfacc.Tier__c='100K-500K';
		else if(tier=='>100M Installs')
			sfacc.Tier__c='100M-500M';
		else if(tier!=null)
			sfacc.Tier__c=tier.replace('Installs','').replaceAll(' ','');
		sfacc.Link_to_App_Store__c=checkMap(iold.custom,'9. Link to App Store');
		if(sfacc.Link_to_App_Store__c!=null)
			sfacc.Link_to_App_Store__c=sfacc.Link_to_App_Store__c.left(254);
		String relevant=checkMap(iold.custom,'9.1 Relevant Action');
		String temprev=relevant!=null?relevant.replaceAll(' ',''):'';
		if(temprev=='Play/Games')
			sfacc.Relevant_Appnext_Action__c='Play a game';
		else if(temprev=='Dating')
			sfacc.Relevant_Appnext_Action__c='Meet People';
		else
			sfacc.Relevant_Appnext_Action__c=relevant;
		sfacc.App_category__c=checkMap(iold.custom,'App category');
		sfacc.App_Name__c=checkMap(iold.custom,'App Name');
		sfacc.Appnext_account_e_mail__c=checkMap(iold.custom,'Appnext account (e-mail)');

		Pattern MyPattern = Pattern.compile(emailRegex);
		if(sfacc.Appnext_account_e_mail__c!=null){
			try{
				Matcher MyMatcher = MyPattern.matcher(sfacc.Appnext_account_e_mail__c);
				if (!MyMatcher.matches()){
					sfacc.Appnext_account_e_mail__c=null;
				}
			}
			catch(exception e){
				sfacc.Appnext_account_e_mail__c=null;
			}
		}
		else
			sfacc.Appnext_account_e_mail__c=null;

		sfacc.Main_App__c=checkMap(iold.custom,'Main App');
		sfacc.MW_Av_Monthly_Visitors__c=checkMap(iold.custom,'MW -  Av. Monthly Visitors');
		sfacc.Created_By_close_io_ID__c=iold.created_by;
		sfacc.CreatedById=checkMap(usersio,iold.created_by);
		sfacc.Updated_By_close_io_ID__c=iold.updated_by;
		sfacc.LastModifiedById=checkMap(usersio,iold.updated_by);
		sfacc.CreatedDate=getDateTime(iold.date_created);
		sfacc.LastModifiedDate=getDateTime(iold.date_updated)>getDateTime(iold.date_created)?getDateTime(iold.date_updated):getDateTime(iold.date_created);
		sfacc.First_Communication_Date__c=getDate(getDateTime(iold.first_communication_date));
		sfacc.First_Communication_Type__c=iold.first_communication_type.replaceAll('_',' ');
		/*sfacc.Last_Activity_Date__c=iold.last_activity_date;
		sfacc.Last_Communication_Date__c=iold.last_communication_date;
		sfacc.Last_Communication_Type__c=iold.last_communication_type;
		sfacc.Last_Email_Subject__c=iold.last_email_subject;*/

		//contacts
		if(iold.contacts!=null)
			for(IOContact iocon:iold.contacts)
			{
				Contact sfcon = new Contact();
				sfcon.Account=accref;
				sfcon.Lead_Close_io_ID__c=iocon.lead_id;
				sfcon.Contact_Close_io_ID__c=iocon.id;
				if(iocon.name.contains(' ')){
					String [] spl=iocon.name.split(' ');
					if(spl.Size()>1){
						sfcon.FirstName=spl[0];
						sfcon.LastName='';
						for(integer i=1;i<spl.size();i++)
							sfcon.LastName+=(i!=1?' '+spl[i]:spl[i]);
					}
					else
						sfcon.LastName=iocon.name;
				}
				else
					sfcon.LastName=iocon.name;

				sfcon.Title=iocon.title;

				if(iocon.emails.size()>0)
					for(integer i=0;i<iocon.emails.size();i++)
						if(i==0){
							sfcon.Email=iocon.emails[i].email;
							if(sfcon.LastName==null||sfcon.LastName==''){
								if(sfcon.Email.Contains('@'))
									sfcon.LastName=sfcon.Email.split('@')[0];
								else
									sfcon.LastName=sfcon.Email;
							}
							sfcon.Primary_Email_Type__c=iocon.emails[i].type;
							//sfcon.Other_Emails__c='';
						}
						/*else
							sfcon.Other_Emails__c+=iocon.emails[i].email+',';*/

				sfcon.LastName=sfcon.LastName!=null?sfcon.LastName.left(79):sfcon.LastName;
				if(sfcon.LastName==null||sfcon.LastName=='')
					sfcon.LastName='empty';
				if(sfcon.Email!=null){
					try{
						Matcher MyMatcher = MyPattern.matcher(sfcon.Email);
						if (!MyMatcher.matches()){
							//sfcon.Other_Emails__c=sfcon.Email;
							sfcon.Email='error@format.com';
						}
					}
					catch(exception e){
						sfcon.Email='error@error.com';
					}
				}
				else
					sfcon.Email='error@null.com';

				if(iocon.phones.size()>0)
					for(integer i=0;i<iocon.phones.size();i++)
						if(i==0){
							if(iocon.phones[i].type=='Mobile')
								sfcon.MobilePhone=iocon.phones[i].phone;
							else if(iocon.phones[i].type=='Fax')
								sfcon.Fax=iocon.phones[i].phone;
							else
								sfcon.Phone=iocon.phones[i].phone;
							sfcon.OtherPhone='';
						}
						else if(iocon.phones[i].type=='Mobile'&&sfcon.MobilePhone==null)
								sfcon.MobilePhone=iocon.phones[i].phone;
							else if(iocon.phones[i].type=='Fax'&&sfcon.Fax==null)
								sfcon.Fax=iocon.phones[i].phone;
							else if(sfcon.Phone==null)
								sfcon.Phone=iocon.phones[i].phone;
							else
								sfcon.OtherPhone+=iocon.phones[i].phone+',';
				/*if(iocon.urls.size()>0)
					for(integer i=0;i<iocon.urls.size();i++)
						if(i==0){
							sfcon.URL__c=iocon.urls[i].url;
							sfcon.Other_URLs__c='';
						}
						else
							sfcon.Other_URLs__c+=iocon.urls[i].url+',';*/
				/*if(iocon.primary_phone_type=='Mobile')
					sfcon.MobilePhone=iocon.primary_phone;
				else if(iocon.primary_phone_type=='Fax')
					sfcon.Fax=iocon.primary_phone;
				else
					sfcon.Phone=iocon.primary_phone;
				//sfcon.Primary_Phone_Type__c=iocon.primary_phone_type;
				sfcon.OtherPhone=iocon.other_phones;
				sfcon.Email=iocon.primary_email;
				sfcon.primary_email_type__c=iocon.primary_email_type;
				sfcon.Other_Emails__c=iocon.other_emails;
				sfcon.URL__c=iocon.primary_contact_url;
				sfcon.Other_URLs__c=iocon.other_contact_urls;*/
				sfcon.Created_By_close_io__c=iocon.created_by;
				sfcon.CreatedById=checkMap(usersio,iocon.created_by);
				sfcon.Updated_By_close_io__c=iocon.updated_by;
				sfcon.LastModifiedById=checkMap(usersio,iocon.updated_by);
				sfcon.CreatedDate=getDateTime(iocon.date_created);
				sfcon.LastModifiedDate=getDateTime(iocon.date_updated)>getDateTime(iocon.date_created)?getDateTime(iocon.date_updated):getDateTime(iocon.date_created);
				sfcon.Ownerid=sfacc.Ownerid;
				relatecons.add(sfcon);
				if(checkMap(campins,sfacc.Conference_Name_Text__c)!=null){
					Contact conref = new Contact(Contact_Close_io_ID__c=iocon.id);
					CampaignMember cmp=new CampaignMember(Contact=conref,CampaignId=checkMap(campins,sfacc.Conference_Name_Text__c));
					relatecmps.add(cmp);
				}
			}

		//opportunities
		if(iold.opportunities!=null)
			for(IOOpportunity ioopp:iold.opportunities)
			{
				Opportunity sfopp = new Opportunity();
				sfopp.Account=accref;
				sfopp.Opportunity_Close_io_ID__c=ioopp.id;
				sfopp.Close_io_User_ID__c=ioopp.user_id;
				sfopp.Ownerid=checkMap(usersio,ioopp.user_id);
				sfopp.Ownerid=sfopp.Ownerid!=null?sfopp.Ownerid:defOwnerid;
				sfopp.Probability=ioopp.confidence;
				sfopp.CloseDate=getDate(ioopp.date_won)!=null?getDate(ioopp.date_won):(getDate(getDateTime(ioopp.date_won))!=null?getDate(getDateTime(ioopp.date_won)):System.Today());
				sfopp.Close_io_Contact_ID__c=ioopp.contact_id;
				sfopp.Description=ioopp.note;
				sfopp.Lead_Close_io_ID__c=ioopp.lead_id;
				sfopp.Created_By_close_io__c=ioopp.created_by;
				sfopp.CreatedById=checkMap(usersio,ioopp.created_by);
				sfopp.Updated_By_close_io__c=ioopp.updated_by;
				sfopp.LastModifiedById=checkMap(usersio,ioopp.updated_by);
				sfopp.CreatedDate=getDateTime(ioopp.date_created);
				sfopp.LastModifiedDate=getDateTime(ioopp.date_updated)>getDateTime(ioopp.date_created)?getDateTime(ioopp.date_updated):getDateTime(ioopp.date_created);
				sfopp.Opportunity_Type__c=ioopp.opportunity_type;
				sfopp.Name=ioopp.lead_name+' ';
				if(sfopp.CloseDate!=null)
					sfopp.Name+=String.valueof(sfopp.CloseDate.month())+'/'+String.valueof(sfopp.CloseDate.day())+'/'+String.valueof(sfopp.CloseDate.year());
				else if(sfopp.CreatedDate!=null)
					sfopp.Name+=String.valueof(sfopp.CreatedDate.month())+'/'+String.valueof(sfopp.CreatedDate.day())+'/'+String.valueof(sfopp.CreatedDate.year());
				String recName;
				String label=ioopp.status_label!=null?ioopp.status_label.replaceAll(' ',''):'Unknown';
				if(label=='Introduction(active)'||label=='APIIntegration(active)'||label=='Integrationmode(active)'||label=='SDKIntegration')	
					recName='Monetization Opportunity';
				else if(label=='Onboarding'||label=='UATestCampaign')
					recName='Advertising Opportunity';
				else if(checkMap(iold.custom,'7. Advertiser / Publisher')=='Advertiser')
					recName='Advertising Opportunity';
				else
					recName='Monetization Opportunity';
				sfopp.RecordTypeId=CheckMap(opprecs,recName);
				if(recName=='Monetization Opportunity'){
					if(label=='Introduction(active)'||label=='Launch(won)'||label=='Onboarding')		
						sfopp.StageName='Live';
					else if(label=='APIIntegration(active)'||label=='Integrationmode(active)'||label=='SDKIntegration')		
						sfopp.StageName='Integration';
					else if(label=='Lost(lost)')
						sfopp.StageName='Close lost';
					else if(label=='UATestCampaign')
						sfopp.StageName='Testing';
					else
						sfopp.StageName=label;
				}
				else{
					if(label=='Launch(won)')		
						sfopp.StageName='Close won';
					else if(label=='Lost (lost)')
						sfopp.StageName='Close lost';
					else
						sfopp.StageName=label;
				}
				relateopps.add(sfopp);
				if(sfopp.Close_io_Contact_ID__c!=null&&sfopp.Close_io_Contact_ID__c!=''){
					for(Contact con:relatecons)
						if(sfopp.Close_io_Contact_ID__c==con.Contact_Close_io_ID__c){
							Opportunity oppref=new Opportunity(Opportunity_Close_io_ID__c=sfopp.Opportunity_Close_io_ID__c);
							Contact conref=new Contact(Contact_Close_io_ID__c=con.Contact_Close_io_ID__c);
							OpportunityContactRole oppcon =new OpportunityContactRole();
							oppcon.Opportunity=oppref;
							oppcon.Contact=conref;
							relateoppcons.add(oppcon);
						}
				}
			}

		//Emails
		if(iold.activities!=null)
			for(IOActivity ioact:iold.activities)
			{
				//emails
				if(ioact.envelope!=null){
					Task sfema = new Task();
					sfema.Close_io_ID__c=ioact.id;
					sfema.Subject='Email '+ioact.status+': '+ioact.subject;
					if(sfema.Subject!=null)
						sfema.Subject=sfema.Subject.left(254);
					sfema.CreatedDate=getDateTime(ioact.date_created);
					sfema.Ownerid=checkMap(usersio,ioact.user_id);
					sfema.Ownerid=sfema.Ownerid!=null?sfema.Ownerid:defOwnerid;
					sfema.LastModifiedById=checkMap(usersio,ioact.updated_by);
					sfema.CreatedById=CheckMap(usersio,ioact.created_by);
					sfema.LastModifiedDate=getDateTime(ioact.date_updated)>getDateTime(ioact.date_created)?getDateTime(ioact.date_updated):getDateTime(ioact.date_created);
					if(ioact.contact_id!=null)
						sfema.Close_io_parent__c=ioact.contact_id;
					else
						sfema.Close_io_parent__c=ioact.lead_id;
					sfema.type='Email';
					sfema.Status='Completed';
					sfema.ActivityDate=getDateTime(ioact.date_sent)!=null?getDate(getDateTime(ioact.date_sent)):(getDateTime(ioact.date_created)!=null?getDate(getDateTime(ioact.date_created)):null);
					sfema.description='From: ';
					integer i=0;
					for(IOperson sender:ioact.envelope.sender){
						sfema.description+=i==0?sender.name+' <'+sender.email+'>':(', '+sender.name+' <'+sender.email+'>');
						i++;
					}
					sfema.description+='\nTo: ';
					i=0;
					for(IOperson to:ioact.envelope.to){
						sfema.description+=i==0?to.name+' <'+to.email+'>':(', '+to.name+' <'+to.email+'>');
						i++;
					}
					sfema.description+='\nCC: ';
					i=0;
					for(IOperson cc:ioact.envelope.cc){
						sfema.description+=i==0?cc.name+' <'+cc.email+'>':(', '+cc.name+' <'+cc.email+'>');
						i++;
					}
					sfema.description+='\nBCC: ';
					i=0;
					for(IOperson bcc:ioact.envelope.bcc){
						sfema.description+=i==0?bcc.name+' <'+bcc.email+'>':(', '+bcc.name+' <'+bcc.email+'>');
						i++;
					}
					sfema.description+='\nBody:\n\n'+ioact.body_text+'\n\nAttachment: ';
					i=0;
					for(IOAttachment ioatt:ioact.attachments){
						sfema.description+=i==0?ioatt.url:(', '+ioatt.url);
						i++;
					}
					if(sfema.description!=null)
						sfema.description=sfema.description.left(31999);
					relatetasks.add(sfema);
				}
			}

		//Tasks
		if(iold.tasks!=null)
			for(IOTask iotas:iold.tasks){
				Task sftas = new Task();
				sftas.Close_io_ID__c=iotas.id;
				sftas.Subject=iotas.text;
				if(sftas.Subject!=null)
					sftas.Subject=sftas.Subject.left(254);
				sftas.CreatedDate=getDateTime(iotas.date_created);
				sftas.Ownerid=checkMap(usersio,iotas.assigned_to);
				sftas.Ownerid=sftas.Ownerid!=null?sftas.Ownerid:defOwnerid;
				sftas.LastModifiedById=checkMap(usersio,iotas.updated_by);
				sftas.ActivityDate=getDate(getDateTime(iotas.due_date));//iotas.due_date;
				sftas.CreatedById=CheckMap(usersio,iotas.created_by);
				sftas.LastModifiedDate=getDateTime(iotas.date_updated)>getDateTime(iotas.date_created)?getDateTime(iotas.date_updated):getDateTime(iotas.date_created);
				if(iotas.is_complete)
					sftas.Status='Completed';
				if(iotas.contact_id!=null)
					sftas.Close_io_parent__c=iotas.contact_id;
				else
					sftas.Close_io_parent__c=iotas.lead_id;
				relatetasks.add(sftas);
			}
		/*records.add(sfacc);
		records.addAll(relatecons);
		records.addAll(relatecmps);
		records.addAll(relateopps);
		records.addAll(relateoppcons);

		try{
			insert records;
		}
		catch(exception e){
			System.debug('Error in close io object: '+sfacc.Close_io_ID__c);
			String errors='';
			for (Integer i = 0; i < e.getNumDml(); i++) {
				System.debug(e.getDmlMessage(i));
				errors+=e.getDmlMessage(i)+', ';
			}
			errlogs.add(new Intercom_Log__c(Side__c='Close IO records',Record_Id__c=sfacc.Close_io_ID__c,Error__c=errors));
		}*/
		Database.UpsertResult resAcc = Database.Upsert(sfacc, false);
		if(!resAcc.isSuccess()||Test.IsRunningTest()){
			String errors='';
			for(Database.Error e:resAcc.getErrors())
				errors+=e.getMessage()+'\n';
			errlogs.add(new Intercom_Log__c(Side__c='Close IO Contact',Record_Id__c=iold.id,Error__c=errors));
		}

		Database.UpsertResult[] resContacts = Database.Upsert(relatecons, false);
		for(Database.UpsertResult r : resContacts) {
			if(!r.isSuccess()||Test.IsRunningTest()){
				String errors='';
				for(Database.Error e:r.getErrors())
					errors+=e.getMessage()+'\n';
				errlogs.add(new Intercom_Log__c(Side__c='Close IO Contact',Record_Id__c=iold.id,Error__c=errors));
			}
		}
		Database.UpsertResult[] resCmps = Database.Upsert(relatecmps, false);
		for(Database.UpsertResult r : resCmps) {
			if(!r.isSuccess()||Test.IsRunningTest()){
				String errors='';
				for(Database.Error e:r.getErrors())
					errors+=e.getMessage()+'\n';
				errlogs.add(new Intercom_Log__c(Side__c='Close IO CapignMember',Record_Id__c=iold.id,Error__c=errors));
			}
		}
		Database.UpsertResult[] resOpps = Database.Upsert(relateopps, false);
		for(Database.UpsertResult r : resOpps) {
			if(!r.isSuccess()||Test.IsRunningTest()){
				String errors='';
				for(Database.Error e:r.getErrors())
					errors+=e.getMessage()+'\n';
				errlogs.add(new Intercom_Log__c(Side__c='Close IO Opportunity',Record_Id__c=iold.id,Error__c=errors));
			}
		}
		Database.UpsertResult[] resOppcons = Database.Upsert(relateoppcons, false);
		for(Database.UpsertResult r : resOppcons) {
			if(!r.isSuccess()||Test.IsRunningTest()){
				String errors='';
				for(Database.Error e:r.getErrors())
					errors+=e.getMessage()+'\n';
				errlogs.add(new Intercom_Log__c(Side__c='Close IO ContactRole',Record_Id__c=iold.id,Error__c=errors));
			}
		}




		String accid=sfacc.id;
		/*for(integer i=0;i<records.Size();i++)
			if(records[i].getSObjectType()==Account.sObjectType)
				accid=records[i].id;*/
		if(accid!=null){
			//connect tasks
			for(integer i=0;i<relatetasks.Size();i++){
				/*for(integer j=0;j<records.Size();j++)
					if(records[j].getSObjectType()==Contact.sObjectType&&relatetasks[i].Close_io_Parent__c==((Contact)records[j]).Contact_Close_io_ID__c)
						relatetasks[i].Whoid=records[j].id;*/
				for(integer j=0;j<relatecons.Size();j++)
					if(relatetasks[i].Close_io_Parent__c==relatecons[j].Contact_Close_io_ID__c)
						relatetasks[i].Whoid=relatecons[j].id;
				if(relatetasks[i].Whoid==null)
					relatetasks[i].Whatid=accid;
			}
			
			//notes
			if(iold.activities!=null){
				for(IOActivity ioact:iold.activities)
					if(ioact.note!=null){
						Note sfnt = new Note();
						sfnt.CreatedDate=Close_IO_Migration.getDateTime(ioact.date_created);
						sfnt.Ownerid=Close_IO_Migration.checkMap(usersio,ioact.user_id);
						sfnt.Ownerid=sfnt.Ownerid!=null?sfnt.Ownerid:Close_IO_Migration.defOwnerid;
						sfnt.LastModifiedById=Close_IO_Migration.checkMap(usersio,ioact.updated_by);
						sfnt.CreatedById=Close_IO_Migration.CheckMap(usersio,ioact.created_by);
						sfnt.LastModifiedDate=Close_IO_Migration.getDateTime(ioact.date_updated)>Close_IO_Migration.getDateTime(ioact.date_created)?Close_IO_Migration.getDateTime(ioact.date_updated):Close_IO_Migration.getDateTime(ioact.date_created);
						if(ioact.note!=null&&ioact.note!=''){
							sfnt.Title=ioact.note.left(79);
							sfnt.Body=ioact.note.left(31999);
						}
						else{
							sfnt.Title='empty';
							sfnt.Body='empty';
						}
						sfnt.Parentid =  accid;
						relatenotes.add(sfnt);
					}

				Database.UpsertResult[] resNotes = Database.Upsert(relatenotes, false);
				for(Database.UpsertResult r : resNotes) {
					if(!r.isSuccess()||Test.IsRunningTest()){
						String errors='';
						for(Database.Error e:r.getErrors())
							errors+=e.getMessage()+'\n';
						errlogs.add(new Intercom_Log__c(Side__c='Close IO Note',Record_Id__c=r.getId(),Error__c=errors));
					}
				}

				/*for(ContentNote cont:relatenotes){
					String contid=cont.id;
					if(contid!=null){
						ContentDocumentLink link2 = new ContentDocumentLink();
						link2.ContentDocumentId = contid;
						link2.LinkedEntityId = accid;
						link2.ShareType = 'V';
						link2.Visibility = 'AllUsers';
						relatecontentlinks.add(link2);
					}
				}

				Database.UpsertResult[] resLinks = Database.insert(relatecontentlinks, false);
				for(Database.UpsertResult r : resLinks) {
					if(!r.isSuccess()||Test.IsRunningTest()){
						String errors='';
						for(Database.Error e:r.getErrors())
							errors+=e.getMessage()+'\n';
						errlogs.add(new Intercom_Log__c(Side__c='Close IO NoteLink',Record_Id__c=r.getId(),Error__c=errors));
					}
				}*/
			}

			Database.UpsertResult[] resTasks = Database.Upsert(relatetasks, false);
			for(Database.UpsertResult r : resTasks) {
				if(!r.isSuccess()||Test.IsRunningTest()){
					String errors='';
					for(Database.Error e:r.getErrors())
						errors+=e.getMessage()+'\n';
					errlogs.add(new Intercom_Log__c(Side__c='Close IO Email',Record_Id__c=r.getId(),Error__c=errors));
				}
			}
		}
		if(errlogs.size()>0)
			insert errlogs;
	}
}
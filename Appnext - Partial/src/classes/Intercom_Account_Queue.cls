public class Intercom_Account_Queue implements Queueable{
	public String StartPoint;

    public List <Lead> ConvertSf;
    public List <Intercom_Sync.IntercomLead> Leads4UpdateIn;
    
    public List <Intercom_Sync.IntercomLead> syncbacks;
    
    public List <Account> accs;
	public List <Account> leftaccs;
	public List <Intercom_Sync.IntercomLead> backs;
	public Map <String,String> Accids;
	public Map <String,String> newAccids;
	public Intercom_Account_Queue(List <Account> acs,List <Intercom_Sync.IntercomLead> bks, List <Lead> ql4c,List <Intercom_Sync.IntercomLead> ld4c, List <Intercom_Sync.IntercomLead> sbacks, String StartP) {
        StartPoint = StartP;
		syncbacks = sbacks;
        ConvertSf=ql4c;
        Leads4UpdateIn=ld4c;
		if(bks==null){
			backs=new List<Intercom_Sync.IntercomLead>();
			for(Account acc:acs)
				if(acc.id==null)
					backs.add(new Intercom_Sync.IntercomLead(acc.Intercom_id__c,acc.Email__c,acc.Phone__c,null,'contact'));
		}
		if(acs.size()<5000){
			accs=acs;
			leftaccs=new List <Account>();
		}
		else{
			accs=new List <Account>();
			for(integer i=0;i<5000&&acs.size()>0;i++){
				accs.add(acs[0]);
				acs.remove(0);
			}
			leftaccs=acs;
		}
		Accids = new Map <String,String>();
		newAccids = new Map <String,String>();
		for(Account acc:accs){
			String accid=acc.id;
			if(accid!=null)
				Accids.put(acc.Intercom_id__c,acc.id);
			else
				newAccids.put(acc.Intercom_id__c,null);
		}
	}
	public void execute(QueueableContext context) {
		System.debug('all accs: '+accs);
		List <Contact> cons= new List <Contact>();
		List <Contact> nsyncons= new List <Contact>();
		for(Account acc:accs)
			acc.Integration_updated_at__c=System.Now();
		List <Intercom_Log__c> errlogs = new List <Intercom_Log__c>();
		Database.UpsertResult[] resAccs = Database.Upsert(accs, false);
		for (Database.UpsertResult r : resAccs) {
			if(!r.isSuccess()||Test.IsRunningTest()){
				String errors='';
				for(Database.Error e:r.getErrors())
					errors+=e.getMessage()+'\n';
				errlogs.add(new Intercom_Log__c(Side__c='Salesforce',Record_Id__c=r.getId(),Error__c=errors));
			}
		}
		System.debug('all accs: '+accs);
		for(Account acc:accs)
			if(newAccids.containsKey(acc.Intercom_id__c)){
				newAccids.put(acc.Intercom_id__c,acc.id);
				String s = (acc.Last_Name__c!=null&&acc.Last_Name__c!=''?acc.Last_Name__c:(acc.LastName__c!=null&&acc.LastName__c!=''?acc.LastName__c:(acc.Name!=null&&acc.Name!=''?acc.Name:(acc.Email__c!=null&&acc.Email__c!=''?acc.Email__c:'empty'))));
				if(s==null||s=='')
					System.debug('Alert: '+acc.id);
				cons.add(new Contact(Intercom_id__c=acc.Intercom_id__c,Accountid=acc.id,Email=acc.Email__c,Phone=acc.Phone__c,LastName=s,FirstName=acc.First_Name__c,Title=acc.Job_title__c,LinkedIn__c=acc.LinkedIn__c));
			}
			else if(Accids.containsKey(acc.Intercom_id__c)){
				if(acc.Contact__c!=null)
					nsyncons.add(new Contact(id=acc.Contact__c,AccountId=acc.id,Intercom_id__c=acc.Intercom_id__c,Title=(acc.Job_title__c!=null?acc.Job_title__c:null)));
				else
					cons.add(new Contact(Intercom_id__c=acc.Intercom_id__c,Title=(acc.Job_title__c!=null?acc.Job_title__c:null)));
			}
		for(Intercom_Sync.IntercomLead ild:backs){
			if(newAccids.containsKey(ild.id))
				ild.custom_attributes.sf_id=newAccids.get(ild.id);
			else if(Accids.containsKey(ild.id))
				ild.custom_attributes.sf_id=Accids.get(ild.id);
		}
		System.debug('not synced cons: '+nsyncons);
		System.debug('Contacts: '+cons);
		System.debug('Backs: '+backs+' newaccids: '+newAccids);
		if(Test.IsRunningTest()){
			nsyncons.add([select id from contact limit 1]);
		}
		Schema.SObjectField f = Contact.Fields.Intercom_id__c;
		Database.UpsertResult[] resCons = Database.Upsert(cons, f, false);
		for (Database.UpsertResult r : resCons) {
			if(!r.isSuccess()||Test.IsRunningTest()){
				String errors='';
				for(Database.Error e:r.getErrors())
					errors+=e.getMessage()+'\n';
				errlogs.add(new Intercom_Log__c(Side__c='Salesforce',Record_Id__c=r.getId(),Error__c=errors));
			}
		}
		Database.UpsertResult[] resNCons = Database.Upsert(nsyncons, false);
		for (Database.UpsertResult r : resNCons) {
			if(!r.isSuccess()||Test.IsRunningTest()){
				String errors='';
				for(Database.Error e:r.getErrors())
					errors+=e.getMessage()+'\n';
				errlogs.add(new Intercom_Log__c(Side__c='Salesforce',Record_Id__c=r.getId(),Error__c=errors));
			}
		}
		insert errlogs;
		if(!Test.IsRunningTest()){
			if(leftaccs.size()>0)
				ID jobID1 = System.enqueueJob(new Intercom_Account_Queue(leftaccs,backs,ConvertSf,Leads4UpdateIn,syncbacks,StartPoint));
            else{
                if(syncbacks!=null)
                    backs.addAll(syncbacks);
				ID jobID2 = System.enqueueJob(new ConvertLeads_Queue(ConvertSf,Leads4UpdateIn,backs,StartPoint));
            }
		}
		else{
			integer i=0;
			i++;
		}
	}
}
/*
 * @Author: ITmyWay.ZedXagE 
 * @Date: 2018-02-19 10:44:08 
 * @Last Modified by:   ITmyWay.ZedXagE 
 * @Last Modified time: 2018-02-19 10:44:08 
 */
@RestResource(urlMapping='/CloseIOBigEmails')
global class Close_IO_Migration_Big_Emails implements Queueable {
	global List <Close_IO_Migration.IOActivity> ioacts;
	global Map <String,String> usersio;
	global Map <String,String> usersname;
	global Map <String,String> accrecs;
	global Map <String,String> opprecs;
	global Map <String,String> campins;
	@HttpPut
	global static void Init() {
		String datajson=RestContext.request.requestBody.toString();
		System.debug(datajson);
		List <Close_IO_Migration.IOActivity> ioacts;
		try{
			ioacts =(List <Close_IO_Migration.IOActivity>)JSON.deserialize(datajson, List<Close_IO_Migration.IOActivity>.class);
			Map <String,String> usersio=Close_IO_Migration.getUsersIO();
			Map <String,String> usersname=Close_IO_Migration.getUsersName();
			Map <String,String> accrecs=Close_IO_Migration.getRecTypes('Account');
			Map <String,String> opprecs=Close_IO_Migration.getRecTypes('Opportunity');
			Map <String,String> campins=Close_IO_Migration.getCamp();
			ID jobID = System.enqueueJob(new Close_IO_Migration_Big_Emails(ioacts,usersio,usersname,accrecs,opprecs,campins));
		}
		catch(exception e){
			insert new Intercom_Log__c(Side__c='Close IO email json error',Error__c=datajson.left(131072));
		}
	}
	global Close_IO_Migration_Big_Emails(List <Close_IO_Migration.IOActivity> icts,Map <String,String> usio,Map <String,String> usname,Map <String,String> accr,Map <String,String> oppr,Map <String,String> cmps) {
		ioacts=icts;
		usersio=usio;
		usersname=usname;
		accrecs=accr;
		opprecs=oppr;
		campins=cmps;
	}
	global void execute(QueueableContext context) {
		if(ioacts.size()>0){
			if(ioacts[0]!=null){
				Close_IO_Migration.IOActivity ioact=ioacts[0];
				//email
				if(ioact.envelope!=null){
					Task sfema = new Task();
					sfema.Close_io_ID__c=ioact.id;
					sfema.Subject='Email '+ioact.status+': '+ioact.subject;
					if(sfema.Subject!=null)
						sfema.Subject=sfema.Subject.left(254);
					sfema.CreatedDate=Close_IO_Migration.getDateTime(ioact.date_created);
					sfema.Ownerid=Close_IO_Migration.checkMap(usersio,ioact.user_id);
					sfema.Ownerid=sfema.Ownerid!=null?sfema.Ownerid:Close_IO_Migration.defOwnerid;
					sfema.LastModifiedById=Close_IO_Migration.checkMap(usersio,ioact.updated_by);
					sfema.CreatedById=Close_IO_Migration.CheckMap(usersio,ioact.created_by);
					sfema.LastModifiedDate=Close_IO_Migration.getDateTime(ioact.date_updated)>Close_IO_Migration.getDateTime(ioact.date_created)?Close_IO_Migration.getDateTime(ioact.date_updated):Close_IO_Migration.getDateTime(ioact.date_created);
					if(ioact.contact_id!=null){
						List <contact> cons = [select id from Contact where Contact_Close_io_ID__c=:ioact.contact_id];
						if(cons.size()>0)
							sfema.Whoid=cons[0].id;
						sfema.Close_io_parent__c=ioact.contact_id;
					}
					else if(ioact.lead_id!=null){
						List <Account> accs = [select id from Account where Close_io_ID__c=:ioact.lead_id];
						if(accs.size()>0)
							sfema.Whatid=accs[0].id;
						sfema.Close_io_parent__c=ioact.lead_id;
					}
					sfema.type='Email';
					sfema.Status='Completed';
					sfema.ActivityDate=Close_IO_Migration.getDateTime(ioact.date_sent)!=null?Close_IO_Migration.getDate(Close_IO_Migration.getDateTime(ioact.date_sent)):(Close_IO_Migration.getDateTime(ioact.date_created)!=null?Close_IO_Migration.getDate(Close_IO_Migration.getDateTime(ioact.date_created)):null);
					sfema.description='From: ';
					integer i=0;
					for(Close_IO_Migration.IOperson sender:ioact.envelope.sender){
						sfema.description+=i==0?sender.name+' <'+sender.email+'>':(', '+sender.name+' <'+sender.email+'>');
						i++;
					}
					sfema.description+='\nTo: ';
					i=0;
					for(Close_IO_Migration.IOperson to:ioact.envelope.to){
						sfema.description+=i==0?to.name+' <'+to.email+'>':(', '+to.name+' <'+to.email+'>');
						i++;
					}
					sfema.description+='\nCC: ';
					i=0;
					for(Close_IO_Migration.IOperson cc:ioact.envelope.cc){
						sfema.description+=i==0?cc.name+' <'+cc.email+'>':(', '+cc.name+' <'+cc.email+'>');
						i++;
					}
					sfema.description+='\nBCC: ';
					i=0;
					for(Close_IO_Migration.IOperson bcc:ioact.envelope.bcc){
						sfema.description+=i==0?bcc.name+' <'+bcc.email+'>':(', '+bcc.name+' <'+bcc.email+'>');
						i++;
					}
					sfema.description+='\nBody:\n\n'+ioact.body_text+'\n\nAttachment: ';
					i=0;
					for(Close_IO_Migration.IOAttachment ioatt:ioact.attachments){
						sfema.description+=i==0?ioatt.url:(', '+ioatt.url);
						i++;
					}
					if(sfema.description.length()>32000)
						sfema.description=sfema.description.left(32000);
					try{
						Upsert sfema;
					}
					catch(exception e){
						System.debug('Error in close io Big Email');
						String errors='';
						for (Integer j = 0; j < e.getNumDml(); j++) {
							System.debug(e.getDmlMessage(j));
							errors+=e.getDmlMessage(j)+', ';
						}
						insert new Intercom_Log__c(Side__c='Close IO Big  ',Record_Id__c=ioact.id,Error__c=errors);
					}
				}
				//note
				if(ioact.note!=null){
					Note sfnt = new Note();
					sfnt.CreatedDate=Close_IO_Migration.getDateTime(ioact.date_created);
					sfnt.Ownerid=Close_IO_Migration.checkMap(usersio,ioact.user_id);
					sfnt.Ownerid=sfnt.Ownerid!=null?sfnt.Ownerid:Close_IO_Migration.defOwnerid;
					sfnt.LastModifiedById=Close_IO_Migration.checkMap(usersio,ioact.updated_by);
					sfnt.CreatedById=Close_IO_Migration.CheckMap(usersio,ioact.created_by);
					sfnt.LastModifiedDate=Close_IO_Migration.getDateTime(ioact.date_updated)>Close_IO_Migration.getDateTime(ioact.date_created)?Close_IO_Migration.getDateTime(ioact.date_updated):Close_IO_Migration.getDateTime(ioact.date_created);
					if(ioact.note!=null&&ioact.note!=''){
						sfnt.Title=ioact.note.left(79);
						sfnt.Body=ioact.note.left(31999);
					}
					else{
						sfnt.Title='empty';
						sfnt.Body='empty';
					}
					if(ioact.lead_id!=null){
						List <Account> accs = [select id from Account where Close_io_ID__c=:ioact.lead_id];
						if(accs.size()>0)
							sfnt.Parentid = accs[0].id;
					}
					try{
						Upsert sfnt;
					}
					catch(exception e){
						System.debug('Error in close io Big Note object');
						String errors='';
						for (Integer i = 0; i < e.getNumDml(); i++) {
							System.debug(e.getDmlMessage(i));
							errors+=e.getDmlMessage(i)+', ';
						}
						insert new Intercom_Log__c(Side__c='Close IO Big Note',Record_Id__c=ioact.id,Error__c=errors);
					}
					/*String contid=sfnt.id;
					if(contid!=null){
						ContentDocumentLink link2 = new ContentDocumentLink();
						link2.ContentDocumentId = contid;
						if(ioact.lead_id!=null){
							List <Account> accs = [select id from Account where Close_io_ID__c=:ioact.lead_id];
							if(accs.size()>0)
								link2.LinkedEntityId=accs[0].id;
						}
						link2.ShareType = 'V';
						link2.Visibility = 'AllUsers';
						try{
							insert link2;
						}
						catch(exception e){
							System.debug('Error in close io Big Note Link');
							String errors='';
							for (Integer i = 0; i < e.getNumDml(); i++) {
								System.debug(e.getDmlMessage(i));
								errors+=e.getDmlMessage(i)+', ';
							}
							insert new Intercom_Log__c(Side__c='Close IO Big Note Link',Record_Id__c=ioact.id,Error__c=errors);
						}
					}*/
				}
			}
			ioacts.remove(0);
			if(!Test.isRunningTest())
				ID jobID = System.enqueueJob(new Close_IO_Migration_Big_Emails(ioacts,usersio,usersname,accrecs,opprecs,campins));
		}
	}
}
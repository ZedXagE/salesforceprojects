public class ConvertLeads_Queue implements Queueable{
	public String StartPoint;
	private List <Lead> ConvertSf;
	private List <Intercom_Sync.IntercomLead> Leads4UpdateIn;
    public List <Intercom_Sync.IntercomLead> syncbacks;

	public ConvertLeads_Queue (List <Lead> ql4c,List <Intercom_Sync.IntercomLead> ld4c,List <Intercom_Sync.IntercomLead> sbks, String StartP){
		StartPoint = StartP;
        syncbacks=sbks;
		this.ConvertSf=ql4c;
		this.Leads4UpdateIn=ld4c;
	}
	public void execute(QueueableContext context) {
		if(ConvertSf.size()>0){
			try{
				Update ConvertSf[0];
			}
			catch(exception e){
				insert new Intercom_Log__c(Side__c='Salesforce',Record_Id__c=ConvertSf[0].id,Error__c=e.getMessage());
			}
			String lid=ConvertSf[0].id;
			Database.LeadConvert lc = new Database.LeadConvert();
			lc.setLeadId(ConvertSf[0].id);
    		lc.setDoNotCreateOpportunity(true);
			LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
			lc.setConvertedStatus(convertStatus.MasterLabel);
			try{
				Database.LeadConvertResult lcr = Database.convertLead(lc);
				Boolean suces=false;
				if(lcr.isSuccess()){
					suces=true;
					String accid=lcr.getAccountId();
					for(Intercom_Sync.IntercomLead ilead:Leads4UpdateIn)
						if(ilead.custom_attributes.sf_id==lid)
							ilead.custom_attributes.sf_id=accid;
					if(Test.IsRunningTest())
						suces=false;
				}
				if(suces==false){
					String errors='';
					for(Database.Error e:lcr.getErrors())
						errors+=e.getMessage()+'\n';
					insert new Intercom_Log__c(Side__c='Salesforce',Record_Id__c=ConvertSf[0].id,Error__c=errors);
				}
			}
			catch (exception e){
				insert new Intercom_Log__c(Side__c='Salesforce',Record_Id__c=ConvertSf[0].id,Error__c=e.getMessage());
			}
			finally{
				ConvertSf.remove(0);
				if(!Test.IsRunningTest())
					ID jobID = System.enqueueJob(new ConvertLeads_Queue(ConvertSf,Leads4UpdateIn,syncbacks,StartPoint));
			}
		}
		else{
            if(!Test.IsRunningTest()){
                if(syncbacks!=null)
                	Leads4UpdateIn.addAll(syncbacks);
				ID jobID1 = System.enqueueJob(new Intercom_SyncBack_Queue(Leads4UpdateIn,StartPoint));
            }
		}
   	}
}
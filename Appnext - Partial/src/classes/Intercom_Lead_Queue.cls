public class Intercom_Lead_Queue implements Queueable{
    public String StartPoint;
    public List <Account> accs;
    public List <Intercom_Sync.IntercomLead> accbks;
    
    public List <Lead> ConvertSf;
    public List <Intercom_Sync.IntercomLead> Leads4UpdateIn;
    
    public List <Intercom_Sync.IntercomLead> syncbacks;

	public List <Lead> lds;
	public List <Lead> leftlds;
	public List <Intercom_Sync.IntercomLead> backs;
	public Map <String,String> ldids;
	public Map <String,String> newldids;
	public Intercom_Lead_Queue(List <Lead> ls,List <Intercom_Sync.IntercomLead> bks, List <Account> acs,List <Intercom_Sync.IntercomLead> acsbks, List <Lead> ql4c,List <Intercom_Sync.IntercomLead> ld4c, List <Intercom_Sync.IntercomLead> sbacks, String StartP) {
        StartPoint = StartP;
		syncbacks = sbacks;
        accs=acs;
        accbks=acsbks;
        ConvertSf=ql4c;
        Leads4UpdateIn=ld4c;
		if(bks==null){
			backs=new List<Intercom_Sync.IntercomLead>();
			for(Lead ld:ls)
				if(ld.id==null)
					backs.add(new Intercom_Sync.IntercomLead(ld.Intercom_id__c,ld.Email,ld.Phone,null,'contact'));
		}
		if(ls.size()<5000){
			lds=ls;
			leftlds=new List <Lead>();
		}
		else{
			lds=new List <Lead>();
			for(integer i=0;i<5000&&lds.size()>0;i++){
				lds.add(lds[0]);
				ls.remove(0);
			}
			leftlds=ls;
		}
		ldids = new Map <String,String>();
		newldids = new Map <String,String>();
		for(Lead ld:lds){
			String ldid=ld.id;
			if(ldid!=null)
				ldids.put(ld.Intercom_id__c,ld.id);
			else
				newldids.put(ld.Intercom_id__c,null);
		}
	}
	public void execute(QueueableContext context) {
		for(Lead ld:lds)
			ld.Integration_updated_at__c=System.Now();
		List <Intercom_Log__c> errlogs = new List <Intercom_Log__c>();
		Database.UpsertResult[] resAccs = Database.Upsert(lds, false);
		for (Database.UpsertResult r : resAccs) {
			if(!r.isSuccess()||Test.IsRunningTest()){
				String errors='';
				for(Database.Error e:r.getErrors())
					errors+=e.getMessage()+'\n';
				errlogs.add(new Intercom_Log__c(Side__c='Salesforce',Record_Id__c=r.getId(),Error__c=errors));
			}
		}
		for(Lead ld:lds)
			if(newldids.containsKey(ld.Intercom_id__c))
				newldids.put(ld.Intercom_id__c,ld.id);
		for(Intercom_Sync.IntercomLead ild:backs){
			if(newldids.containsKey(ild.id))
				ild.custom_attributes.sf_id=newldids.get(ild.id);
			else if(ldids.containsKey(ild.id))
				ild.custom_attributes.sf_id=ldids.get(ild.id);
		}
		insert errlogs;
		if(!Test.IsRunningTest()){
            if(leftlds.size()>0)
				ID jobID1 = System.enqueueJob(new Intercom_Lead_Queue(leftlds,backs,accs,accbks,ConvertSf,Leads4UpdateIn,syncbacks,StartPoint));
            else{
                if(syncbacks!=null)
                	backs.addAll(syncbacks);
				ID jobID2 = System.enqueueJob(new Intercom_Account_Queue(accs,accbks,ConvertSf,Leads4UpdateIn,backs,StartPoint));
            }
		}

	}
}
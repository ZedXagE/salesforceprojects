@isTest(seealldata=true)
public class Tests_IntercomIO {
	public static String inst='appnext.my';
	static testMethod void testleads(){
		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();
		StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'ioleads' LIMIT 1];
		req.requestBody=sr.Body;
		req.requestURI = 'https://'+inst+'.salesforce.com/CloseIO';
		req.httpMethod = 'PUT';
		RestContext.request = req;
		RestContext.response = res;
		Close_IO_Migration.Init();
	}
	static testMethod void testemails(){
		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();
		StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'ioemails' LIMIT 1];
		req.requestBody=sr.Body;
		req.requestURI = 'https://'+inst+'.salesforce.com/CloseIOBigEmails';
		req.httpMethod = 'PUT';
		RestContext.request = req;
		RestContext.response = res;
		Close_IO_Migration_Big_Emails.Init();
	}
	static testMethod void testnotes(){
		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();
		StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'ionotes' LIMIT 1];
		req.requestBody=sr.Body;
		req.requestURI = 'https://'+inst+'.salesforce.com/CloseIOBigEmails';
		req.httpMethod = 'PUT';
		RestContext.request = req;
		RestContext.response = res;
		Close_IO_Migration_Big_Emails.Init();
	}
	static testMethod void testFirstSync(){
		Lead ld1 = new Lead(Email='testnewcontact1@gmail.com',lastname='test1',Company='test1');
		insert ld1;
		Lead ld2 = new Lead(Email='testnewconvert4@gmail.com',lastname='test4',Company='test4');
		insert ld2;
		Account acc = new Account(Name='test1');
		insert acc;
		insert new Contact (Accountid=acc.id,lastname='test1',Email='testnewuser1@gmail.com');
		Account acc1 = new Account(Name='test2');
		insert acc1;
		contact con = new Contact (Accountid=acc1.id,lastname='test2',Email='testnewuser2@gmail.com');
		insert con;
		Intercom_Sync.TryInit(null,null);
		Intercom_Sync.IntercomLead ild1 = new Intercom_Sync.IntercomLead('test','test@test.com','05067856754','','contact');
		ild1.changed(ld1);
		ild1.changed(acc);
		Intercom_Sync.UpsertAccounts(new List <Account>{acc});
		Intercom_Sync.UpsertLeads(new List <Lead>{ld1});
		Intercom_Sync.IntercomLead ild2 = new Intercom_Sync.IntercomLead('test','test@test.com','05067856754','','contact',1);
		Intercom_Sync.IntercomLead ild3 = new Intercom_Sync.IntercomLead('test','test@test.com','05067856754','','contact',1,'test','','','','','','');
		ild3.companies = new Intercom_Sync.rescompany();
		ild3.companies.companies = new List <Intercom_Sync.company>();
		ild3.getName();
		Intercom_Sync.company comp = new Intercom_Sync.company();
		comp.name='test';
		ild3.companies.companies.add(comp);
		ild3.getName();
		ild3.name='test';
		ild3.getName();
		ild3.email=null;
		ild3.getLastName();
		ild3.email='test@tewst.com';
		ild3.getLastName();
		ild3.custom_attributes.Last_name_c='test';
		ild3.getLastName();
		ild3.getNameCompany(ild3.email);
		ild3.location_data = new Intercom_Sync.locationData();
		ild3.tags = new Intercom_Sync.restag();
		ild3.tags.tags = new List <Intercom_Sync.tag>();
		lead ld =ild3.Lead('i');
		ld=ild3.Lead('s');
		ld=ild3.Lead(ld2,'i');
		ld=ild3.Lead(ld2,'s');
		update ld1;
		update ld2;
		update acc;
		update acc1;
		Intercom_Sync ins = new Intercom_Sync();
		ins.execute(null);
		ins.getTimeDelay(System.now(),System.now());
		ins.getTimeDelay(0,1);
		Intercom_Sync.NewIntercomLead testnew = new Intercom_Sync.NewIntercomLead(ild1);
		ID jobID = System.enqueueJob(new Intercom_Lead_Queue(new List <Lead>{new Lead()},null,null,null,null,null,null,null));
        ID jobID1 = System.enqueueJob(new Intercom_Account_Queue(new List <Account>{new Account()},null,null,null,null,null));
		ID jobID2 = System.enqueueJob(new ConvertLeads_Queue(new List <Lead>{ld1},new List <Intercom_Sync.IntercomLead>{ild2},null,null));
		ID jobID3 = System.enqueueJob(new ConvertLeads_Queue(new List <Lead>{new Lead()},new List <Intercom_Sync.IntercomLead>(),null,null));
        ID jobID4 = System.enqueueJob(new Intercom_SyncBack_Queue(new List <Intercom_Sync.IntercomLead>{ild2,ild3},null));

        Intercom_Sync.fortestloop();
	}
	static testMethod void testTrigger(){
		Lead ld1 = new Lead(Email='testnewcontact1@gmail.com',lastname='test1',Company='test1',intercom_id__c='12341');
		insert ld1;
		Lead ld2 = new Lead(Email='testnewconvert4@gmail.com',lastname='test4',Company='test4',intercom_id__c='12342');
		insert ld2;
		Account acc = new Account(Name='test1',intercom_id__c='12345');
		insert acc;
		insert new Contact (Accountid=acc.id,lastname='test1',Email='testnewuser1@gmail.com',intercom_id__c='12343');
		Account acc1 = new Account(Name='test2',intercom_id__c='12344');
		insert acc1;
		contact con = new Contact (Accountid=acc1.id,lastname='test2',Email='testnewuser2@gmail.com',intercom_id__c='12344');
		insert con;
		Intercom_Sync.TryInit(null,null);
		Intercom_Sync.IntercomLead ild1 = new Intercom_Sync.IntercomLead('test','test@test.com','05067856754','','contact');
		ild1.changed(ld1);
		ild1.changed(acc);
		Intercom_Sync.UpsertAccounts(new List <Account>{acc});
		Intercom_Sync.UpsertLeads(new List <Lead>{ld1});
		Intercom_Sync.IntercomLead ild2 = new Intercom_Sync.IntercomLead('test','test@test.com','05067856754','','contact',1);
		Intercom_Sync.IntercomLead ild3 = new Intercom_Sync.IntercomLead('test','test@test.com','05067856754','','contact',1,'test','','','','','','');
		ild3.companies = new Intercom_Sync.rescompany();
		ild3.companies.companies = new List <Intercom_Sync.company>();
		ild3.getName();
		Intercom_Sync.company comp = new Intercom_Sync.company();
		comp.name='test';
		ild3.companies.companies.add(comp);
		ild3.getName();
		ild3.name='test';
		ild3.getName();
		ild3.email=null;
		ild3.getLastName();
		ild3.email='test@tewst.com';
		ild3.getLastName();
		ild3.custom_attributes.Last_name_c='test';
		ild3.getLastName();
		ild3.getNameCompany(ild3.email);
		ild3.location_data = new Intercom_Sync.locationData();
		ild3.tags = new Intercom_Sync.restag();
		ild3.tags.tags = new List <Intercom_Sync.tag>();
		lead ld =ild3.Lead('i');
		ld=ild3.Lead('s');
		ld=ild3.Lead(ld2,'i');
		ld=ild3.Lead(ld2,'s');
		update ld1;
		update ld2;
		update acc;
		update acc1;
		delete ld1;
		delete con;
		delete acc;
	}
}
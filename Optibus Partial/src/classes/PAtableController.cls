public class PAtableController{
    public static Map <String,Integer> order = new Map <String,Integer>{'NA'=>1,'UK'=>2,'GER'=>3,'ISR'=>4,'ROW'=>5};
    public String year{get;set;}
    public List <selectoption> years {get;set;}
    public List <record> records {get;set;}
    public class record implements Comparable{
        public String location {get;set;}
        public String type {get;set;}
        public String ReportQ1 {get;set;}
        public String ReportQ2 {get;set;}
        public String ReportQ3 {get;set;}
        public String ReportQ4 {get;set;}
        public String ReportYearly {get;set;}
        public String ReportPilotQ1 {get;set;}
        public String ReportPilotQ2 {get;set;}
        public String ReportPilotQ3 {get;set;}
        public String ReportPilotQ4 {get;set;}
        public String ReportPilotYearly {get;set;}
        public Planned__c planned {get;set;}
        public Integer ActualQ1 {get;set;}
        public Integer ActualQ2 {get;set;}
        public Integer ActualQ3 {get;set;}
        public Integer ActualQ4 {get;set;}
        public Integer PilotActualQ1 {get;set;}
        public Integer PilotActualQ2 {get;set;}
        public Integer PilotActualQ3 {get;set;}
        public Integer PilotActualQ4 {get;set;}
        public Decimal PlannedTotalQ1 {get;set;}
        public Decimal PlannedTotalQ2 {get;set;}
        public Decimal PlannedTotalQ3 {get;set;}
        public Decimal PlannedTotalQ4 {get;set;}
        public Integer ActualTotal {get;set;}
        public Integer PilotActualTotal {get;set;}
        public Decimal PlannedTotal {get;set;}
        public record(){
            ActualQ1=0;
            ActualQ2=0;
            ActualQ3=0;
            ActualQ4=0;
            PilotActualQ1=0;
            PilotActualQ2=0;
            PilotActualQ3=0;
            PilotActualQ4=0;
            ActualTotal=0;
            PilotActualTotal=0;
            PlannedTotal=0;
            PlannedTotalQ1=0;
            PlannedTotalQ2=0;
            PlannedTotalQ3=0;
            PlannedTotalQ4=0;
        }
        public Integer compareTo(Object compareTo) {
            // Cast argument to OpportunityWrapper
            record comprec = (record)compareTo;
            // The return value of 0 indicates that both elements are equal.
            Integer returnValue = 0;
            if(order.containsKey(this.location)&&order.containsKey(comprec.location)){
                if (order.get(this.location) > order.get(comprec.location)) {
                    // Set return value to a positive value.
                    returnValue = 1;
                } else if (order.get(this.location) < order.get(comprec.location)) {
                    // Set return value to a negative value.
                    returnValue = -1;
                }
            }
            return returnValue;      
        }
    }
    public PAtableController() {
        years = new List <selectoption>();
        Date mydat=System.Today();
        year=String.valueof(mydat.Year());
        mydat=mydat.addYears(+3);
        years.add(new selectoption(String.valueof(mydat.Year()),String.valueof(mydat.Year())));
        mydat=mydat.addYears(-1);
        years.add(new selectoption(String.valueof(mydat.Year()),String.valueof(mydat.Year())));
        mydat=mydat.addYears(-1);
        years.add(new selectoption(String.valueof(mydat.Year()),String.valueof(mydat.Year())));
        mydat=mydat.addYears(-1);
        years.add(new selectoption(String.valueof(mydat.Year()),String.valueof(mydat.Year())));
        mydat=mydat.addYears(-1);
        years.add(new selectoption(String.valueof(mydat.Year()),String.valueof(mydat.Year())));
        mydat=mydat.addYears(-1);
        years.add(new selectoption(String.valueof(mydat.Year()),String.valueof(mydat.Year())));
        InitTable();
    }
    public void InitTable(){
        records=new List <record>();
        Map <String,Date> Quarters = new Map <String,Date>();
        Quarters.put('Q0', date.newInstance(Integer.valueof(year),1,1));
        Quarters.put('Q1', date.newInstance(Integer.valueof(year),4,1));
        Quarters.put('Q2', date.newInstance(Integer.valueof(year),7,1));
        Quarters.put('Q3', date.newInstance(Integer.valueof(year),10,1));
        Quarters.put('Q4', date.newInstance(Integer.valueof(year)+1,1,1));
        record rec=new record();
        String tempReg='';
        List <String> locations = new List <String>{'GER','ISR','NA','ROW','UK'};
        //Leads
        List <Lead> leads = [select id,Region__c,CreatedDate from Lead where (lead.Lead_Type__c = 'Operator' or lead.Lead_Type__c = 'Agency') and CreatedDate>=:Quarters.get('Q0') and CreatedDate<:Quarters.get('Q4')  Order by Region__c];
        if(leads.size()>0){
            rec=new record();
            tempReg='';
            for(Lead rc:leads){
                if(rc.Region__c!=tempReg&&tempreg!=''){
                    records.add(rec);
                    rec=new record();
                }
                rec.location=rc.Region__c;
                rec.type='Leads';
                if(rc.CreatedDate<Quarters.get('Q1'))
                    rec.ActualQ1++;
                else if(rc.CreatedDate<Quarters.get('Q2'))
                    rec.ActualQ2++;
                else if(rc.CreatedDate<Quarters.get('Q3'))
                    rec.ActualQ3++;
                else
                    rec.ActualQ4++;
                tempReg=rc.Region__c;
            }
            records.add(rec);
        }
        AddNone('Leads');

        //Opportunities
        List <Opportunity> Opportunities = [select id,Region__c,CreatedDate from Opportunity where CreatedDate>=:Quarters.get('Q0') and CreatedDate<:Quarters.get('Q4') and Contract_Type__c ='Subscription' and Type = 'New Business' Order by Region__c];
        if(Opportunities.size()>0){
            rec = new record();
            tempReg='';
            for(Opportunity rc:Opportunities){
                if(rc.Region__c!=tempReg&&tempreg!=''){
                    records.add(rec);
                    rec = new record();
                }
                rec.location=rc.Region__c;
                rec.type='Opportunities';
                if(rc.CreatedDate!=null){
                    if(rc.CreatedDate>=Quarters.get('Q0')&&rc.CreatedDate<Quarters.get('Q4')){
                        if(rc.CreatedDate<Quarters.get('Q1'))
                            rec.ActualQ1++;
                        else if(rc.CreatedDate<Quarters.get('Q2'))
                            rec.ActualQ2++;
                        else if(rc.CreatedDate<Quarters.get('Q3'))
                            rec.ActualQ3++;
                        else
                            rec.ActualQ4++;
                    }
                }
                tempReg=rc.Region__c;
            }
            records.add(rec);
        }
        AddNone('Opportunities');

        //Pilot
        List <Opportunity> pilot = [select id,Region__c,Pilot_Start_Date__c,Pilot_Expected_Start_Date__c from Opportunity where ( (Pilot_Start_Date__c>=:Quarters.get('Q0') and Pilot_Start_Date__c<:Quarters.get('Q4')) OR (Pilot_Expected_Start_Date__c>=:Quarters.get('Q0') and Pilot_Expected_Start_Date__c<:Quarters.get('Q4')) ) and Contract_Type__c ='Subscription' and Type = 'New Business' Order by Region__c];
        if(pilot.size()>0){
            rec = new record();
            tempReg='';
            for(Opportunity rc:pilot){
                if(rc.Region__c!=tempReg&&tempreg!=''){
                    records.add(rec);
                    rec = new record();
                }
                rec.location=rc.Region__c;
                rec.type='Pilot';
                if(rc.Pilot_Start_Date__c!=null){
                    if(rc.Pilot_Start_Date__c>=Quarters.get('Q0')&&rc.Pilot_Start_Date__c<Quarters.get('Q4')){
                        if(rc.Pilot_Start_Date__c<Quarters.get('Q1'))
                            rec.ActualQ1++;
                        else if(rc.Pilot_Start_Date__c<Quarters.get('Q2'))
                            rec.ActualQ2++;
                        else if(rc.Pilot_Start_Date__c<Quarters.get('Q3'))
                            rec.ActualQ3++;
                        else
                            rec.ActualQ4++;
                    }
                }
                //Pilot
                if(rc.Pilot_Expected_Start_Date__c!=null){
                    if(rc.Pilot_Expected_Start_Date__c>=Quarters.get('Q0')&&rc.Pilot_Expected_Start_Date__c<Quarters.get('Q4')){
                        if(rc.Pilot_Expected_Start_Date__c<Quarters.get('Q1'))
                            rec.PilotActualQ1++;
                        else if(rc.Pilot_Expected_Start_Date__c<Quarters.get('Q2'))
                            rec.PilotActualQ2++;
                        else if(rc.Pilot_Expected_Start_Date__c<Quarters.get('Q3'))
                            rec.PilotActualQ3++;
                        else
                            rec.PilotActualQ4++;
                    }
                }
                tempReg=rc.Region__c;
            }
            records.add(rec);
        }
        AddNone('Pilot');

        //Commercial
        List <Opportunity> commercial = [select id,Region__c,Commercial_Date__c from Opportunity where Commercial_Date__c>=:Quarters.get('Q0') and Commercial_Date__c<:Quarters.get('Q4') and Contract_Type__c ='Subscription' and Type = 'New Business' Order by Region__c];
        if(commercial.size()>0){
            rec=new record();
            tempReg='';
            for(Opportunity rc:commercial){
                if(rc.Region__c!=tempReg&&tempreg!=''){
                    records.add(rec);
                    rec=new record();
                }
                rec.location=rc.Region__c;
                rec.type='Commercial';
                if(rc.Commercial_Date__c<Quarters.get('Q1'))
                    rec.ActualQ1++;
                else if(rc.Commercial_Date__c<Quarters.get('Q2'))
                    rec.ActualQ2++;
                else if(rc.Commercial_Date__c<Quarters.get('Q3'))
                    rec.ActualQ3++;
                else
                    rec.ActualQ4++;
                tempReg=rc.Region__c;
            }
            records.add(rec);
        }
        AddNone('Commercial');
        
        //Closed Won
        List <Opportunity> closedwon = [select id,Region__c,Closed_Won_Date__c from Opportunity where Closed_Won_Date__c>=:Quarters.get('Q0') and Closed_Won_Date__c<:Quarters.get('Q4') and Contract_Type__c ='Subscription' and Type = 'New Business' Order by Region__c];
        if(closedwon.size()>0){
            rec=new record();
            tempReg='';
            for(Opportunity rc:closedwon){
                if(rc.Region__c!=tempReg&&tempreg!=''){
                    records.add(rec);
                    rec=new record();
                }
                rec.location=rc.Region__c;
                rec.type='Closed Won';
                if(rc.Closed_Won_Date__c<Quarters.get('Q1'))
                    rec.ActualQ1++;
                else if(rc.Closed_Won_Date__c<Quarters.get('Q2'))
                    rec.ActualQ2++;
                else if(rc.Closed_Won_Date__c<Quarters.get('Q3'))
                    rec.ActualQ3++;
                else
                    rec.ActualQ4++;
                tempReg=rc.Region__c;
            }
            records.add(rec);
        }
        AddNone('Closed Won');

        //Closed Lost
        List <Opportunity> closedlost = [select id,Region__c,Closed_Lost_Date__c from Opportunity where Closed_Lost_Date__c>=:Quarters.get('Q0') and Closed_Lost_Date__c<:Quarters.get('Q4') and Contract_Type__c ='Subscription' and Type = 'New Business' Order by Region__c];
        if(closedlost.size()>0){
            rec=new record();
            tempReg='';
            for(Opportunity rc:closedlost){
                if(rc.Region__c!=tempReg&&tempreg!=''){
                    records.add(rec);
                    rec=new record();
                }
                rec.location=rc.Region__c;
                rec.type='Closed Lost';
                if(rc.Closed_Lost_Date__c<Quarters.get('Q1'))
                    rec.ActualQ1++;
                else if(rc.Closed_Lost_Date__c<Quarters.get('Q2'))
                    rec.ActualQ2++;
                else if(rc.Closed_Lost_Date__c<Quarters.get('Q3'))
                    rec.ActualQ3++;
                else
                    rec.ActualQ4++;
                tempReg=rc.Region__c;
            }
            records.add(rec);
        }
        AddNone('Closed Lost');

        ConnectPlanned();

        AddTotalLocation('Leads');
        AddTotalLocation('Opportunities');
        AddTotalLocation('Pilot');
        AddTotalLocation('Commercial');
        AddTotalLocation('Closed Won');
        AddTotalLocation('Closed Lost');

        for(record re:records){
                String UrlID='/lightning/r/Report/';
                if(runningInASandbox()){
                    if(re.type=='Leads')
                        UrlID+='00O8E000000LI1mUAG';
                    else if(re.type=='Opportunities')
                        UrlID+='00O8E000000IMfJUAW';
                    else if(re.type=='Commercial')
                        UrlID+='00O8E000000LI1wUAG';
                    else if(re.type=='Pilot')
                        UrlID+='00O8E000000LI21UAG';
                    else if(re.type=='Closed Won')
                        UrlID+='00O8E000000LI2BUAW';
                    else if(re.type=='Closed Lost')
                        UrlID+='00O8E000000LI2LUAW';
                }
                else{
                    if(re.type=='Leads')
                        UrlID+='00O0Y000007AwDqUAK';
                    else if(re.type=='Opportunities')
                        UrlID+='00O0Y000007SDOZUA4';
                    else if(re.type=='Commercial')
                        UrlID+='00O0Y000007AwDtUAK';
                    else if(re.type=='Pilot')
                        UrlID+='00O0Y000007AwDuUAK';
                    else if(re.type=='Closed Won')
                        UrlID+='00O0Y000007AwDsUAK';
                    else if(re.type=='Closed Lost')
                        UrlID+='00O0Y000007AwDrUAK';
                }
                 UrlID+='/view?fv0=';
                if(re.location!='Total')
                    UrlID+=re.location;
                else{
                    for(String k:order.Keyset())
                        UrlID+=k+',';
                    UrlID=UrlID.left(UrlID.length()-1);
                }
                re.ReportQ1=UrlID+'&fv1='+getStrDate(Quarters.get('Q0'))+'&fv2='+getStrDate(Quarters.get('Q1'));
                re.ReportQ2=UrlID+'&fv1='+getStrDate(Quarters.get('Q1'))+'&fv2='+getStrDate(Quarters.get('Q2'));
                re.ReportQ3=UrlID+'&fv1='+getStrDate(Quarters.get('Q2'))+'&fv2='+getStrDate(Quarters.get('Q3'));
                re.ReportQ4=UrlID+'&fv1='+getStrDate(Quarters.get('Q3'))+'&fv2='+getStrDate(Quarters.get('Q4'));
                re.ReportYearly=UrlID+'&fv1='+getStrDate(Quarters.get('Q0'))+'&fv2='+getStrDate(Quarters.get('Q4'));
                if(re.type=='Pilot'){
                    String UrlPilot='/lightning/r/Report/';
                    if(runningInASandbox())
                        UrlPilot+='00O8E000000IDgrUAG';
                    else
                        UrlPilot+='00O0Y000007jZlFUAU';
                    UrlPilot+='/view?fv0=';
                    if(re.location!='Total')
                        UrlPilot+=re.location;
                    else{
                        for(String k:order.Keyset())
                            UrlPilot+=k+',';
                        UrlPilot=UrlPilot.left(UrlPilot.length()-1);
                    }
                    re.ReportPilotQ1=UrlPilot+'&fv1='+getStrDate(Quarters.get('Q0'))+'&fv2='+getStrDate(Quarters.get('Q1'));
                    re.ReportPilotQ2=UrlPilot+'&fv1='+getStrDate(Quarters.get('Q1'))+'&fv2='+getStrDate(Quarters.get('Q2'));
                    re.ReportPilotQ3=UrlPilot+'&fv1='+getStrDate(Quarters.get('Q2'))+'&fv2='+getStrDate(Quarters.get('Q3'));
                    re.ReportPilotQ4=UrlPilot+'&fv1='+getStrDate(Quarters.get('Q3'))+'&fv2='+getStrDate(Quarters.get('Q4'));
                    re.ReportPilotYearly=UrlPilot+'&fv1='+getStrDate(Quarters.get('Q0'))+'&fv2='+getStrDate(Quarters.get('Q4'));
                }
            }

    }
    public void AddNone(String typ){
        List <String> locations = new List <String>{'GER','ISR','NA','ROW','UK'};
        for(String loc:locations){
            Boolean exst=false;
            for(record rec:records){
                if(rec.type==typ&&rec.location==loc)
                    exst=true;
            }
            if(!exst){
                record rec = new record();
                rec.location=loc;
                rec.type=typ;
                records.add(rec);
            }
        }
    }
    public void AddTotalLocation(String typ){
        record frec=new record();
        frec.location='Total';
        frec.type=typ;
        for(record rec:records){
            if(frec.type==rec.type){
                frec.ActualQ1+=rec.ActualQ1;
                frec.ActualQ2+=rec.ActualQ2;
                frec.ActualQ3+=rec.ActualQ3;
                frec.ActualQ4+=rec.ActualQ4;
                frec.PilotActualQ1+=rec.PilotActualQ1;
                frec.PilotActualQ2+=rec.PilotActualQ2;
                frec.PilotActualQ3+=rec.PilotActualQ3;
                frec.PilotActualQ4+=rec.PilotActualQ4;
                frec.ActualTotal+=rec.ActualTotal;
                frec.PilotActualTotal+=rec.PilotActualTotal;
                frec.PlannedTotalQ1+=rec.Planned.Planned_Q1__c!=null?rec.Planned.Planned_Q1__c:0;
                frec.PlannedTotalQ2+=rec.Planned.Planned_Q2__c!=null?rec.Planned.Planned_Q2__c:0;
                frec.PlannedTotalQ3+=rec.Planned.Planned_Q3__c!=null?rec.Planned.Planned_Q3__c:0;
                frec.PlannedTotalQ4+=rec.Planned.Planned_Q4__c!=null?rec.Planned.Planned_Q4__c:0;
            }
        }
        frec.PlannedTotal=frec.PlannedTotalQ1+frec.PlannedTotalQ2+frec.PlannedTotalQ3+frec.PlannedTotalQ4;
        records.add(frec);
    }
    public void ConnectPlanned(){
        List <Planned__c> patables= [select Year__c,Type__c,Location__c,Planned_Q1__c,Planned_Q2__c,Planned_Q3__c,Planned_Q4__c from Planned__c where Year__c=:year];
        for(record rec:records){
            for(Planned__c pat:patables){
                if(rec.type==pat.Type__c&&rec.location==pat.Location__c){
                    rec.Planned=pat;
                    break;
                }
            }
            if(rec.Planned==null){
                rec.Planned = new Planned__c();
                rec.Planned.Year__c = year;
                rec.Planned.Type__c = rec.type;
                rec.Planned.Location__c = rec.location;
                rec.Planned.Name=year+'-'+rec.type+'-'+rec.location;
            }
        }
        SortPlanned();
        CalcTotals();
    }
    public void SortPlanned(){
        records.sort();
    }
    public void CalcTotals(){
        for(record rec:records){
            rec.ActualTotal=rec.ActualQ1+rec.ActualQ2+rec.ActualQ3+rec.ActualQ4;
            rec.PilotActualTotal=rec.PilotActualQ1+rec.PilotActualQ2+rec.PilotActualQ3+rec.PilotActualQ4;
            rec.PlannedTotal=0;
            if(rec.Planned.Planned_Q1__c!=null)
                rec.PlannedTotal+=rec.Planned.Planned_Q1__c;
            if(rec.Planned.Planned_Q2__c!=null)
                rec.PlannedTotal+=rec.Planned.Planned_Q2__c;
            if(rec.Planned.Planned_Q3__c!=null)
                rec.PlannedTotal+=rec.Planned.Planned_Q3__c;
            if(rec.Planned.Planned_Q4__c!=null)
                rec.PlannedTotal+=rec.Planned.Planned_Q4__c;
        }
    }
    public void SavePlanned(){
        List <Planned__c> patables4Up = new List <Planned__c>();
        for(record rec:records)
            if(rec.location!='Total')
                patables4Up.add(rec.Planned);
        upsert patables4Up;
        CalcTotals();
    }
    public String getStrDate(Date mydat){
        return mydat.month()+'/'+mydat.day()+'/'+mydat.year();
    }
    public static Boolean runningInASandbox() {
        return [SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
    }
}
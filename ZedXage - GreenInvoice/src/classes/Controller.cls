global class Controller {
	//example: Controller.CreateOne('invoice','he',0,1,'ILS','test','edan@itmyway.com','0500001254','test',null,null,null,10,1234,'ILS',1,'desc');
	global class client {
		global String id;//not must
		global Boolean add;//if to add as client
		global String name;
		global List <String> emails;
		global String phone;//not must
		global String address;
		global String city;//not must
		global String zip;//not must
		global String country;//not must
	}
	global class income {
		global Decimal price;
		global Integer quantity;
		global String description;
		global String currency_c;//USD ILS EUR
		global Integer vatType;//0 - vat will be added, 1 - vat included, 2 - vat free 
	}
	global class payment {
		global String date_c;//yyyy-mm-dd
		global Integer type;//1 cash - 3 credit card - 5 paypal
		global Decimal price;
		global String currency_c;// USD ILS...
		global Integer dealType;//1 regular - 2 payments - 3 credit not must
		global Integer cardNum;
		global Integer chequeNum;
		global String chequeString;
	}
	global class params {
		global Long timestamp;
		global Integer type;//invoice-320   reciept-400  refund-330
		global String lang;//default 'he' can be 'en'
		global Integer vatType;//0 - default on business, 1 - vat free, 2 - mixed
		global Boolean attachment; //default true
		global String currency_c;
		global client client;
		global List <income> income;
		global List <payment> payment;
	}
	global static HttpResponse Create(Integer doc_type,String lang,Integer vattype,Boolean attachment,String currency_c,String client_id,Boolean client_add,String client_name,String client_email,String client_phone,String client_address,String client_country,String client_city,String client_zip,List <income> incomes,List <payment> payments){
		List <GreenInvoice_Settings__c> GN=[select Endpoint_Doc__c,Endpoint_Token__c,id__c,secret__c from GreenInvoice_Settings__c Limit 1];
		if(GN.size()>0)
			return Document(GN[0].Endpoint_Token__c,GN[0].Endpoint_Doc__c,GN[0].id__c,GN[0].secret__c,doc_type,lang,vattype,attachment,currency_c,client_id,client_add,client_name,client_email,client_phone,client_address,client_country,client_city,client_zip,incomes,payments);
		else 
			return null;
	}
	global static HttpResponse CreateOne(String doc,String lang,Integer vattype,Integer payment_vattype,String currency_c,String client_name,String client_email,String client_phone,String client_address,String client_country,String client_city,String client_zip,Decimal payment_amount,Integer payment_four_number,String payment_currency,Integer payment_quantity,String payment_description){
		payment payment = new payment();
		Datetime myDate=System.now().addSeconds(Timezone.getTimeZone('Asia/Jerusalem').getOffset(System.now())/1000);
		payment.date_c=String.valueof(myDate.year())+'-'+(myDate.month()>9?String.valueof(myDate.month()):'0'+String.valueof(myDate.month()))+'-'+(myDate.day()>9?String.valueof(myDate.day()):'0'+String.valueof(myDate.day()));
		payment.type=3;
		payment.price=payment_amount.setScale(2);
		payment.dealType=1;
		payment.cardNum=payment_four_number;
		payment.currency_c=payment_currency;

		income income = new income();
		income.quantity=payment_quantity;
		income.price=payment_amount.setScale(2);
		income.currency_c=payment_currency;
		income.vatType=payment_vattype;
		income.description=payment_description;
		List <GreenInvoice_Settings__c> GN=[select Endpoint_Doc__c,Endpoint_Token__c,id__c,secret__c from GreenInvoice_Settings__c Limit 1];
		if(GN.size()>0)
			return Document(GN[0].Endpoint_Token__c,GN[0].Endpoint_Doc__c,GN[0].id__c,GN[0].secret__c,(doc=='invoice'?320:(doc=='refund'?330:(doc=='mas'?305:400))),lang,vattype,true/*attachment*/,currency_c,null,false/*client_add*/,client_name,client_email,client_phone,client_address,client_country,client_city,client_zip,new List <income> {income},new List <payment> {payment});
		else 
			return null;
	}
	global static HttpResponse CreateOneGlobal(String doc,String lang,Integer vattype,Integer payment_vattype,String currency_c,String client_name,String client_email,String client_phone,String client_address,String client_country,String client_city,String client_zip,Decimal payment_amount,Integer payment_four_number,String payment_currency,Integer payment_quantity,String payment_description,integer pamtype,String cheqnum){
		payment payment = new payment();
		Datetime myDate=System.now().addSeconds(Timezone.getTimeZone('Asia/Jerusalem').getOffset(System.now())/1000);
		payment.date_c=String.valueof(myDate.year())+'-'+(myDate.month()>9?String.valueof(myDate.month()):'0'+String.valueof(myDate.month()))+'-'+(myDate.day()>9?String.valueof(myDate.day()):'0'+String.valueof(myDate.day()));
		payment.type=pamtype;
		payment.price=payment_amount.setScale(2);
		payment.dealType=1;
		payment.cardNum=payment_four_number;
		payment.currency_c=payment_currency;
		payment.chequeString=cheqnum;

		income income = new income();
		income.quantity=payment_quantity;
		income.price=payment_amount.setScale(2);
		income.currency_c=payment_currency;
		income.vatType=payment_vattype;
		income.description=payment_description;
		List <GreenInvoice_Settings__c> GN=[select Endpoint_Doc__c,Endpoint_Token__c,id__c,secret__c from GreenInvoice_Settings__c Limit 1];
		if(GN.size()>0)
			return Document(GN[0].Endpoint_Token__c,GN[0].Endpoint_Doc__c,GN[0].id__c,GN[0].secret__c,(doc=='invoice'?320:(doc=='refund'?330:(doc=='mas'?305:400))),lang,vattype,true/*attachment*/,currency_c,null,false/*client_add*/,client_name,client_email,client_phone,client_address,client_country,client_city,client_zip,new List <income> {income},new List <payment> {payment});
		else 
			return null;
	}
	global static HttpResponse CreateOneGlobalDate(String doc,String lang,Integer vattype,Integer payment_vattype,String currency_c,String client_name,String client_email,String client_phone,String client_address,String client_country,String client_city,String client_zip,Decimal payment_amount,Integer payment_four_number,String payment_currency,Integer payment_quantity,String payment_description,integer pamtype,String cheqnum,Datetime dat){
		payment payment = new payment();
		Datetime myDate=dat;
		payment.date_c=String.valueof(myDate.year())+'-'+(myDate.month()>9?String.valueof(myDate.month()):'0'+String.valueof(myDate.month()))+'-'+(myDate.day()>9?String.valueof(myDate.day()):'0'+String.valueof(myDate.day()));
		payment.type=pamtype;
		payment.price=payment_amount.setScale(2);
		payment.dealType=1;
		payment.cardNum=payment_four_number;
		payment.currency_c=payment_currency;
		payment.chequeString=cheqnum;

		income income = new income();
		income.quantity=payment_quantity;
		income.price=payment_amount.setScale(2);
		income.currency_c=payment_currency;
		income.vatType=payment_vattype;
		income.description=payment_description;
		List <GreenInvoice_Settings__c> GN=[select Endpoint_Doc__c,Endpoint_Token__c,id__c,secret__c from GreenInvoice_Settings__c Limit 1];
		if(GN.size()>0)
			return Document(GN[0].Endpoint_Token__c,GN[0].Endpoint_Doc__c,GN[0].id__c,GN[0].secret__c,(doc=='invoice'?320:(doc=='refund'?330:(doc=='mas'?305:400))),lang,vattype,true/*attachment*/,currency_c,null,false/*client_add*/,client_name,client_email,client_phone,client_address,client_country,client_city,client_zip,new List <income> {income},new List <payment> {payment});
		else 
			return null;
	}
	global static HttpResponse Document(String endpoint_tok,String endpoint_doc,String id,String secret,Integer doc_type,String lang,Integer vattype,Boolean attachment,String currency_c,String client_id,Boolean client_add,String client_name,String client_email,String client_phone,String client_address,String client_country,String client_city,String client_zip,List <income> incomes,List <payment> payments){
		Map<String,String> cred = new Map<String,String>{'id'=>id,'secret'=>secret};
		HttpResponse response=zedHTTP(endpoint_tok,'POST',JSON.serialize(cred),null);
		String apikey=response.getHeader('X-Authorization-Bearer');
		System.debug('apikey:'+apikey);
		if(apikey!=null&&apikey!=''){
			params params=new params();
			params.timestamp = System.now().addSeconds(Timezone.getTimeZone('Asia/Jerusalem').getOffset(System.now())/1000).getTime()/1000;
			params.type=doc_type;
			params.lang=lang;
			params.attachment=attachment;
			params.vatType=vattype;
			params.currency_c=currency_c;

			client client = new client();
			client.id=client_id;
			client.add=client_add;
			client.name=client_name;
			client.emails=new list <String> {client_email};
			client.phone=client_phone;
			client.address=client_address;
			client.country=client_country;
			client.city=client_city;
			client.zip=client_zip;

			params.client=client;

			if(incomes!=null){
				params.income=new List <income>();
				for(income income:incomes)
					params.income.add(income);
			}
			if(payments!=null){
				params.payment=new List <payment>();
				for(payment payment:payments)
					params.payment.add(payment);
			}
			//String signature = EncodingUtil.base64Encode(crypto.generateMac('hmacSHA256',Blob.valueOf(JSON.serialize(params)),Blob.valueOf(secret)));
			String jsondata=JSON.serialize(params).replace('date_c', 'date').replace('currency_c', 'currency').replace('chequeNum', 'Old_chequeNum').replace('chequeString', 'chequeNum');
			//String postdata='data='+EncodingUtil.urlEncode(jsondata, 'UTF-8');
			System.debug(jsondata);
			//System.debug(postdata);
			HttpResponse res=zedHTTP(endpoint_doc,'POST',jsondata,apikey);
			system.debug(res.getBody());
			return res;
		}
		else
			return null;
	}
	global static HttpResponse zedHTTP(String endpoint,String method,String data,String auth){
		Http http=new Http();
		HttpRequest req=new HttpRequest();
		req.setendpoint(endpoint);
		req.setmethod(method);
		req.setHeader('Content-Type', 'application/json');
		if(auth!=null)
			req.setHeader('Authorization', 'Bearer '+auth);
		req.setbody(data);
		HttpResponse res = new HttpResponse();
		if(!Test.isRunningTest()){
			try{
				res = http.send(req);
			}
			catch(exception e){
				res = null;
			}
		}
		else
			res.setHeader('X-Authorization-Bearer','test');
		return res;
	}
}
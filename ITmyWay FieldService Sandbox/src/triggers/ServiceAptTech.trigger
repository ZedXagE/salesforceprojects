trigger ServiceAptTech on ServiceAppointment (before insert, after insert, before update) {
    System.debug(UserInfo.getName());
    String UserName = UserInfo.getName();
    for(ServiceAppointment serv:Trigger.New){
        if(Trigger.IsInsert){
            if((UserName != 'Avi Amsalem' && UserName != 'Eyal Zarankin') || Test.isRunningTest()){
                if(Trigger.IsBefore){
                    System.debug('Before Serv: not avi and not eyal');
                    List <WorkOrder> woks = [select Service_Start__c,Service_End__c from WorkOrder where id=:serv.ParentRecordId];
                    if(woks.size()>0){
                        if(woks[0].Service_Start__c!=null){
                            serv.Service_Start__c = woks[0].Service_Start__c;
                            serv.SchedStartTime = woks[0].Service_Start__c;
                        }
                        if(woks[0].Service_End__c!=null){
                            serv.Service_End__c = woks[0].Service_End__c;
                            serv.SchedEndTime = woks[0].Service_End__c;
                        }
                    }
                }
                if(Trigger.IsAfter){
                    System.debug('After Serv: not avi and not eyal');
                    List <ServiceResource> srs = [select id from ServiceResource where RelatedRecordId=:serv.CreatedById];
                    if(srs.Size() > 0 && serv.SchedStartTime != null && serv.SchedEndTime != null)
                        insert new AssignedResource(ServiceResourceId=srs[0].id,ServiceAppointmentId=serv.id);
                }
            }
        }
        if(Trigger.IsUpdate){
            ServiceAppointment oldserv = Trigger.OldMap.get(serv.id);
            if((UserName != 'Avi Amsalem' && UserName != 'Eyal Zarankin') || Test.isRunningTest()){
                if(serv.Service_Start__c != oldserv.Service_Start__c)
                    serv.SchedStartTime=serv.Service_Start__c;
                if(serv.Service_End__c != oldserv.Service_End__c)
                    serv.SchedEndTime=serv.Service_End__c;
            }
        }
    }
}
public class ServiceNewWHController {
    public Working_Hours__c wh{get;set;}
    public Integer Err{get;set;}
    public String ITrec{get;set;}
    public ServiceNewWHController(ApexPages.StandardController stdController){
        Err=0;
        ServiceAppointment sa = [select id, SchedStartTime, SchedEndTime, Account_ID__c, Account_ID__r.Main_Project__c, Description, Type_Of_Work__c, Type_Of_Service__c, Other_Type_Of_Work__c from ServiceAppointment where id =: stdController.getRecord().id];
        List <Contact> cons = [select id from Contact where FirstName=:UserInfo.getFirstName() and LastName=:UserInfo.getLastName() and Account.Name LIKE: '%ITmyWay%'];
        List <RecordType> recs = [select id from RecordType where SobjectType='Working_Hours__c' and Name='IT'];
        if(recs.size()>0)
            ITrec=recs[0].id;
        else
            Err=3;
        if(Err==0){
            wh = new Working_Hours__c (
                Support_Type__c='On site',
                Start__c=sa.SchedStartTime,
                End__c=sa.SchedEndTime,
                Account__c=sa.Account_ID__c,
                Service_Appointment__c=sa.id,
                Description_Of_Work__c=sa.Description,
                Type_Of_Work__c=sa.Type_Of_Work__c,
                Type_Of_Service__c=sa.Type_Of_Service__c,
                Other_Type_Of_Work__c=sa.Other_Type_Of_Work__c,
                Project__c=sa.Account_ID__r.Main_Project__c
            );
            if(cons.size()>0)
                wh.Performed_By_2__c=cons[0].id;
            else{
                wh = new Working_Hours__c ();
                Err=4;
            }
        }
        else
            wh = new Working_Hours__c ();
        if(Test.IsRunningTest()){
            wh.Start__c=System.Now();
            wh.End__c=System.Now();
        }
    }
    public void SaveWH(){
        wh.Date__c = date.newinstance(wh.Start__c.year(), wh.Start__c.month(), wh.Start__c.day());
        System.debug(wh);
        try{
            insert wh;
            Err = 1;
        }
        catch(exception e){
            Err = 2;
        }
    }
}
public class ServiceReportHandler {
    public String PDF{get;set;}
    public String GEN{get;set;}
    public String SignatureData{get;set;}
    public ServiceAppointment serv{get;set;}
    public ServiceReportHandler(ApexPages.StandardController stdController) {
        signatureData = '';
        PDF = ApexPages.currentPage().getParameters().get('PDF');
        if(PDF!=null&&PDF!='')
            PDF='PDF';
        GEN = ApexPages.currentPage().getParameters().get('GEN');
        serv = [select id,Account_ID__r.Name,ParentRecordid,AppointmentNumber,Signature__c from ServiceAppointment where id=:stdController.getRecord().id];
    }
    public pageReference saveSig(){
        if(signatureData!=null&&signatureData!='')
            serv.Signature__c = '<img style="width: 100%;height:150px;" src="' + signatureData + '"/>';
        else
            serv.Signature__c = null;
        update serv;
		PageReference newPage = Page.ServiceReport;
		newPage.getParameters().put('id',serv.id);
		newPage.getParameters().put('PDF','1');
		newPage.getParameters().put('GEN','1');
        newPage.setRedirect(true);
        return newPage;
    }
    public pageReference CheckCreateAttchment(){
        if(PDF=='PDF'&&GEN=='1'){
            GEN='0';
            ApexPages.currentPage().getParameters().put('GEN',GEN);
            attachment at = new Attachment();
            if(!Test.isRunningTest()){
                Blob pdf1 = ApexPages.currentPage().getcontentAsPdf();
                at.Body=pdf1;
            }
            else{
                at.body=Blob.valueOf('Unit Test Attachment Body');
            }
            at.OwnerId = UserInfo.getUserId();
            at.ParentId = serv.ParentRecordid;
            at.Name=serv.Account_ID__r.Name+'-'+serv.AppointmentNumber+'.pdf';
            at.ContentType = 'application/pdf';
            insert at;
        }
        return null;
    }
}
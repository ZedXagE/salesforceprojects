@isTest(SeeallData=true)
public class ServiceTests{
    static testMethod void testPDF(){
        PageReference pg=Page.ServiceReport;
        pg.getParameters().put('PDF','1');
		pg.getParameters().put('GEN','1');
        Account acc = new Account(name='test');
        insert acc;
        WorkOrder wo= new WorkOrder(Accountid=acc.id);
        insert wo;
        ServiceAppointment serv = new ServiceAppointment(ParentRecordId=wo.id);
        insert serv;
        ApexPages.StandardController controller1 = new ApexPages.StandardController(serv);
        ServiceReportHandler cc= new ServiceReportHandler(controller1);
        Test.setCurrentPage(pg);
        cc.PDF='PDF';
        cc.GEN='1';
        cc.CheckCreateAttchment();
        cc.saveSig();
    }
    static testMethod void testWH(){
        PageReference pg=Page.ServiceNewWH;
        Account acc = new Account(name='test');
        insert acc;
        WorkOrder wo= new WorkOrder(Accountid=acc.id);
        insert wo;
        ServiceAppointment serv = new ServiceAppointment(ParentRecordId=wo.id);
        insert serv;
        ApexPages.StandardController controller1 = new ApexPages.StandardController(serv);
        ServiceNewWHController cc= new ServiceNewWHController(controller1);
        Test.setCurrentPage(pg);
        cc.SaveWH();
    }
    static testMethod void testInsert(){
        WorkOrder wok = new WorkOrder(WorkTypeId=[select id from WorkType Limit 1].id,Service_Start__c=System.Now(),Service_End__c=System.Now().addDays(1));
        insert wok;
        ServiceAppointment serv = [select id,Service_Start__c,Service_End__c from ServiceAppointment where ParentRecordId=:wok.id limit 1];
        serv.Service_Start__c=serv.Service_Start__c.addHours(1);
        serv.Service_End__c=serv.Service_End__c.addHours(1);
        update serv;
    }
}
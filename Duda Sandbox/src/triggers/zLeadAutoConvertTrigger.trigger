/*
 * @Author: ZedXagE - ITmyWay 
 * @Date: 2018-10-24 15:43:35 
 * @Last Modified by:   ZedXagE - ITmyWay 
 * @Last Modified time: 2018-10-24 15:43:35 
 */

trigger zLeadAutoConvertTrigger on Lead (after insert) {
    List <Lead> leads = new List <Lead>();
    for(Lead ld:Trigger.New)
        leads.add(ld);
    System.enqueueJob(new zLeadAutoConvertQueue(leads));
}
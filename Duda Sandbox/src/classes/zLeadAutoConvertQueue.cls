/*
 * @Author: ZedXagE - ITmyWay 
 * @Date: 2018-10-24 12:07:18 
 * @Last Modified by:   ZedXagE - ITmyWay 
 * @Last Modified time: 2018-10-24 12:07:18 
 */

public class zLeadAutoConvertQueue implements Queueable{
	private final Integer limitsendEmail=10;
	private List <Lead> leads;
	private List <Lead> temp;
	public zLeadAutoConvertQueue(List<Lead> lds) {
		leads = lds;
		temp = new List <Lead>();
		if(leads.size()>0){
			for(integer i=0;leads.size()!=0&&i<limitsendEmail;i++) {
				temp.add(leads[0]);
				leads.remove(0);
			}
		}
	}
	public Void execute(QueueableContext context){
		if(temp.size()>0) {
			Map <String,Set <String>> fldVals = new Map <String,Set <String>>();
			fldVals.put('Id',new Set <String>());
			fldVals.put('Email',new Set <String>());
			fldVals.put('Domain',new Set <String>());
			fldVals.put('Website',new Set <String>());

			Map <String,String> matches = new Map <String,String>();
			Map <String,String> accsowners = new Map <String,String>();
			Map <String,String> ldscons= new Map <String,String>();

			for(Lead ld:temp){
				fldVals.get('Id').add(ld.id);
				if(ld.Email!=null)
					fldVals.get('Email').add(ld.Email);
			}

			List <Lead> leadsFlds = [select id,Domain__c,Company_Website__c from lead where id in:fldVals.get('Id')];
			for(Lead ld:leadsFlds){
				if(ld.Domain__c!=null)
					fldVals.get('Domain').add(ld.Domain__c.tolowercase());
				if(ld.Company_Website__c!=null)
					fldVals.get('Website').add(ld.Company_Website__c.tolowercase());
			}

			for(Account acc:[select id,InternalCompanyName_lower__c,Website_Lower_Case__c,Ownerid from Account where (InternalCompanyName_lower__c in:fldVals.get('Domain') OR Website_Lower_Case__c in:fldVals.get('Website')) and ABM__c=true]){
				for(Lead ld:leadsFlds){
					if(ld.Domain__c!=null&&ld.Domain__c.tolowercase()==acc.InternalCompanyName_lower__c)
						matches.put(ld.id,acc.id);
					else if(ld.Company_Website__c!=null&&ld.Company_Website__c.tolowercase()==acc.Website_Lower_Case__c)
						matches.put(ld.id,acc.id);
				}
				accsowners.put(acc.id,acc.Ownerid);
			}

			for(Contact con:[select id,Email from Contact where Email in: fldVals.get('Email') and Account.ABM__c=true])
				ldscons.put(con.Email,con.id);
			System.debug(fldVals);
			System.debug(ldscons);
			System.debug(accsowners);
			System.debug(matches);
			//convert
			for(Lead ld:temp) {
				try {
					if(matches.containsKey(ld.id)){
						Database.LeadConvert lc = new Database.LeadConvert();
						lc.setLeadId(ld.id);
						lc.setDoNotCreateOpportunity(true);
						lc.setAccountId(matches.get(ld.id));
						lc.setOwnerId(accsowners.get(matches.get(ld.id)));
						lc.setSendNotificationEmail(true);
						if(ldscons.containsKey(ld.Email)&&ldscons.get(ld.Email)!=null)
							lc.setContactId(ldscons.get(ld.Email));
						LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
						lc.setConvertedStatus(convertStatus.MasterLabel);
						Database.LeadConvertResult lcr = Database.convertLead(lc);
					}
				}
				catch (DmlException e) {
					System.debug('An unexpected error has occurred: ' + e.getMessage());
				}
			}
			UpdateAccLkpAndSendEmail(matches);
			if(!Test.isRunningTest()) System.enqueueJob(new zLeadAutoConvertQueue(leads));
		}
	}
	@future
	public static void UpdateAccLkpAndSendEmail(Map<String,String> ldaccs){
		System.debug(ldaccs);
		List <Lead> lds4up = [select id,Email,Account__c,Duda_Lead_Source__c,DM_Referral_c__c from Lead where id in:ldaccs.keySet()];
		System.debug(lds4up);
		Map<ID, Account> acs4up = new Map<ID, Account>([select id,Duda_Lead_Source__c,Referral__c,Responded_Marketing__c,Started_A_Free_Trail__c,Owner.Email from Account where id in:ldaccs.values()]);
		for(Lead ld:lds4up){
			ld.Account__c = ldaccs.get(ld.id);
			if(ldaccs.get(ld.id)!=null){
				acs4up.get(ldaccs.get(ld.id)).Responded_Marketing__c=true;
                acs4up.get(ldaccs.get(ld.id)).Started_A_Free_Trail__c=true;
				acs4up.get(ldaccs.get(ld.id)).Duda_Lead_Source__c=ld.Duda_Lead_Source__c;
				acs4up.get(ldaccs.get(ld.id)).Referral__c=ld.DM_Referral_c__c;
			}
		}
		try{
			update lds4up;
			update acs4up.values();
		}
		catch (DmlException e) {
			System.debug('An unexpected error has occurred: ' + e.getMessage());
		}
		/*Map <String,String> ownerEmails = new Map <String,String>();
		for(Account acc:acs4up.values())
			ownerEmails.put(acc.id,acc.Owner.Email);
		//send email
		for(Lead ld:lds4up){
			try{
				if(ownerEmails.containsKey(ldaccs.get(ld.id))&&ld.Email!=null){
					Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
					String[] toAddresses = new String[] {ownerEmails.get(ldaccs.get(ld.id))};
					mail.setToAddresses(toAddresses);
					mail.setTargetObjectId(ld.id);
					mail.setTemplateId(Label.ABMTemplateId);
					if(!Test.isRunningTest())
						Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
				}
			}
			catch (DmlException e) {
				System.debug('An unexpected error has occurred: ' + e.getMessage());
			}
		}*/
	}
}
public class USDCurrencyUpdate {
	@future (callout=true)
	public static void getDollar(id Curid) {
			List <Transaction_Card__c> tccs = [select id,Dollar_Exchange_Rate__c From Transaction_Card__c where id=:Curid];
			if(tccs.size()>0)
			{
			Transaction_Card__c tcc=tccs[0];
			HttpRequest req = new HttpRequest();
			req.setEndpoint('https://www.boi.org.il/currency.xml?curr=01');
			req.setMethod('GET');
			Http http = new Http();
			HTTPResponse res = http.send(req);
			System.debug(res.getBody());
			String data=res.getBody();
			String dollar='';
			if(data.contains('<RATE>'))
			{
				String [] spl=data.split('<RATE>');
				if(spl[1].contains('</RATE>'))
				{
					String [] spl2=spl[1].split('</RATE>');
					dollar=spl2[0];
				}
				else
					dollar='3.55';
			}
			else
				dollar='3.55';
			tcc.Dollar_Exchange_Rate__c=Decimal.valueof(dollar);
			update tcc;
		}
	}
}
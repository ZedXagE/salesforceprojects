public class NepalPriceController {
	public String CurId;
	public String Status;
	public String URL{get;set;}
	public Transaction_Card__c tcc{get;set;}
	public Nepal_Price_List__c npl;
	public Boolean newcharge{get;set;}
	public Boolean main{get;set;}
	public Boolean recharge{get;set;}
	public Boolean retDev{get;set;}
	public Boolean badStat{get;set;}
	//public Decimal Price{get;set;}
	public integer deltadays {get;set;}
	public Card_Device__c Dev{get;set;}
	public NepalPriceController() {
		CurId = ApexPages.currentPage().getParameters().get('id');
		Status = ApexPages.currentPage().getParameters().get('status');
		AssemblyParameters__c asp=[select Form_URL__c from AssemblyParameters__c where name='Nepal'];
		URL=asp.Form_URL__c;
		newcharge = false;
		badStat = false;
		recharge = false;
		retDev = false;
		main = false;
		deltadays=0;
		if(Status=='bad')
			badStat=true;
		Dev = new Card_Device__c();
		if(CurId!=null&&CurId!='')
			InitCalc();
		else
			main = true;
	}
	public void InitCalc(){
		boolean newtcc=true;
		try{
			tcc=[Select id,TookFine__c,CurrencyIsoCode,Device__c,Status__c,Card_Device__c,Date_Of_Return__c,Token_Id__c,CC_Expiration_Date__c,No_of_Payments__c,Cost_Calculated_Planned_N__c,Date_Of_Delivery_Device__c,Scheduled_Return_Date__c,CreatedDate,Account_Passenger__r.Phone,Account_Addressing__r.PersonMobilePhone,Account_Addressing__r.Phone,Account_Addressing__r.Name,Account_Addressing__r.PersonEmail from Transaction_Card__c where id=:Curid];
		}
		catch(exception e){
			tcc=[Select id,TookFine__c,CurrencyIsoCode,Device__c,Status__c,Card_Device__c,Date_Of_Return__c,Token_Id__c,CC_Expiration_Date__c,No_of_Payments__c,Cost_Calculated_Planned_N__c,Date_Of_Delivery_Device__c,Scheduled_Return_Date__c,CreatedDate,Account_Passenger__r.Phone,Account_Addressing__r.PersonMobilePhone,Account_Addressing__r.Phone,Account_Addressing__r.Name,Account_Addressing__r.PersonEmail from Transaction_Card__c where IMEI__c=:Curid and Record_Type_Name_Text__c='נפאל' and Status__c='בטיול - נפאל'];
			newtcc=false;
		}
		npl=[select Price__c,Price_Exception__c,Min_Period__c,Max_Period__c,Fine__c from Nepal_Price_List__c Where Device__c=:tcc.Device__c];
		if(newtcc&&tcc.Status__c=='חדש')
			NewCalc();
		else if(!newtcc&&tcc.Status__c=='בטיול - נפאל')
			ReturnCalc();
	}
	public void NewCalc(){
		newcharge=true;
		tcc.CurrencyIsoCode='USD';
		tcc.Cost_Calculated_Planned_N__c = 0;
		//tcc.Cost_Calculated_Planned_N__c=Calc(tcc.Date_Of_Delivery_Device__c.daysBetween(tcc.Scheduled_Return_Date__c)+1,npl.Price__c);
		//Price=tcc.Cost_Calculated_Planned_N__c;
		deltadays =System.Today().daysBetween(tcc.Scheduled_Return_Date__c)+1;
		if(tcc.Card_Device__c==null){
			if(!Test.isRunningTest())
				Dev = [select id,imei__c from Card_Device__c where DeviceLocation__c='נפאל' and Inventory__c='במלאי' and Status__c='תקין' limit 1];
			else
				Dev = [select id,imei__c from Card_Device__c where IMEI__c='1234' limit 1];
			tcc.Card_Device__c=dev.id;
		}
		else{
			Dev = [select id,imei__c from Card_Device__c where id=:tcc.Card_Device__c];
		}
	}
	public void Paid(){
		if(tcc.Status__c=='חדש'){
			tcc.Status__c='בטיול - נפאל';
			update tcc;
			Card_Device__c cdev = [select id, Inventory__c from Card_Device__c where id=:tcc.Card_Device__c];
			cdev.Inventory__c = 'לא במלאי';
			update cdev;
		}
	}
	public void ReturnCalc(){
		retDev = true;
		deltadays = tcc.Scheduled_Return_Date__c.daysBetween(System.Today());
		if(deltadays>0)
			recharge=true;
		if(badStat){
			recharge=true;
			tcc.Cost_Calculated_Planned_N__c = npl.Fine__c;
			tcc.TookFine__c=true;	
		}
	}
	/*public Decimal Calc(Decimal days,Decimal pr){
		Decimal indays=0;
		Decimal outdays=0;
		if(days<npl.Min_Period__c)
			indays=npl.Min_Period__c;
		else if(days<npl.Max_Period__c)
			indays=days;
		else{
			indays=npl.Max_Period__c;
			outdays=days-npl.Max_Period__c;
		}
		return ((indays*pr)+(outdays*pr)).setScale(2);
	}*/
	/*public void ReturnCalc(){
		retDev = true;
		if(Calc(tcc.Date_Of_Delivery_Device__c.daysBetween(System.Today())+1,npl.Price__c)>tcc.Cost_Calculated_Planned_N__c){
			Price=Calc(tcc.Scheduled_Return_Date__c.daysBetween(System.Today())+1,npl.Price_Exception__c);
			tcc.Cost_Calculated_Planned_N__c+=Price;
			recharge=true;
		}
		if(badStat&&tcc.TookFine__c!=true){
			recharge=true;
			if(Price!=null)
				Price+=npl.Fine__c;
			else
				Price=npl.Fine__c;
			tcc.TookFine__c=true;
			tcc.Cost_Calculated_Planned_N__c+=npl.Fine__c;
			recharge=true;
		}
	}*/
	public PageReference Tranzilla(){
		if(newcharge){
			update tcc;
		}
		/*if(recharge){
			Http http=new Http();
			HttpRequest req=new HttpRequest();
			req.setendpoint('https://secure5.tranzila.com/cgi-bin/tranzila71u.cgi');
			req.setmethod('POST');
			string pos='supplier=magnusintetok&sum='+Price+'&expdate='+tcc.CC_Expiration_Date__c+'&currency=2&TranzilaPW=zJkelm&TranzilaTK='+tcc.Token_id__c+'&cred_type=1&tranmode=A&SF_Transaction_Id='+tcc.id+'&contact='+tcc.Account_Addressing__r.name+'&email='+tcc.Account_Addressing__r.PersonEmail+'&phone='+tcc.Account_Addressing__r.PersonMobilePhone;
			req.setbody(pos);
			HttpResponse res;
			String str='';
			if(tcc.Token_Id__c!='testspec'){
				res = http.send(req);
				str=res.getbody();
			}
			else
				str='test&Response=000&ConfirmationCode=100&index=1&';
			if(str.contains('Response=000')){
				string con='';
				string ind='';
				String [] spl1=str.split('ConfirmationCode=');
				String [] spl2=str.split('index=');
				if(spl1[1].contains('&')){
					String [] spl3=spl1[1].split('&');
					con=spl3[0];
				}
				else
					con=spl1[1];
				if(spl2[1].contains('&')){
					String [] spl4=spl2[1].split('&');
					ind=spl4[0];
				}
				else
					ind=spl2[1];
				Payment__c pay=new Payment__c();
				pay.Parent_Transaction__c=tcc.id;
				pay.Expiration_Date__c =tcc.CC_Expiration_Date__c;
				pay.Token__c=tcc.Token_Id__c;
				pay.Result__c='000';
				pay.Payment_Transaction_ID__c=con;
				pay.index__c=ind;
				pay.masof__c='magnusintetok';
				TimeZone tz = TimeZone.getTimeZone('Asia/Jerusalem');
				DateTime localTime = System.now().AddSeconds(tz.getOffset(System.now())/1000);
				pay.Payment_Time__c=localTime;
				pay.Sum__c=Price;
				pay.CurrencyIsoCode='USD';
				insert pay;
				Card_Device__c cdev = [select id, Inventory__c from Card_Device__c where id=:tcc.Card_Device__c];
				cdev.Inventory__c = 'במלאי';
				update cdev;
				tcc.Status__c='הסתיים';
				update tcc;
				String ur='/apex/SuccessVF';
				PageReference pageRef = new PageReference(ur); //it works
				pageref.setRedirect(true);
				return pageRef;
			}
			else{
				update tcc;
				String ur='/apex/ERROR';
				PageReference pageRef = new PageReference(ur); //it works
				pageref.setRedirect(true);
				return pageRef;
			}
			update tcc;
		}*/
		if(retDev){
			Card_Device__c cdev = [select id,status__c,Inventory__c from Card_Device__c where id=:tcc.Card_Device__c];
			cdev.Inventory__c = 'במלאי';
			if(badStat)
				cdev.status__c = 'לתיקון';
			update cdev;
			tcc.Status__c='הסתיים';
			update tcc;
		}
		return null;
	}
}
/*
 * @Author: ZedXagE - ITmyWay 
 * @Date: 2018-09-20 11:18:14 
 * @Last Modified by: ZedXagE - ITmyWay
 * @Last Modified time: 2018-09-20 16:02:58
 */

// NetSuiteCustomers.java
global class NetSuiteCustomers{
    global List <ObjList> objList;
    global static final map <String, String> problems = new Map <String,String>{'currency_c'=>'currency'};

    global NetSuiteCustomers(){
        objList = new List<ObjList>();
    }

    // ObjList.java
    global class ObjList {
        global String uid;
        global String integrationType;
        global Data data;
        global ObjList(){
            uid = String.valueof(Crypto.getRandomLong());
            integrationType = 'NS_SyncCustomer';
            data = new data();
        }
        global ObjList(Account acc,String terms){
            uid = String.valueof(Crypto.getRandomLong());
            integrationType = 'NS_SyncCustomer';
            data = new data(acc,terms);
        }
    }

    // Data.java
    global class Data {
        global String internalid;
        global String externalid;
        global String companyname;
        global String subsidiary;
        global String terms;
        global String currency_c;
        global String category;
        global String fax;
        global String phone;
        global String addressee;
        global String comments;
        global String isAddressChanging;
        global String custentity_ind_sf_customer_id;
        global String custentity_ind_sf_customer_name;
        global List <Addressbook> addressbook;
        global Data(){
            addressbook = new List <Addressbook>();
        }
        global Data(Account acc, String trms){
            addressbook = new List <Addressbook>();
            externalid = acc.id;
            companyname = acc.name;
            custentity_ind_sf_customer_id = acc.id;
            custentity_ind_sf_customer_name = acc.name;
            phone = acc.phone;
            fax = acc.fax;
            category = acc.recordtype.name;
            addressee = acc.name;
            comments = acc.description;
            currency_c = acc.CurrencyIsoCode;
            subsidiary = acc.Subsidiary__c;
            terms = trms;
            isAddressChanging = 'T';
            addressbook.add(new Addressbook(acc,'B'));
            addressbook.add(new Addressbook(acc,'S'));
        }
    }

    // Addressbook.java
    global class Addressbook {
        global String country;
        global String addr1;
        global String city;
        global String state;
        global String zip;
		global String addrphone;
        global String attention;
        global String defaultbilling;
        global String defaultshipping;
        global Addressbook(Account acc,String kind){
            if(kind=='B'){
                country = acc.BillingCountryCode;
                city = acc.BillingCity;
                state = acc.BillingStateCode;
                zip = acc.BillingPostalCode;
                addr1 = acc.BillingStreet;
				addrphone = acc.Bill_To_Phone__c;
				attention = acc.Bill_To_Attn__c!=null?(acc.Bill_To_Attn__c+(acc.Bill_To_Email__c!=null?' '+acc.Bill_To_Email__c:'')):(acc.Bill_To_Email__c!=null?acc.Bill_To_Email__c:'');
                defaultbilling = 'T';
            }
            else if(kind=='S'){
                country = acc.ShippingCountryCode;
                city = acc.ShippingCity;
                state = acc.ShippingStateCode;
                zip = acc.ShippingPostalCode;
                addr1 = acc.ShippingStreet;
				addrphone = acc.Ship_To_Phone__c;
				attention = acc.Ship_To_Attn__c!=null?(acc.Ship_To_Attn__c+(acc.Ship_To_Email__c!=null?' '+acc.Ship_To_Email__c:'')):(acc.Ship_To_Email__c!=null?acc.Ship_To_Email__c:'');
                defaultshipping = 'T';
            }
        }
    }

    global override String toString(){
        String js = JSON.serialize(this);
        if(js!=null)
            for(String k:problems.keySet())
                js = js.replaceAll(k,problems.get(k));
        return js;
    }
}
@RestResource(urlMapping='/nsfulfillments')
global class NetSuiteFulfillments {
    global class NSrequest{
        global String uid;
        global String integrationType;
        global postData postData; 
    }
    global class postData {
        global String Sf_order_id;
        global String NS_order_num;
        global String NS_FF_ID;
        global String Status;
        global String Packagetrackingnumber;
        global List <item> items; 
    }
    global class item {
        global String Item_name;
        global String Quantity;
        global String Start_date;
        global String End_date;
        global String Product_Version;
        global String Image_Version;
        global String Serial_number;
    }
    global class NSResponse{
        global String message;
        global String status;
    }
    @HttpPost
    global static NSResponse insertAssets() {
        NSResponse nsres = new NSResponse();
        nsres.status = 'failed';
        nsres.message = 'missing products with those item names';
        try{
            System.debug(RestContext.request.requestBody.toString());
            NSrequest nsreq = (NSrequest)JSON.deserialize(RestContext.request.requestBody.toString(), NSrequest.class);
            Map <String,String> prodids = new Map <String,String>();
            for(item it:nsreq.postData.items)
                prodids.put(it.Item_name,null);
            List <Asset> assets = new List <Asset>();
            List <Product2> prods = [select id,ProductCode from Product2 where ProductCode in: prodids.keyset() and ProductCode!=null];
            if(prods.size()==prodids.keySet().size()){
                for(Product2 prod:prods)
                    prodids.put(prod.ProductCode,prod.id);
                nsres.message = 'no order found with this id';
                List <Order> ords = [select id,NetSuite_ID__c,Accountid,Status from Order where id =: nsreq.postData.Sf_order_id];
                if(ords.size()>0){
                    for(item it:nsreq.postData.items){
                        integer numofserials = 0;
                        String [] spl = it.Serial_number.split(',');
                        for(String s:spl){
                            if(s!=null&&s!=''){
                                Asset asset = new Asset();
                                asset.Name = it.Item_name;
                                asset.AccountId = ords[0].Accountid;
                                asset.Order__c = ords[0].id;
                                asset.InstallDate = DateStr(it.Start_date);
                                asset.UsageEndDate = DateStr(it.End_date);
                                asset.SerialNumber = s;
                                asset.Version__c = it.Product_Version;
                                asset.Image_Version__c = it.Image_Version;
                                asset.Product2id = prodids.get(it.Item_name);
                                asset.Tracking_Number__c = nsreq.postData.Packagetrackingnumber;
                                asset.NetSuite_Status__c = nsreq.postData.Status;
                                asset.NetSuite_ID__c = nsreq.postData.NS_FF_ID;
                                assets.add(asset);
                                numofserials++;
                            }
                        }
                        if(it.Quantity!=null&&it.Quantity!=''){
                            if(numofserials<Integer.valueof(it.Quantity)){
                                for(integer i=numofserials;i<Integer.valueof(it.Quantity);i++){
                                    Asset asset = new Asset();
                                    asset.Name = it.Item_name;
                                    asset.AccountId = ords[0].Accountid;
                                    asset.Order__c = ords[0].id;
                                    asset.InstallDate = DateStr(it.Start_date);
                                    asset.UsageEndDate = DateStr(it.End_date);
                                    asset.Version__c = it.Product_Version;
                                    asset.Image_Version__c = it.Image_Version;
                                    asset.Product2id = prodids.get(it.Item_name);
                                    asset.Tracking_Number__c = nsreq.postData.Packagetrackingnumber;
                                    asset.NetSuite_Status__c = nsreq.postData.Status;
                                    asset.NetSuite_ID__c = nsreq.postData.NS_FF_ID;
                                    assets.add(asset);
                                }
                            }
                        }
                    }
                    insert assets;
                    if(ords[0].NetSuite_ID__c==null){
                        try{
                            ords[0].NetSuite_ID__c = nsreq.postData.NS_order_num;
                            if(ords[0].Status=='Approved')
                                ords[0].Status = 'Pending Fulfillment';
                            update ords[0];
                        }
                        catch(exception e){}
                    }
                    nsres.status = 'success';
                    nsres.message = '';
                }
            }
        }
        catch(exception e){
            nsres.message = e.getMessage();
        }
        return nsres;
    }

    global static Date DateStr(String dat){
        if(dat!=null){
            String [] spl = dat.split('/');
            if(spl.size()==3)
                return Date.newinstance(Integer.valueof(spl[2]),Integer.valueof(spl[1]),Integer.valueof(spl[0]));
        }
        return null;
    }
}
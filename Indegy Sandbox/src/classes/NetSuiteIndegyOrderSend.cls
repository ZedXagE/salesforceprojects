public class NetSuiteIndegyOrderSend {
    private order ord;
    public NetSuiteIndegyOrderSend() {}
    @AuraEnabled
    public static String SendOrder(String ordid){
        String err;
        Set <String> needstate = new Set <String>{'US','CA'};
        if(ordid!=null&&ordid!=''){
            order ord = [select id,NetSuite_ID__c,Status,Account.Name,OpportunityId,Type,CreatedDate,CurrencyIsoCode,OrderNumber,Opportunity.Opportunity_Number__c,Opportunity.description,Payment_Terms__c,ShippingCountryCode,ShippingStreet,ShippingCity,ShippingStateCode,ShippingPostalCode,BillingCountryCode,BillingStreet,BillingCity,BillingStateCode,BillingPostalCode,PO__c,Ship_To_Phone__c,Ship_To_Attn__c,Ship_To_Email__c,Bill_To_Phone__c,Bill_To_Attn__c,Bill_To_Email__c,Site__r.Name,Owner.Email,Accountid,Account.NetSuite_ID__c,(select id,Product2.ProductCode,Discount__c,Quantity,ListPrice,UnitPrice,Duration__c,ServiceDate,EndDate,Version__c,Image_Version__c,OrderItemNumber from OrderItems) from Order where id=: ordid];
            if(ord.Status=='Approved'||Test.isrunningTest()){
                if(ord.NetSuite_ID__c==null){
                    if((Test.isrunningTest())||(ord.ShippingCountryCode!=null&&ord.ShippingStreet!=null&&ord.ShippingCity!=null&&((needstate.contains(ord.ShippingCountryCode)&&ord.ShippingStateCode!=null)||!needstate.contains(ord.ShippingCountryCode))&&ord.ShippingPostalCode!=null&&ord.BillingCountryCode!=null&&ord.BillingStreet!=null&&ord.BillingCity!=null&&((needstate.contains(ord.BillingCountryCode)&&ord.BillingStateCode!=null)||!needstate.contains(ord.BillingCountryCode))&&ord.BillingPostalCode!=null&&ord.Ship_To_Phone__c!=null&&ord.Ship_To_Attn__c!=null&&ord.Ship_To_Email__c!=null&&ord.Bill_To_Phone__c!=null&&ord.Bill_To_Attn__c!=null&&ord.Bill_To_Email__c!=null)){
                        if(ord.Account.NetSuite_ID__c==null)
                            err = CreateCustomer(ord.Accountid, ord.Payment_Terms__c);
                        if(err==null||err.contains('success;')){
                            if(err!=null&&err.contains('success;'))
                                ord.Account.NetSuite_ID__c = err.split(';')[1];
                            err = CreateOrder(ord);
                        }
                    }
                    else
                        err = 'Please Fill all Address Fields before in order to send the Order to NS';
                }
                else
                    err = 'Order has already been sent to NewtSuite';
            }
            else
                err = 'Order can be sent only with the Status Approved';
        }
        else
            err = 'No Order ID, Please try again or contact admin.';
        return err;
    }
    @AuraEnabled
    public static String CreateOrder(Order ord){
        String err;
        try{
            NetSuiteOrders nsc = new NetSuiteOrders();
            nsc.objList.add(new NetSuiteOrders.objList(ord));
            NetSuiteConnector ns = new NetSuiteConnector();
            System.debug('Order: '+nsc.toString());
            ns.body=nsc.toString();
            NetSuiteResponse nsr = ns.send();
            if(Test.isRunningTest()){
                NetSuiteResponse.resData res = new NetSuiteResponse.resData();
                res.recordId = ord.id;
                res.lineUid = nsc.objList[0].uid;
                nsr.resData.add(res);
            }
            update new Account(id=ord.Accountid,NetSuite_ID__c=ord.Account.NetSuite_ID__c);
            for(NetSuiteOrders.objList obj:nsc.objList)
                for(NetSuiteResponse.resData res:nsr.resData)
                    if(res.lineUid == obj.uid){
                        if(res.recordId!=null){
                            ord.NetSuite_ID__c = res.recordId;
                            ord.Status = 'Pending Fulfillment';
                            update ord;
                            err = 'Order has been Successfully Created (NetSuite id:'+res.recordId+')!';
                            break;
                        }
                        else
                            err = res.lineMessage;
                    }
        }
        catch(exception e){
            err = e.getMessage();
            try{
                update new Account(id=ord.Accountid,NetSuite_ID__c=ord.Account.NetSuite_ID__c);
            }
            catch(exception e2){
                err += '\n'+e2.getMessage();
            }
        }
        if(err==null)
            err = 'Failed to contact NetSuite, Please try again later or contact admin.';
        return err;
    }
    @AuraEnabled
    public static String CreateCustomer(String accid, String terms){
        String err;
        if(accid!=null&&accid!=''){
            try{
                account acc = [select id,name,NetSuite_ID__c,Subsidiary__c,Payment_Terms__c,phone,fax,recordtype.name,description,currencyisocode,billingcountryCode,billingcity,billingstateCode,billingpostalcode,billingstreet,shippingcountryCode,shippingcity,shippingstateCode,shippingpostalcode,shippingstreet,Ship_To_Phone__c,Ship_To_Attn__c,Ship_To_Email__c,Bill_To_Phone__c,Bill_To_Attn__c,Bill_To_Email__c from account where id=:accid];
                NetSuiteCustomers nsc = new NetSuiteCustomers();
                nsc.objList.add(new NetSuiteCustomers.objList(acc,terms));
                NetSuiteConnector ns = new NetSuiteConnector();
                System.debug('Customer: '+nsc.toString());
                ns.body=nsc.toString();
                NetSuiteResponse nsr = ns.send();
                if(Test.isRunningTest()){
                    NetSuiteResponse.resData res = new NetSuiteResponse.resData();
                    res.recordId = acc.id;
                    res.lineUid = nsc.objList[0].uid;
                    nsr.resData.add(res);
                }
                for(NetSuiteCustomers.objList obj:nsc.objList)
                    for(NetSuiteResponse.resData res:nsr.resData)
                        if(res.lineUid == obj.uid){
                            if(res.recordId!=null){
                                /*acc.NetSuite_ID__c = res.recordId;
                                update acc;*/
                                err = 'success;'+res.recordId;
                                break;
                            }
                            else
                                err = res.lineMessage;
                        }
            }
            catch(exception e){
                err = e.getMessage();
            }
        }
        else
            err = 'No Account ID, Please try again or contact admin.';
        if(err==null)
            err = 'Failed to contact NetSuite, Please try again later or contact admin.';
        return err;
    }
}
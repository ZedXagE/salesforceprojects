/*
Copyright 2016 salesforce.com, inc. All rights reserved.

Use of this software is subject to the salesforce.com Developerforce Terms of Use and other applicable terms that salesforce.com may make available, as may be amended from time to time. You may not decompile, reverse engineer, disassemble, attempt to derive the source code of, decrypt, modify, or create derivative works of this software, updates thereto, or any part thereof. You may not use the software to engage in any development activity that infringes the rights of a third party, including that which interferes with, damages, or accesses in an unauthorized manner the servers, networks, or other properties or services of salesforce.com or any third party.

WITHOUT LIMITING THE GENERALITY OF THE FOREGOING, THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. IN NO EVENT SHALL SALESFORCE.COM HAVE ANY LIABILITY FOR ANY DAMAGES, INCLUDING BUT NOT LIMITED TO, DIRECT, INDIRECT, SPECIAL, INCIDENTAL, PUNITIVE, OR CONSEQUENTIAL DAMAGES, OR DAMAGES BASED ON LOST PROFITS, DATA OR USE, IN CONNECTION WITH THE SOFTWARE, HOWEVER CAUSED AND, WHETHER IN CONTRACT, TORT OR UNDER ANY OTHER THEORY OF LIABILITY, WHETHER OR NOT YOU HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
*/

global class SoftphoneContactSearchController {
    global String phonenum{get;set;}
    global String recid{get;set;}
    global SoftphoneContactSearchController(){
        phonenum = ApexPages.currentPage().getParameters().get('phonenumber');
        recid = '';
        if(phonenum!=null&&phonenum!=''){
            List <Sobject> sobs = getList(phonenum);
            if(sobs.size()>0)
                recid = sobs[0].id;
        }
    }
    global void startCheck(){
        if(recid!=null&&recid!=''){
            try{
                insert new Task(Subject='Inbound Call',ActivityDate=System.Today(),CallObject='VoiceSpin',CallType='Inbound',Type='Call',WhoId=recid,Status='Completed');
            }
            catch(exception e){}
        }
    }
    @AuraEnabled
    global static String outGoing(String recid){
        try{
            insert new Task(Subject='Outbound Call',ActivityDate=System.Today(),CallObject='VoiceSpin',CallType='Outbound',Type='Call',WhoId=recid,Status='Completed');
            return 'success';
        }
        catch(exception e){return 'failed';}
    }
    webService static String getContacts(String name) {
        List<Sobject> contactList = getList(name);
        return JSON.serialize(contactList);
    }
    global static List <Sobject> getList(String name){
        List<Sobject> contactList = new List<Sobject>();
        for (Contact contact: [SELECT Id, Phone, Name, Title, Account.Name FROM Contact WHERE (id = :name OR Name LIKE :('%' + name + '%') OR firstname LIKE :('%' + name + '%') OR lastname LIKE :('%' + name + '%') OR phone LIKE :('%' + name + '%')) LIMIT 10])
            contactList.add(contact);
        //added by ZedXagE
        for (Lead ld: [SELECT Id, Phone, Name, Title FROM Lead WHERE (id = :name OR Name LIKE :('%' + name + '%') OR firstname LIKE :('%' + name + '%') OR lastname LIKE :('%' + name + '%') OR phone LIKE :('%' + name + '%')) LIMIT 10])
            contactList.add(ld);
        return contactList;
    }
}
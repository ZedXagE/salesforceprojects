/*
Copyright 2016 salesforce.com, inc. All rights reserved.

Use of this software is subject to the salesforce.com Developerforce Terms of Use and other applicable terms that salesforce.com may make available, as may be amended from time to time. You may not decompile, reverse engineer, disassemble, attempt to derive the source code of, decrypt, modify, or create derivative works of this software, updates thereto, or any part thereof. You may not use the software to engage in any development activity that infringes the rights of a third party, including that which interferes with, damages, or accesses in an unauthorized manner the servers, networks, or other properties or services of salesforce.com or any third party.

WITHOUT LIMITING THE GENERALITY OF THE FOREGOING, THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. IN NO EVENT SHALL SALESFORCE.COM HAVE ANY LIABILITY FOR ANY DAMAGES, INCLUDING BUT NOT LIMITED TO, DIRECT, INDIRECT, SPECIAL, INCIDENTAL, PUNITIVE, OR CONSEQUENTIAL DAMAGES, OR DAMAGES BASED ON LOST PROFITS, DATA OR USE, IN CONNECTION WITH THE SOFTWARE, HOWEVER CAUSED AND, WHETHER IN CONTRACT, TORT OR UNDER ANY OTHER THEORY OF LIABILITY, WHETHER OR NOT YOU HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
*/

public class SoftphoneProviderHelper {
	public class ProviderFactory
    {
        public ProviderFactory(){}
        public SoftphoneProviderHelper.SoftphoneProvider newProvider(String className)
        {
            Type providerImpl = Type.forName(className);
            if (providerImpl == null){
                return null;
            }
            return (SoftphoneProviderHelper.SoftphoneProvider) providerImpl.newInstance();
        }
    }

    public class CallResult {
        public String status;
        public String toNumber;
        public String fromNumber;
        public String accoundId;
        public String provider;
        public String error;
        public String duration;
        public DateTime startTime;
        
        public String extension;
        public String agent;
        public Boolean success;
        public String errormsg;
        public String uniqueid;

    }

    public interface SoftphoneProvider {
      CallResult makeCall(String account, String token, String toNumber, String fromNumber, String recid);
    }

    /*
     * Example of a call provider, using the Twilio Helper Package
     */
    public class TwilioProvider implements SoftphoneProvider {
      public CallResult makeCall(String account, String token, String toNumber, String fromNumber, String recid) {
          /* uncomment this code once you installed the Twilio Helper Package in your org
           * more info here: https://www.twilio.com/docs/libraries/salesforce#installation


          TwilioRestClient client = new TwilioRestClient(account, token);

          Map<String,String> params = new Map<String,String> {
                  'To'   => toNumber,
                  'From' => fromNumber,
                  'Url' => 'http://twimlets.com/holdmusic?Bucket=com.twilio.music.ambient'
              };
          TwilioCall call = client.getAccount().getCalls().create(params);
          CallResult result = new CallResult();
          result.status = call.getStatus();
          return result;

          */
          CallResult result = new CallResult();
          result.status = 'TWILIO_IS_NOT_INSTALLED';
          return result;
        }
    }

    public class DummyProvider implements SoftphoneProvider {
      public CallResult makeCall(String account, String token, String toNumber, String fromNumber, String recid) {
            CallResult result = new CallResult();
            result.status = 'DUMMY_RESPONSE_OK';
            result.toNumber = toNumber;
            result.fromNumber = fromNumber;
            result.accoundId = account;
            result.provider = 'DUMMY_PROVIDER';
            result.duration = '10sec';
            result.startTime = DateTime.now();
          System.debug(result);
            return result;
        }
    }
    public class VoiceSpinAgent implements SoftphoneProvider {
      public CallResult makeCall(String account, String token, String toNumber, String fromNumber, String recid) {
          CallResult result = new CallResult();
          user us = [select id,agent__c from user where id=:Userinfo.getUserId()];
          String agent = us.agent__c!=null?us.agent__c:'';
          HttpResponse res = zedHTTP('http://dmymedia.pbx.voicespin.com/voicespin/call.php?','GET',new Map <String, String> {'agent' => agent,'destination' => toNumber,'recid' => recid});
          if(res!=null){
            System.debug(res.getBody());
            result = (CallResult)System.JSON.deserialize(res.getBody(), CallResult.class);
          }
          result.toNumber = toNumber;
          result.agent = agent;
          System.debug(result);
          return result;
        }
    }
    public class VoiceSpinExtension implements SoftphoneProvider {
      public CallResult makeCall(String account, String token, String toNumber, String fromNumber, String recid) {
          CallResult result = new CallResult();
          user us = [select id,extension from user where id=:Userinfo.getUserId()];
          String extension = us.extension!=null?us.extension:'';
          HttpResponse res = zedHTTP('http://dmymedia.pbx.voicespin.com/voicespin/call.php?','GET',new Map <String, String> {'extension' => extension,'destination' => toNumber,'recid' => recid});
          if(res!=null){
            System.debug(res.getBody());
            result = (CallResult)System.JSON.deserialize(res.getBody(), CallResult.class);
          }
          result.toNumber = toNumber;
          result.extension = extension;
          System.debug(result);
          return result;
        }
    }
    public static HttpResponse zedHTTP(String endpoint,String method,Map <String,String> data){
		String response='';
		Http http=new Http();
		HttpRequest req=new HttpRequest();
		String body='';
		for(String key:data.Keyset()){
			if(body!='')
				body+='&';
			body+=key+'='+data.get(key);
        }
        req.setendpoint(endpoint+body);
		req.setmethod(method);
		req.setbody(body);
        HttpResponse res;
		if(!Test.isRunningTest())
			res = http.send(req);
		return res;
	}
}
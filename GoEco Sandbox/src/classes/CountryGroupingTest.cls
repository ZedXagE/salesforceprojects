@isTest
public class CountryGroupingTest {
    static TestMethod void tes(){
        insert new Country_grouping__c(Name='East',Countries__c='Israel',Owner_ids__c=Userinfo.getUserId());
        insert new Country_grouping__c(Name='West',Countries__c='USA',Owner_ids__c=Userinfo.getUserId());
        insert new Country_grouping__c(Name='else',Owner_ids__c=Userinfo.getUserId());
        Account acc = new Account(Name='test');
        insert acc;
        Contact con = new Contact(Accountid=acc.id,LastName='test',Email='test@test.com',MailingCountry='Israel');
        insert con;
        Lead ld = new Lead(Lastname='test',Company='test',Email='test@test.com',Country='Israel',Lead_Type__c='test');
        insert ld;
        insert new Lead(Lastname='test',Company='test',Email='test1@test.com',Lead_Type__c='test');
        con.MailingCountry='USA';
        update con;
        ld.Country='USA';
        update ld;
    }
}
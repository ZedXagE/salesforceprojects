/*
 * @Author: ZedXagE - ITmyWay 
 * @Date: 2018-10-07 11:05:02 
 * @Last Modified by: ZedXagE - ITmyWay
 * @Last Modified time: 2018-10-07 12:03:51
 */

public class CountryGroupingHandler {
    public static void Grouping(List <sobject> sobs, String rectype, String Trig){
        String CountryField = rectype=='Contact'?'MailingCountry':'Country';
        Boolean OwnerCheck = Trig=='I'&&rectype!='Contact'?true:false;
        List <Country_Grouping__c> cogs = [select Id,Name,Countries__c,Owner_Ids__c,Index__c from Country_Grouping__c where Countries__c!=null];
        List <Country_Grouping__c> elsecog = [select Id,Name,Countries__c,Owner_Ids__c,Index__c from Country_Grouping__c where Countries__c=null limit 1];
        List <Country_Grouping__c> upcogs = new List <Country_Grouping__c>();

        for(sobject sob:sobs){
            if(sob.get(CountryField)==null) {
                if(elsecog.size()>0){
                    if(OwnerCheck&&elsecog[0].Owner_Ids__c!=null&&sob.get('Lead_Type__c')!=null){
                        if(elsecog[0].Index__c==null) elsecog[0].Index__c = 0;
                        String [] Owners = elsecog[0].Owner_Ids__c.replaceAll(' ','').split(';');
                        sob.put('OwnerId', Owners[Integer.valueof(elsecog[0].Index__c)]);
                        if(Owners.size()!=1){
                            if(elsecog[0].Index__c==Owners.size()-1) elsecog[0].Index__c = 0;
                            else elsecog[0].Index__c = elsecog[0].Index__c+1;
                            upcogs.add(elsecog[0]);
                        }
                    }
                }
            }
            else {
                for(Country_Grouping__c cog:cogs){
                    try{
                        if(cog.Countries__c.tolowercase().contains( ((String) sob.get(CountryField)).tolowercase())){
                            sob.put('Country_Grouping__c',cog.Name);
                            if(OwnerCheck&&cog.Owner_Ids__c!=null&&sob.get('Lead_Type__c')!=null){
                                if(cog.Index__c==null) cog.Index__c = 0;
                                String [] Owners = cog.Owner_Ids__c.replaceAll(' ','').split(';');
                                sob.put('OwnerId', Owners[Integer.valueof(cog.Index__c)]);
                                if(Owners.size()!=1){
                                    if(cog.Index__c==Owners.size()-1) cog.Index__c = 0;
                                    else cog.Index__c = cog.Index__c+1;
                                    upcogs.add(cog);
                                }
                            }
                            break;
                        }
                    }
                    catch(exception e){}
                }
            }
        }

        update upcogs;
    }
}
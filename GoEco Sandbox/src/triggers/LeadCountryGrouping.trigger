trigger LeadCountryGrouping on Lead (before insert,before update) {
    List <Lead> CountryToCheck = new List <Lead>();
    String Trig = '';
    if(Trigger.IsInsert){
        Trig = 'I';
        for(Lead ld:Trigger.New)
            /*if(ld.Country!=null)*/{
                ld.Country_Grouping__c=null;
            	CountryToCheck.add(ld);
            }
    }
    else if(Trigger.IsUpdate){
        Trig = 'U';
        for(Lead ld:Trigger.New){
            Lead oldld = Trigger.OldMap.get(ld.id);
            if(ld.Country!=oldld.Country){
                ld.Country_Grouping__c=null;
                if(ld.Country!=null)
            		CountryToCheck.add(ld);
            }
        }
    }
    if(CountryToCheck.size()>0)
		CountryGroupingHandler.Grouping(CountryToCheck,'Lead',Trig);
}
trigger ContactCountryGrouping on Contact (before insert,before update) {
    List <Contact> CountryToCheck = new List <Contact>();
    String Trig = '';
    if(Trigger.IsInsert){
        Trig = 'I';
        for(Contact con:Trigger.New)
            if(con.MailingCountry!=null){
                Con.Country_Grouping__c=null;
            	CountryToCheck.add(con);
            }
    }
    else if(Trigger.IsUpdate){
        Trig = 'U';
        for(Contact con:Trigger.New){
            Contact oldcon = Trigger.OldMap.get(con.id);
            if(con.MailingCountry!=oldcon.MailingCountry){
                Con.Country_Grouping__c=null;
                if(con.MailingCountry!=null)
            		CountryToCheck.add(con);
            }
        }
    }
    if(CountryToCheck.size()>0)
		CountryGroupingHandler.Grouping(CountryToCheck,'Contact',Trig);
}
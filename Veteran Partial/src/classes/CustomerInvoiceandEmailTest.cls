@isTest(seealldata=true)
public class CustomerInvoiceandEmailTest {
  static testMethod void newservice()
  {
      account acc=[select id from account limit 1];
      ApexPages.StandardController controller = new ApexPages.StandardController(acc);
      PageReference pgRef = Page.NewServiceStandard; //Create Page Reference - 'Appt_New' is the name of Page
      Test.setCurrentPage(pgRef); //Set the page for Test Method
      ApexPages.currentPage().getParameters().put('id', acc.id);//Pass Id to page
      NewServiceController sc = new NewServiceController(controller);
      sc.Create();
  }
  static testMethod void Testserv()
  {
      CKSW_BASE__Service__c serv=[select id from CKSW_BASE__Service__c limit 1];
      Catalog_Order__c co=new Catalog_Order__c(Service_Lookup__c=serv.id);
      insert co;
      ApexPages.StandardController controller = new ApexPages.StandardController(serv);
      PageReference pgRef = Page.ServiceInvoiceRec; //Create Page Reference - 'Appt_New' is the name of Page
      Test.setCurrentPage(pgRef); //Set the page for Test Method
      ApexPages.currentPage().getParameters().put('id', serv.id);//Pass Id to page
      ServiceInvoiceRedirect sc = new ServiceInvoiceRedirect(controller);
      sc.redi();
  }
    static testMethod void Testci()
    {
        Catalog_Order__c co=new Catalog_Order__c();
        insert co;
        ApexPages.StandardController controller = new ApexPages.StandardController(co);
        PageReference pgRef = Page.CustomerInvoice; //Create Page Reference - 'Appt_New' is the name of Page
		Test.setCurrentPage(pgRef); //Set the page for Test Method
		ApexPages.currentPage().getParameters().put('id', co.id);//Pass Id to page
        CustomerInvoiceController sc = new CustomerInvoiceController(controller);
        sc.signatureData='12,13,14';
        sc.GeneratePDF();
    }
        static testMethod void Testcia()
    {
      CKSW_BASE__Service__c serv=[select id from CKSW_BASE__Service__c limit 1];
        Catalog_Order__c co=new Catalog_Order__c(Service_Lookup__c=serv.id);
        insert co;
        Document doc = new Document();
                doc.Name='test1';
                doc.AuthorId = UserInfo.getUserId();
                doc.FolderId = UserInfo.getUserId();
                doc.ContentType = 'image/jpeg';
                doc.Type='jpg';
                doc.IsPublic=true;
                doc.Body =EncodingUtil.base64Decode('b64');
                insert doc;
        ApexPages.StandardController controller = new ApexPages.StandardController(co);
        PageReference pgRef = Page.CustomerInvoice1; //Create Page Reference - 'Appt_New' is the name of Page
		Test.setCurrentPage(pgRef); //Set the page for Test Method
		ApexPages.currentPage().getParameters().put('id', co.id);//Pass Id to page
        ApexPages.currentPage().getParameters().put('sigpic', doc.id);//Pass Id to page
        CustomerInvoiceControllerAccept sc = new CustomerInvoiceControllerAccept(controller);
        sc.signatureData='12,13,14';
        sc.GeneratePDF();
        sc.back();
    }
    static testMethod void Testem()
    {
        //Account acc=new Account(lastname='test',personemail='test@test.com',Dispatcher_Description__c='test',);
      	account acc=[select id from account limit 1];
        //insert acc;
        Catalog_Order__c co=new Catalog_Order__c(account__c=acc.id);
        insert co;
        attachment at = new Attachment();
        at.body=Blob.valueOf('Unit Test Attachment Body');
        at.OwnerId = UserInfo.getUserId();
        at.ParentId = co.Id;
        at.Name=co.Name+'.pdf';
        at.ContentType = 'application/pdf';
        insert at;
        ApexPages.StandardController controller = new ApexPages.StandardController(co);
        PageReference pgRef = Page.InvoiceEmail; //Create Page Reference - 'Appt_New' is the name of Page
		Test.setCurrentPage(pgRef); //Set the page for Test Method
		ApexPages.currentPage().getParameters().put('id', co.id);//Pass Id to page
        ApexPages.currentPage().getParameters().put('atid', at.id);//Pass Id to page
        InvoiceEmailController sc = new InvoiceEmailController(controller);
        sc.tex='test@test.com';
        sc.SendEmail();
    }
}
public class CustomerInvoiceController {
    public String signatureData {set; get;}
    public String signaturepic {set; get;}
    public String dat {set; get;}
    public String datcomplete {set; get;}
    public String pagename {set; get;}
    public Boolean balance {set; get;}
    public String nextpagename {set; get;}
    public id Curid{get;set;}
    public String Retid{get;set;}
    public Catalog_Order__c co{get;set;}
		public Boolean sigin{get;set;}
    public Boolean disc{get;set;}
    public CustomerInvoiceController(ApexPages.StandardController controller)
    {
        balance=false;
        String strurl = ApexPages.currentPage().getUrl();
        strurl = strurl.split('apex/')[1];
        strurl = strurl.split('\\?')[0];
        system.debug(strurl);
        if(strurl=='customerinvoicerec')
            pagename='r';
        else
            pagename='e';

        if(pagename=='r')
            nextpagename='CustomerInvoice1rec';
        else
            nextpagename='CustomerInvoice1';
      disc=false;
        datcomplete='';
      dat='';
			sigin=false;
            Curid=ApexPages.currentPage().getParameters().get('id');
            Retid=ApexPages.currentPage().getParameters().get('retid');
        co=[select id,Signature__c,Total_Discount__c,Service_Lookup__c,name,Max_Discount__c,Service_Lookup__r.CKSW_BASE__Finish__c,Service_Lookup__r.CKSW_BASE__Appointment_Start__c,Service_Lookup__r.CKSW_BASE__Appointment_Finish__c,Service_Lookup__r.Balance__c from Catalog_Order__c where id=:Curid];
        if(co.Service_Lookup__r.CKSW_BASE__Appointment_Start__c!=null)
        dat=co.Service_Lookup__r.CKSW_BASE__Appointment_Start__c.format('MM/dd/yyyy hh:mm a', 'PST');
        if(co.Service_Lookup__r.CKSW_BASE__Appointment_Finish__c!=null&&co.Service_Lookup__r.CKSW_BASE__Appointment_Start__c!=null)
        dat+=' - '+co.Service_Lookup__r.CKSW_BASE__Appointment_Finish__c.format('hh:mm a', 'PST');
        if(co.Service_Lookup__r.CKSW_BASE__Finish__c!=null)
        datcomplete=co.Service_Lookup__r.CKSW_BASE__Finish__c.format('MM/dd/yyyy hh:mm a', 'PST');
        if(co.Max_Discount__c!=null&&co.Max_Discount__c!=0)
          disc=true;
        if(co.Service_Lookup__r.Balance__c==0)
            balance=true;
    }
    public pagereference GeneratePDF(){
      sigin=false;
        List <string> b64 =new list <string>();
        String ur='/apex/'+nextpagename+'?id='+co.id+'&page='+pagename+'&retid='+Retid;
        if (signatureData != null && signatureData != '')
        {
            co.Signature__c = '<img style="width: 100%;height:150px;" src="' + signatureData + '"/>';
            update co;
            b64=signatureData.split(',');
            if(b64.size()>1)
            {
                Document doc = new Document();
                doc.Name=co.name;
                doc.AuthorId = UserInfo.getUserId();
                doc.FolderId = UserInfo.getUserId();
                doc.ContentType = 'image/jpeg';
                doc.Type='jpg';
                doc.IsPublic=true;
                doc.Body =EncodingUtil.base64Decode(b64[1]);
                insert doc;
                ur='/apex/'+nextpagename+'?id='+co.id+'&sigpic='+doc.id+'&page='+pagename+'&retid='+Retid;
            }
            PageReference newPage = new PageReference(ur);
            newPage.setRedirect(true);
            return newPage;
        }
        else
        {
          sigin=true;
          return null;
        }
    }
}
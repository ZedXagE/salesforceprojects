public class RatingStarsController{
    public String objid {get;set {
            objid = value;
            if (!initialized&&objname!=null) {
                init();
                initialized = true;
            }
        }
    }
    private boolean initialized;
    public String objname {get;set {
            objname = value;
            if (!initialized&&objid!=null) {
                init();
                initialized = true;
            }
        }
    }
    public String currentField {get;set;}
    public String currentRate {get;set;}
    public sobject obj{get;set;}
    public List <Rating_Settings__c> rates{get;set;}
    public RatingStarsController(){
        initialized = false;
    }
    public void init(){
        rates = [select Label__c,isEditable__c,Field_Api__c from Rating_Settings__c where Object_Api__c =: objname Order by Name];
        String flds='';
        for(Rating_Settings__c rate:rates)
            flds+=','+rate.Field_Api__c;
        String qr = 'select id'+flds+' from '+objname+' where id=\''+objid+'\'';
        obj = Database.query(qr);
    }
    public void UpdateRate(){
        if(objid!=null&&currentField!=null&&currentField!=''&&currentRate!=null&&currentRate!=''){
            obj.put(currentField,Integer.valueof(currentRate));
            update obj;
        }
    }
}
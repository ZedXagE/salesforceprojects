/*
 * @Author: ZedXagE - ITmyWay 
 * @Date: 2018-12-25 09:50:55 
 * @Last Modified by:   ZedXagE - ITmyWay 
 * @Last Modified time: 2018-12-25 09:50:55 
 */

trigger LeadDownloadFile on Lead (after insert,after update) {
    Map <String,List<String>> ups = new Map <String,List<String>>();
    for(Lead ld:Trigger.new){
        ups.put(ld.id, new List <String>());
        if(Trigger.isUpdate){
            if(ld.Form_Submission_Date_and_Time__c!=null&&ld.Form_Submission_Date_and_Time__c!=Trigger.OldMap.get(ld.id).Form_Submission_Date_and_Time__c){
                String formname = ld.Web_Form_Name__c!=null?ld.Web_Form_Name__c:'';
                if(ITmyWayEngagementController.formnames.containsKey(formname))
                    formname = ITmyWayEngagementController.formnames.get(formname);
                String subj = '';
                Boolean ok = false;
                if(formname.contains('Report')){
                    ok = true;
                    subj = ld.CTA_Description__c!=null?ld.CTA_Description__c:'';
                }
                else if(formname.contains('Webinar')){
                    ok = true;
                    subj = ld.Webinar_Name__c!=null?ld.Webinar_Name__c:'';
                }
                else if(formname.contains('Event')){
                    ok = true;
                    subj = ld.Event_Name__c!=null?ld.Event_Name__c:'';
                }
                if(ok)  ups.get(ld.id).add('Form Handler: '+formname+' - '+subj);
            }
        }
        else if(Trigger.isInsert){
            if(ld.Form_Submission_Date_and_Time__c!=null){
                String formname = ld.Web_Form_Name__c!=null?ld.Web_Form_Name__c:'';
                if(ITmyWayEngagementController.formnames.containsKey(formname))
                    formname = ITmyWayEngagementController.formnames.get(formname);
                String subj = '';
                Boolean ok = false;
                if(formname.contains('Report')){
                    ok = true;
                    subj = ld.CTA_Description__c!=null?ld.CTA_Description__c:'';
                }
                else if(formname.contains('Webinar')){
                    ok = true;
                    subj = ld.Webinar_Name__c!=null?ld.Webinar_Name__c:'';
                }
                else if(formname.contains('Event')){
                    ok = true;
                    subj = ld.Event_Name__c!=null?ld.Event_Name__c:'';
                }
                if(ok)  ups.get(ld.id).add('Form Handler: '+formname+' - '+subj);
            }
        }
    }
    ITmyWayEngagementController.createTask(ups);
}
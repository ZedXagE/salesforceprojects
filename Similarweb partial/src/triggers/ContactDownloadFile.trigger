/*
 * @Author: ZedXagE - ITmyWay 
 * @Date: 2018-12-25 09:50:47 
 * @Last Modified by:   ZedXagE - ITmyWay 
 * @Last Modified time: 2018-12-25 09:50:47 
 */
 
trigger ContactDownloadFile on Contact (after insert,after update) {
    Map <String,List<String>> ups = new Map <String,List<String>>();
    
    for(Contact con:Trigger.new){
        ups.put(con.id, new List <String>());
        if(Trigger.isUpdate){
            if(con.Form_Submission_Date_and_Time__c!=null&&con.Form_Submission_Date_and_Time__c!=Trigger.OldMap.get(con.id).Form_Submission_Date_and_Time__c){
                String formname = con.Web_Form_Name__c!=null?con.Web_Form_Name__c:'';
                if(ITmyWayEngagementController.formnames.containsKey(formname))
                    formname = ITmyWayEngagementController.formnames.get(formname);
                String subj = '';
                Boolean ok = false;
                if(formname.contains('Report')){
                    ok = true;
                    subj = con.CTA_Description__c!=null?con.CTA_Description__c:'';
                }
                else if(formname.contains('Webinar')){
                    ok = true;
                    subj = con.Webinar_Name__c!=null?con.Webinar_Name__c:'';
                }
                else if(formname.contains('Event')){
                    ok = true;
                    subj = con.Event_Name__c!=null?con.Event_Name__c:'';
                }
                if(ok)  ups.get(con.id).add('Form Handler: '+formname+' - '+subj);
            }
        }
        else if(Trigger.isInsert){
            if(con.Form_Submission_Date_and_Time__c!=null){
                String formname = con.Web_Form_Name__c!=null?con.Web_Form_Name__c:'';
                if(ITmyWayEngagementController.formnames.containsKey(formname))
                    formname = ITmyWayEngagementController.formnames.get(formname);
                String subj = '';
                Boolean ok = false;
                if(formname.contains('Report')){
                    ok = true;
                    subj = con.CTA_Description__c!=null?con.CTA_Description__c:'';
                }
                else if(formname.contains('Webinar')){
                    ok = true;
                    subj = con.Webinar_Name__c!=null?con.Webinar_Name__c:'';
                }
                else if(formname.contains('Event')){
                    ok = true;
                    subj = con.Event_Name__c!=null?con.Event_Name__c:'';
                }
                if(ok)  ups.get(con.id).add('Form Handler: '+formname+' - '+subj);
            }
        }
    }
    ITmyWayEngagementController.createTask(ups);
}
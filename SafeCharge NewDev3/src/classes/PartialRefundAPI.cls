/*
 * @Author: ZedXagE - ITmyWay 
 * @Date: 2018-12-19 15:11:15 
 * @Last Modified by:   ZedXagE - ITmyWay 
 * @Last Modified time: 2018-12-19 15:11:15 
 */
@RestResource(urlMapping='/partialrefund')
global class PartialRefundAPI {
	global class PRrequest{
        global PRaccount account;
        global List <PRmid> TID;
    }
	global class PRaccount {
        global String id;
        global String clientId;
	}
	global class PRmid {
        global String currency_c;
        global Decimal value;
		global Decimal Tid;
	}
	global class PResponse{
        global PRerror error;
		global String status;
		global PResponse(){
			error = new PRerror();
		}
    }
	global class PRerror{
		global String message;
		global String code;
	}
    @HttpPost
    global static PResponse insertMIDS() {
		PResponse resp = new PResponse();
		resp.status = 'Failure';
		String amex = Label.PartialRefund_Amex;
		try{
			PRrequest req = (PRrequest)JSON.deserialize(RestContext.request.requestBody.toString().replaceAll('currency','currency_c'), PRrequest.class);
			if(req.account!=null&&req.TID!=null){
				List <Compliance__c> comps = [select id,(select id from Banks__r where RecordType.Name=:amex limit 1),(select id from GW_Names__r where Client_ID__c=:req.account.clientId limit 1) from Compliance__c where id=:req.account.id limit 1];
				if(comps.size()>0){
					if(comps[0].Banks__r.size()>0){
						Merchant_Bank__c bank = comps[0].Banks__r[0];
						GW_Name__c gw = new GW_Name__c();
						if(comps[0].GW_Names__r.size()>0)
							gw = comps[0].GW_Names__r[0];
						else{
							gw.Client_ID__c = req.account.clientId;
							gw.Compliance__c = comps[0].id;
							try{
								insert gw;
							}
							catch(exception e2){
								resp.error.message = 'Failed insert GW name: '+e2.getMessage();
								resp.error.code = '5';
								return resp;
							}
						}
						List <MID__c> mids = new List <MID__c>();
						for(PRmid rmid:req.TID){
							MID__c mid = new MID__c();
							mid.Bank__c = bank.id;
							mid.GW_Name__c = gw.id;
							mid.MID__c = rmid.value!=null?String.valueof(rmid.value):null;
							mid.TID__c = rmid.Tid!=null?String.valueof(rmid.Tid):null;
							mid.Currency__c = rmid.currency_c;
							mids.add(mid);
						}
						try{
							insert mids;
							resp.status = 'Success';
							resp.error = null;
							return resp;
						}
						catch(exception e2){
							resp.error.message = 'Failed insert MIDS: '+e2.getMessage();
							resp.error.code = '6';
							return resp;
						}
					}
					else{
						resp.error.message = 'Amex is not set for the merchant';
						resp.error.code = '4';
					}
				}
				else{
					resp.error.message = 'Company Profile ID not found';
					resp.error.code = '3';
				}
			}
			else{
				if(req.account==null){
					resp.error.message = 'no account passed';
					resp.error.code = '1';
				}
				else{
					resp.error.message = 'no TID passed';
					resp.error.code = '2';
				}
			}
		}
		catch(exception e){
			resp.error.message = 'JSON parse error';
			resp.error.code = '0';
		}
		return resp;
	}
}
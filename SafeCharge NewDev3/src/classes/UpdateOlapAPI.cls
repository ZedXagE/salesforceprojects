/*
 * @Author: ZedXagE - ITmyWay 
 * @Date: 2018-01-06 15:11:15 
 * @Last Modified by:   ZedXagE - ITmyWay 
 * @Last Modified time: 2018-01-06 15:11:15 
 */
@RestResource(urlMapping='/updateolap')
global class UpdateOlapAPI {
	global class UOlap {
        global Decimal multiClientId;
        global String bank;
		global String APM;
        global String date_c;
        global Decimal volume;
        global Decimal count;
        global Decimal ratio;
        global Decimal revenue;
        global Decimal chb_count;
        global Decimal serial;
	}
	global class UOesponse{
		global String status;
		global Integer errorCode;
		global String errorDescription;
		global List <UOerror> error;
		global UOesponse(){
			error = new List <UOerror>();
		}
    }
	global class UOerror{
		global Decimal multiClientId;
		global String bankOrAPM;
		global String date_c;
		global Decimal serial;
		global Decimal error_code;
		global String error_description;
	}
    @HttpPost
    global static String updateOlaps() {
		UOesponse resp = new UOesponse();
		resp.status = 'failure';
		try{
			List <UOlap> olaps = (List <UOlap>)JSON.deserialize(RestContext.request.requestBody.toString().replaceAll('date','date_c'), List <UOlap>.class);
			if(olaps!=null){
				Map <String,String> cpids = new Map <String,String>();
				for(UOlap olap:olaps)
					cpids.put(String.valueof(olap.multiClientId),null);
				for(GW_Name__c gw:[select id,ClientID_for_OLAP__c,Compliance__c from GW_Name__c where ClientID_for_OLAP__c in:cpids.keySet()])
					cpids.put(gw.ClientID_for_OLAP__c,gw.Compliance__c);
				Boolean failed = false;
				List <OLAP__c> olaps4ins = new List <OLAP__c>();
				for(UOlap olap:olaps){
					if(cpids.containsKey(String.valueof(olap.multiClientId))&&cpids.get(String.valueof(olap.multiClientId))!=null){
						Date dat;
						boolean datsuccess = true;
						if(olap.date_c!=null){
							try{
								dat = getdat(olap.date_c);
							}
							catch(exception edat){
								datsuccess=false;
							}
						}
						if(datsuccess){
							OLAP__c newolap = new OLAP__c(
								APM__c = olap.APM,
								Bank__c = olap.bank,
								Count__c = olap.count,
								Chargeback_Count__c = olap.chb_count,
								multiClientId__c = String.valueof(olap.multiClientId),
								Ratio__c = olap.ratio,
								Revenue__c = olap.revenue,
								Serial__c = olap.serial,
								Volume__c = olap.volume,
								Company_Profile__c = cpids.get(String.valueof(olap.multiClientId)),
								Date__c = dat
							);
							olaps4ins.add(newolap);
						}
						else{
							failed = true;
							resp.errorDescription = 'some records could not be processed';
							resp.errorCode = 2;
							UOerror er = getError(olap);
							er.error_code = 3;
							er.error_description = 'cant parse date';
							resp.error.add(er);
						}
					}
					else{
						failed = true;
						resp.errorDescription = 'some records could not be processed';
						resp.errorCode = 2;
						UOerror er = getError(olap);
						er.error_code = 2;
						er.error_description = 'merchant could not be located';
						resp.error.add(er);
					}
				}
				Database.SaveResult[] lsr =Database.insert(olaps4ins, false);
				for (integer i=0;i<lsr.size();i++) {
					if (!lsr[i].isSuccess()) {
						failed = true;
						resp.errorDescription = 'some records could not be processed';
						resp.errorCode = 2;
						UOerror er = getError(olaps[i]);
						er.error_code = 4;
						String errmsg = '';
						for(Database.error error:lsr[i].getErrors())
							errmsg += error.getMessage()+'; ';
						er.error_description = errmsg;
						resp.error.add(er);
					}
				}
				if(!failed)
					resp.status = 'success';
			}
			else{
				resp.errorDescription = 'list is empty';
				resp.errorCode = 1;
			}
		}
		catch(exception e){
			resp.errorDescription = 'JSON parse error';
			resp.errorCode = 0;
		}
		String resstr = JSON.serialize(resp);
		resstr.replaceAll('error_code','error code').replaceAll('error_description','error description').replaceAll('date_c','date');
		sendEmail(resstr);
		return resstr;
	}
	global static UOerror getError(UOlap olap){
		UOerror er = new UOerror();
		er.multiClientId = olap.multiClientId;
		er.date_c = olap.date_c;
		er.serial = olap.serial;
		er.bankOrAPM = olap.bank!=null?olap.bank:olap.APM;
		return er;
	}
	global static Date getdat(string dat){
		return Date.newinstance(Integer.valueof(dat.split('-')[0]),Integer.valueof(dat.split('-')[1]),Integer.valueof(dat.split('-')[2]));
	}
	global static void sendEmail(string body){
		try{
			MerchantConfig__c merch = [select Exception_email__c from MerchantConfig__c limit 1];
			Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
			String[] toAddresses = new String[] {merch.Exception_email__c};
			mail.setToAddresses(toAddresses);
			mail.setSenderDisplayName('Salesforce update Olaps API');
			mail.setSubject('Salesforce update Olaps API');
			mail.setHtmlBody(body);
			if(!Test.isRunningTest())
				Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
		}
		catch(exception e){}
	}
}
global class Controller {
	global Map<String, String> strs{get;set;}
	private Map<String, String> enLang = new Map<String, String>{
		'Wrong PIN, Please try again'=>'Wrong PIN, Please try again',
		'Please choose Verification Method'=>'Please choose Verification Method',
		'Remember me'=>'Remember me',
		'LOG IN'=>'LOG IN',
		'Verify'=>'Verify',
		'No PIN? Click Here to try again'=>'No PIN? Click Here to try again',
		'Too many tries, please try again later.'=>'Too many tries, please try again later.',
		'You are now logged in!'=>'You are now logged in!'
	};
	private Map<String, String> heLang = new Map<String, String>{
		'Wrong PIN, Please try again'=>'קוד לא נכון, נסה שנית',
		'Please choose Verification Method'=>'אנא בחר שיטת אישור',
		'Remember me'=>'זכור אותי',
		'LOG IN'=>'התחבר',
		'Verify'=>'אשר',
		'No PIN? Click Here to try again'=>'לא קיבלת קוד? לחן כאן לנסות שוב',
		'Too many tries, please try again later.'=>'יותר מדי נסיונות, נסה שנית מאוחר יותר.',
		'You are now logged in!'=>'אתה עכשיו מחובר!'
	};
	private Map<String, String> frLang = new Map<String, String>{
		'Wrong PIN, Please try again'=>'mot de passe incorecte, Veuillez réésayer ',
		'Please choose Verification Method'=>'Veuillez choisir la méthode de verification',
		'Remember me'=>'Se souvenir de moi',
		'LOG IN'=>'SE CONNECTER',
		'Verify'=>'vérifier',
		'No PIN? Click Here to try again'=>'Pas de code pin? Cliquer ici pour réesayer',
		'Too many tries, please try again later.'=>'Plusieurs echec de connexion, veuillez réésayer plus tard',
		'You are now logged in!'=>'Vous etes connecté !'
	};
	private Map<String, String> ruLang = new Map<String, String>{
		'Wrong PIN, Please try again'=>'Неверный PIN-код. Повторите попытку.',
		'Please choose Verification Method'=>'Выберите способ проверки',
		'Remember me'=>'Запомнить меня',
		'LOG IN'=>'Авторизация',
		'Verify'=>'Проверка',
		'No PIN? Click Here to try again'=>'Нет PIN-кода? Нажмите сюда, чтобы получить еще раз',
		'Too many tries, please try again later.'=>'Слишком много попыток, повторите попытку позже.',
		'You are now logged in!'=>'Вы вошли в систему!'

	};
	//Settings
	global class Settings{
		global String Title {get;set;}
		global String Logo {get;set;}
		global String Header_Color {get;set;}
		global String Background_Color {get;set;}
		global String Auth_Color {get;set;}
	}
	global List <selectOption> sendways {get;set;}
	global String way {get;set;}
	global Settings Settings {get;set;}
	global Boolean Remember {get;set;}
	global String log {get;set;}
	global String lang {get;set;}
	global String pin {get;set;}
	global Boolean err {get;set;}
	global Boolean translator {get;set;}

	global Boolean smsOn {get;set;}
	global Boolean smsOnly {get;set;}

	@TestVisible private String privatepin;
	private integer tries;
	private string returl;

	@TestVisible private String UserAgent;
	@TestVisible private String UserIp;

	@TestVisible private String OrgWideID;
	@TestVisible private String Email;
	@TestVisible private String Phone;
	@TestVisible private String AuthStr;

	private static Blob secret_key = Blob.valueOf('zxcvasdfqwerasdf');
	
	//was private due to icone request for non authenticated goes global
	@TestVisible
	global static String encrypt(String strData) {
		Blob data = Blob.valueOf(strData);
		Blob encrypted = Crypto.encryptWithManagedIV('AES128', secret_key, data);
		return EncodingUtil.Base64Encode(encrypted);
	}

	@TestVisible
	global static String decrypt(String encryptedStr) {
		Blob decrypted = Crypto.decryptWithManagedIV('AES128', secret_key, EncodingUtil.Base64Decode(encryptedStr));
		return decrypted.toString();
	}

	global Controller() {
		lang=ApexPages.currentPage().getParameters().get('lang');
		if(lang==null||lang=='')
			lang='en';
		if(lang=='ru')
			strs = ruLang;
		else if(lang=='fr')
			strs = frLang;
		else if(lang=='he')
			strs = heLang;
		else
			strs = enLang;
		UserIp=ApexPages.currentPage().getHeaders().get('X-Salesforce-SIP');
		UserAgent=ApexPages.currentPage().getHeaders().get('User-Agent');
		Remember=false;
		Email=ApexPages.currentPage().getParameters().get('Email');
		Phone=ApexPages.currentPage().getParameters().get('Phone');
		AuthStr='';
		if(Email!=null)
			AuthStr+=Email;
		if(Phone!=null)
			AuthStr+=';'+Phone;
		way='Email';
		err=false;
		log='0';
		tries=0;
		returl = ApexPages.currentPage().getParameters().get('returl');
		sendways = new List <selectOption>();
		initSettings();
	}

	@TestVisible
	private void initSettings(){
		smsOn=false;
		Auth_Settings__c ASettings=new Auth_Settings__c();
		Settings = new Settings();
		List <Auth_Settings__c> AuthSettings=[select Title__c,Logo__c,Header_Color__c,Background_Color__c,Auth_Color__c,Org_Wide_ID__c,SMS_Only__c,Translator__c From Auth_Settings__c Limit 1];
		if(AuthSettings.size()>0)
			ASettings=AuthSettings[0];
		Settings.Title=ASettings.Title__c!=null?ASettings.Title__c:'';
		Settings.Logo=ASettings.Logo__c!=null?ASettings.Logo__c:'';
		Settings.Header_Color=ASettings.Header_Color__c!=null?ASettings.Header_Color__c:'white';
		Settings.Background_Color=ASettings.Background_Color__c!=null?ASettings.Background_Color__c:'white';
		Settings.Auth_Color=ASettings.Auth_Color__c!=null?ASettings.Auth_Color__c:'white';
		OrgWideID=ASettings.Org_Wide_ID__c!=null?ASettings.Org_Wide_ID__c:'';
		smsOnly=ASettings.SMS_Only__c!=null?ASettings.SMS_Only__c:false;
		translator=ASettings.Translator__c!=null?ASettings.Translator__c:false;
		if(OrgWideID!=''){
			smsOn=true;
			initPickList();
			if(smsOnly==true)
				way='SMS';
		}
	}

	@TestVisible
	private void initPickList(){
		sendways.add(new selectOption('Email','Email'));
		sendways.add(new selectOption('SMS','SMS'));
		way='Email';
	}
	global Static String getLogged(){
		Cookie login = ApexPages.currentPage().getCookies().get('Authzed');
		String UserIp=ApexPages.currentPage().getHeaders().get('X-Salesforce-SIP');
		String UserAgent=ApexPages.currentPage().getHeaders().get('User-Agent');
		if(login!=null&&login.getValue()!=null) {
			String cookieData = decrypt(login.getValue());
			if (cookieData.startsWith('AuthStr:')&&cookieData.contains(',UserIp:')&&cookieData.contains(',UserAgent:')){

				String cookieIp = (cookieData.Split(',UserIp:')[1]).split(',UserAgent:')[0];
				String cookieAgent = cookieData.Split(',UserAgent:')[1];
				if(cookieIp==UserIp&&cookieAgent==UserAgent)
					return (cookieData.split('AuthStr:')[1]).split(',UserIp:')[0];
				else
					Logout();
			}
		}
		return null;
	}
	global PageReference Logcheck(){
		String logout = ApexPages.currentPage().getParameters().get('logout');
		if(logout=='1'){
			Logout();
		}
		if(returl!=null){
			if (getLogged() != null || logout=='1') {
				PageReference pageRef = new PageReference(returl); //it works
				pageref.setRedirect(true);
				return pageRef;
			}
		}
		if((smsOnly&&Phone!=null)||(OrgWideID==''&&Email!=null))
			Login();
		return null;
	}

	global void tryAgain(){
		log='0';
		privatepin=null;
		pin=null;
	}

	global void Login(){
		if(AuthStr!=null&&AuthStr!=''){
			privatepin=getpin();
			sendPIN();
			log='1';
		}
	}

	global PageReference Verify(){
		err=false;
		if(privatepin!=null&&privatepin==pin){
			//set cookie
			Cookie login;
			if(Remember)
            	login = new Cookie('Authzed',encrypt('AuthStr:' + AuthStr + ',UserIp:' + UserIp + ',UserAgent:' + UserAgent),null,604800,true);//7 days
			else
            	login = new Cookie('Authzed',encrypt('AuthStr:' + AuthStr + ',UserIp:' + UserIp + ',UserAgent:' + UserAgent),null,-1,true);//session
			ApexPages.currentPage().setCookies(new Cookie[]{login});
			log='2';
			if(returl!=null){
				PageReference pageRef = new PageReference(returl); //it works
				pageref.setRedirect(true);
				return pageRef;
			}
			return null;
		}
		else{
			err=true;
			pin='';
			tries++;
			if(tries>3){
				err=false;
				log='-1';
			}
			return null;
		}
	}

	global Static void Logout(){
		Cookie login = new Cookie('Authzed',null,null,0,true);
		ApexPages.currentPage().setCookies(new Cookie[]{login});
	}
	
	@TestVisible
	private String getPIN(){
		String p = String.valueof((((Decimal)Math.random())*1000000).setScale(0));
		for(integer i=6;i>p.length();i--)
			p='0'+p;
		return p;
	}

	@TestVisible
	private void sendPIN(){
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		List <String> toAddresses = new List <String>();
		String message='Authenticate PIN: '+privatepin;
		if(OrgWideID!=''&&way=='SMS'&&Phone!=null){
			if(OrgWideID=='API'){
				InfoSMS.Send(message,Phone);
			}
			else{
				if(!Test.isRunningTest())
					mail.setOrgWideEmailAddressId(OrgWideID);
				toAddresses.add(Phone+'@sms.inforu.co.il');
				mail.setToAddresses(toAddresses);
				mail.setSubject('Authorization');
				mail.setHtmlBody(message);
				if(!Test.isRunningTest())
					Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
			}
		}
		else if(Email!=null){
			mail.setSenderDisplayName('Authenticator');
			toAddresses.add(Email);
			mail.setToAddresses(toAddresses);
			mail.setSubject('Authorization');
			mail.setHtmlBody(message);
			if(!Test.isRunningTest())
				Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
		}
	}

}
/*
 * @Author: ZedXagE - ITmyWay 
 * @Date: 2018-10-04 11:06:46 
 * @Last Modified by: ZedXagE - ITmyWay
 * @Last Modified time: 2018-10-04 11:54:45
 */

global class InfoSMS
{
	global static void Send(String message, String phones)
    {
		List <InfoSMS_Settings__c> smsSettings = [select Name,Password__c,Sender__c from InfoSMS_Settings__c Limit 1];
		if(smsSettings.size()>0)
			Send(smsSettings[0].Name,smsSettings[0].Password__c,smsSettings[0].Sender__c,message,phones);
		else System.debug('no settings');
    }
	global static void Send(String sender, String message, String phones)
    {
		List <InfoSMS_Settings__c> smsSettings = [select Name,Password__c from InfoSMS_Settings__c Limit 1];
		if(smsSettings.size()>0)
			Send(smsSettings[0].Name,smsSettings[0].Password__c,sender,message,phones);
		else System.debug('no settings');
    }
	@Future(callout=true)
    global static void Send(String username, String password, String sender, String message, String phones)
    {
		phones = phones.replaceAll(' ','');
		phones = phones.replaceAll(',',';');
		String xml = 'InforuXML=<?xml version="1.0" encoding="utf-8"?><Inforu><User><Username>' + username + '</Username><Password>' + password + '</Password></User><Content Type="sms"><Message>'+message+'</Message></Content><Recipients><PhoneNumber>'+phones+'</PhoneNumber></Recipients><Settings><Sender>'+sender+'</Sender></Settings></Inforu>';
		System.debug(phones);
		HttpRequest req = new HttpRequest();
		req.setEndpoint('http://api.inforu.co.il/SendMessageXml.ashx');
		req.setMethod('POST');
		req.setBody(xml);
		System.debug(xml);
		HttpResponse res;
		if(!Test.isRunningTest()) res = new Http().send(req);
		if(res!=null) System.debug(res.getBody());
    }
}
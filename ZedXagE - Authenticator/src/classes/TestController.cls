@isTest
public class TestController {
	static testMethod void test() {
        Auth_Settings__c auts = new Auth_Settings__c(Name='test');
        insert auts;
		Controller c = New Controller();
        c.tryAgain();
        c.AuthStr='test@test.com:050';
        c.Email='test@test.com';
        c.Phone='050';
        c.initSettings();
        c.initPickList();
        c.LogCheck();
        String s=Controller.getLogged();
        c.Login();
        C.Verify();
        Controller.Logout();
        s=Controller.encrypt('test');
        s=Controller.decrypt(s);
        c.OrgWideID='test';
        c.way='SMS';
        c.SendPIN();
        c.pin=c.privatepin;
        c.Verify();
        InfoSMS_Settings__c smss = new InfoSMS_Settings__c(Name='test',Password__c='test',Sender__c='test');
        insert smss;
        InfoSMS.Send('test','0500000000');
        InfoSMS.Send('Authorization','test','0500000000');
	}
}
@isTest
public with sharing class CruiseHandlerTest {
    
    @testSetup static void setup() {
        	//PREPARE DATA
		Account acc = new Account();
		acc.Name = 'testAcc';
		acc.First_Name__c = 'testLast';
		acc.Last_Name__c = 'test';
		acc.Phone = '0549234443';
		acc.Email__c = 'test@gmail.com';
		acc.Account_Status__c = 'Club Member';
		acc.Interest__c = 'Basic Course';	
		insert acc;
		
		Boats__c boat1 = new Boats__c();
		boat1.Name = 'מיתוס';
		boat1.License_Number__c = 222222; 
		boat1.Type__c = 'Sailboat'; 
		boat1.Max_Passenger__c = 12; 
		boat1.Docking__c = 'Wharf'; 
		insert boat1;
		  
		Certification__c cer = new Certification__c();
		cer.Name = 'מיתוס';
		cer.Boat__c = boat1.Id;
		cer.Account_Name__c = acc.Id;
		insert cer;
		  
		Transaction__c tran = new Transaction__c();
		tran.Name = 'Alon';
		tran.Account_Name__c = acc.Id;
		tran.Type__c = 'Sailboat';
		Date today = System.today();
		tran.Date_of_Transaction__c = today.addDays(-30);
		tran.Start_Date__c = today.addDays(-20);
		tran.End_Date__c = today.addDays(10);
		tran.Subscripption_Type__c = 'Bank Hours';
		tran.Total_Hours__c = 20;
		tran.weekday__c = 2;
		tran.weekend__c = 3;
		insert tran;       
    }
    /**
     * SEAT-32	: Insert and Update cruise with boat not certificated for Account
	 * Author	: Shira Hai
	 *
	 * @method	: checkAccountHasCertificateTest
	 * @date	: 21/11/2017
	 *
	 * @static
	 */
    static testMethod void checkAccountHasCertificateTest() {
		//PREPARE DATA
		setup();
		
        Account acc = [SELECT Id FROM Account limit 1];
        Transaction__c tran = [SELECT Id FROM Transaction__c limit 1];
        Boats__c boat1 = [SELECT Id FROM Boats__c limit 1];
        Certification__c cer = [SELECT Id FROM Certification__c limit 1];
        
        Boats__c boat2 = new Boats__c();
		boat2.Name = 'אננדה';
		boat2.License_Number__c = 222222; 
		boat2.Type__c = 'Sailboat'; 
		boat2.Max_Passenger__c = 12; 
		boat2.Docking__c = 'Wharf'; 
		insert boat2;
        
		Cruise__c cruise1 = new Cruise__c();
        RecordType clubRecordType = [SELECT Id,Name,DeveloperName FROM RecordType WHERE DeveloperName =: 'Club' LIMIT 1];
        cruise1.RecordTypeId = clubRecordType.Id;
		cruise1.AccountName__c = acc.Id;
		cruise1.Transaction__c = tran.Id;
		cruise1.Boat__c = boat2.Id; 
		cruise1.Cruise_Type__c = 'Club'; 
		cruise1.Status__c = 'Approved'; 
		Date crDate = System.today().addDays(1);
		cruise1.Start_Date__c = Datetime.newInstance(crDate.year(), crDate.month(), crDate.day(), 13, 00, 00);
		cruise1.End_Date__c = Datetime.newInstance(crDate.year(), crDate.month(), crDate.day(), 17, 00, 00);
        
        insert cruise1;
	        
	    List <Cruise__c> cruiseList = [SELECT Id, Status__c, AccountName__c FROM Cruise__c WHERE AccountName__c=: acc.Id];
	    //System.assertEquals(cruiseList[0].Status__c,'No Qualification');
	    system.debug('cruiseList.Status__c= '+ cruiseList[0].Status__c);
        
        cruise1.Boat__c = boat1.Id; 
        update cruise1;
        
        cruiseList = [SELECT Id, Status__c, AccountName__c FROM Cruise__c WHERE AccountName__c=: acc.Id];
        //System.assertEquals(cruiseList[0].Status__c,'Awaiting for Approval');
	    system.debug('cruiseList.Status__c= '+ cruiseList[0].Status__c);
        //try insert wrong boat
        /*
        try{
	        insert cruise1;
            system.debug('insert succeeded');
        } catch(Exception ex){
            system.debug('ex.getMessage()= '+ex.getMessage());
            //System.assertEquals(ex.getMessage(), label.No_Certification_for_account);
        }
        cruise1.Boat__c = boat1.Id; 
        insert cruise1;
        
        test.startTest();
        //try update wrong boat
        cruise1.Boat__c = boat2.Id; 
        try{
	        update cruise1;
            system.debug('update succeeded');
	        
        } catch(Exception ex){
            system.debug('ex.getMessage()= '+ex.getMessage());
            //system.debug('error in updating');
        }
        test.stopTest();
        */
    }
    
    /**
     * SEAT-26	: check CalculateExtraHours for vacation and weekend
	 * Author	: Shira Hai
	 *
	 * @method	: CalculateExtraHoursTest
	 * @date	: 26/11/2017
	 *;
	 * @static
	 */
    static testMethod void CalculateExtraHoursTest(){
        setup();
        
        Account acc = [SELECT Id FROM Account limit 1];
        Transaction__c tran = [SELECT Id FROM Transaction__c limit 1];
        Boats__c boat1 = [SELECT Id FROM Boats__c limit 1];
        Certification__c cer = [SELECT Id FROM Certification__c limit 1];
        
        Cruise__c cruise1 = new Cruise__c();
		cruise1.AccountName__c = acc.Id;
		cruise1.Transaction__c = tran.Id;
		cruise1.Boat__c = boat1.Id; 
		cruise1.Cruise_Type__c = 'Club'; 
		cruise1.Status__c = 'Awaiting for Approval'; 
		Date crDate = System.today().addDays(1);
        
        cruise1.Start_Date__c = DateTime.newInstance(2017, 11, 27, 13, 00, 00);
		cruise1.End_Date__c = Datetime.newInstance(2017, 12, 2, 17, 00, 00);
        
        insert cruise1;
        
        Cruise__c cruiseSel = [SELECT Id, Extra_Hours_Charge__c FROM Cruise__c limit 1];
        //system.assertEquals(cruiseSel.Extra_Hours_Charge__c, 20.5);
        
        test.startTest();
            cruise1.Holiday__c = true;
            update cruise1;
        test.stopTest();
        
        cruiseSel = [SELECT Id, Extra_Hours_Charge__c , Hidden_Total_Hours__c FROM Cruise__c limit 1];
        Double holHours = cruiseSel.Hidden_Total_Hours__c * 150/100 - cruiseSel.Hidden_Total_Hours__c;
    }
}
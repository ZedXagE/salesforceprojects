public with sharing class CruiseHandler {
    
    private hlp_triggerChanges isChangedHelper = new hlp_triggerChanges();
    private CruiseManager manager = new CruiseManager();
    private final String CLUB_RECORD_TYPE_DEV_NAME = 'Club';
    
   /* SEAT-32: before insert: in entering cruise in record type club verify the account 
    * 			has certification for the boat 
	*
	* @author	: Shira Hai
	* @date		: 23/11/17
	*
	* @method 	checkAccountHasCertificate
	* @param 	List<Cruise__c>	: Trigger newList
	*/
    public void checkAccountHasCertificate(List<Cruise__c> newList){
        Set<Id> AccIdSet = new Set<Id> ();
        List<Cruise__c> clubCruiseList = new List<Cruise__c>();
       	Map<Id,Account> cruiseToAccountMap = new Map<Id,Account>();
        //Id subscriptionRecordTypeId = Schema.SObjectType.Transaction__c.getRecordTypeInfosByName().get('Subscription').getRecordTypeId(); //schema returns hebrew keys
        RecordType clubRecordType = [SELECT Id,Name,DeveloperName 
                                     FROM RecordType 
                                     WHERE DeveloperName =: CLUB_RECORD_TYPE_DEV_NAME LIMIT 1];

    	for (Cruise__c crs : newList){
            if(crs.RecordTypeId == clubRecordType.Id){
                AccIdSet.add(crs.AccountName__c);
                clubCruiseList.add(crs);
            }
    	}
    	
    	if(AccIdSet != null && !AccIdSet.isEmpty()){
	    	Map<Id,Account> realatedAccountMap = new Map<Id,Account>(
				[Select Id, Name, 
			 	(Select Name, Boat__c From Account_Name__r) 
			 	From Account 
			 	WHERE Id in: AccIdSet]);

    		for(Cruise__c cruiseItem : clubCruiseList){
	    		Account relatedAccount = realatedAccountMap.get(cruiseItem.AccountName__c);
	    		if(relatedAccount != null){
	    			cruiseToAccountMap.put(cruiseItem.Id,relatedAccount);
	    		}
	    	}    
		    manager.checkAccountHasCertificate(clubCruiseList, cruiseToAccountMap );
    	}
    }
    
     /* SEAT-26: for insert calculation friday and saturday extra hours, if vacation - calculate all hours as extra hours
        *
        * @author	: Shira Hai
        * @date		: 26/11/17
        *
        * @method 	CalculateExtraHours
        * @param 	List<Cruise__c>	: newList
        *
        */
      public void CalculateExtraHours(List<Cruise__c> newList){
      	DateTime startDate;
      	DateTime endDate;
      	Double extraHours;
       	Double extraHolidayHours;
        
      	for (Cruise__c crs : newList){
      		startDate = crs.Start_Date__c;
      		endDate = crs.End_Date__c;
            if(crs.Holiday__c){
                extraHolidayHours = crs.Hidden_Total_Hours__c;
                extraHolidayHours = (extraHolidayHours * 150 / 100 ) - extraHolidayHours ;
                crs.Extra_Hours_Charge__c = extraHolidayHours;
            }
            else{
      			extraHours =  manager.CalculateExtraHours(startDate , endDate);
      			crs.Extra_Hours_Charge__c = extraHours;
            }
      	}
      }
      	
      /* SEAT-26: for update calculation friday and saturday extra hours, if vacation - calculate all hours as extra hours
        *
        * @author	: Shira Hai
        * @date		: 26/11/17
        *
        * @method 	CalculateExtraHours
        * @param 	List<Cruise__c>	: newList
        *
        */	
    public void CalculateExtraHours(Map <Id, Cruise__c> newMap, Map <Id, Cruise__c> oldMap){
  	  	List <Cruise__c> CruiseUpdateList = new  List <Cruise__c>();
        Set<Id> cruiseIdSet = new Set<Id>();
      	
        List<String> cruiseChange = new List<String>{'Start_Date__c', 'End_Date__c', 'Holiday__c'};
        isChangedHelper.setNewCollection('cruisChange',cruiseChange);
        if(!isChangedHelper.getCollectionChanges('cruisChange').isEmpty()){
            cruiseIdSet = isChangedHelper.getCollectionChanges('cruisChange');
        }
          
        for(Id idItem: cruiseIdSet){
    		CruiseUpdateList.add(newMap.get(idItem));
		}
          
        if(!CruiseUpdateList.isEmpty()){
             this.CalculateExtraHours(CruiseUpdateList);
        }
    }
      
    public class RecordTypeNotExistException extends Exception{}
}
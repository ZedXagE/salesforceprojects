@isTest
private class SchedulerServiceTest {
	static testMethod void createSchedulerTest() {
        
        Account acc = new Account();
        acc.Name = 'hadar test';
        acc.Login_Token__c = '123456789';
        insert acc;
        
        SchedulerService ss = new SchedulerService();
        Transaction__c tran = ss.createTransaction(null, acc.Id);
        
        List<Cruise__c> existingCruiseList = new List<Cruise__c>();
        List<Cruise__c> cList = new List<Cruise__c>();
        
        Cruise__c cruiseItem = new Cruise__c();
        cruiseItem.Start_Date__c = Datetime.newInstance(2017, 10, 16, 08, 00, 00);
        cruiseItem.End_Date__c = Datetime.newInstance(2017, 10, 16, 11, 00, 00);
        
        Cruise__c cruiseItem2 = new Cruise__c();
        cruiseItem2.Start_Date__c = Datetime.newInstance(2017, 10, 16, 13, 00, 00);
        cruiseItem2.End_Date__c = Datetime.newInstance(2017, 10, 16, 18, 00, 00);
        
        Cruise__c cruiseItem3 = new Cruise__c();
        cruiseItem3.Start_Date__c = Datetime.newInstance(2017, 10, 16, 19, 00, 00);
        cruiseItem3.End_Date__c = Datetime.newInstance(2017, 10, 16, 20, 00, 00);
        
        Cruise__c cruiseItem4 = new Cruise__c();
        cruiseItem4.Start_Date__c = Datetime.newInstance(2017, 10, 16, 21, 00, 00);
        cruiseItem4.End_Date__c = Datetime.newInstance(2017, 10, 16, 23, 00, 00);
        
        existingCruiseList.add(cruiseItem);
        existingCruiseList.add(cruiseItem2);
        existingCruiseList.add(cruiseItem3);
        existingCruiseList.add(cruiseItem4);
        
        SeatimeSchedulerWebappController.SecureAccount securAcc = SeatimeSchedulerWebappController.getSecureAccount(acc.Login_Token__c);
        Map<Id,Boats__c> idBoatsObjMap = SeatimeSchedulerWebappController.getAccountBoats(securAcc);
        
        Test.startTest();
        SchedulerService service = new SchedulerService(acc.Id, 2, idBoatsObjMap, System.today());
		//service.setTimeSlots(null,existingCruiseList, null, Datetime.newInstance(2017, 10, 16, 00, 00, 00));
        Test.stopTest();
		
    }
}
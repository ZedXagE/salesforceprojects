public class CruiseCancelled {
    public static void beforeDelete(list<Co_Cruise_Members__c>Co_Cruise_Members) {
        for (Co_Cruise_Members__c l : Co_Cruise_Members) {
            if(l.Cruise__c!=null&&l.Account_Co__c!=null){
                map<string, object> params = new map<string, object>();
                params.put('CoMemberID', l.Id);
                Flow.Interview.Delete_Co_Member flow = new Flow.Interview.Delete_Co_Member(params);
                flow.start();
            }
        }
    }
    
}
trigger MemberDeleteTrigger on Co_Cruise_Members__c (before delete) {
    if (Trigger.isDelete)
        CruiseCancelled.beforeDelete(Trigger.old);
}
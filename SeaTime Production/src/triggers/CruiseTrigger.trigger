trigger CruiseTrigger on Cruise__c (before insert, before update ,before delete , after insert , after update , after delete, after undelete) {
	
    CruiseHandler handler = new CruiseHandler();
	//before
    if (Trigger.isBefore && Trigger.isInsert){ 
        handler.checkAccountHasCertificate(Trigger.new );
        handler.CalculateExtraHours(Trigger.new );
    } 
    
    if (Trigger.isBefore && Trigger.isUpdate){ 
        handler.checkAccountHasCertificate(Trigger.new );
        handler.CalculateExtraHours(Trigger.newMap, Trigger.oldMap );
    } 
    
    if (Trigger.isBefore && Trigger.isDelete){
    } 
    
    //after 
    if (Trigger.isAfter && Trigger.isInsert){ 
    } 
    
    if (Trigger.isAfter && Trigger.isUpdate){
    } 
    
    if (Trigger.isAfter && Trigger.isDelete){
    }
    
    if (Trigger.isAfter && Trigger.isUndelete){ 
    }
	    
}
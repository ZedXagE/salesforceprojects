trigger DeleteDepositReceipt on ContentDocument (before delete) {
    Set <id> docs = new Set <id>();
    for(ContentDocument con:Trigger.Old)
        docs.add(Con.id);
    List <ContentVersion> conts = [select DepositReceipt__c from ContentVersion where ContentDocumentid in: docs];
    Set <String> dels = new Set <String>();
    for(ContentVersion cont:conts)
        if(cont.DepositReceipt__c!=null)
            dels.add(cont.DepositReceipt__c);
    if(!Test.isRunningTest())
        DepositReceiptController.deleteDepositReceipt(dels);
}
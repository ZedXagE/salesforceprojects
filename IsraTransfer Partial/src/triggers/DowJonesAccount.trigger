trigger DowJonesAccount on Account (after insert) {
    Set <String> accids = new Set <String>();
    for(Account acc:Trigger.new)
        accids.add(acc.id);
    DowJonesHandler.checkAccounts(accids);
}
trigger GenerateDepositReceipt on Transfer__c (after insert) {
    Set <Id> depids = new Set <Id>();
    for(Transfer__c tran:Trigger.New)
        if(tran.DepositReceipt__c==true)
            depids.add(tran.id);
    DepositReceiptController.insertDepositReceipt(depids);
}
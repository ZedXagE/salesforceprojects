//ITmyWay - ZedXagE	24/10/17 - issue 1274
//XE Rate Alert Check
public class RateAlertCheck {
	@future (callout=true)
	public static void XE() {
		//Initialize data
		Set<String> AllOptions = new Set<String>();
		Map <String,Set<String>> CallsNeeded = new Map <String,Set<String>>();
		Map <String,Decimal> RatesNeeded = new Map <String,Decimal>();
		List <Rate_Alerts__c> RateAlerts = [select id,From__c,To__c,Rate__c,Match_Found__c from Rate_Alerts__c where Match_Found__c=FALSE];

		if(RateAlerts.size()>0){
			//add all needed calls to map to make each FROM currency to all his TO currencies
			for(Rate_Alerts__c Rate:RateAlerts){
				AllOptions.add(Rate.From__c+','+Rate.To__c);
				AllOptions.add(Rate.To__c+','+Rate.From__c);
			}
			System.debug(AllOptions);

			//Generate minimum calls
			GenerateCalls(AllOptions,CallsNeeded);
			System.debug(CallsNeeded);

			/*Map <String, List <String>> wanted = new Map <String, List <String>>();
			//Make call for each From in Callsneeded
			for(String fromRate:CallsNeeded.keySet())
            	wanted.put(fromRate,new List <String> (CallsNeeded.get(fromRate)));

			Map <String,List <PortalXE.XEResult>> results = PortalXE.getRates(wanted,true,null);
        	for(String fromRate:results.keySet()){
				for(PortalXE.XEResult result:results.get(fromRate)){
					RatesNeeded.put(fromRate+','+result.quotecurrency,result.mid);
					RatesNeeded.put(result.quotecurrency+','+fromRate,1/result.mid);
				}
			}*/
			//Make call for each From in Callsneeded
			for(String fromRate:CallsNeeded.keySet()){
				if(!Test.isRunningTest()){
					List <PortalXE.XEResult> results = PortalXE.getRates(fromRate,new List <String>(CallsNeeded.get(fromRate)));
					for(PortalXE.XEResult result:results){
						RatesNeeded.put(fromRate+','+result.quotecurrency,result.mid);
						RatesNeeded.put(result.quotecurrency+','+fromRate,1/result.mid);
					}
				}
			}

			//Update the needed Rate
			rateAlerts4up(RateAlerts,RatesNeeded);
		}
	}
	public static void GenerateCalls(Set <String> AllOptions,Map <String,Set<String>> CallsNeeded) {
		for(String Option:AllOptions){
			String Fro=Option.split(',')[0];
			String To=Option.split(',')[1];
			if(CallsNeeded.containsKey(Fro))
				CallsNeeded.get(Fro).add(To);
			else
				if(CallsNeeded.containsKey(To))
					CallsNeeded.get(To).add(Fro);
				else
					CallsNeeded.put(Fro,new Set <String>{To});
		}
	}
	//Update the needed Rate
	public static void rateAlerts4up(List <Rate_Alerts__c> RateAlerts,Map <String,Decimal> RatesNeeded) {
		List <Rate_Alerts__c> RateAlerts4Up = new List <Rate_Alerts__c>();
		for(Rate_Alerts__c Rate:RateAlerts){
			String CheckRate=Rate.From__c;
			CheckRate+=',';
			CheckRate+=Rate.To__c;
			if(!Test.isRunningTest()){
				if(Rate.Rate__c<=RatesNeeded.get(CheckRate)){
					Rate.Match_Found__c=TRUE;
					RateAlerts4Up.add(Rate);
				}
			}
		}
		update RateAlerts4Up;
	}
}
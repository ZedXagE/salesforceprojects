@isTest
public class DowJonesTest {
	static testMethod void tes() {
		insert new DJ_Settings__c(Name='test',Username__c='test',Password__c='test',Namespace__c='1',Endpoint__c='https://djrc.api.dowjones.com/v1/search/name');
		Account acc = new Account(name='Muhamad');
		insert acc;
		Deposit_Account__c da = new Deposit_Account__c(Account__c=acc.Id, Name='Deposite Account USD', Currency__c='USD');
        insert da;
		insert new DepositReceipt__c(name='r-1');
		insert new DepositReceipt__c(name='r-2');
		Transfer__c t = new Transfer__c(Account__c=acc.Id, Deposit_Account__c=da.Id, Amount__c=1000, Type__c = 'Deposit',Status__c='Completed');
        Transfer__c t2 = new Transfer__c(Account__c=acc.Id, Deposit_Account__c=da.Id, Amount__c=1000, Type__c = 'Deposit',Status__c='Pending');
        insert t;
        insert t2;
		ApexPages.StandardController con = new ApexPages.StandardController(acc);
		DowJonesAccount dja = new DowJonesAccount(con);
		dja.curid=acc.id;
		dja.init();
	}
}
//edit by ZedXagE-ITmyWay 03/12/2017 - 1436
@isTest(seealldata=true)
public class TestRateAlertCheck {
	static testMethod void Test() {
        Rate_Alerts__c rt=new Rate_Alerts__c(Match_Found__c=FALSE,From__c='USD',To__c='GBP');
        insert rt;
		ScheduleRateAlertCheck sch=new ScheduleRateAlertCheck();
		sch.execute(null);
        TransactionsPDFController ts=new TransactionsPDFController();
        Deposit_Account__c da=[select id from Deposit_Account__c limit 1];
        Transaction__c tr1=new Transaction__c(Deposit_Account__c=da.id,Type_of_Transaction__c='Deposit',Amount__c=1);
        Transaction__c tr2=new Transaction__c(Deposit_Account__c=da.id,Type_of_Transaction__c='Withdrawal',Amount__c=1);
        insert tr1;
        insert tr2;
        TradeActivationON tn=new TradeActivationON();
		tn.execute(null);
        TradeActivationOFF tf=new TradeActivationOFF();
		tf.execute(null);
	}
}
public class DepositReceiptController {
	public String recnum{get;set;}
	public DepositReceiptController(ApexPages.StandardController stdController) {
		recnum = ApexPages.currentPage().getParameters().get('num');
	}
	@future(callout=true)
	public static void insertDepositReceipt(Set <id> trans){
		List <DepositReceipt__c> dels = new List <DepositReceipt__c>();
		List <DepositReceipt__c> unused = new List <DepositReceipt__c>();
		DepositReceipt__c lastRec = new DepositReceipt__c();
		for(DepositReceipt__c dep:[select id,Name,Number__c,Last__c from DepositReceipt__c order by Number__c])
			if(dep.Last__c)
				lastrec=dep;
			else
				unused.add(dep);
		Map <String ,ContentVersion> newRec = new Map <String ,ContentVersion>();
		Map <String, Id> recNums = new Map <String, Id>();
		Map <Id, Decimal> trannums = new Map <Id, Decimal>();
		List <ContentDocumentLink> contlinks = new List <ContentDocumentLink>();
		Decimal lastnum = lastRec.Number__c;
		for(id tran:trans){
			String recnum;
			if(unused.size()>0){
				trannums.put(tran,Integer.valueof(unused[0].Name.split('-')[1]));
				recnum=unused[0].Name;
				dels.add(unused[0]);
				unused.remove(0);
			}
			else{
				lastnum++;
				trannums.put(tran,lastnum);
				recnum='DR-'+String.valueof(lastnum);
			}
			newRec.put(recnum,doc(tran,recnum));
			recNums.put(recnum,tran);
		}
		insert newRec.values();
		lastRec.Number__c = lastnum;
		lastRec.Name = 'DR-'+String.valueof(lastnum);
		update lastRec;
		List <ContentVersion> contvers = [select id,ContentDocumentId,DepositReceipt__c from ContentVersion where DepositReceipt__c in: newRec.KeySet()];
		for(ContentVersion cont:contvers){
			ContentDocumentLink cDe = new ContentDocumentLink();
			cDe.ContentDocumentId = cont.ContentDocumentId;
			cDe.LinkedEntityId = recNums.get(cont.DepositReceipt__c); // you can use objectId,GroupId etc
			cDe.ShareType = 'I'; // Inferred permission, checkout description of ContentDocumentLink object for more details
			cDe.Visibility = 'InternalUsers';
			contlinks.add(cDe);
		}
		insert contlinks;
		List <Transfer__c> trans4up = [select id,Receipt_Number__c from Transfer__c Where id in: trannums.keySet()];
		for(Transfer__c tran:trans4up)
			tran.Receipt_Number__c = trannums.get(tran.id);
		Database.update(trans4up,false);
		delete dels;
		if(Test.isRunningTest()){
			delete new ContentDocument(id=contvers[0].ContentDocumentId);
		}
	}
	public static ContentVersion doc(Id tranid,String recnum){
		PageReference pdf=Page.DepositReceipt;
		pdf.getParameters().put('id', tranid);
		pdf.getParameters().put('num', recnum);
		Blob body;
        if(!Test.isRunningTest())
            body = pdf.getContent();
        else
            body = blob.valueof('test');
		ContentVersion v = new ContentVersion();
        v.versionData = body;
		v.DepositReceipt__c = recnum;
        v.title = recnum;
        v.pathOnClient = '/'+recnum+'.pdf';
        return v;
	}
	@future
	public static void deleteDepositReceipt(Set <String> conts){
		List <DepositReceipt__c> dels = new List <DepositReceipt__c>();
		for(String cont:conts)
			dels.add(new DepositReceipt__c(Name=cont,Number__c=Integer.valueof(cont.split('-')[1])));
		insert dels;
	}
}
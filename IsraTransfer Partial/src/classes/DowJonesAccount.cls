public class DowJonesAccount {
	public string curid;
	public DowJonesAccount(ApexPages.StandardController con) {
		if(ApexPages.currentPage()!=null&&ApexPages.currentPage().getUrl()!=null)
			curid=ApexPages.currentPage().getParameters().get('id');
	}
	public PageReference init(){
		if(curid!=null&&curid!=''){
			Account acc = [select id,FirstName,LastName,Dow_Jones__c,DowJones_Last_Check__c from Account where id=:curid];
			DowJonesHandler dj = new DowJonesHandler();
			String name;
			if(acc.FirstName!=null)	name = acc.FirstName+' '+acc.LastName;
			else	name = acc.LastName;
			dj.Params.put('name',name);
			HttpResponse res = dj.send();
			Boolean prob = DowJonesHandler.condition(res);
			acc.Dow_Jones__c = prob;
			acc.DowJones_Last_Check__c = System.Now();
			update acc;
			insert new Dow_Jones_Log__c(Name__c=name,Dow_Jones_Suspicious__c=acc.Dow_Jones__c);
			PageReference pgref = new PageReference('/'+curid);
			pgref.setRedirect(true);
			return pgref;
		}
		return null;
	}
}
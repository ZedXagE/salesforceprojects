global class CurrencyCloudInsert implements Queueable  {
	//public List <CurrencyCloud_Rate__c> cur4ins;
	public List <String> errs;
	global CurrencyCloudInsert(/*List <CurrencyCloud_Rate__c> rates, */List <String> msgs) {
		//cur4ins = rates;
		errs = msgs;
	}
	global void execute(QueueableContext sc) {
		/*try{	insert cur4ins;	}
		catch(exception e){	errs.add(e.getMessage());}*/
		ErrorHandle(errs);
   	}
	public static void ErrorHandle(List <String> msgs){
		String msg='';
		for(String ms:msgs)
			msg+=ms+'.\n\n';
		if(msg!=''){
			try{
				Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
				message.toAddresses = new String[] { 'edan@itmyway.com', 'yotam@itmyway.com' };
				message.subject = 'CurrencyCloud Request Error';
				message.plainTextBody = msg;
				Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
				Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
			}
			catch(exception e){}
		}
    }
}
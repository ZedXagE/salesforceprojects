/*ITmyWay - ZedXagE	24/10/17 - issue 1274
checks rates from xe and check wich rate alert should be alerted

to make it every 30 min run in dev console both lines:

System.schedule('Schedule Rate Alert Check 0', '0 0 * * * ?', new ScheduleRateAlertCheck());
System.schedule('Schedule Rate Alert Check 30', '0 30 * * * ?', new ScheduleRateAlertCheck());
*/
global class ScheduleRateAlertCheck implements Schedulable {
	global void execute(SchedulableContext sc) {
		RateAlertCheck.XE();
   	}
}
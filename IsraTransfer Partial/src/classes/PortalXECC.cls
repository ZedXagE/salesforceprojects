public class PortalXECC {
    // xe's json result format
    public class XEResult {
        public String quotecurrency;
        public Decimal mid;
    }

    private class XERoot {
        public List<XEResult> to;
    }

    public class currencycloud{
        public String client_buy_currency;
        public String mid_market_rate;
    }

    public class currencycloudOauth{
        public String auth_token;
    }

    public static Decimal getRate(String fromCurrency, String toCurrency) { return getRate(fromCurrency, toCurrency, true, null); }
    public static Decimal getRate(String fromCurrency, String toCurrency, Boolean isLive) { return getRate(fromCurrency, toCurrency, isLive, null); }
    public static Decimal getRate(String fromCurrency, String toCurrency, Boolean isLive, Date dDate) {
		if (!isLive) {
			Map<String, Decimal> fromToUSD = new Map<String, Decimal>{
                'EUR' => 1.220,
                'CHF' => 1.036,
                'CAD' => 0.803,
                'AUD' => 0.796,
                'GBP' => 1.377,
                'ZAR' => 0.081,
                'DKK' => 0.163,
                'NZD' => 0.726,
                'NIS' => 0.289
			};
			if (fromCurrency == 'USD' || toCurrency == 'USD')
				return fromCurrency == 'USD' ? 1 / fromToUSD.get(toCurrency) : fromToUSD.get(fromCurrency);
			else
				return fromToUSD.get(fromCurrency) / fromToUSD.get(toCurrency);
		}
        return getRates(new Map<String,List<String>>{fromCurrency => new List<String>{ toCurrency }}, isLive, dDate).get(fromCurrency)[0].mid;
    }

    public static List<XEResult> getRates(String fromCurrency, List<String> toCurrencies) { return getRates(new Map<String,List<String>>{fromCurrency => toCurrencies}, true, null).get(fromCurrency); }
    public static List<XEResult> getRates(String fromCurrency, List<String> toCurrencies, Boolean isLive) { return getRates(new Map<String,List<String>>{fromCurrency => toCurrencies}, isLive, null).get(fromCurrency);}
    public static Map<String,List<XEResult>> getRates(Map<String, List<String>> wanted, Boolean isLive, Date dDate) {
        Map <String,List<XEResult>> rates = new Map <String,List<XEResult>>();
        /*if(wanted.keyset().size()==1&&wanted.get(new List <String>(wanted.keyset())[0]).size()==1){
            String frc = new List <String>(wanted.keyset())[0];
            String toc = wanted.get(new List <String>(wanted.keyset())[0])[0];
            Datetime timeout = System.now();
            Integer timeoutSearch = (-1)*Integer.valueof(Label.ScheduledSearchTimeOut);
            timeout = timeout.addHours(timeoutSearch);
            List <CurrencyCloud_Rate__c> curs = [select Mid_Rate__c,From_Currency__c,To_Currency__c from CurrencyCloud_Rate__c where From_Currency__c=:frc and To_Currency__c=:toc and CreatedDate>=:timeout Order By CreatedDate DESC];
            if(curs.size()>0){
                XEResult xer = new XEResult();
                xer.quotecurrency =  curs[0].To_Currency__c;
                xer.mid = curs[0].Mid_Rate__c;
                rates.put(frc, new List <XEResult>{xer});
                return rates;
            }
        }*/
        //List <CurrencyCloud_Rate__c> cur4ins = new List <CurrencyCloud_Rate__c>();
        List <String> ErrorMsgs = new List <String>();
        String loginname = Label.Currency_Settings_name;
        Xe_Login__c xeLogin = [SELECT Username__c, Password__c FROM Xe_Login__c where name=:loginname LIMIT 1];
        String oauth = 'https://api.currencycloud.com/v2/authenticate/api';
        String token;
        //TOKEN
        try{
            HttpRequest reqoa = new HttpRequest();
            reqoa.setTimeOut(120000);
            reqoa.setEndpoint(oauth);
            reqoa.setMethod('POST');
            reqoa.setHeader('Content-Type','multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW');
            String data = '------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"login_id\"\r\n\r\n'+xeLogin.Username__c+'\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"api_key\"\r\n\r\n'+xeLogin.Password__c+'\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--';
            System.debug('Oauth body: '+data);
            reqoa.setbody(data);
            HTTPResponse resoa;
            if(!Test.isRunningTest())
                resoa = new Http().send(reqoa);
            else{
                resoa = new HTTPResponse();
                resoa.setStatusCode(200);
                currencycloudOauth cro = new currencycloudOauth();
                cro.auth_token = 'test';
                resoa.setBody(JSON.Serialize(cro));
            }
            if(resoa!=null){
                System.debug('Oauth res: '+resoa);
                if(resoa.getStatusCode()==200){
                    try{
                        String oauthbody = resoa.getBody();
                        currencycloudOauth oa = (currencycloudOauth) JSON.deserialize(oauthbody, currencycloudOauth.class);
                        token = oa.auth_token;
                        if(token==null) ErrorMsgs.add('Token is NULL');
                    }
                    catch(exception e2){ ErrorMsgs.add('Serialize Error: '+e2.getMessage());    }
                }
                else{   ErrorMsgs.add('Response for Token is not in Status OK');    }
            }
            else{   ErrorMsgs.add('Response for Token NULL'); }
        }
        catch(exception e){ ErrorMsgs.add(e.getMessage());    }
        System.debug('Token: '+token);

        for(String fromC:wanted.keySet()){
            String fromCurrency = fromC;
            List <String> toCurrencies = wanted.get(fromC);
            rates.put(fromC,new List<XEResult>());
            if (!isLive) {
                for (String c : toCurrencies) {
                    XEResult r = new XEResult();
                    r.quotecurrency = c;
                    r.mid = getRate(fromCurrency, c, isLive, dDate);
                    rates.get(fromCurrency).add(r);
                }
                return rates;
            }

            XERoot root = new XERoot();
            root.to = new List<XEResult>();            

            //REQUESTS
            if(token != null){
                fromCurrency = fromCurrency == 'NIS' ? 'ILS' : fromCurrency;
                for (String toCurrency : toCurrencies){
                    toCurrency = (toCurrency == 'NIS' ? 'ILS' : toCurrency);
                    String endpoint = 'https://api.currencycloud.com/v2/rates/detailed?sell_currency=' + fromCurrency + '&buy_currency=' + toCurrency + '&fixed_side=sell&amount=1';
                    try{
                        HttpRequest req = new HttpRequest();
                        req.setTimeOut(120000);
                        req.setEndpoint(endpoint);
                        req.setMethod('GET');
                        req.setHeader('X-Auth-Token', token);
                        HTTPResponse res;
                        if(!Test.isRunningTest())
                            res = new Http().send(req);
                        else{
                            res = new HTTPResponse();
                            res.setStatusCode(200);
                            currencycloud cr = new currencycloud();
                            cr.client_buy_currency = toCurrency;
                            cr.mid_market_rate = '1';
                            res.setBody(JSON.Serialize(cr));
                        }
                        if(res!=null){
                            System.debug('Response cur: '+res);
                            if(res.getStatusCode()==200){
                                try{
                                    String body = res.getBody();
                                    System.debug(body);
                                    currencycloud curres = (currencycloud) JSON.deserialize(body, currencycloud.class);
                                    XEResult xer = new XEResult();
                                    xer.quotecurrency = curres.client_buy_currency == 'ILS' ? 'NIS' : curres.client_buy_currency;
                                    xer.mid = Decimal.valueof(curres.mid_market_rate);
                                    root.to.add(xer);
                                }
                                catch(exception e2){    System.debug('Serialize Error: '+fromCurrency+' -> '+toCurrency+' '+e2.getMessage());  }
                            }
                            else{  ErrorMsgs.add('Response for '+fromCurrency+' -> '+toCurrency+' is not in Status OK');  }
                        }
                        else{   ErrorMsgs.add('Response for '+fromCurrency+' -> '+toCurrency+' is NULL');    }
                    }
                    catch(exception e){ ErrorMsgs.add('Error get Currency: '+fromCurrency+' -> '+toCurrency+' '+e.getMessage());    }
                }
            }

            //Insert currency log
            /*for(XEResult xer:root.to){
                if(xer.mid!=null&&xer.mid!=0){
                    cur4ins.add(getRec(fromC,xer.quotecurrency,xer.mid));
                    cur4ins.add(getRec(xer.quotecurrency,fromC,1/xer.mid));
                }
            }*/
            rates.get(fromC).addAll(root.to);
        }
        if(!Test.isRunningTest())
            ID jobID = System.enqueueJob(new CurrencyCloudInsert(/*cur4ins,*/ErrorMsgs));
        return rates;
    }
    
    /*public static CurrencyCloud_Rate__c getRec(String FromC, String ToC, Decimal mid){
        return new CurrencyCloud_Rate__c(From_Currency__c=Fromc,To_Currency__c=ToC,Mid_Rate__c=mid);
    }*/
    /*public static void CurrenciesToCheck(){
        Integer timeoutSearch = (-1)*Integer.valueof(Label.ScheduledSearchTimeOut);
        Integer timeoutSuccess = Integer.valueof(Label.ScheduledSuccessTimeOut);
        Integer timeoutFailed = Integer.valueof(Label.ScheduledFailedTimeOut);
        DateTime nowtime = System.Now();
        //Initialize data
        Set<String> AllOptions = new Set<String>();
		Map <String,Set<String>> CallsNeeded = new Map <String,Set<String>>();
		List <CurrencyCloud_Rate__c> CurrentRates = [select From_Currency__c,To_Currency__c,Mid_Rate__c from CurrencyCloud_Rate__c where CreatedDate>=:nowtime.addHours(timeoutSearch).addMinutes(5)];
        String [] currencies = Label.ScheduledCurrencies.split(',');

        for(integer i=0;i<currencies.size();i++){
            for(integer j=0;j<currencies.size();j++)
                if(j!=i)
                    AllOptions.add(currencies[i]+','+currencies[j]);
        }
        Boolean Success = true;

		if(CurrentRates.size()>0)
			for(CurrencyCloud_Rate__c Rate:CurrentRates)
                if(AllOptions.contains(Rate.From_Currency__c+','+Rate.To_Currency__c))
				    AllOptions.remove(Rate.From_Currency__c+','+Rate.To_Currency__c);

        //Generate minimum calls
        GenerateCalls(AllOptions,CallsNeeded);
        System.debug(CallsNeeded);
        Map <String, List <String>> wanted = new Map <String, List <String>>();
        //Make call for each From in Callsneeded
        for(String fromRate:CallsNeeded.keySet())
            wanted.put(fromRate,new List <String> (CallsNeeded.get(fromRate)));

        Map <String,List <XEResult>> results = PortalXE.getRates(wanted,true,null);

        for(String fromRate:wanted.keySet())
            if(results.get(fromRate).Size()!=wanted.get(fromRate).size())    Success=false;

        integer nextrun = Success==true?timeoutSuccess:timeoutFailed;
        System.debug(nowtime);
        nowtime = nowtime.addHours(nextrun);
        System.debug(nowtime+' '+nowtime.hour());
        String sch = nowtime.second()+' '+nowtime.minute()+' '+nowtime.hour()+' '+nowtime.day()+' '+nowtime.month()+' ? '+nowtime.year();

        for(CronTrigger ct:[SELECT Id,CronJobDetail.Name FROM CronTrigger where CronJobDetail.Name='Next Run Currencies'])
            System.abortJob(ct.Id);

        String jobID = System.schedule('Next Run Currencies', sch, new CurrencyCloudScheduler());
    }
    public static void GenerateCalls(Set <String> AllOptions,Map <String,Set<String>> CallsNeeded) {
		for(String Option:AllOptions){
			String Fro=Option.split(',')[0];
			String To=Option.split(',')[1];
			if(CallsNeeded.containsKey(Fro))
				CallsNeeded.get(Fro).add(To);
			else
				if(CallsNeeded.containsKey(To))
					CallsNeeded.get(To).add(Fro);
				else
					CallsNeeded.put(Fro,new Set <String>{To});
		}
	}*/
}
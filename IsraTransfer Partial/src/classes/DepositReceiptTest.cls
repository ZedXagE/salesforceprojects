@isTest(seealldata=true)
public class DepositReceiptTest {
	static TestMethod void tes() {
		Transfer__c tran = [select id,Account__c,Deposit_Account__c from Transfer__c where Type__c='Deposit' order by CreatedDate DESC limit 1];
		insert new Transfer__c(Type__c='Deposit',Account__c=tran.Account__c,Deposit_Account__c=tran.Deposit_Account__c,amount__c=100,Transfer_Date__c=System.Today(),Receipt_Number__c=1);
		DepositReceiptController.insertDepositReceipt(new Set <id>{tran.id});
		DepositReceiptController.deleteDepositReceipt(new Set <String>{'DR-1'});
	}
}
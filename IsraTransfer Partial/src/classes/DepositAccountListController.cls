public with sharing class DepositAccountListController {
    public String searchAccountName {get; set;}
    public String searchCurrency {get; set;}
    
    public ApexPages.StandardSetController setCon {
        get {
            if(setCon == null) {
                getAll();
            }
            return setCon;
        }
        set;
    }

    // Initialize setCon and return a list of records
    public List<Deposit_Account__c> getDepositAccounts() {
         return (List<Deposit_Account__c>) setCon.getRecords();
    }
    
    public List<SelectOption> getCurrencies() {
        Schema.DescribeFieldResult F = Deposit_Account__c.Currency__c.getDescribe();
        List<Schema.PicklistEntry> currencyList = F.getPicklistValues();
        
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('',''));
        
        for(Schema.PicklistEntry p : currencyList){
            options.add(new SelectOption(p.getValue(),p.getLabel()));
        }
         return options;
    }
    
    public DepositAccountListController() {
    }
    
    public void doSearch(){
        String query = 'SELECT Subtractions__c, Name, Description__c, Currency__c, Balance__c, Additions__c, Account__c, Account__r.Name FROM Deposit_Account__c ';
        String whereStatment = '';
        if(searchAccountName != null && searchAccountName != '')
            whereStatment += 'Account__r.Name LIKE \'%' + String.escapeSingleQuotes(searchAccountName) + '%\' ';
        if(searchCurrency != null && searchCurrency != ''){
            if(whereStatment != '') whereStatment += 'AND ';
            whereStatment += 'Currency__c LIKE \'%' + String.escapeSingleQuotes(searchCurrency) + '%\' ';
        }
        if(whereStatment != '')
            query += 'WHERE ' + whereStatment;
            
        query += 'ORDER BY LastModifiedDate DESC';
            
        system.debug('>>>>>' +query);
        setCon = new ApexPages.StandardSetController(Database.getQueryLocator(query));
    }
    
    public void getAll()
    {
        setCon = new ApexPages.StandardSetController(Database.getQueryLocator(
                      [SELECT Subtractions__c, Name, Description__c, Currency__c, Balance__c, Additions__c, Account__c, Account__r.Name
                       FROM Deposit_Account__c
                       ORDER BY LastModifiedDate DESC]));
    }
    
    public PageReference save(){
        setCon.save();
    
        return null;
    /*  PageReference result=ApexPages.currentPage();
        result.setRedirect(true);
          
        return result;*/
    }
   
    public PageReference create(){
        PageReference acctPage = new PageReference('/a08/e');
    //  acctPage.setRedirect(true);
        return acctPage;
 
    }
    
    static testMethod void testDepositAccountListController() {
        Account a = new Account(Name='Test Account', Email__c='test@test.com', Passport__c='11111111', Phone='555 555', Type='Other');
        insert a;
        
        List<Deposit_Account__c> daList = new List<Deposit_Account__c>();
        Deposit_Account__c da = new Deposit_Account__c(Account__c=a.Id, Name='Test Account USD', Currency__c='USD');
        daList.add(da);
        da = new Deposit_Account__c(Account__c=a.Id, Name='Test Account EUR', Currency__c='EUR');
        daList.add(da);
        insert daList;
        
        DepositAccountListController cont = new DepositAccountListController();
        Test.startTest();
            cont.getDepositAccounts();
            
            cont.searchAccountName = 'Test';
            cont.searchCurrency = 'USD';
            
            cont.doSearch();
            system.assertEquals(true, cont.getDepositAccounts().size() >= 1);
            
            cont.create();
            
            cont.getCurrencies();
        Test.stopTest();        
    }
}
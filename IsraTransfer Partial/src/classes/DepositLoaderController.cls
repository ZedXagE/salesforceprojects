/**
 * Created by nkarmi on 8/19/2018.
 */

public with sharing class DepositLoaderController {
    /**
     *
     *
     * @param accountId
     * @param swiftData
     * @param tofesData [0: Account Number, 1: Takboul Number, 2: Amount, 3: Currency]
     *
     * @return
     */
    @AuraEnabled
    public static Map<String, String> saveDeposit(String accountId, String swiftData, String[] tofesData) {
        String[] userInfo = swiftData.split('\n');
        userInfo.remove(0);//Remove Account Id
        if(userInfo.size() > 3){//Handle long name issue
            userInfo[0] += ' ' + userInfo[1];
            userInfo.remove(1);
        }
        List<Transfer__c> lstTransfers = [SELECT Id FROM Transfer__c WHERE Takbul_Num__c = :tofesData[1].trim()];
        if(lstTransfers.size() > 0){
            return generateResponse('fail', Label.Duplicate_Takbul);
        }
        List<Bank_Details__c> bankDetails = [SELECT Id FROM Bank_Details__c WHERE Name = :userInfo[0].trim() + ' Sender Bank' AND Account__c = :accountId];
        Account objAccount = [SELECT Id, Name FROM Account WHERE Id = :accountId];
        List<Currency_To_Country__mdt> lstCurrencyToCountries = [SELECT Country__c FROM Currency_To_Country__mdt WHERE DeveloperName = :tofesData[3]];
        try {
            Bank_Details__c objBankDetails;
            if (bankDetails.size() > 0) {
                objBankDetails = bankDetails[0];
            } else {
                /**
                 * Create Bank Detail Info
                 */
                objBankDetails = new Bank_Details__c();
                objBankDetails.Account__c = accountId;
                objBankDetails.Name = userInfo[0].trim() + ' Sender Bank';
                String payee = userInfo[0].trim();
                if(payee.length() > 40){//Only 40 character, because the limit is 40 for Payee field
                    payee = payee.substring(0, 39);
                }
                objBankDetails.Payee__c = payee;
                objBankDetails.Sender_Recevier__c = 'Sender';
                objBankDetails.Hide__c = true;
                objBankDetails.Street_Address_Of_Receiver__c = userInfo[1].trim() + ' ' + (userInfo.size() > 2 ? userInfo[2].trim() : '');
                objBankDetails.Country__c = lstCurrencyToCountries[0].Country__c;
                objBankDetails.LoadedFROMTofes__c = true;
                insert objBankDetails;
            }
            String depositAccountName = objAccount.Name + ' - ' + tofesData[3].trim();
            List<Deposit_Account__c> depositAccounts = [SELECT Id, Name,Currency__c FROM Deposit_Account__c WHERE Name = :depositAccountName];

            Deposit_Account__c objDepositAccount;
            if (depositAccounts.size() > 0) {
                objDepositAccount = depositAccounts[0];
            } else {
                /**
                 * Insert Deposit Account
                 */
                objDepositAccount = new Deposit_Account__c();
                objDepositAccount.Name = depositAccountName;
                objDepositAccount.Currency__c = tofesData[3];
                objDepositAccount.Account__c = accountId;
                if (tofesData[0].trim() == Label.AACI_Account_Id) {
                    objDepositAccount.AACI__c = true;
                }
                insert objDepositAccount;
            }
            /**
             * Create Transfer
             */
            Transfer__c objTransfer = new Transfer__c();
            objTransfer.Account__c = accountId;
            objTransfer.Deposit_Account__c = objDepositAccount.Id;
            objTransfer.Type__c = 'Deposit';
            objTransfer.Amount__c = Double.valueOf(tofesData[2].trim().replace(',', ''));
            objTransfer.Status__c = 'Pending';
            if (tofesData[0].trim() == Label.AACI_Account_Id) {
                objTransfer.AACI__c = true;
            }
            objTransfer.Takbul_Num__c = tofesData[1].trim();
            objTransfer.Sender_Bank_Detail__c = objBankDetails.Id;
            objTransfer.Transfer_Date__c = System.today();
            objTransfer.Transfer_Stage__c = 'Received < $50 K';
            insert objTransfer;
        } catch (Exception ex) {
            return generateResponse('fail', Label.Issue_with_parsing_files);
        }

        return generateResponse('success', Label.Success_Message);
    }

    private static Map<String,String> generateResponse(String status, String message){
        Map<String,String> response = new Map<String, String>();
        response.put('status', status);
        response.put('message', message);
        return response;
    }
}
public class DowJonesHandler {
	public static Integer Calloutlim=100;
	public String Username;
	public String Password;
	public String Namespace;
	public String Auth;
	public String Endpoint;
	public Map <String,String> Params;
	public DowJonesHandler() {
		List <DJ_Settings__c> djs = [select Username__c,Password__c,Namespace__c,EndPoint__c from DJ_Settings__c];
		if(djs.size()>0){
			Username = djs[0].Username__c;
			Password = djs[0].Password__c;
			Namespace = djs[0].Namespace__c;
			Endpoint = djs[0].EndPoint__c;
			Auth = genOauth(Username,Password,Namespace);
		}
		Params = new Map <String,String>();
	}
	public static String genOauth(String username, String password, String namespace){
		String myString = namespace+'/'+username+':'+password;
		Blob myBlob = Blob.valueof(myString);
		return EncodingUtil.base64Encode(myBlob);
	}
	public HttpResponse send(){
		String data = '';
		for(String k:Params.KeySet()){
			if(data!='')	data += '&';
			if(k!=null&&Params.get(k)!=null)
				data += EncodingUtil.UrlEncode(k,'UTF-8')+'='+EncodingUtil.UrlEncode(Params.get(k),'UTF-8');
		}
		return zedHTTP(Endpoint,'GET',data,Auth);
	}
	public static HttpResponse zedHTTP(String endpoint,String method,String data,String auth){
		Http http=new Http();
		HttpRequest req=new HttpRequest();
        req.setTimeout(60000);
		req.setendpoint(endpoint+'?'+data);
		req.setmethod(method);
		if(auth!=null)	req.setHeader('Authorization', 'Basic '+auth);
		HttpResponse res = new HttpResponse();
		if(!Test.isRunningTest())	res = http.send(req);
		System.debug('Endpoint+Params: '+endpoint+'?'+data);
		System.debug('Method: '+method);
		System.debug('Auth: '+auth);
		if(res!=null)
			System.debug('Response: '+res.getBody());
		return res;
	}
	@future(callout=true)
	public static void checkAccounts(Set <String> accids){
		Map <String,Account> accs = new Map <String,Account>([select id,FirstName,LastName,Dow_Jones__c,DowJones_Last_Check__c from Account where id in:accids]);
		if(accs.size()>0){
			DowJonesHandler dj = new DowJonesHandler();
			Set <String> dones = new Set <String>();
			List <Account> accs4up = new List <Account>();
			List <Dow_Jones_Log__c> djlogs = new List <Dow_Jones_Log__c>();
			Boolean anotherOne = false;
			integer i = 0;
			for(String k:accs.KeySet()){
				if(i<Calloutlim){
					dj.Params.clear();
					String name;
					if(accs.get(k).FirstName!=null)	name = accs.get(k).FirstName+' '+accs.get(k).LastName;
					else	name = accs.get(k).LastName;
					dj.Params.put('name',name);
					HttpResponse res = dj.send();
					accs.get(k).DowJones_Last_Check__c = System.Now();
					if(condition(res))
						accs.get(k).Dow_Jones__c = true;
					else
						accs.get(k).Dow_Jones__c = false;
					accs4up.add(accs.get(k));
					djlogs.add(new Dow_Jones_Log__c(Name__c=name,Dow_Jones_Suspicious__c=accs.get(k).Dow_Jones__c));
					i++;
					dones.add(k);
				}
				else {
					anotherOne = true;
					break;
				}
			}
			update accs4up;
			insert djlogs;
			if(anotherOne){
				for(String k:dones)
					if(accs.containsKey(k))
						accs.remove(k);
				checkAccounts(accs.keySet());
			}
		}
	}
	@future(callout=true)
	public static void checkTransfers(Set <String> tranids){
		Map <String,Transfer__c> trans = new Map <String,Transfer__c>([select id,Bank_Details_Payee__c,DowJones_Suspicious__c,DowJones_Last_Check__c,Account__c,Account__r.FirstName,Account__r.LastName,Account__r.Dow_Jones__c,Account__r.DowJones_Last_Check__c from Transfer__c where id in:tranids]);
		if(trans.size()>0){
			DowJonesHandler dj = new DowJonesHandler();
			Set <String> dones = new Set <String>();
			List <Account> accs4up = new List <Account>();
			List <Transfer__c> trans4up = new List <Transfer__c>();
			List <Dow_Jones_Log__c> djlogs = new List <Dow_Jones_Log__c>();
			Boolean anotherOne = false;
			integer i = 0;
			for(String k:trans.KeySet()){
				if(i<Calloutlim-1){
					if(trans.get(k).Account__c!=null){
						dj.Params.clear();
						String name;
						if(trans.get(k).Account__r.FirstName!=null)	name = trans.get(k).Account__r.FirstName+' '+trans.get(k).Account__r.LastName;
						else	name = trans.get(k).Account__r.LastName;
						dj.Params.put('name',name);
						HttpResponse res = dj.send();
						trans.get(k).Account__r.DowJones_Last_Check__c = System.Now();
						if(condition(res))
							trans.get(k).Account__r.Dow_Jones__c = true;
						else
							trans.get(k).Account__r.Dow_Jones__c = false;
						accs4up.add(new Account(id=trans.get(k).Account__c,Dow_Jones__c=trans.get(k).Account__r.Dow_Jones__c,DowJones_Last_Check__c=trans.get(k).Account__r.DowJones_Last_Check__c));
						djlogs.add(new Dow_Jones_Log__c(Name__c=name,Dow_Jones_Suspicious__c=trans.get(k).Account__r.Dow_Jones__c));
						i++;
					}
					if(trans.get(k).Bank_Details_Payee__c!=null){
						dj.Params.clear();
						dj.Params.put('name',trans.get(k).Bank_Details_Payee__c);
						HttpResponse res = dj.send();
						trans.get(k).DowJones_Last_Check__c = System.Now();
						if(condition(res))
							trans.get(k).DowJones_Suspicious__c = true;
						else
							trans.get(k).DowJones_Suspicious__c = false;
						trans4up.add(trans.get(k));
						djlogs.add(new Dow_Jones_Log__c(Name__c=trans.get(k).Bank_Details_Payee__c,Dow_Jones_Suspicious__c=trans.get(k).DowJones_Suspicious__c));
						i++;
					}
					dones.add(k);
				}
				else {
					anotherOne = true;
					break;
				}
			}
			update accs4up;
			update trans4up;
			insert djlogs;
			if(anotherOne){
				for(String k:dones)
					if(trans.containsKey(k))
						trans.remove(k);
				checkTransfers(trans.keySet());
			}
		}
	}
	public static Boolean condition(HttpResponse res){
		if((res!=null&&res.getbody()!=null&&res.getbody().contains('<body>'))||Test.isRunningTest())	return true;
		else	return false;
	}
}
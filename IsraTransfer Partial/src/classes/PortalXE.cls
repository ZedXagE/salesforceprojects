public class PortalXE {
    // xe's json result format
    public class XEResult {
        public String quotecurrency;
        public Decimal mid;
    }

    private class XERoot {
        public List<XEResult> to;
    }

    public class currencycloud{
        public String client_buy_currency;
        public String mid_market_rate;
        public String currency_pair;
    }

    public class currencycloudOauth{
        public String auth_token;
    }

    public static Decimal getRate(String fromCurrency, String toCurrency) { return getRate(fromCurrency, toCurrency, true, null); }
    public static Decimal getRate(String fromCurrency, String toCurrency, Boolean isLive) { return getRate(fromCurrency, toCurrency, isLive, null); }
    public static Decimal getRate(String fromCurrency, String toCurrency, Boolean isLive, Date dDate) {
		if (!isLive) {
			Map<String, Decimal> fromToUSD = new Map<String, Decimal>{
                'EUR' => 1.220,
                'CHF' => 1.036,
                'CAD' => 0.803,
                'AUD' => 0.796,
                'GBP' => 1.377,
                'ZAR' => 0.081,
                'DKK' => 0.163,
                'NZD' => 0.726,
                'NIS' => 0.289
			};
			if (fromCurrency == 'USD' || toCurrency == 'USD')
				return fromCurrency == 'USD' ? 1 / fromToUSD.get(toCurrency) : fromToUSD.get(fromCurrency);
			else
				return fromToUSD.get(fromCurrency) / fromToUSD.get(toCurrency);
		}
        return getRates(fromCurrency, new List<String>{ toCurrency }, isLive, dDate)[0].mid;
    }

    public static List<XEResult> getRates(String fromCurrency, List<String> toCurrencies) { return getRates(fromCurrency, toCurrencies, true, null); }
    public static List<XEResult> getRates(String fromCurrency, List<String> toCurrencies, Boolean isLive) { return getRates(fromCurrency, toCurrencies, isLive, null); }
    public static List<XEResult> XEgetRates(String fromCurrency, List<String> toCurrencies, Boolean isLive, Date dDate) {
		if (!isLive) {
			List<XEResult> to = new List<XEResult>();
			for (String c : toCurrencies) {
				XEResult r = new XEResult();
				r.quotecurrency = c;
				r.mid = getRate(fromCurrency, c, isLive, dDate);
				to.add(r);
			}
			return to;
		}
        fromCurrency = fromCurrency == 'NIS' ? 'ILS' : fromCurrency;
        String toCurrency = '';
        for (String c : toCurrencies)
            toCurrency += (c == 'NIS' ? 'ILS' : c) + ',';
        toCurrency = toCurrency.substring(0, toCurrency.length() - 1);

        String endpoint = (dDate == null || dDate == Date.today()) ?
            ('https://xecdapi.xe.com/v1/convert_from.json/?from=' + fromCurrency + '&to=' + toCurrency) :
            ('https://xecdapi.xe.com/v1/historic_rate.json/?from=' + fromCurrency + '&to=' + toCurrency + '&date=' + dDate.year() + '-' + (dDate.month() < 10 ? '0' + dDate.month() : '' + dDate.month())  + '-' + (dDate.day() < 10 ? '0' + dDate.day() : '' + dDate.day()));
        Xe_Login__c xeLogin = [SELECT Username__c, Password__c FROM Xe_Login__c where name='1' LIMIT 1];
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endpoint);
        req.setMethod('GET');
        req.setHeader('Authorization', 'Basic ' + EncodingUtil.base64Encode(Blob.valueOf(xeLogin.Username__c + ':' + xeLogin.Password__c)));
        HTTPResponse res = new Http().send(req);
        String body = res.getBody();
        System.debug(body);
        XERoot root = (XERoot)JSON.deserialize(body, XERoot.class);
        for (XEResult r : root.to)
            r.quotecurrency = r.quotecurrency == 'ILS' ? 'NIS' : r.quotecurrency;
        return root.to;
    }
    public static List<XEResult> getRates(String fromC, List<String> toCurrencies, Boolean isLive, Date dDate) {
        List <String> ErrorMsgs = new List <String>();
        String loginname = Label.Currency_Settings_name;
        String endp = 'api';
        if(loginname.contains('sandbox'))
            endp='devapi';
        if(!Test.isRunningTest()){
            if(loginname=='1')
                return XEgetRates(fromC,toCurrencies,isLive,dDate);
        }
        else XEgetRates(fromC,toCurrencies,isLive,dDate);
        Xe_Login__c xeLogin = [SELECT Username__c, Password__c FROM Xe_Login__c where name=:loginname LIMIT 1];
        String oauth = 'https://'+endp+'.currencycloud.com/v2/authenticate/api';
        String token;
        //TOKEN
        try{
            HttpRequest reqoa = new HttpRequest();
            reqoa.setTimeOut(120000);
            reqoa.setEndpoint(oauth);
            reqoa.setMethod('POST');
            reqoa.setHeader('Content-Type','multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW');
            String data = '------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"login_id\"\r\n\r\n'+xeLogin.Username__c+'\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"api_key\"\r\n\r\n'+xeLogin.Password__c+'\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--';
            System.debug('Oauth body: '+data);
            reqoa.setbody(data);
            HTTPResponse resoa;
            if(!Test.isRunningTest())
                resoa = new Http().send(reqoa);
            else{
                resoa = new HTTPResponse();
                resoa.setStatusCode(200);
                currencycloudOauth cro = new currencycloudOauth();
                cro.auth_token = 'test';
                resoa.setBody(JSON.Serialize(cro));
            }
            if(resoa!=null){
                System.debug('Oauth res: '+resoa);
                if(resoa.getStatusCode()==200){
                    try{
                        String oauthbody = resoa.getBody();
                        currencycloudOauth oa = (currencycloudOauth) JSON.deserialize(oauthbody, currencycloudOauth.class);
                        token = oa.auth_token;
                        if(token==null) ErrorMsgs.add('Token is NULL');
                    }
                    catch(exception e2){ ErrorMsgs.add('Serialize Error: '+e2.getMessage());    }
                }
                else{   ErrorMsgs.add('Response for Token is not in Status OK');    }
            }
            else{   ErrorMsgs.add('Response for Token NULL'); }
        }
        catch(exception e){ ErrorMsgs.add(e.getMessage());    }
        System.debug('Token: '+token);
        XERoot root = new XERoot();
        root.to = new List<XEResult>();   

        String fromCurrency = fromC;
        if (!isLive) {
            for (String c : toCurrencies) {
                XEResult r = new XEResult();
                r.quotecurrency = c;
                r.mid = getRate(fromCurrency, c, isLive, dDate);
                root.to.add(r);
            }
            return root.to;
        }

        //REQUESTS
        if(token != null){
            fromCurrency = fromCurrency == 'NIS' ? 'ILS' : fromCurrency;
            for (String toCurrency : toCurrencies){
                toCurrency = (toCurrency == 'NIS' ? 'ILS' : toCurrency);
                String Origincurrencypair = fromCurrency+toCurrency;
                String endpoint = 'https://'+endp+'.currencycloud.com/v2/rates/detailed?sell_currency=' + fromCurrency + '&buy_currency=' + toCurrency + '&fixed_side=sell&amount=1';
                try{
                    HttpRequest req = new HttpRequest();
                    req.setTimeOut(120000);
                    req.setEndpoint(endpoint);
                    req.setMethod('GET');
                    req.setHeader('X-Auth-Token', token);
                    HTTPResponse res;
                    if(!Test.isRunningTest())
                        res = new Http().send(req);
                    else{
                        res = new HTTPResponse();
                        res.setStatusCode(200);
                        currencycloud cr = new currencycloud();
                        cr.client_buy_currency = toCurrency;
                        cr.mid_market_rate = '1';
                        res.setBody(JSON.Serialize(cr));
                    }
                    if(res!=null){
                        System.debug('Response cur: '+res);
                        if(res.getStatusCode()==200){
                            try{
                                String body = res.getBody();
                                System.debug(body);
                                currencycloud curres = (currencycloud) JSON.deserialize(body, currencycloud.class);
                                XEResult xer = new XEResult();
                                xer.quotecurrency = curres.client_buy_currency == 'ILS' ? 'NIS' : curres.client_buy_currency;
                                Decimal rate=null;
                                if(curres.currency_pair==Origincurrencypair)
                                    rate = Decimal.valueof(curres.mid_market_rate);
                                else
                                    rate = 1/Decimal.valueof(curres.mid_market_rate);
                                xer.mid = rate.setScale(4);
                                System.debug(xer);
                                root.to.add(xer);
                            }
                            catch(exception e2){    System.debug('Serialize Error: '+fromCurrency+' -> '+toCurrency+' '+e2.getMessage());  }
                        }
                        else{  ErrorMsgs.add('Response for '+fromCurrency+' -> '+toCurrency+' is not in Status OK');  }
                    }
                    else{   ErrorMsgs.add('Response for '+fromCurrency+' -> '+toCurrency+' is NULL');    }
                }
                catch(exception e){ ErrorMsgs.add('Error get Currency: '+fromCurrency+' -> '+toCurrency+' '+e.getMessage());    }
            }
        }
        if(!Test.isRunningTest()&&ErrorMsgs.size()>0)
            ID jobID = System.enqueueJob(new CurrencyCloudInsert(ErrorMsgs));
        return  root.to;
    }
}
/*
 * @Author: ZedXagE - ITmyWay 
 * @Date: 2018-05-22 10:56:37 
 * @Last Modified by:   ZedXagE - ITmyWay 
 * @Last Modified time: 2018-05-22 10:56:37 
 */

trigger EmailRelation on EmailMessage (before insert, after insert) {
    if(Trigger.isBefore)
        EmailRelationQueue.beforeChecks(Trigger.New);
    if(Trigger.isAfter)
        ID jobID2 = System.enqueueJob(new EmailRelationQueue(Trigger.New,null,null,null));
}
/*
 * @Author: ZedXagE - ITmyWay 
 * @Date: 2018-05-22 10:56:27 
 * @Last Modified by: ZedXagE - ITmyWay
 * @Last Modified time: 2018-05-22 11:00:08
 */

public class EmailRelationQueue implements Queueable{
	public List <EmailMessage> msgs;
	public List <EmailMessage> msgsdelete;
	public List <EmailMessage> leftmsgs;
	public List <EmailMessageRelation> msgsRelation;
    public List <TaskRelation> tsksRelation;
	public final integer Lim=5000;
	private integer count=0;
	public EmailRelationQueue(List <EmailMessage> mgs,List <EmailMessageRelation> mgsRelation, List <TaskRelation> tksRelation, List <EmailMessage> del) {
		if(mgs.size()<lim){
			msgs = mgs;
			leftmsgs = new List <EmailMessage>();
		}
		else{
            msgs = new List <EmailMessage>();
			count=0;
			for(integer i=0;i<lim&&i<mgs.size();i++){
				msgs.add(mgs[i]);
				mgs.remove(i);
				i--;
				count++;
			}
			leftmsgs = mgs;
		}
		if(mgsRelation!=null){
			msgsRelation = mgsRelation;
		}
		else{
			msgsRelation = new List <EmailMessageRelation>();
		}
        if(tksRelation!=null){
            tsksRelation = tksRelation;
		}
		else{
            tsksRelation = new List <TaskRelation>();
		}
		if(del!=null){
            msgsdelete = del;
		}
		else{
            msgsdelete = new List <EmailMessage>();
		}
	}

	public void execute(QueueableContext context) {
		Map <String,String> Emails = new Map <String,String>();

		for(EmailMessage emsg:msgs){
			if(emsg.FromAddress!=null&&emsg.FromAddress!=''&&emsg.ToAddress!=null&&emsg.ToAddress!=''){
				Emails.put(emsg.FromAddress.replaceAll(' ',''),null);
				if(emsg.ToAddress!=null)
					for(String eml:emsg.ToAddress.split(';'))
						Emails.put(eml.replaceAll(' ',''),null);
				if(emsg.CcAddress!=null)
					for(String eml:emsg.CcAddress.split(';'))
						Emails.put(eml.replaceAll(' ',''),null);
				if(emsg.BccAddress!=null)
					for(String eml:emsg.BccAddress.split(';'))
						Emails.put(eml.replaceAll(' ',''),null);
			}
		}
		for(Contact con:[select id, Email from Contact where Email in: Emails.KeySet()]){
			Emails.put(con.Email,'Contact;'+con.id);
		}
		for(User use:[select id, Email from User where Email in: Emails.KeySet()]){
			Emails.put(use.Email,'User;'+use.id);
		}
		

		for(EmailMessage emsg:msgs){
			if(emsg.FromAddress!=null&&emsg.FromAddress!=''&&emsg.ToAddress!=null&&emsg.ToAddress!=''){
				System.debug('after: '+emsg.RelatedToId);
				if(emsg.RelatedToId==null)
					msgsdelete.add(emsg);
				else{
					if(Emails.get(emsg.FromAddress)!=null){
						msgsRelation.add(
							new EmailMessageRelation(
								EmailMessageId=emsg.id,
								RelationAddress=emsg.FromAddress,
								RelationId=Emails.get(emsg.FromAddress).split(';')[1],
								RelationType='FromAddress'
							)
						);
						if(Emails.get(emsg.FromAddress).split(';')[0]=='Contact'){
							tsksRelation.add(
								new TaskRelation(
									TaskId=emsg.activityId,
									RelationId=Emails.get(emsg.FromAddress).split(';')[1]
								)
							);
						}
					}
					if(emsg.ToAddress!=null)
						for(String eml:emsg.ToAddress.split(';')){
							eml=eml.replaceAll(' ','');
							if(Emails.get(eml)!=null){
								msgsRelation.add(
									new EmailMessageRelation(
										EmailMessageId=emsg.id,
										RelationAddress=eml,
										RelationId=Emails.get(eml).split(';')[1],
										RelationType='ToAddress'
									)
								);
								if(Emails.get(eml).split(';')[0]=='Contact'){
									tsksRelation.add(
										new TaskRelation(
											TaskId=emsg.activityId,
											RelationId=Emails.get(eml).split(';')[1]
										)
									);
								}
							}
						}
					if(emsg.CcAddress!=null)
						for(String eml:emsg.CcAddress.split(';')){
							eml=eml.replaceAll(' ','');
							if(Emails.get(eml)!=null){
								msgsRelation.add(
									new EmailMessageRelation(
										EmailMessageId=emsg.id,
										RelationAddress=eml,
										RelationId=Emails.get(eml).split(';')[1],
										RelationType='CcAddress'
									)
								);
								if(Emails.get(eml).split(';')[0]=='Contact'){
									tsksRelation.add(
										new TaskRelation(
											TaskId=emsg.activityId,
											RelationId=Emails.get(eml).split(';')[1]
										)
									);
								}
							}
						}
					if(emsg.BccAddress!=null)
						for(String eml:emsg.BccAddress.split(';')){
							eml=eml.replaceAll(' ','');
							if(Emails.get(eml)!=null){
								msgsRelation.add(
									new EmailMessageRelation(
										EmailMessageId=emsg.id,
										RelationAddress=eml,
										RelationId=Emails.get(eml).split(';')[1],
										RelationType='BccAddress'
									)
								);
								if(Emails.get(eml).split(';')[0]=='Contact'){
									tsksRelation.add(
										new TaskRelation(
											TaskId=emsg.activityId,
											RelationId=Emails.get(eml).split(';')[1]
										)
									);
								}
							}
						}
				}
			}
			else
				msgsdelete.add(emsg);
		}
		if(!Test.IsRunningTest()){
			if(leftmsgs.size()>0)
				ID jobID1 = System.enqueueJob(new EmailRelationQueue(leftmsgs,msgsRelation,tsksRelation,msgsdelete));
			else
				ID jobID2 = System.enqueueJob(new InsertEmailRelationQueue(msgsRelation,tsksRelation,msgsdelete));
		}
	}

	public static void beforeChecks(List <EmailMessage> msgs) {
		Set <String> usrs = new Set <String>();
		for(user use:[select email from user])
			usrs.add(use.email);
		Map <String,String> Emails = new Map <String,String>();
		
		for(EmailMessage emsg:msgs){
			if(emsg.FromAddress!=null&&emsg.FromAddress!=''&&emsg.ToAddress!=null&&emsg.ToAddress!=''){
				if(!usrs.contains(emsg.FromAddress))
					Emails.put(emsg.FromAddress.replaceAll(' ',''),null);
				if(emsg.ToAddress!=null)
					for(String eml:emsg.ToAddress.split(';')){
						eml=eml.replaceAll(' ','');
						if(!usrs.contains(eml))
							Emails.put(eml.replaceAll(' ',''),null);
					}
				if(emsg.CcAddress!=null)
					for(String eml:emsg.CcAddress.split(';')){
						eml=eml.replaceAll(' ','');
						if(!usrs.contains(eml))
							Emails.put(eml.replaceAll(' ',''),null);
					}
				if(emsg.BccAddress!=null)
					for(String eml:emsg.BccAddress.split(';')){
						eml=eml.replaceAll(' ','');
						if(!usrs.contains(eml))
							Emails.put(eml,null);
					}
			}
		}
		for(Contact con:[select id, Accountid, Email from Contact where Email in: Emails.KeySet() and Accountid!=null]){
			Emails.put(con.Email,con.Accountid);
		}
		for(OpportunityContactRole oppc:[select id, Opportunityid, Contact.Email from OpportunityContactRole where Contact.Email in: Emails.KeySet()]){
			Emails.put(oppc.Contact.Email,oppc.Opportunityid);
		}
		
		System.debug(Emails);

		for(EmailMessage emsg:msgs){
			if(emsg.FromAddress!=null&&emsg.FromAddress!=''&&emsg.ToAddress!=null&&emsg.ToAddress!=''){
				emsg.Status='3';
				if(Emails.get(emsg.FromAddress)!=null)
					emsg.RelatedToId=Emails.get(emsg.FromAddress);
				if(emsg.ToAddress!=null)
					for(String eml:emsg.ToAddress.split(';')){
						eml=eml.replaceAll(' ','');
						if(Emails.get(eml)!=null){
							emsg.RelatedToId=Emails.get(eml);
							Break;
						}
					}
				System.debug('before: '+emsg.RelatedToId);
			}
		}
	}

}
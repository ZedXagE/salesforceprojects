/*
 * @Author: ZedXagE - ITmyWay 
 * @Date: 2018-05-22 10:56:27 
 * @Last Modified by: ZedXagE - ITmyWay
 * @Last Modified time: 2018-05-22 11:00:08
 */

public class BeforeEmailRelationQueue{
	/*public List <EmailMessage> msgs;
	public List <EmailMessage> leftmsgs;
	public final integer Lim=5000;
	public BeforeEmailRelationQueue(List <EmailMessage> mgs) {
		if(mgs.size()<lim){
			msgs = mgs;
			leftmsgs = new List <EmailMessage>();
		}
		else{
			for(integer i=0;i<lim;i++){
				msgs.add(mgs[i]);
				mgs.remove(i);
			}
			leftmsgs = mgs;
		}
	}

	public void execute(QueueableContext context) {
		Set <String> usrs = new Set <String>();
		for(user use:[select email from user])
			usrs.add(use.email);
		Map <String,String> Emails = new Map <String,String>();
		
		for(EmailMessage emsg:msgs){
			if(!usrs.contains(emsg.FromAddress))
				Emails.put(emsg.FromAddress.replaceAll(' ',''),null);
			if(emsg.ToAddress!=null)
				for(String eml:emsg.ToAddress.split(';')){
					eml=eml.replaceAll(' ','');
					if(!usrs.contains(eml))
						Emails.put(eml.replaceAll(' ',''),null);
				}
			if(emsg.CcAddress!=null)
				for(String eml:emsg.CcAddress.split(';')){
					eml=eml.replaceAll(' ','');
					if(!usrs.contains(eml))
						Emails.put(eml.replaceAll(' ',''),null);
				}
			if(emsg.BccAddress!=null)
				for(String eml:emsg.BccAddress.split(';')){
					eml=eml.replaceAll(' ','');
					if(!usrs.contains(eml))
						Emails.put(eml,null);
				}
		}
		for(Contact con:[select id, Accountid, Email from Contact where Email in: Emails.KeySet() and Accountid!=null]){
			Emails.put(con.Email,con.Accountid);
		}
		for(OpportunityContactRole oppc:[select id, Opportunityid, Contact.Email from OpportunityContactRole where Contact.Email in: Emails.KeySet()]){
			Emails.put(oppc.Contact.Email,oppc.Opportunityid);
		}
		
		System.debug(Emails);

		for(EmailMessage emsg:msgs){
			emsg.Status='3';
			if(Emails.get(emsg.FromAddress)!=null)
				emsg.RelatedToId=Emails.get(emsg.FromAddress);
			if(emsg.ToAddress!=null)
				for(String eml:emsg.ToAddress.split(';')){
					eml=eml.replaceAll(' ','');
					if(Emails.get(eml)!=null){
						emsg.RelatedToId=Emails.get(eml);
						Break;
					}
				}
			System.debug('before: '+emsg.RelatedToId);
		}
		if(!Test.IsRunningTest()&&leftmsgs.size()>0)
			ID jobID = System.enqueueJob(new BeforeEmailRelationQueue(leftmsgs));
	}*/

}
/*
 * @Author: ZedXagE - ITmyWay 
 * @Date: 2018-05-22 10:56:15 
 * @Last Modified by:   ZedXagE - ITmyWay 
 * @Last Modified time: 2018-05-22 10:56:15 
 */

public class InsertEmailRelationQueue implements Queueable{
	public List <EmailMessage> leftdelmsgs;
	public List <EmailMessage> delmsgs;
	public List <EmailMessageRelation> leftmsgsRelation;
	public List <EmailMessageRelation> msgsRelation;
	public List <TaskRelation> tsksRelation;
    public List <TaskRelation> lefttsksRelation;
	public final integer Lim=Test.isRunningTest()==true?1:3000;
	private integer count=0;
	public InsertEmailRelationQueue(List <EmailMessageRelation> mgsRelation, List <TaskRelation> tksRelation, List <EmailMessage> del) {
		if(mgsRelation.size()<lim){
			msgsRelation = mgsRelation;
			leftmsgsRelation = new List <EmailMessageRelation>();
		}
		else{
			msgsRelation = new List <EmailMessageRelation>();
			count=0;
			for(integer i=0;count<lim&&i<mgsRelation.size();i++){
				msgsRelation.add(mgsRelation[i]);
				mgsRelation.remove(i);
				i--;
				count++;
			}
			leftmsgsRelation = mgsRelation;
		}
		if(tksRelation.size()<lim){
			tsksRelation = tksRelation;
			lefttsksRelation = new List <TaskRelation>();
		}
		else{
			tsksRelation = new List <TaskRelation>();
			count=0;
			for(integer i=0;count<lim&&i<tksRelation.size();i++){
				tsksRelation.add(tksRelation[i]);
				tksRelation.remove(i);
				i--;
				count++;
			}
			lefttsksRelation = tksRelation;
		}
		if(del.size()<lim){
			delmsgs = del;
			leftdelmsgs = new List <EmailMessage>();
		}
		else{
			delmsgs = new List <EmailMessage>();
			count=0;
			for(integer i=0;count<lim&&i<del.size();i++){
				delmsgs.add(del[i]);
				del.remove(i);
				i--;
				count++;
			}
			leftdelmsgs = del;
		}
	}

	public void execute(QueueableContext context) {
		Database.SaveResult[] resRelations = Database.Insert(msgsRelation, false);

		for (Database.SaveResult r : resRelations)
			if(!r.isSuccess()||Test.IsRunningTest())
				for(Database.Error e:r.getErrors())
					System.debug(e.getMessage());
		
		Database.SaveResult[] resTRelations = Database.Insert(tsksRelation, false);

		for (Database.SaveResult r : resTRelations)
			if(!r.isSuccess()||Test.IsRunningTest())
				for(Database.Error e:r.getErrors())
					System.debug(e.getMessage());

		delete delmsgs;

		if(!Test.IsRunningTest()&&(leftmsgsRelation.size()>0||lefttsksRelation.size()>0||leftdelmsgs.size()>0))
			ID jobID = System.enqueueJob(new InsertEmailRelationQueue(leftmsgsRelation,lefttsksRelation,leftdelmsgs));
	}

}
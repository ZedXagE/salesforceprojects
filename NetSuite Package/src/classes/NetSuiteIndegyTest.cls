@isTest
public class NetSuiteIndegyTest {
	static testMethod void NetSuiteSendOrder() {
		insert new Settings__c (Name='test', End_Point__c='test', Script__c='test', Deploy__c='test', Consumer_Key__c='test', Consumer_Secret__c='test', Token_ID__c='test', Token_Secret__c='test', Realm__c='test');
		Account acc = new Account(name='testacc');
		insert acc;
		Opportunity opp = new Opportunity(CloseDate=System.Today(),Accountid=acc.id,name='testopp',StageName='Closed Won');
		insert opp;
		Order ord = new Order(EffectiveDate=System.Today(),Accountid=acc.id,Opportunityid=opp.id,Status='Draft');
		insert ord;
		NetSuiteIndegyOrderSend.sendOrder(ord.id);
	}
	static testMethod void NetSuitegetFulfillment() {
		insert new Settings__c (Name='test', End_Point__c='test', Script__c='test', Deploy__c='test', Consumer_Key__c='test', Consumer_Secret__c='test', Token_ID__c='test', Token_Secret__c='test', Realm__c='test');
		Account acc = new Account(name='testacc');
		insert acc;
		Opportunity opp = new Opportunity(CloseDate=System.Today(),Accountid=acc.id,name='testopp',StageName='Closed Won');
		insert opp;
		Order ord = new Order(EffectiveDate=System.Today(),Accountid=acc.id,Opportunityid=opp.id,Status='Draft',NetSuite_ID__c='123456');
		insert ord;
		Product2 prod = new Product2(Name='123456',ProductCode='123456');
		insert prod;
		RestRequest req = new RestRequest();
		RestContext.request = req;
		req.requestBody=Blob.valueOf('{"uid": "1445161409240","integrationType": "SF_PushFulfillmentData","postData": {"Sf_order_num" : "'+ord.id+'","Sg_order_line_num" : "1","Item_name" : "'+prod.ProductCode+'","Start_date" : "12/05/2018","End_date" : "12/06/2018","Product_Version" : "v1","Image_Version" : "v1","Serial_number" : "serial1,serial2,serial3,serial4"}}');
		RestResponse res = new RestResponse();
		RestContext.request = req;
		RestContext.response = res;
		Test.startTest();
        NetSuiteFulfillMents.NSResponse nsres = new NetSuiteFulfillMents.NSResponse();
		nsres=NetSuiteFulfillMents.insertAssets();
	}
	static testMethod void NetSuitegetInvoice() {
		insert new Settings__c (Name='test', End_Point__c='test', Script__c='test', Deploy__c='test', Consumer_Key__c='test', Consumer_Secret__c='test', Token_ID__c='test', Token_Secret__c='test', Realm__c='test');
		Account acc = new Account(name='testacc');
		insert acc;
		Opportunity opp = new Opportunity(CloseDate=System.Today(),Accountid=acc.id,name='testopp',StageName='Closed Won');
		insert opp;
		Order ord = new Order(EffectiveDate=System.Today(),Accountid=acc.id,Opportunityid=opp.id,Status='Draft',NetSuite_ID__c='123456');
		insert ord;
		
		RestRequest req = new RestRequest();
		RestContext.request = req;
		req.requestBody=Blob.valueOf('{"uid": "1445161409240","integrationType": "SF_UpdateInvoicePrintURL","postData": {"createfrom_order_sfdc_id" : "'+ord.id+'","customer_sfdc_id" : "'+acc.id+'","internalid" : "1","pdfUrl" : "someItem"}}');
		RestResponse res = new RestResponse();
		RestContext.request = req;
		RestContext.response = res;
		Test.startTest();
        NetSuiteInvoices.NSResponse nsres = new NetSuiteInvoices.NSResponse();
		nsres=NetSuiteInvoices.insertInvoices();
	}
}
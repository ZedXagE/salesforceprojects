/*
 * @Author: ZedXagE - ITmyWay 
 * @Date: 2018-09-20 11:18:14 
 * @Last Modified by: ZedXagE - ITmyWay
 * @Last Modified time: 2018-09-20 11:23:58
 */
// NetSuiteResponse.java
global class NetSuiteResponse {
	global String status;
	global String message;
	global String succLines;
	global String failedLines;
	global List <resData> resData;
	global error error;
	global NetSuiteResponse(){
		resData = new List<resData>();
		error = new error();
	}

	// resData.java
	global class resData {
		global String integrationType;
		global String lineStatus;
		global String lineMessage;
		global String action;
		global String lineUid;
		global String recordId;
		global String seconderyRecordId;
	}
	// error.java
	global class error {
		global String code;
		global String message;
	}

	global override String toString(){
		String js = JSON.serialize(this);
		return js;
	}
}
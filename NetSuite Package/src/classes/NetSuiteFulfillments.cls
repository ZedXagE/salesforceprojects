@RestResource(urlMapping='/nsfulfillments')
global class NetSuiteFulfillments {
    global class NSrequest{
        global String uid;
        global String integrationType;
        global postData postData; 
    }
	global class postData {
        global String internalid;
        global String Sf_order_num;
		global String Sg_order_line_num;
		global String Item_name;
		global String Start_date;
		global String End_date;
		global String Product_Version;
		global String Image_Version;
		global String Serial_number;
	}
	global class NSResponse{
        global String message;
		global String status;
    }
    @HttpPost
    global static NSResponse insertAssets() {
		NSResponse nsres = new NSResponse();
		nsres.status = 'failed';
		nsres.message = 'no product found with this item name';
		try{
			NSrequest nsreq = (NSrequest)JSON.deserialize(RestContext.request.requestBody.toString(), NSrequest.class);
			List <Asset> assets = new List <Asset>();
			List <Product2> prods = [select id,ProductCode from Product2 where ProductCode =: nsreq.postData.Item_name and ProductCode!=null];
            if(prods.size()>0){
				nsres.message = 'no order found with this id';
                List <Order> ords = [select id,NetSuite_ID__c from Order where id =: nsreq.postData.Sf_order_num];
				if(ords.size()>0){
                    String [] spl = nsreq.postData.Serial_number.split(',');
					nsres.status = 'success';
					nsres.message = '';
					for(String s:spl){
						if(s!=null&&s!=''){
							Asset asset = new Asset();
							asset.Name = nsreq.postData.Item_name;
							asset.Order__c = ords[0].id;
							asset.InstallDate = DateStr(nsreq.postData.Start_date);
							asset.UsageEndDate = DateStr(nsreq.postData.End_date);
							asset.SerialNumber = s;
							asset.Version__c = nsreq.postData.Product_Version;
							asset.Image_Version__c = nsreq.postData.Image_Version;
							asset.Product2id = prods[0].id;
							assets.add(asset);
						}
					}
					insert assets;
                }
            }
		}
		catch(exception e){
			nsres.message = e.getMessage();
		}
		return nsres;
    }

	global static Date DateStr(String dat){
		if(dat!=null){
			String [] spl = dat.split('/');
			if(spl.size()==3)
				return Date.newinstance(Integer.valueof(spl[2]),Integer.valueof(spl[1]),Integer.valueof(spl[0]));
		}
		return null;
	}
}
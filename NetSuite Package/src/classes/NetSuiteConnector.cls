/*
 * @Author: ZedXagE - ITmyWay 
 * @Date: 2018-09-20 11:18:26 
 * @Last Modified by:   ZedXagE - ITmyWay 
 * @Last Modified time: 2018-09-20 11:18:26 
 */

global class NetSuiteConnector {
    global String body;
    global String shortendpoint;
    global String endpoint;
    global String method;
    global String script;
    global String deploy;
    global String consumerKey;
    global String consumerSecret;
    global String tokenId;
    global String tokenSecret;
    global String realm;
    global Map <String,String> auth;
    global NetSuiteConnector(){
        List <Settings__c> settings = [select End_Point__c, Script__c, Deploy__c, Consumer_Key__c, Consumer_Secret__c, Token_ID__c, Token_Secret__c, Realm__c from Settings__c Limit 1];
        if(settings.size()>0){
            shortendpoint = settings[0].End_Point__c;
            endpoint = settings[0].End_Point__c;
            script = settings[0].Script__c;
            deploy = settings[0].Deploy__c;
            consumerKey = settings[0].Consumer_Key__c;
            consumerSecret = settings[0].Consumer_Secret__c;
            tokenId = settings[0].Token_ID__c;
            tokenSecret = settings[0].Token_Secret__c;
            realm = settings[0].Realm__c;
        }
        endpoint = '';
        endpoint += shortendpoint!=null?shortendpoint:'';
        endpoint += script!=null?('?script='+script):'';
        endpoint += deploy!=null?((script!=null?'&':'?')+'deploy='+deploy):'';
    }
    global HttpResponse zedHTTP(){
		Http http=new Http();
		HttpRequest req=new HttpRequest();
		req.setendpoint(endpoint);
		req.setmethod(method);
        req.setTimeout(20000);
		req.setHeader('Content-Type', 'application/json');
        if(auth!=null)
            for(String key:auth.keySet())
                req.setHeader(key, auth.get(key));
        if(body!=null)        
		    req.setbody(body);
		HttpResponse res = new HttpResponse();
		if(!Test.isRunningTest())
			res = http.send(req);
		return res;
	}
    global NetSuiteResponse send(){
        if(method==null)
            method = body!=null?'POST':'GET';
        Map <String, String> params = new Map <String, String>();
        params.put('deploy', deploy);
        params.put('oauth_consumer_key', consumerKey);
        params.put('oauth_nonce', String.valueOf(Crypto.getRandomLong()));
        params.put('oauth_signature_method', 'HMAC-SHA1');
        params.put('oauth_timestamp', String.valueOf(DateTime.now().getTime()/1000));
        params.put('oauth_token', tokenId);
        params.put('oauth_version', '1.0');
        params.put('script', script);
        String baseString, oauthString, key;
        baseString = method + '&' + urlencode(shortendpoint) + '&' + urlencode(urlStr(params));
        key = consumerSecret + '&' + tokenSecret;
        System.debug('Signature base string: '+baseString);
        System.debug('Key: '+key);
        params.put('oauth_signature', HmacSHA1(baseString,key));
        params.put('realm', realm);
        oauthString = 'OAuth ' + oauthStr(params);
        System.debug('Authorization: '+oauthString);
        System.debug('End Point: '+endpoint);
        auth = new Map <String,String>{'Authorization' => oauthString};
        if(body!=null)
            auth.put('Content-Length', String.valueof(body.length()));
        HttpResponse res = zedHTTP();
        NetSuiteResponse nsr = new NetSuiteResponse();
        if(res!=null){
            System.debug(res.getStatusCode() + ' Body = '+res.getBody());
            try{
                nsr = (NetSuiteResponse) JSON.deserialize(res.getBody(), NetSuiteResponse.class);
                System.debug(nsr);
                return nsr;
            }
            catch (exception e){
                nsr =  new NetSuiteResponse();
                nsr.status='SalesForce Error';
                nsr.message = e.getMessage();
                System.debug(nsr);
                return nsr;
            }
        }
        return new NetSuiteResponse();
    }
    global static String urlencode(String str){
        return EncodingUtil.urlEncode(str, 'UTF-8');
    }
    global static String urlStr(Map <String, String> params){
        String str='';
        for(String k:params.KeySet())
            str += k + '=' + params.get(k) + '&';
        return str.left(str.length()-1);
    }
    global static String oauthStr(Map <String, String> params){
        String str='';
        for(String k:params.KeySet())
            if(k!='script' && k!='deploy')
                str += k + '="' + (k!='oauth_signature'?urlencode(params.get(k)):params.get(k)) + '", ';
        return str.left(str.length()-2);
    }
    global static String HmacSHA1(String base,String key){
        Blob data = crypto.generateMac('HmacSHA1', Blob.valueOf(base), Blob.valueOf(key));
        return urlencode(EncodingUtil.base64encode(data));
    }
}
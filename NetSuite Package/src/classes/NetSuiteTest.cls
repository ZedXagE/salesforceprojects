/*
 * @Author: ZedXagE - ITmyWay 
 * @Date: 2018-09-20 14:23:15 
 * @Last Modified by:   ZedXagE - ITmyWay 
 * @Last Modified time: 2018-09-20 14:23:15 
 */

@isTest
public class NetSuiteTest {
	static testMethod void NetSuiteTests() {
		insert new Settings__c (Name='test', End_Point__c='test', Script__c='test', Deploy__c='test', Consumer_Key__c='test', Consumer_Secret__c='test', Token_ID__c='test', Token_Secret__c='test', Realm__c='test');
		Account acc = new Account(name='testacc');
		insert acc;
		Opportunity opp = new Opportunity(CloseDate=System.Today(),Accountid=acc.id,name='testopp',StageName='Closed Won');
		insert opp;
		Order ord = new Order(EffectiveDate=System.Today(),Accountid=acc.id,Opportunityid=opp.id,Status='Draft');
		insert ord;
		String contacts =  '{"objList" :[ {"uid" : 1987465413,"integrationType" : "NS_SyncContact", "data" : { "recordtype": "contact", "internalid" : null,"externalid": "1234567891011", "firstname": "Tes2t","lastname" : "contact","company": "1397",  "email" : "testcustomer@test.com","officephone" : "0502122122", "fax" : "0502122123", "addressbook": [ {"country":  "IL","addr1": "address1","addr2": "address2","city": "city","state" : "state", "zip" : "42135", "defaultbilling": "T", "defaultshipping": "T","attention" : "","override": "F" }]}}]}';
		
		NetSuiteContacts nsco = new NetSuiteContacts();
		nsco = (NetSuiteContacts) json.deserialize(contacts, NetSuiteContacts.class);
		system.debug(nsco);
		nsco.objList.add(new NetSuiteContacts.objList());

		String customers = '{"objList" :[ {"uid" : 1987465413,"integrationType" : "NS_SyncCustomer", "data" : { "recordtype": "customer", "internalid" : null, "isinactive" : "F", "autoname" : "F","externalid": "123456789","companyname": "Company name", "subsidiary": "17","email" : "test@tst.com","url" : "","terms" : "2", "vatregnumber" : "999999999","custentity_il_taxpayer_id" : "999999999","currency" : "<CurrencyIsoCode>","category" : "<type>","fax" : "<fax>", "phone" : "<phone>", "addressee":"<name>","comments":"<Description>","isAddressChanging" : "F", "addressbook": [ {"country":  "<billingcountry>", "addr1": "<billingstreet>","city": "<billingcity>","state" : "<billingstate>", "zip" : "<billingpostalcode>", "defaultbilling": "T","defaultshipping": "F","override": "F" },{"country":  "<shippingcountry>", "addr1": "<shippingstreet>","city": "<shippingcity>","state" : "<shippingstate>", "zip" : "<shippingpostalcode>", "defaultbilling": "F", "defaultshipping": "T","override": "F" }]}}]}';
		
		NetSuiteCustomers nscu = new NetSuiteCustomers();
		nscu.objList.add(new NetSuiteCustomers.objList(acc));
		nscu.objList.add(new NetSuiteCustomers.objList());
		nscu = (NetSuiteCustomers) json.deserialize(customers, NetSuiteCustomers.class);
		system.debug(nscu);
		nscu.objList.add(new NetSuiteCustomers.objList());

		NetSuiteOrders nso = new NetSuiteOrders();
		nso.objList.add(new NetSuiteOrders.objList(ord));
		nso.objList.add(new NetSuiteOrders.objList());
		system.debug(nso);

		NetSuiteConnector ns = new NetSuiteConnector();
		ns.body=nscu.toString();
		NetSuiteResponse nsr = ns.send();
		system.debug(nsr);
	}
}
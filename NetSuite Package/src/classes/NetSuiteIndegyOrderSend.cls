public class NetSuiteIndegyOrderSend {
	private order ord;
	public NetSuiteIndegyOrderSend() {}
	@AuraEnabled
	public static String SendOrder(String ordid){
		String err;
		if(ordid!=null&&ordid!=''){
			order ord = [select id,NetSuite_ID__c,Status,OpportunityId,Type,Opportunity.CloseDate,CurrencyIsoCode,OrderNumber,Opportunity.description,Payment_Terms__c,ShippingCountryCode,ShippingStreet,ShippingCity,ShippingStateCode,ShippingPostalCode,Site__r.Name,Owner.Email,Accountid,Account.NetSuite_ID__c,(select id,Product2.ProductCode,Quantity,ListPrice,UnitPrice,Duration__c,ServiceDate,EndDate from OrderItems) from Order where id=: ordid];
			if(ord.Status=='Draft'){
				if(ord.NetSuite_ID__c==null){
					if(ord.Account.NetSuite_ID__c==null)
						err = CreateCustomer(ord.Accountid);
					if(err==null||err.contains('success;')){
						if(err!=null&&err.contains('success;'))
							ord.Account.NetSuite_ID__c = err.split(';')[1];
						err = CreateOrder(ord);
					}
				}
				else
					err = 'Order has already been sent to NewtSuite';
			}
			else
				err = 'Order can be sent only with the Status Draft';
		}
		else
			err = 'No Order ID, Please try again or contact admin.';
		return err;
	}
	@AuraEnabled
	public static String CreateOrder(Order ord){
		String err;
		try{
			NetSuiteOrders nsc = new NetSuiteOrders();
			nsc.objList.add(new NetSuiteOrders.objList(ord));
			NetSuiteConnector ns = new NetSuiteConnector();
			System.debug(nsc.toString());
			ns.body=nsc.toString();
			NetSuiteResponse nsr = ns.send();
			if(Test.isRunningTest()){
				NetSuiteResponse.resData res = new NetSuiteResponse.resData();
				res.recordId = ord.id;
				res.lineUid = nsc.objList[0].uid;
				nsr.resData.add(res);
			}
			for(NetSuiteOrders.objList obj:nsc.objList)
				for(NetSuiteResponse.resData res:nsr.resData)
					if(res.lineUid == obj.uid){
						if(res.recordId!=null){
							ord.NetSuite_ID__c = res.recordId;
							ord.Status = 'Pending Fulfillment';
							update ord;
							err = 'Order has been Successfully Created (NetSuite id:'+res.recordId+')!';
							break;
						}
						else
							err = res.lineMessage;
					}
		}
		catch(exception e){
			err = e.getMessage();
		}
		if(err==null)
			err = 'Failed to contact NetSuite, Please try again later or contact admin.';
		return err;
	}
	@AuraEnabled
	public static String CreateCustomer(String accid){
		String err;
		if(accid!=null&&accid!=''){
			try{
				account acc = [select id,name,NetSuite_ID__c,Subsidiary__c,Payment_Terms__c,phone,fax,recordtype.name,description,currencyisocode,billingcountryCode,billingcity,billingstateCode,billingpostalcode,billingstreet,shippingcountryCode,shippingcity,shippingstateCode,shippingpostalcode,shippingstreet from account where id=:accid];
				NetSuiteCustomers nsc = new NetSuiteCustomers();
				nsc.objList.add(new NetSuiteCustomers.objList(acc));
				NetSuiteConnector ns = new NetSuiteConnector();
				System.debug(nsc.toString());
				ns.body=nsc.toString();
				NetSuiteResponse nsr = ns.send();
				if(Test.isRunningTest()){
					NetSuiteResponse.resData res = new NetSuiteResponse.resData();
					res.recordId = acc.id;
					res.lineUid = nsc.objList[0].uid;
					nsr.resData.add(res);
				}
				for(NetSuiteCustomers.objList obj:nsc.objList)
					for(NetSuiteResponse.resData res:nsr.resData)
						if(res.lineUid == obj.uid){
							if(res.recordId!=null){
								acc.NetSuite_ID__c = res.recordId;
								update acc;
								err = 'success;'+res.recordId;
								break;
							}
							else
								err = res.lineMessage;
						}
			}
			catch(exception e){
				err = e.getMessage();
			}
		}
		else
			err = 'No Account ID, Please try again or contact admin.';
		if(err==null)
			err = 'Failed to contact NetSuite, Please try again later or contact admin.';
		return err;
	}
}
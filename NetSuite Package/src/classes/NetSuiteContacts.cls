/*
 * @Author: ZedXagE - ITmyWay 
 * @Date: 2018-09-20 11:18:14 
 * @Last Modified by: ZedXagE - ITmyWay
 * @Last Modified time: 2018-09-20 11:23:52
 */
// NetSuiteContacts.java
global class NetSuiteContacts {
    global List <ObjList> objList;
	global static final map <String, String> problems = new Map <String,String>{'currency_c'=>'currency','override_c'=>'override'};

	global NetSuiteContacts(){
		objList = new List<ObjList>();
	}

	// ObjList.java
	global class ObjList {
		global long uid;
		global String integrationType;
		global Data data;
		global ObjList(){
			uid = Crypto.getRandomLong();
			data = new data();
		}
	}

	// Data.java
	global class Data {
		global String recordtype;
		global String internalid;
		global String externalid;
		global String firstname;
		global String lastname;
		global String company;
		global String email;
		global String officephone;
		global String fax;
		global List <Addressbook> addressbook;
		global Data(){
			addressbook = new List <Addressbook>();
		}
	}

	// Addressbook.java
	global class Addressbook {
		global String country;
		global String addr1;
		global String addr2;
		global String city;
		global String state;
		global String zip;
		global String defaultbilling;
		global String defaultshipping;
		global String attention;
		global String override_c;
	}

	global override String toString(){
		String js = JSON.serialize(this);
		if(js!=null)
			for(String k:problems.keySet())
				js = js.replaceAll(k,problems.get(k));
		return js;
	}
}
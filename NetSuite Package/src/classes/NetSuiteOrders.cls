/*
 * @Author: ZedXagE - ITmyWay 
 * @Date: 2018-09-20 11:18:14 
 * @Last Modified by: ZedXagE - ITmyWay
 * @Last Modified time: 2018-09-20 16:02:58
 */
// NetSuiteOrders.java
global class NetSuiteOrders{
    global List <ObjList> objList;
	global static final map <String, String> problems = new Map <String,String>{'currency_c'=>'currency'};

	global NetSuiteOrders(){
		objList = new List<ObjList>();
	}

	// ObjList.java
	global class ObjList {
		global String uid;
		global String integrationType;
		global Data data;
		global ObjList(){
			uid = String.valueof(Crypto.getRandomLong());
			integrationType = 'NS_CreaseSalesOrder_sync';
			data = new data();
		}
		global ObjList(Order ord){
			uid = String.valueof(Crypto.getRandomLong());
			integrationType = 'NS_CreaseSalesOrder_sync';
			data = new data(ord);
		}
	}

	// Data.java
	global class Data {
		global String internalid;
		global String externalid;
		global String custbody_ind_sf_opp_id;
		global String custbody_ind_so_order_type;
		global String trandate;
		global String entity;
		global String currency_c;
		global String custbody_ind_sf_opp_number;
		global String custbody_ind_sf_order_number;
		global String memo;
		global String terms;
		global String shipcountry;
		global String shipaddr1;
		global String shipcity;
		global String shipstate;
		global String shipzip;
		global String addressee;
		global String salesrep;
		global String custcol_ind_site_name;
		global List <Item> item;
		global Data(){
			item = new List <Item>();
		}
		global Data(Order ord){
			item = new List <Item>();
			externalid = ord.id;
			custbody_ind_sf_opp_id = ord.OpportunityId;
			custbody_ind_so_order_type = ord.Type;
			trandate = StrDate(ord.Opportunity.CloseDate);
			entity = ord.Account.NetSuite_ID__c;
			currency_c = ord.CurrencyIsoCode;
			//custbody_ind_sf_opp_number = ord.Opportunity.Number;
			custbody_ind_sf_order_number = ord.OrderNumber;
			memo = ord.Opportunity.description;
			terms = ord.Payment_Terms__c;
			shipcountry = ord.ShippingCountryCode;
			shipaddr1 = ord.ShippingStreet;
			shipcity = ord.ShippingCity;
			shipstate = ord.ShippingStateCode;
			shipzip = ord.ShippingPostalCode;
			addressee = ord.Site__r.Name;
			salesrep = ord.Owner.Email;
			custcol_ind_site_name = ord.Site__r.Name;
			for(OrderItem ordi:ord.OrderItems)
				item.add(new item(ordi));
		}
	}

	// Addressbook.java
	global class Item {
		global String item;
		global String custcol_ind_so_sf_order_line;
		global String custcol_ind_so_serial_number;
		global String quantity;
		global String custcol_ind_so_list_price;
		global String custcol_ind_so_discount_percent;
		global String rate;
		global String custcol_ind_period;
		global String custcol_ind_start_date;
		global String custcol_ind_end_date;
		global Item(OrderItem ordi){
			item = ordi.Product2.ProductCode;
			custcol_ind_so_sf_order_line = ordi.id;
			quantity = String.valueof(ordi.Quantity);
			custcol_ind_so_list_price = String.valueof(ordi.ListPrice);
			custcol_ind_so_discount_percent = String.valueof(ordi.Discount__c);
			rate = String.valueof(ordi.UnitPrice);
			custcol_ind_period = String.valueof(ordi.Duration__c);
			custcol_ind_start_date = StrDate(ordi.ServiceDate);
			custcol_ind_end_date = StrDate(ordi.EndDate);
		}
	}

	global override String toString(){
		String js = JSON.serialize(this);
		if(js!=null)
			for(String k:problems.keySet())
				js = js.replaceAll(k,problems.get(k));
		return js;
	}

	global static String StrDate(Date dat){
		if(dat!=null)
			return String.valueof(dat.day())+'/'+String.valueof(dat.month())+'/'+String.valueof(dat.year());
		return null;
	}

}
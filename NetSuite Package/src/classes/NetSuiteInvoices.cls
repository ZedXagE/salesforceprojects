@RestResource(urlMapping='/nsinvoices')
global class NetSuiteInvoices {
    global class NSrequest{
        global String uid;
        global String integrationType;
        global postData postData; 
    }
	global class postData {
        global String internalid;
        global String pdfUrl;
		global String createfrom_order_sfdc_id;
		global String customer_sfdc_id;
	}
	global class NSResponse{
        global String message;
		global String status;
    }
    @HttpPost
    global static NSResponse insertInvoices() {
		NSResponse nsres = new NSResponse();
		nsres.status = 'failed';
		nsres.message = 'no order found with this id';
		try{
			NSrequest nsreq = (NSrequest)JSON.deserialize(RestContext.request.requestBody.toString(), NSrequest.class);
			List <Order> ords = [select id,NetSuite_ID__c from Order where id =: nsreq.postData.createfrom_order_sfdc_id];
			List <Account> accs = [select id,NetSuite_ID__c from Account where id =: nsreq.postData.customer_sfdc_id];
			if(ords.size()>0||accs.size()>0){
				nsres.status = 'success';
				nsres.message = '';
				NetSuiteInvoice__c nsi = new NetSuiteInvoice__c(NetSuite_ID__c=nsreq.postData.internalid,Link__c=nsreq.postData.pdfUrl);
				if(ords.size()>0) nsi.Order__c = ords[0].id;
				if(accs.size()>0) nsi.Account__c = accs[0].id;
            }
		}
		catch(exception e){
			nsres.message = e.getMessage();
		}
		return nsres;
	}
}
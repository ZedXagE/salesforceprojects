global class ControllerV2 {
	//Endpoint https://secure5.tranzila.com/cgi-bin/tranzila71u.cgi
	//Charge (endpoint,supplier,password,expdate,token,currency,credtype,tranmode,imaam,price,additionals)
	global static HttpResponse Charge(String endpoint,String supplier,String tranzilaPW,String expDate,String tranzilaTK,String curren,String credType,String tranMode,String iMaam,String price, Map <String,String> additional){
		Map <String,String> data=new Map <String,String> {
			'supplier'=>supplier,
			'sum'=>price,
			'expdate'=>expDate,
			'currency'=>curren,
			'TranzilaPW'=>tranzilaPW,
			'TranzilaTK'=>tranzilaTK,
			'cred_type'=>credType,
			'tranmode'=>tranMode,
			'IMaam'=>iMaam
		};
		if(additional!=null)
			for(String key:additional.Keyset())
				data.put(key,additional.get(key));
		HttpResponse response=zedHTTP(Endpoint,'POST',data);
		return response;
	}
	//Charge (expdate,token,currency,credtype,tranmode,imaam,price,customsettings_name,additionals)
	global static HttpResponse Charge(String expDate,String tranzilaTK,String curren,String credType,String tranMode,String iMaam,String price,String TSname, Map <String,String> additional){
		List <Tranzila_Settingss__c> TS=[select Name,Endpoint__c,Supplier__c,TranzilaPW__c,CreditPass__c from Tranzila_Settingss__c Where Name=:TSname Limit 1];
		if(TS.Size()>0)
			return Charge(TS[0].Endpoint__c,TS[0].Supplier__c,TS[0].TranzilaPW__c,expDate,tranzilaTK,curren,credType,tranMode,iMaam,price,additional);
		else
			return null;
	}
	//added CreditPass
	//added authnr for payment id
	//tranmode 'C'+index
	//Refund (endpoint,supplier,password,creditpass,expdate,token,currency,credtype,tranmode,authnr,price,additionals)
	global static HttpResponse Refund(String endpoint,String supplier,String tranzilaPW,String creditPass,String expDate,String tranzilaTK,String curren,String credType,String tranMode,String authNr,String price, Map <String,String> additional){
		Map <String,String> data=new Map <String,String> {
			'supplier'=>supplier,
			'sum'=>price,
			'expdate'=>expDate,
			'currency'=>curren,
			'TranzilaPW'=>tranzilaPW,
			'TranzilaTK'=>tranzilaTK,
			'cred_type'=>credType,
			'tranmode'=>tranMode,
			'CreditPass'=>creditPass,
			'authnr'=>authNr
		};
		if(additional!=null)
			for(String key:additional.Keyset())
				data.put(key,additional.get(key));
		HttpResponse response=zedHTTP(Endpoint,'POST',data);
		return response;
	}
	//Refund (expdate,token,currency,credtype,tranmode,authnr,price,additionals)
	global static HttpResponse Refund(String expDate,String tranzilaTK,String curren,String credType,String tranMode,String authNr,String price, String TSname,Map <String,String> additional){
		List <Tranzila_Settingss__c> TS=[select Name,Endpoint__c,Supplier__c,TranzilaPW__c,CreditPass__c from Tranzila_Settingss__c Where Name=:TSname Limit 1];
		if(TS.Size()>0)
			return Refund(TS[0].Endpoint__c,TS[0].Supplier__c,TS[0].TranzilaPW__c,TS[0].CreditPass__c,expDate,tranzilaTK,curren,credType,tranMode,authNr,price,additional);
		else
			return null;
	}
	global static HttpResponse zedHTTP(String endpoint,String method,Map <String,String> data){
		Http http=new Http();
		HttpResponse res = new HttpResponse();
		HttpRequest req=new HttpRequest();
		req.setendpoint(endpoint);
		req.setmethod(method);
		String body='';
		for(String key:data.Keyset()){
			if(body!='')
				body+='&';
			body+=key+'='+data.get(key);
		}
		System.debug(body);
		req.setbody(body);
		if(!Test.isRunningTest()){
			try{
				res = http.send(req);
				return res;
			}
			catch(exception e){
				return null;
			}
		}
		else
			return null;
	}
}
/*
 * @Author: 
                                                        `/////                                                                                         
    smmmmmmmmmmmmmmmm-                                 .NMMMM`  :mmmmmd-      -dmmNmd-                                           /mmmNmmmmmmmmmmm:    
    yMMMMMMMMMMMMMMMM-                                 .NMMMM`   .dMMMMN+    +NMMMMy`                                            +MMMMMMMMMMMMMMM:    
    .--------+NMMMMMh`      `:+osyso+:`         :+syso/:MMMMM`    `sMMMMMy``hMMMMN/     `:/+osyysso/-`        `:+syso/.:::::`    +MMMMM/---------`    
           `sMMMMMm:      :hMMMMMMMMMMNs`    `oNMMMMMMMMMMMMM`      :mMMMMmmMMMMy`      .MMMMMMMMMMMMNo`    `sNMMMMMMMMMMMMM:    +MMMMM.              
          /NMMMMNo`     `yMMMMo-``.oMMMMd`  `dMMMMm+:-:+MMMMM`       `yMMMMMMMN/        `y+:.```-+mMMMMo   `dMMMMNo:--/NMMMM:    +MMMMMNNNNNNNNNs     
        -dMMMMMh.       +MMMMh::::::dMMMM+  yMMMMd`    .NMMMM`        `dMMMMMMs            `-:/+oodMMMMy   sMMMMN.     mMMMM:    +MMMMMMMMMMMMMMs     
      `sMMMMMm:         hMMMMMMMMMMMMMMMMs  mMMMMo     .NMMMM`       -mMMMMMMMMh.       .smMMMMMMMMMMMMy   yMMMMh      mMMMM:    +MMMMM:--------.     
     /NMMMMNo`          yMMMMh:::::::::::.  mMMMMs     .NMMMM`      oNMMMModMMMMN/     -NMMMMo:.` oMMMMy   sMMMMm`     mMMMM:    +MMMMM.              
   `dMMMMMNo/////////:  -NMMMMo.      ./s.  oMMMMN/`  `+MMMMM`    .hMMMMm: `sMMMMMy`   oMMMMd     sMMMMy   .NMMMMmo//oyMMMMM:    +MMMMM+/////////.    
   -MMMMMMMMMMMMMMMMMm   -dMMMMMNdddmNMMM-  `yMMMMMMNNMMMMMMM`   /NMMMMh`    /NMMMMm-  -NMMMMmyyymMMMMMy    -hMMMMMMMMNMMMMM:    +MMMMMMMMMMMMMMM:    
   .hddddddddddddddddy     -ohmNMMMMMNmho.    :ymMMMNh+-hdddh`  +hdddd+       .hddddh-  .odNMMNds:+ddddo      ./osys+:`MMMMM-    /ddddddddddddddd-    
                                `````            ```                                        ``              `+/-.` `.:hMMMMd                          
                                                                                                            `NMMMMMMMMMMMMy`                          
                                                                                                            `oyyhddddhyo/`                            
 * @Date: 2018-11-25 14:26:38 
 * @Last Modified by:   ZedXagE - ITmyWay 
 * @Last Modified time: 2018-11-25 14:26:38 
 */

trigger ConfigurationCopy on Configuration__c (before insert,before update) {
    Set <string> mains = new Set <string>();
    for(RecordType rec:[select id from RecordType where Name like: '%Unit%' and IsActive=true and SobjectType='Configuration__c'])
        mains.add(rec.id);
    Set <String> fields = new Set <String>();
    String fieldsqr = '';
    Schema.SObjectType ctype = Schema.getGlobalDescribe().get('Configuration__c');
    Map<String, Schema.SObjectField> editflds = ctype.getDescribe().fields.getMap();

    for(String fieldName : editflds.keySet())
        if(editflds.get(fieldName).getDescribe().isUpdateable()&&String.valueof(editflds.get(fieldName).getDescribe().getType())!='REFERENCE'&&fieldName.tolowercase()!='name'){
            fields.add(fieldName);
            fieldsqr+=fieldName+',';
        }

    fieldsqr = fieldsqr.left(fieldsqr.length()-1);
    Set <String> types = new Set <String>();
    for(Configuration_Map__mdt conf:[select API_Field__c from Configuration_Map__mdt Order by Label])
        types.add(conf.API_Field__c);

    Set <String> ids4select = new Set <String>();
    Set <String> confids = new Set <String>();
    List <Configuration__c> conf4change = new List <Configuration__c>();
    for(Configuration__c conf:Trigger.New)
        if(mains.contains(conf.RecordTypeId))
            for(String type:types)
                if(Trigger.isInsert){
                    if(conf.get(type)!=null&&conf.get(type)!=''){
                        conf4change.add(conf);
                        ids4select.add(String.valueof(conf.get(type)));
                    }
                }
                else{
                    confids.add(conf.id);
                    if(conf.get(type)!=null&&conf.get(type)!=''&&conf.get(type)!=Trigger.OldMap.get(conf.id).get(type)){
                        conf4change.add(conf);
                        ids4select.add(String.valueof(conf.get(type)));
                    }
                }
    
    List <Configuration__c> tempSubs = Database.query('select id,'+fieldsqr+' from Configuration__c Where id in: ids4select');
    Map <String,Configuration__c> Subs = new Map <String,Configuration__c>();
    for(Configuration__c conf:tempSubs)
        Subs.put(conf.id,conf);
    
    for(Configuration__c conf:conf4change)
        for(String type:types)
            if(Trigger.isInsert){
                if(conf.get(type)!=null&&conf.get(type)!='')
                    for(String fld:fields)
                        if(Subs.get(String.valueof(conf.get(type))).get(fld)!=null&&Subs.get(String.valueof(conf.get(type))).get(fld)!='')
                            conf.put(fld, Subs.get(String.valueof(conf.get(type))).get(fld));
            }
            else{
                if(conf.get(type)!=null&&conf.get(type)!=''&&conf.get(type)!=Trigger.OldMap.get(conf.id).get(type))
                    for(String fld:fields)
                        if(Subs.get(String.valueof(conf.get(type))).get(fld)!=null&&Subs.get(String.valueof(conf.get(type))).get(fld)!='')
                            conf.put(fld, Subs.get(String.valueof(conf.get(type))).get(fld));
            }
    try{
        List <Asset__c> assets = [select id,Set_Config__c from Asset__c where Set_Config__c=false and Configuration__c in: confids];
        for(Asset__c ass:assets)
            ass.Set_Config__c = true;
        update assets;
    }
    catch(exception e){}
}
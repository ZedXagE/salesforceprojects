/**
 * Created by nkarmi on 11/20/2018.
 */

trigger AssetTrigger on Asset__c (after insert, after update) {
    TRG_Asset.run(trigger.newMap, trigger.oldMap, trigger.isBefore, trigger.isAfter, trigger.isInsert, trigger.isUpdate, trigger.isDelete, trigger.isUndelete);
}
/*
 * @Author: 
                                                        `/////                                                                                         
    smmmmmmmmmmmmmmmm-                                 .NMMMM`  :mmmmmd-      -dmmNmd-                                           /mmmNmmmmmmmmmmm:    
    yMMMMMMMMMMMMMMMM-                                 .NMMMM`   .dMMMMN+    +NMMMMy`                                            +MMMMMMMMMMMMMMM:    
    .--------+NMMMMMh`      `:+osyso+:`         :+syso/:MMMMM`    `sMMMMMy``hMMMMN/     `:/+osyysso/-`        `:+syso/.:::::`    +MMMMM/---------`    
           `sMMMMMm:      :hMMMMMMMMMMNs`    `oNMMMMMMMMMMMMM`      :mMMMMmmMMMMy`      .MMMMMMMMMMMMNo`    `sNMMMMMMMMMMMMM:    +MMMMM.              
          /NMMMMNo`     `yMMMMo-``.oMMMMd`  `dMMMMm+:-:+MMMMM`       `yMMMMMMMN/        `y+:.```-+mMMMMo   `dMMMMNo:--/NMMMM:    +MMMMMNNNNNNNNNs     
        -dMMMMMh.       +MMMMh::::::dMMMM+  yMMMMd`    .NMMMM`        `dMMMMMMs            `-:/+oodMMMMy   sMMMMN.     mMMMM:    +MMMMMMMMMMMMMMs     
      `sMMMMMm:         hMMMMMMMMMMMMMMMMs  mMMMMo     .NMMMM`       -mMMMMMMMMh.       .smMMMMMMMMMMMMy   yMMMMh      mMMMM:    +MMMMM:--------.     
     /NMMMMNo`          yMMMMh:::::::::::.  mMMMMs     .NMMMM`      oNMMMModMMMMN/     -NMMMMo:.` oMMMMy   sMMMMm`     mMMMM:    +MMMMM.              
   `dMMMMMNo/////////:  -NMMMMo.      ./s.  oMMMMN/`  `+MMMMM`    .hMMMMm: `sMMMMMy`   oMMMMd     sMMMMy   .NMMMMmo//oyMMMMM:    +MMMMM+/////////.    
   -MMMMMMMMMMMMMMMMMm   -dMMMMMNdddmNMMM-  `yMMMMMMNNMMMMMMM`   /NMMMMh`    /NMMMMm-  -NMMMMmyyymMMMMMy    -hMMMMMMMMNMMMMM:    +MMMMMMMMMMMMMMM:    
   .hddddddddddddddddy     -ohmNMMMMMNmho.    :ymMMMNh+-hdddh`  +hdddd+       .hddddh-  .odNMMNds:+ddddo      ./osys+:`MMMMM-    /ddddddddddddddd-    
                                `````            ```                                        ``              `+/-.` `.:hMMMMd                          
                                                                                                            `NMMMMMMMMMMMMy`                          
                                                                                                            `oyyhddddhyo/`                            
 * @Date: 2018-11-25 14:26:38 
 * @Last Modified by:   ZedXagE - ITmyWay 
 * @Last Modified time: 2018-11-25 14:26:38 
 */

trigger CheckUniqueRole on Product2 (before insert, before update) {
        Boolean error=false;
        Set <String> parids = new Set <String>();
        Map <String,String> rolebyParent = new Map <String,String>();
        Map <String,String> rolebyGrandParent = new Map <String,String>();
        for(Product2 prod:Trigger.New)
            if((Trigger.isInsert||(Trigger.isUpdate&&prod.Role__c!=Trigger.Oldmap.get(prod.id).Role__c))&&prod.Parent_Product__c!=null&&prod.Role__c!=null){
                rolebyParent.put(prod.Role__c,prod.Parent_Product__c);
                rolebyGrandParent.put(prod.Role__c,prod.Parent_Product__c);
                parids.add(prod.Parent_Product__c);
            }

        for(Product2 prod:[select id,Head_Of_Tree__c from Product2 where id in:parids])
            for(String k:rolebyGrandParent.keySet())
                if(rolebyGrandParent.get(k)==prod.id){
                    rolebyGrandParent.put(k,prod.Head_Of_Tree__c);
                }

        for(Product2 prod:[select id,Parent_Product__c,Head_Of_Tree__c,Role__c from Product2 where Role__c in:rolebyParent.keyset()])
            if(prod.Head_Of_Tree__c==rolebyGrandParent.get(prod.Role__c)&&prod.Parent_Product__c!=rolebyParent.get(prod.Role__c))
                error = true;
        if(error)
            Trigger.New[0].addError('Role already exsits on this tree');

}
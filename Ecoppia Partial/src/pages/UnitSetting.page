<apex:page StandardController="Site__c" language="en" extensions="UnitSettingController" showHeader="false" sidebar="false" standardStylesheets="false" docType="html-5.0">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
        <link rel="stylesheet" href="{!URLFOR($Resource.ZedXagEToolBox, 'css/bootstrap.min.css')}" crossorigin="anonymous"/>
        <link rel="stylesheet" href="{!URLFOR($Resource.ZedXagEToolBox, 'css/all.css')}" crossorigin="anonymous"/>
        <script src="{!URLFOR($Resource.ZedXagEToolBox, 'js/jquery-3.3.1.min.js')}" crossorigin="anonymous"></script>
        <script src="{!URLFOR($Resource.ZedXagEToolBox, 'js/popper.min.js')}" crossorigin="anonymous"></script>
        <script src="{!URLFOR($Resource.ZedXagEToolBox, 'js/bootstrap.min.js')}" crossorigin="anonymous"></script>
        <style>
            label{
                display: block !important;
            }
            table{
                margin: 0px !important;
            }
            th{
                vertical-align: middle !important;
            }
            .loader {
                width: 100px;
                height: 100px;
                top: 50%;
                left: 50%;
                margin-top: -50px;
                margin-left: -50px;
                animation: spin 2s linear infinite;
                display: block;
                position: fixed;
            }
            .overlay {
                display:none;
                opacity:0.8;
                background-color:#ccc;
                position:fixed;
                width:100%;
                height:100%;
                top:0px;
                left:0px;
                z-index:1000;
            }
            @keyframes spin {
                0% { transform: rotate(0deg); }
                100% { transform: rotate(360deg); }
            }
            .container{
                padding-right:0px !important;
                padding-left:0px !important;
            }
            .custom-control-label::after{
                width: 1.75rem !important;
                height: 1.75rem !important;
                top: 0 !important;
                left: -1.75rem !important;
            }
            .custom-control-label::before{
                width: 1.75rem !important;
                height: 1.75rem !important;
                background-color: grey;
                top: 0 !important;
                left: -1.75rem !important;
            }
            .orange{
                background-color: orange !important;
                border-color: orange !important;
            }
            .hard{
                border-color: #dc3545 !important;
                border-width: .15rem !important;
            }
            .soft{
                border-color: #28a745 !important;
                border-width: .15rem !important;
            }
            .read{
                border-color: #007bff !important;
                border-width: .15rem !important;
            }
            .set{
                border-color: #28a745 !important;
                border-width: .15rem !important;
            }
            .unit{
                border-color: #ffc107 !important;
                border-width: .15rem !important;
            }
        </style>
        <script>
            $ = jQuery.noConflict();
            function load(){
                var datsok = true; 
                $.each( $('.dateschoose'), function( i, rec ) {
                    if(rec.checkValidity()==false)
                        datsok = false;
                });
                if(datsok)
                    document.getElementById('overlay').style.display = 'block';
            }
            function ruSure(val){
                var c = confirm("Are you Sure?");
                if (c == true) {
                    load();
                    window[val]();
                }
            }
            function selectAll(obj){
                var ck = $(obj).is(':checked');
                $.each( $('.oneclick'), function( i, rec ) {
                    if($(rec).is(':checked')!=ck)
                        $(rec).click();
                });
            }
        </script>
    </head>
    <body class="bg-transparent">
        <div id="container" class="container-fluid">
            <apex:outPutPanel id="none"></apex:outPutPanel>
            <apex:outPutPanel id="all">
                <script>
                    $( document ).ready(function() {
                        if('{!$User.UIThemeDisplayed}'=='Theme4t')
                            $(".links").removeAttr('href');
                    }); 
                </script>
                <apex:form >
                    <div class="row">
                        <div class="col" align="center">
                            <apex:actionFunction name="sendRowStartClean" action="{!sendRowStartClean}" reRender="all"/>
                            <apex:actionFunction name="sendRowStopClean" action="{!sendRowStopClean}" reRender="all"/>
                            <apex:actionFunction name="sendMaintenance" action="{!sendMaintenance}" reRender="all"/>
                            <apex:actionFunction name="sendRowHardConfig" action="{!sendRowHardConfig}" reRender="all"/>
                            <apex:actionFunction name="sendRowSoftConfig" action="{!sendRowSoftConfig}" reRender="all"/>
                            <apex:actionFunction name="sendUnitConfig" action="{!sendUnitConfig}" reRender="all"/>
                            <apex:commandButton styleclass="btn btn-success font-weight-bold shadow" value="Start Clean" onclick="ruSure('sendRowStartClean');" reRender="none"/>&nbsp;
                            <apex:commandButton styleclass="btn btn-danger font-weight-bold shadow" value="Stop Clean" onclick="ruSure('sendRowStopClean');" reRender="none"/>&nbsp;
                            <apex:commandButton styleclass="btn btn-primary font-weight-bold shadow" value="Keep Alive" action="{!sendRowKeepAlive}" onclick="load();" reRender="all"/>&nbsp;
                            <apex:commandButton styleclass="btn btn-warning font-weight-bold shadow" value="Read Logs" action="{!sendRowReadLogs}" onclick="load();" reRender="all"/>&nbsp;
                            <apex:commandButton styleclass="btn btn-light font-weight-bold shadow orange" value="Maintenance" onclick="ruSure('sendMaintenance');" reRender="none"/>&nbsp;
                            <apex:commandButton styleclass="btn btn-light font-weight-bold shadow hard" value="Hard Config" onclick="ruSure('sendRowHardConfig');" reRender="none"/>&nbsp;
                            <apex:commandButton styleclass="btn btn-light font-weight-bold shadow soft" value="Soft Config" onclick="ruSure('sendRowSoftConfig');" reRender="none"/>&nbsp;
                            <apex:commandButton styleclass="btn btn-light font-weight-bold shadow unit" value="Unit Config" onclick="ruSure('sendUnitConfig');" reRender="none"/>&nbsp;
                            <apex:commandButton styleclass="btn btn-light font-weight-bold shadow read" value="Read Config" action="{!sendRowReadConfig}" onclick="load();" reRender="all"/>
                        </div>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col">
                            <div align="right">
                                <div class="btn-group" role="group">
                                    <apex:commandLink styleclass="btn btn-dark font-weight-bold shadow" action="{!Search}" onclick="load();" reRender="all">
                                        <i class="fas fa-search"></i>
                                    </apex:commandLink>
                                    <apex:commandLink styleclass="btn btn-primary font-weight-bold shadow" action="{!AssetFilter.Toggle}" onclick="load();" reRender="filt">
                                        <i class="fas fa-filter"></i>
                                    </apex:commandLink>
                                </div>
                            </div>
                        </div>
                    </div>
                    <apex:outputPanel id="filt">
                        <script>
                            $(function () {
                                $('[data-toggle="popover"]').popover();
                            })
                        </script>
                        <div id="overlay" align="center" class="overlay"><div id="load" class="loader"><i class="fas fa-cog" style="font-size: 100px;"></i></div></div>
                        <div id="filters" class="my-3 p-1 bg-light rounded shadow" style="{!if(AssetFilter.Showed=true,'','display:none;')}">
                            <div class="row">
                                <div class="col my-auto">
                                    <apex:commandLink styleclass="btn btn-secondary shadow" action="{!AssetFilter.addFilter}" onclick="load();" reRender="filt">
                                        <i class="fas fa-plus"></i>
                                    </apex:commandLink>
                                </div>
                                <div class="col my-auto" align="center">
                                    <h6>
                                        Filters  
                                    </h6>
                                </div>
                                <div class="col" align="right">
                                    <button type="button" class="btn btn-info" data-container="body" data-toggle="popover" data-trigger="focus" data-placement="left" data-content="Used to filter by condtions, in text fields use comma seperated values for 'OR' condition.">
                                        <i class="fas fa-question"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-sm bg-light">
                                    <tbody>
                                        <apex:repeat var="fil" value="{!AssetFilter.filters}">
                                            <tr>
                                                <td>
                                                    <apex:commandLink styleclass="btn btn-danger" action="{!AssetFilter.deleteFilter}" onclick="load();" reRender="filt">
                                                        <apex:param name="curFil" value="{!fil.id}" assignTo="{!AssetFilter.curFil}"/>
                                                        <i class="fas fa-minus"></i>
                                                    </apex:commandLink>
                                                </td>
                                                <td>
                                                    <apex:selectList styleclass="form-control" value="{!fil.field}" multiselect="false" size="1">
                                                        <apex:actionSupport event="onchange" action="{!fil.changeField}" reRender="fldtyp,condtyp"/>
                                                        <apex:selectOptions value="{!fil.fields}"/>
                                                    </apex:selectList>
                                                </td>
                                                <td>
                                                    <apex:outputPanel id="condtyp">
                                                        <apex:selectList styleclass="form-control" value="{!fil.condition}" multiselect="false" size="1">
                                                            <apex:selectOptions value="{!fil.conditions}"/>
                                                        </apex:selectList>
                                                    </apex:outputPanel>
                                                </td>
                                                <td style="vertical-align: middle;">
                                                    <apex:outputPanel id="fldtyp">
                                                        <apex:outputPanel id="fldtyp2" rendered="{!fil.field!=null}">
                                                            <apex:input type="text" styleclass="form-control" value="{!fil.freetxt}" rendered="{!AND(AssetFilter.fieldstype[fil.field]!='PICKLIST',AssetFilter.fieldstype[fil.field]!='BOOLEAN',AssetFilter.fieldstype[fil.field]!='DATE',AssetFilter.fieldstype[fil.field]!='DATETIME')}"/>
                                                            <div class="custom-control custom-checkbox" style="{!if(AssetFilter.fieldstype[fil.field]='BOOLEAN','','display:none;')}">
                                                                <apex:inputCheckBox id="chkcon" styleclass="custom-control-input" value="{!fil.bool}" rendered="{!AssetFilter.fieldstype[fil.field]='BOOLEAN'}"/>
                                                                <apex:outputLabel styleclass="custom-control-label" for="chkcon" rendered="{!AssetFilter.fieldstype[fil.field]='BOOLEAN'}"/>
                                                            </div>
                                                            <apex:input type="datetime-local" styleclass="form-control dateschoose" value="{!fil.dat}" rendered="{!OR(AssetFilter.fieldstype[fil.field]='DATE',AssetFilter.fieldstype[fil.field]='DATETIME')}"/>
                                                            <apex:selectList styleclass="form-control" value="{!fil.picklistval}" multiselect="false" size="1" rendered="{!AssetFilter.fieldstype[fil.field]='PICKLIST'}">
                                                                <apex:selectOptions value="{!fil.picklist}"/>
                                                            </apex:selectList>
                                                        </apex:outputPanel>
                                                    </apex:outputPanel>
                                                </td>
                                            </tr>
                                        </apex:repeat>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </apex:outputPanel>
                    <apex:repeat var="msg" value="{!msgs}">
                        <br/>
                        <div class="alert alert-{!msg.typ} alert-dismissible fade show" role="alert">
                            <apex:outputText value="{!msg.msg}" escape="false"/>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </apex:repeat>
                    <script>
                    $( document ).ready(function() {
                        setTimeout(function() {
                            $('.alert-success').hide(500);
                        }, 7000);
                    }); 
                    </script>
                    <div class="my-3 p-1 bg-light rounded shadow">
                        <div class="table-responsive">
                            <table class="table table-sm table-hover bg-light">
                                <thead>
                                    <tr>
                                        <th scope="col">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" id="allchk" class="custom-control-input" onclick="selectAll(this);"/>
                                                <label class="custom-control-label" for="allchk"/>
                                            </div>
                                        </th>
                                        <th scope="col">
                                            <apex:commandLink value="Name" action="{!Search}" onclick="load();" reRender="all">
                                                <apex:param name="changesortname" value="Name" assignTo="{!fieldsort}"/>
                                                <apex:param name="sortdirectionname" value="{!if(sortdir='ASC','DESC','ASC')}" assignTo="{!sortdir}"/>
                                            </apex:commandLink>
                                        </th>
                                        <apex:repeat var="tab" value="{!tabs}">
                                            <th scope="col">
                                                <apex:commandLink value="{!tab.Label}" action="{!Search}" onclick="load();" reRender="all">
                                                    <apex:param name="changesort" value="{!tab.API_Field__c}" assignTo="{!fieldsort}"/>
                                                    <apex:param name="sortdirection" value="{!if(sortdir='ASC','DESC','ASC')}" assignTo="{!sortdir}"/>
                                                </apex:commandLink>
                                            </th>
                                        </apex:repeat>
                                    </tr>
                                </thead>
                                <tbody>
                                    <apex:repeat var="rec" value="{!robotssort}">
                                        <tr>
                                            <td style="width:1px;">
                                                <div class="custom-control custom-checkbox">
                                                    <apex:inputCheckBox id="chk" styleclass="custom-control-input oneclick" value="{!Robots[rec].checked}"/>
                                                    <apex:outputLabel styleclass="custom-control-label" for="chk"/>
                                                </div>
                                            </td>
                                            <td><a class="links" href="/{!rec}" target="_blank">{!Robots[rec].Asset.Name}</a></td>
                                            <apex:repeat var="tab" value="{!tabs}">
                                                <td>
                                                    <Apex:outputField value="{!Robots[rec].Asset[tab.API_Field__c]}"/>
                                                </td>
                                            </apex:repeat>
                                        </tr>
                                    </apex:repeat>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </apex:form>
            </apex:outPutPanel>
        </div>
    </body>
</apex:page>
/*
 * @Author: 
                                                        `/////                                                                                         
    smmmmmmmmmmmmmmmm-                                 .NMMMM`  :mmmmmd-      -dmmNmd-                                           /mmmNmmmmmmmmmmm:    
    yMMMMMMMMMMMMMMMM-                                 .NMMMM`   .dMMMMN+    +NMMMMy`                                            +MMMMMMMMMMMMMMM:    
    .--------+NMMMMMh`      `:+osyso+:`         :+syso/:MMMMM`    `sMMMMMy``hMMMMN/     `:/+osyysso/-`        `:+syso/.:::::`    +MMMMM/---------`    
           `sMMMMMm:      :hMMMMMMMMMMNs`    `oNMMMMMMMMMMMMM`      :mMMMMmmMMMMy`      .MMMMMMMMMMMMNo`    `sNMMMMMMMMMMMMM:    +MMMMM.              
          /NMMMMNo`     `yMMMMo-``.oMMMMd`  `dMMMMm+:-:+MMMMM`       `yMMMMMMMN/        `y+:.```-+mMMMMo   `dMMMMNo:--/NMMMM:    +MMMMMNNNNNNNNNs     
        -dMMMMMh.       +MMMMh::::::dMMMM+  yMMMMd`    .NMMMM`        `dMMMMMMs            `-:/+oodMMMMy   sMMMMN.     mMMMM:    +MMMMMMMMMMMMMMs     
      `sMMMMMm:         hMMMMMMMMMMMMMMMMs  mMMMMo     .NMMMM`       -mMMMMMMMMh.       .smMMMMMMMMMMMMy   yMMMMh      mMMMM:    +MMMMM:--------.     
     /NMMMMNo`          yMMMMh:::::::::::.  mMMMMs     .NMMMM`      oNMMMModMMMMN/     -NMMMMo:.` oMMMMy   sMMMMm`     mMMMM:    +MMMMM.              
   `dMMMMMNo/////////:  -NMMMMo.      ./s.  oMMMMN/`  `+MMMMM`    .hMMMMm: `sMMMMMy`   oMMMMd     sMMMMy   .NMMMMmo//oyMMMMM:    +MMMMM+/////////.    
   -MMMMMMMMMMMMMMMMMm   -dMMMMMNdddmNMMM-  `yMMMMMMNNMMMMMMM`   /NMMMMh`    /NMMMMm-  -NMMMMmyyymMMMMMy    -hMMMMMMMMNMMMMM:    +MMMMMMMMMMMMMMM:    
   .hddddddddddddddddy     -ohmNMMMMMNmho.    :ymMMMNh+-hdddh`  +hdddd+       .hddddh-  .odNMMNds:+ddddo      ./osys+:`MMMMM-    /ddddddddddddddd-    
                                `````            ```                                        ``              `+/-.` `.:hMMMMd                          
                                                                                                            `NMMMMMMMMMMMMy`                          
                                                                                                            `oyyhddddhyo/`                            
 * @Date: 2018-11-25 14:26:38 
 * @Last Modified by: ZedXagE - ITmyWay
 * @Last Modified time: 2018-11-25 14:27:33
 */

public class HourlyMaintenanceHandler {
	private Map <String, Site__c> sites;
	private Map <String, List <Asset__c>> assets;
	private Set <String> mentids;
	private DateTime nowtime;
	private String malfuncid; 
	public HourlyMaintenanceHandler() {
		TimeZone tz = UserInfo.getTimeZone();
		nowtime = System.Now();
		//nowtime = nowtime.AddSeconds(tz.getOffset(nowtime)/1000);
		malfuncid = [select id from WorkType where name='Malfunction' limit 1].id;
		mentids = new Set <String>();
		for(Relationship_Users__c rel:[select User__c from Relationship_Users__c where Level__c='SF Alerts'])
			mentids.add(rel.User__c);
		sites = new Map <String, Site__c>([select id,Main_Technician__r.RelatedRecordId,Next_Scheduled_Clean__c,Clean_Timeout__c,Low_Battery_Days__c,Low_Clean_Percentage_Days__c,Minimum_Battery_Voltage__c,Minimum_Clean_Percentage__c from Site__c where RecordType.Name='Site' and Status__c='Active' and ((Next_Scheduled_Clean__c!=null and Next_Scheduled_Clean__c>:nowtime and Next_Scheduled_Clean__c<=:nowtime.addHours(1)) OR (Clean_Timeout__c!=null and Clean_Timeout__c<:nowtime and Clean_Timeout__c>=:nowtime.addHours(-1)))]);
		assets = new Map <String , List <Asset__c>>();
		for(String k:sites.keySet())
			assets.put(k,new List <Asset__c>());
		for(Asset__c ass:[select id,Name,Site__c,Site__r.Account__c,Battery_At_End__c,Clean_Percentage__c,Low_Battery_Days_Counter__c,Low_Clean_Percentage_Days_Counter__c,State__c,(select id,Subject,WorkType.Name from Work_Orders__r where Status not in: new Set <String>{'Completed','Closed'}) from Asset__c where Site__c in:sites.keySet() and RecordType.Name='Robotic Unit'])
			assets.get(ass.Site__c).add(ass);
		//for debug
		System.debug('Time: '+nowtime);
		for(String k:sites.keySet()){
			System.debug('Site: '+sites.get(k));
			System.debug('Assets: '+assets.get(k));
		}
		System.debug('Mentions: '+mentids);
	}
	private Decimal NextNum(Decimal num){
		return num!=null?num+1:1;
	}
	public void StartCheck(){
		List <Asset__c> ass4up = new List <Asset__c>();
		List <WorkOrder> woks4ins = new List <WorkOrder>();
		for(String k:sites.KeySet()){
			Integer numofRow = 0;
			String names = '';
			for(Asset__c ass:assets.get(k)){
				Boolean needadd = false;
				//Battery WO
				if(sites.get(k).Next_Scheduled_Clean__c!=null&&sites.get(k).Next_Scheduled_Clean__c>nowtime&&sites.get(k).Next_Scheduled_Clean__c<=nowtime.addHours(1)&&sites.get(k).Minimum_Battery_Voltage__c!=null&&ass.Battery_At_End__c!=null&&ass.Battery_At_End__c<sites.get(k).Minimum_Battery_Voltage__c){
					ass.Low_Battery_Days_Counter__c = NextNum(ass.Low_Battery_Days_Counter__c);
					if(sites.get(k).Low_Battery_Days__c!=null&&ass.Low_Battery_Days_Counter__c>=sites.get(k).Low_Battery_Days__c)
						ass.Low_Battery_Days_Counter__c = 0;
					needadd = true;
					checkCreateWO(ass,woks4ins,'Battery',sites.get(k).Main_Technician__r.RelatedRecordId);
				}
				//Partial Cleaning
				if(sites.get(k).Clean_Timeout__c!=null&&sites.get(k).Clean_Timeout__c<nowtime&&sites.get(k).Clean_Timeout__c>=nowtime.addHours(-1)&&sites.get(k).Minimum_Clean_Percentage__c!=null&&ass.Clean_Percentage__c!=null&&ass.Clean_Percentage__c<sites.get(k).Minimum_Clean_Percentage__c){
					ass.Low_Clean_Percentage_Days_Counter__c = NextNum(ass.Low_Clean_Percentage_Days_Counter__c);
					if(sites.get(k).Low_Clean_Percentage_Days__c!=null&&ass.Low_Clean_Percentage_Days_Counter__c>=sites.get(k).Low_Clean_Percentage_Days__c)
						ass.Low_Clean_Percentage_Days_Counter__c = 0;
					needadd = true;
					checkCreateWO(ass,woks4ins,'Clean',sites.get(k).Main_Technician__r.RelatedRecordId);
				}
				//Unit on Row WO
				if(sites.get(k).Clean_Timeout__c!=null&&sites.get(k).Clean_Timeout__c<nowtime&&sites.get(k).Clean_Timeout__c>=nowtime.addHours(-1)&&ass.State__c!='At base'){
					needadd = true;
					numofRow ++;
					names += ass.Name+', ';
					checkCreateWO(ass,woks4ins,'Row',sites.get(k).Main_Technician__r.RelatedRecordId);
				}
				if(needadd)	ass4up.add(ass);
			}
			if(numofRow>0){
				names = names.left(names.length()-2);
				mentionFeed(String.valueof(numofRow),k,mentids,names);
			}
		}
		update ass4up;
		insert woks4ins;
	}
	public void checkCreateWO(Asset__c ass, List <WorkOrder> woks, String type,String Ownerid){
		String subject;
		if(type=='Battery')
			subject = 'Low Battery';
		else if(type=='Clean')
			subject = 'Low Clean Percentage';
		else if(type=='Row')
			subject = 'Unit On Row';
		Boolean needed = true;
		for(WorkOrder wok:ass.Work_Orders__r){
			if(type=='Battery'){
				if(wok.Subject=='Low Battery'){	needed = false;	break;	}
			}
			else if(type=='Clean'){
				if(wok.Subject=='Low Clean Percentage'){	needed = false;	break;	}
			}
			else if(type=='Row'){
				if(wok.WorkType.Name=='Malfunction'){	needed = false;	break;	}
			}
		}
		if(needed||Test.IsRunningTest()){
			WorkOrder wok = new WorkOrder();
			wok.Asset__c = ass.id;
			wok.AccountId = ass.Site__r.Account__c;
			wok.Site__c = ass.Site__c;
			wok.Subject = subject;
			wok.Priority = 'Medium';
			wok.StartDate = nowtime;
			wok.WorkTypeid = malfuncid;
			wok.Status = 'New';
			if(Ownerid!=null)	wok.Ownerid = Ownerid;
			woks.add(wok);
		}
	}
	public static void mentionFeed(String num, String subjectid, Set <String> mentionids,String txt){
        ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
        ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
        ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
        messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
		for(String k:mentionids){
			ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
			mentionSegmentInput.id = k;
			messageBodyInput.messageSegments.add(mentionSegmentInput);
		}
        textSegmentInput.text = ' Alert: There are '+num+' Robotic Unit on the Rows: '+txt;
        messageBodyInput.messageSegments.add(textSegmentInput);
        feedItemInput.body = messageBodyInput;
        feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
        feedItemInput.subjectId = subjectid;
        ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput);
    }
}
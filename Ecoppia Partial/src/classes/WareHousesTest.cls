/*
 * @Author:                                                                                                                                                      
                                                       `/////                                                                                         
    smmmmmmmmmmmmmmmm-                                 .NMMMM`  :mmmmmd-      -dmmNmd-                                           /mmmNmmmmmmmmmmm:    
    yMMMMMMMMMMMMMMMM-                                 .NMMMM`   .dMMMMN+    +NMMMMy`                                            +MMMMMMMMMMMMMMM:    
    .--------+NMMMMMh`      `:+osyso+:`         :+syso/:MMMMM`    `sMMMMMy``hMMMMN/     `:/+osyysso/-`        `:+syso/.:::::`    +MMMMM/---------`    
           `sMMMMMm:      :hMMMMMMMMMMNs`    `oNMMMMMMMMMMMMM`      :mMMMMmmMMMMy`      .MMMMMMMMMMMMNo`    `sNMMMMMMMMMMMMM:    +MMMMM.              
          /NMMMMNo`     `yMMMMo-``.oMMMMd`  `dMMMMm+:-:+MMMMM`       `yMMMMMMMN/        `y+:.```-+mMMMMo   `dMMMMNo:--/NMMMM:    +MMMMMNNNNNNNNNs     
        -dMMMMMh.       +MMMMh::::::dMMMM+  yMMMMd`    .NMMMM`        `dMMMMMMs            `-:/+oodMMMMy   sMMMMN.     mMMMM:    +MMMMMMMMMMMMMMs     
      `sMMMMMm:         hMMMMMMMMMMMMMMMMs  mMMMMo     .NMMMM`       -mMMMMMMMMh.       .smMMMMMMMMMMMMy   yMMMMh      mMMMM:    +MMMMM:--------.     
     /NMMMMNo`          yMMMMh:::::::::::.  mMMMMs     .NMMMM`      oNMMMModMMMMN/     -NMMMMo:.` oMMMMy   sMMMMm`     mMMMM:    +MMMMM.              
   `dMMMMMNo/////////:  -NMMMMo.      ./s.  oMMMMN/`  `+MMMMM`    .hMMMMm: `sMMMMMy`   oMMMMd     sMMMMy   .NMMMMmo//oyMMMMM:    +MMMMM+/////////.    
   -MMMMMMMMMMMMMMMMMm   -dMMMMMNdddmNMMM-  `yMMMMMMNNMMMMMMM`   /NMMMMh`    /NMMMMm-  -NMMMMmyyymMMMMMy    -hMMMMMMMMNMMMMM:    +MMMMMMMMMMMMMMM:    
   .hddddddddddddddddy     -ohmNMMMMMNmho.    :ymMMMNh+-hdddh`  +hdddd+       .hddddh-  .odNMMNds:+ddddo      ./osys+:`MMMMM-    /ddddddddddddddd-    
                                `````            ```                                        ``              `+/-.` `.:hMMMMd                          
                                                                                                            `NMMMMMMMMMMMMy`                          
                                                                                                            `oyyhddddhyo/`                            
 * @Date: 2018-11-22 13:56:38 
 * @Last Modified by: ZedXagE - ITmyWay
 * @Last Modified time: 2018-11-22 14:12:40
 */

@isTest(seealldata=true)
public class WareHousesTest {
	static TestMethod void tesWareHouses() {
    Asset__c robo = [select id,Site__c from Asset__c where RecordType.Name='Robotic Unit' and Site__c!=null and Site__r.RecordType.Name='Warehouse' and Parent_Asset__c=null limit 1];
		Asset__c ass1 = [select id,Site__c from Asset__c where RecordType.Name='Asset line' and Site__c!=null and Site__r.RecordType.Name='Warehouse' and Parent_Asset__c!=null limit 1];
		Site__c site2 = [select id from Site__c where id!=:ass1.Site__c and id!=:robo.Site__c and RecordType.Name='Warehouse' limit 1];
		WareHousesController con = new WareHousesController();
    con.params='Warehouse;'+ass1.Site__c+';Warehouse;'+site2.id;
		con.search();
    for(String g:con.groups.keyset()){
      for(String s:con.groups.get(g).lines.keyset()){
        con.groups.get(g).lines.get(s).checked = true;
        for(String sk:con.groups.get(g).lines.get(s).subs.keyset()){
          con.groups.get(g).lines.get(s).subs.get(sk).checked = true;
        }
      }
    }
		con.Move();
    con.params='Warehouse;'+robo.Site__c+';All;'+site2.id;
		con.search();
    for(String g:con.groups.keyset()){
      for(String s:con.groups.get(g).lines.keyset()){
        con.groups.get(g).lines.get(s).checked = true;
        for(String sk:con.groups.get(g).lines.get(s).subs.keyset()){
          con.groups.get(g).lines.get(s).subs.get(sk).checked = true;
        }
      }
    }
		con.Move();
	}
}
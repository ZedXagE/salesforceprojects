/**
 * Created by nkarmi on 11/26/2018.
 */

public with sharing class SendAssetConfigExtension {
    private Asset__c selectedAsset;
    private List<String> assetIds;
    public SendAssetConfigExtension(ApexPages.StandardController standardController) {
        if (standardController.getId() != null) {
            this.selectedAsset = [SELECT Id, Asset_ID__c, RecordType.DeveloperName FROM Asset__c WHERE Id = :standardController.getId()];
            this.assetIds = new List<String>{
                    this.selectedAsset.Asset_ID__c
            };
        }
    }

    public Boolean getIsCommunicationStation() {
        if (this.selectedAsset.RecordType.DeveloperName == 'Communication_Station') {
            return true;
        } else {
            return false;
        }
    }

    public void sendRowHardConfig() {
        if (this.selectedAsset != null) {
            UnitRowsCallouts.toMasterHardConfiguration(this.assetIds);
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Configuration has been sent successfully to Ecoppia service, for further information about configuration status please check Status field under Event__c object');
            ApexPages.addMessage(msg);
        }
    }

    public void sendRowSoftConfig() {
        if (this.selectedAsset != null) {
            UnitRowsCallouts.toMasterSoftConfiguration(this.assetIds);
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Configuration has been sent successfully to Ecoppia service, for further information about configuration status please check Status field under Event__c object');
            ApexPages.addMessage(msg);
        }
    }

    public void sendRowKeepAlive() {
        if (this.selectedAsset != null) {
            UnitRowsCallouts.toMasterRowOperation(this.assetIds, 'keepalive');
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Configuration has been sent successfully to Ecoppia service, for further information about configuration status please check Status field under Event__c object');
            ApexPages.addMessage(msg);
        }
    }

    public void sendRowStartClean() {
        if (this.selectedAsset != null) {
            UnitRowsCallouts.toMasterRowOperation(this.assetIds, 'startClean');
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Configuration has been sent successfully to Ecoppia service, for further information about configuration status please check Status field under Event__c object');
            ApexPages.addMessage(msg);
        }
    }

    public void sendRowStopClean() {
        if (this.selectedAsset != null) {
            UnitRowsCallouts.toMasterRowOperation(this.assetIds, 'stopClean');
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Configuration has been sent successfully to Ecoppia service, for further information about configuration status please check Status field under Event__c object');
            ApexPages.addMessage(msg);
        }
    }

    public void sendRowReadLogs() {
        if (this.selectedAsset != null) {
            UnitRowsCallouts.toMasterRowOperation(this.assetIds, 'readLogs');
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Configuration has been sent successfully to Ecoppia service, for further information about configuration status please check Status field under Event__c object');
            ApexPages.addMessage(msg);
        }
    }

    public void sendRowReadConfig() {
        if (this.selectedAsset != null) {
            UnitRowsCallouts.toMasterRowOperation(this.assetIds, 'readConfiguration');
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Configuration has been sent successfully to Ecoppia service, for further information about configuration status please check Status field under Event__c object');
            ApexPages.addMessage(msg);
        }
    }

    public void sendRowMaintenance() {
        if (this.selectedAsset != null) {
            UnitRowsCallouts.toMasterRowOperation(this.assetIds, 'maintenance');
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Configuration has been sent successfully to Ecoppia service, for further information about configuration status please check Status field under Event__c object');
            ApexPages.addMessage(msg);
        }
    }

    public void sendUnitConfig() {
        if (this.selectedAsset != null) {
            UnitRowsCallouts.toMasterRowOperation(this.assetIds, 'setConfig');
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Configuration has been sent successfully to Ecoppia service, for further information about configuration status please check Status field under Event__c object');
            ApexPages.addMessage(msg);
        }
    }

    public void sendCommunicationHardConfig() {
        if (this.selectedAsset != null) {
            CommunicationStationsCallouts.toMasterHardConfiguration(this.assetIds);
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Configuration has been sent successfully to Ecoppia service, for further information about configuration status please check Status field under Event__c object');
            ApexPages.addMessage(msg);
        }
    }

    public void sendCommunicationSoftConfig() {
        if (this.selectedAsset != null) {
            CommunicationStationsCallouts.toMasterSoftConfiguration(this.assetIds);
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Configuration has been sent successfully to Ecoppia service, for further information about configuration status please check Status field under Event__c object');
            ApexPages.addMessage(msg);
        }
    }

    public void clearMessages(){
        ApexPages.getMessages().clear();
    }
}
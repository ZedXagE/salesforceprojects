/**
 * Created by nkarmi on 10/22/2018.
 */

public class UnitRowsCallouts {
    public static void toMasterHardConfiguration(List<String> assetIds) {
        List<Asset__c> assetList = [
                SELECT Id, Asset_ID__c, Row__r.Site__r.Site_ID__c, Name, Manufactory_ID__c, Row__r.Row_Width__c, (SELECT communication__r.Asset_ID__c FROM Units_Relationships_communications__r ORDER BY Priority__c ASC)
                FROM Asset__c
                WHERE Asset_ID__c IN :assetIds
        ];

        if (assetList.size() > 0) {
            List<Event__c> events = WebAPIUtils.generateEventsToMaster(new Map<Id, Asset__c>(assetList).keySet(), WebAPIUtils.objectEnum.ASSET, 'Configuration', 'SF Set Hard Config');
            JSONGenerator gen = JSON.createGenerator(true);
            //Start Array
            gen.writeStartArray();
            for (integer i = 0; i < assetList.size(); i++) {
                /**
                 * Variables
                 */
                Asset__c currentAsset = assetList[i];

                //Start Root Element
                WebAPIUtils.startMessageObject(gen, String.valueOf(events[i].Id), 'toMaster.row.configuration.hard');
                /**
                 *  Start Message Body Section Content
                 */
                WebAPIUtils.writeStringField(gen, 'rowId', currentAsset.Asset_ID__c);
                WebAPIUtils.writeStringField(gen, 'siteId', currentAsset.Row__r.Site__r.Site_ID__c);
                WebAPIUtils.writeStringField(gen, 'name', currentAsset.Name);
                WebAPIUtils.writeStringField(gen, 'manufactoryId', currentAsset.Manufactory_ID__c);
                WebAPIUtils.writeNumberField(gen, 'panelWidth', currentAsset.Row__r.Row_Width__c);
                if (currentAsset.Units_Relationships_communications__r.size() > 0) {
                    gen.writeFieldName('stations');
                    gen.writeStartArray();
                    for (Unit_Relationship_communication__c urc : currentAsset.Units_Relationships_communications__r) {
                        gen.writeString(urc.communication__r.Asset_ID__c);
                    }
                    gen.writeEndArray();
                }

                /**
                 *  End Message Body Content
                 */
                WebAPIUtils.endMessageObject(gen);
            }
            gen.writeEndArray();
            //End Array
            WebAPIUtils.callout(gen.getAsString(), 'POST', new Map<Id, Event__c>(events).keySet());
        }
    }

    public static void toMasterSoftConfiguration(List<String> assetIds) {
        List<Asset__c> assetList = [
                SELECT Id, Asset_ID__c, Is_Enable__c, Version__c, Default_From__c, Default_To__c, Default_Twoway__c
                FROM Asset__c
                WHERE Asset_ID__c IN :assetIds
        ];

        if (assetList.size() > 0) {
            List<Event__c> events = WebAPIUtils.generateEventsToMaster(new Map<Id, Asset__c>(assetList).keySet(), WebAPIUtils.objectEnum.ASSET, 'Configuration', 'SF Set Soft Config');
            JSONGenerator gen = JSON.createGenerator(true);
            //Start Array
            gen.writeStartArray();
            for (integer i = 0; i < assetList.size(); i++) {
                /**
                 * Variables
                 */
                Asset__c currentAsset = assetList[i];

                //Start Root Element
                WebAPIUtils.startMessageObject(gen, String.valueOf(events[i].Id), 'toMaster.row.configuration.soft');
                /**
                 *  Start Message Body Section Content
                 */
                WebAPIUtils.writeStringField(gen, 'rowd', currentAsset.Asset_ID__c);
                gen.writeBooleanField('isEnable', currentAsset.Is_Enable__c);
                gen.writeFieldName('startParameters');
                //Start startParameters object
                gen.writeStartObject();
                if (currentAsset.Version__c != 'E4_04_01' && currentAsset.Version__c != 'T4_01_01') {
                    gen.writeNullField('statPrameters_' + currentAsset.Version__c);
                } else {
                    gen.writeFieldName('statPrameters_' + currentAsset.Version__c);
                    //Start statPrameters_
                    gen.writeStartObject();
                    switch on currentAsset.Version__c {
                        when 'E4_04_01' {
                            WebAPIUtils.writeNumberField(gen, 'from', currentAsset.Default_From__c);
                            WebAPIUtils.writeNumberField(gen, 'to', currentAsset.Default_To__c);
                            gen.writeBooleanField('twoWay', currentAsset.Default_Twoway__c);
                        }
                        when 'T4_01_01' {
                            WebAPIUtils.writeNumberField(gen, 'OrdinaryFirstSegment', currentAsset.Default_From__c);
                            WebAPIUtils.writeNumberField(gen, 'OrdinaryLastSegment', currentAsset.Default_To__c);
                        }
                    }
                    gen.writeEndObject();
                    //End statPrameters_
                }
                gen.writeEndObject();
                //End startParameters object

                /**
                 *  End Message Body Content
                 */
                WebAPIUtils.endMessageObject(gen);
            }
            gen.writeEndArray();
            //End Array
            WebAPIUtils.callout(gen.getAsString(), 'POST', new Map<Id, Event__c>(events).keySet());
        }
    }

    public static void toMasterRowOperation(List<String> assetIds, String operation) {
        List<Asset__c> assetConfigs = [SELECT Version__c FROM Asset__c WHERE Asset_ID__c IN :assetIds];
        Map<String, List<Configuration_Mapping__mdt>> configurationMappingMap = WebAPIUtils.buildConfigurationMap();
        Set<String> usedConfigFields = new Set<String>();

        String queryString = 'SELECT Id, Asset_ID__c, Default_AnyWay__c, Default_Gradual__c, Version__c, Default_From__c,' +
                'Default_To__c,Default_Twoway__c';

        for (Asset__c assetConfig : assetConfigs) {
            if (configurationMappingMap.containsKey(assetConfig.Version__c)) {
                For (Configuration_Mapping__mdt cm : configurationMappingMap.get(assetConfig.Version__c)) {
                    if (!usedConfigFields.contains(cm.Ecoppia_Name__c)) {
                        queryString += ', Configuration__r.' + cm.Ecoppia_Name__c;
                        usedConfigFields.add(cm.Ecoppia_Name__c);
                    }
                }
            }
        }
        queryString += ' FROM Asset__c WHERE Asset_ID__c IN :assetIds';

        List<Asset__c> assetList = Database.query(queryString);

        if (assetList.size() > 0) {
            List<Event__c> events = WebAPIUtils.generateEventsToMaster(new Map<Id, Asset__c>(assetList).keySet(), WebAPIUtils.objectEnum.ASSET, 'Command', 'SF ' + operation);
            JSONGenerator gen = JSON.createGenerator(true);
            //Start Array
            gen.writeStartArray();
            for (integer i = 0; i < assetList.size(); i++) {
                /**
                 * Variables
                 */
                Asset__c currentAsset = assetList[i];

                //Start Root Element
                WebAPIUtils.startMessageObject(gen, String.valueOf(events[i].Id), 'toMaster.row.operation');
                /**
                 *  Start Message Body Section Content
                 */
                WebAPIUtils.writeStringField(gen, 'rowId', currentAsset.Asset_ID__c);
                gen.writeFieldName('operation');
                //Start operation object
                gen.writeStartObject();
                if (operation == 'startClean') {
                    gen.writeFieldName(operation);
                    /**
                     * Start startClean Object
                     */
                    gen.writeStartObject();
                    gen.writeBooleanField('anyway', currentAsset.Default_AnyWay__c);
                    gen.writeBooleanField('gradual', currentAsset.Default_Gradual__c);

                    gen.writeFieldName('unitPrameters');
                    //Start unitParameters object
                    gen.writeStartObject();
                    if ((currentAsset.Version__c != 'E4_04_01' && currentAsset.Version__c != 'T4_01_01') || (currentAsset.Default_From__c == null || currentAsset.Default_To__c == null)) {
                        gen.writeNullField('statPrameters_' + currentAsset.Version__c);
                    } else {
                        gen.writeFieldName('statPrameters_' + currentAsset.Version__c);
                        //Start statPrameters_
                        gen.writeStartObject();
                        switch on currentAsset.Version__c {
                            when 'E4_04_01' {
                                WebAPIUtils.writeNumberField(gen, 'from', currentAsset.Default_From__c);
                                WebAPIUtils.writeNumberField(gen, 'to', currentAsset.Default_To__c);
                                gen.writeBooleanField('twoWay', currentAsset.Default_Twoway__c);
                            }
                            when 'T4_01_01' {
                                WebAPIUtils.writeNumberField(gen, 'OrdinaryFirstSegment', currentAsset.Default_From__c);
                                WebAPIUtils.writeNumberField(gen, 'OrdinaryLastSegment', currentAsset.Default_To__c);
                            }
                        }
                        gen.writeEndObject();
                        //End statPrameters_
                    }
                    gen.writeEndObject();
                    //End unitParameters object
                    gen.writeEndObject();
                    /**
                     * End startClean Object
                     */
                } else if (operation == 'setConfig') {
                    gen.writeFieldName(operation);
                    //Start setConfig Object
                    gen.writeStartObject();
                    gen.writeFieldName('unitConfiguration');
                    //Start unitConfiguration Object
                    gen.writeStartObject();
                    if (configurationMappingMap.containsKey(currentAsset.Version__c)) {
                        for (Configuration_Mapping__mdt cm : configurationMappingMap.get(currentAsset.Version__c)) {
                            if (currentAsset.getSObject('Configuration__r') != null &&
                                    currentAsset.getSObject('Configuration__r').get(cm.Ecoppia_Name__c) != null) {
                                gen.writeObjectField(cm.Version_Name__c, currentAsset.getSObject('Configuration__r').get(cm.Ecoppia_Name__c));
                            } else {
                                gen.writeNullField(cm.Version_Name__c);
                            }
                        }
                    }
                    gen.writeEndObject();
                    //End unitConfiguration Object
                    gen.writeEndObject();
                    //End setConfig Object
                } else {
                    gen.writeNullField(operation);
                }
                gen.writeEndObject();
                //End operation object

                /**
                 *  End Message Body Content
                 */
                WebAPIUtils.endMessageObject(gen);
            }
            gen.writeEndArray();
            if(operation == 'setConfig') {
                List<Object> messages = (List<Object>) JSON.deserializeUntyped(gen.getAsString());
                for (Integer i = 0; i < messages.size(); i++) {
                    events[i].Unit_Configuration_Jason__c = JSON.serializePretty(messages[i]);
                }
                update events;
            }
            //End Array
            WebAPIUtils.callout(gen.getAsString(), 'POST', new Map<Id, Event__c>(events).keySet());
        }
    }
}
/*
 * @Author: 
                                                        `/////                                                                                         
    smmmmmmmmmmmmmmmm-                                 .NMMMM`  :mmmmmd-      -dmmNmd-                                           /mmmNmmmmmmmmmmm:    
    yMMMMMMMMMMMMMMMM-                                 .NMMMM`   .dMMMMN+    +NMMMMy`                                            +MMMMMMMMMMMMMMM:    
    .--------+NMMMMMh`      `:+osyso+:`         :+syso/:MMMMM`    `sMMMMMy``hMMMMN/     `:/+osyysso/-`        `:+syso/.:::::`    +MMMMM/---------`    
           `sMMMMMm:      :hMMMMMMMMMMNs`    `oNMMMMMMMMMMMMM`      :mMMMMmmMMMMy`      .MMMMMMMMMMMMNo`    `sNMMMMMMMMMMMMM:    +MMMMM.              
          /NMMMMNo`     `yMMMMo-``.oMMMMd`  `dMMMMm+:-:+MMMMM`       `yMMMMMMMN/        `y+:.```-+mMMMMo   `dMMMMNo:--/NMMMM:    +MMMMMNNNNNNNNNs     
        -dMMMMMh.       +MMMMh::::::dMMMM+  yMMMMd`    .NMMMM`        `dMMMMMMs            `-:/+oodMMMMy   sMMMMN.     mMMMM:    +MMMMMMMMMMMMMMs     
      `sMMMMMm:         hMMMMMMMMMMMMMMMMs  mMMMMo     .NMMMM`       -mMMMMMMMMh.       .smMMMMMMMMMMMMy   yMMMMh      mMMMM:    +MMMMM:--------.     
     /NMMMMNo`          yMMMMh:::::::::::.  mMMMMs     .NMMMM`      oNMMMModMMMMN/     -NMMMMo:.` oMMMMy   sMMMMm`     mMMMM:    +MMMMM.              
   `dMMMMMNo/////////:  -NMMMMo.      ./s.  oMMMMN/`  `+MMMMM`    .hMMMMm: `sMMMMMy`   oMMMMd     sMMMMy   .NMMMMmo//oyMMMMM:    +MMMMM+/////////.    
   -MMMMMMMMMMMMMMMMMm   -dMMMMMNdddmNMMM-  `yMMMMMMNNMMMMMMM`   /NMMMMh`    /NMMMMm-  -NMMMMmyyymMMMMMy    -hMMMMMMMMNMMMMM:    +MMMMMMMMMMMMMMM:    
   .hddddddddddddddddy     -ohmNMMMMMNmho.    :ymMMMNh+-hdddh`  +hdddd+       .hddddh-  .odNMMNds:+ddddo      ./osys+:`MMMMM-    /ddddddddddddddd-    
                                `````            ```                                        ``              `+/-.` `.:hMMMMd                          
                                                                                                            `NMMMMMMMMMMMMy`                          
                                                                                                            `oyyhddddhyo/`                            
 * @Date: 2018-11-25 14:26:38 
 * @Last Modified by:   ZedXagE - ITmyWay 
 * @Last Modified time: 2018-11-26 10:26:38 
 */

public class DeleteEventHandler {
	private Map <String,Event__c> events;
	public Integer daysback;
	public DeleteEventHandler() {
		daysback = (-1)*Integer.valueof(Label.Delete_Event);
	}
	public void StartDelete(){
		events = new Map <String,Event__c>([select id,(select id from Work_Orders__r) from Event__c where CreatedDate<:System.Now().addDays(daysback) Limit 5000]);
		Boolean anotherOne = events.keySet().Size()==5000?true:false;
		for(String k:events.keySet())
			if(events.get(k).Work_Orders__r.size()>0)
				events.remove(k);
		Database.delete(events.values(),false);
		if(anotherOne)	StartDelete();
	}
}
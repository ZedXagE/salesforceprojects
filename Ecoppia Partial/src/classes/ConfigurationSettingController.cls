/*
 * @Author:                                                                                                                                                      
                                                       `/////                                                                                         
    smmmmmmmmmmmmmmmm-                                 .NMMMM`  :mmmmmd-      -dmmNmd-                                           /mmmNmmmmmmmmmmm:    
    yMMMMMMMMMMMMMMMM-                                 .NMMMM`   .dMMMMN+    +NMMMMy`                                            +MMMMMMMMMMMMMMM:    
    .--------+NMMMMMh`      `:+osyso+:`         :+syso/:MMMMM`    `sMMMMMy``hMMMMN/     `:/+osyysso/-`        `:+syso/.:::::`    +MMMMM/---------`    
           `sMMMMMm:      :hMMMMMMMMMMNs`    `oNMMMMMMMMMMMMM`      :mMMMMmmMMMMy`      .MMMMMMMMMMMMNo`    `sNMMMMMMMMMMMMM:    +MMMMM.              
          /NMMMMNo`     `yMMMMo-``.oMMMMd`  `dMMMMm+:-:+MMMMM`       `yMMMMMMMN/        `y+:.```-+mMMMMo   `dMMMMNo:--/NMMMM:    +MMMMMNNNNNNNNNs     
        -dMMMMMh.       +MMMMh::::::dMMMM+  yMMMMd`    .NMMMM`        `dMMMMMMs            `-:/+oodMMMMy   sMMMMN.     mMMMM:    +MMMMMMMMMMMMMMs     
      `sMMMMMm:         hMMMMMMMMMMMMMMMMs  mMMMMo     .NMMMM`       -mMMMMMMMMh.       .smMMMMMMMMMMMMy   yMMMMh      mMMMM:    +MMMMM:--------.     
     /NMMMMNo`          yMMMMh:::::::::::.  mMMMMs     .NMMMM`      oNMMMModMMMMN/     -NMMMMo:.` oMMMMy   sMMMMm`     mMMMM:    +MMMMM.              
   `dMMMMMNo/////////:  -NMMMMo.      ./s.  oMMMMN/`  `+MMMMM`    .hMMMMm: `sMMMMMy`   oMMMMd     sMMMMy   .NMMMMmo//oyMMMMM:    +MMMMM+/////////.    
   -MMMMMMMMMMMMMMMMMm   -dMMMMMNdddmNMMM-  `yMMMMMMNNMMMMMMM`   /NMMMMh`    /NMMMMm-  -NMMMMmyyymMMMMMy    -hMMMMMMMMNMMMMM:    +MMMMMMMMMMMMMMM:    
   .hddddddddddddddddy     -ohmNMMMMMNmho.    :ymMMMNh+-hdddh`  +hdddd+       .hddddh-  .odNMMNds:+ddddo      ./osys+:`MMMMM-    /ddddddddddddddd-    
                                `````            ```                                        ``              `+/-.` `.:hMMMMd                          
                                                                                                            `NMMMMMMMMMMMMy`                          
                                                                                                            `oyyhddddhyo/`                            
 * @Date: 2018-11-22 13:56:38 
 * @Last Modified by:   ZedXagE - ITmyWay 
 * @Last Modified time: 2018-11-22 13:56:38 
 */

public class ConfigurationSettingController {
	private String curid;
	private String E4mainrecid;
	private String T4mainrecid;
	private String mainflds;
	public String status{get;set;}
	public String MissingMain {get;set;}
	public Map <String, String> types{get;set;}
	public List <selectoption> MainOps {get;set;}
	public Map <String, List <selectoption>> SubOps {get;set;}
	public Map <String, String> SubOpsSelected {get;set;}
	public Map <String, Configuration__c> Units{get;set;}
	public Map <String, AssetCheck> Robots{get;set;}
	public Map <String, String> RobotByUnit{get;set;}
	public Map <String, AssetCheck> RobotsUninit{get;set;}
	public Map <String, Configuration__c> Subs{get;set;}
	public Filters AssetFilter{get;set;}
	public class msg{
		public String msg{get;set;}
		public String typ{get;set;}
		public msg(String m,String t){
			msg = m;
			typ = t;
		}
	}
	public List<msg> msgs{get;set;}
	public class AssetCheck{
		public Asset__c asset{get;set;}
		public Boolean checked{get;set;}
		public AssetCheck(Asset__c ass){
			asset=ass;
			checked=false;
		}
	}
	public ConfigurationSettingController(ApexPages.StandardController controller) {
		curid = ApexPages.currentPage().getParameters().get('id');
		AssetFilter = new Filters('Asset__c');
		init();
	}
	private void init(){
		status = '1';
		msgs = new List <msg>();
		E4mainrecid = [select id from RecordType where Name='E4 - Unit' and IsActive=true and SobjectType='Configuration__c' limit 1].id;
		T4mainrecid = [select id from RecordType where Name='T4 - Unit' and IsActive=true and SobjectType='Configuration__c' limit 1].id;
		types = new Map <String,String>();
		for(Configuration_Map__mdt conf:[select Record_Type_Name__c,API_Field__c,Label from Configuration_Map__mdt Order by Label])
			types.put(conf.Record_Type_Name__c,conf.API_Field__c);

		SubOps = new Map <String, List <selectoption>>();
		SubOpsSelected = new Map <String, String>();
		Subs = new Map <String, Configuration__c>([select id,Name,RecordType.Name from Configuration__c where RecordType.Name in:types.keySet() and Site__c=:curid Order by RecordType.Name,Name]);
		for(String s:Subs.keySet())
			if(SubOps.containsKey(Subs.get(s).RecordType.Name))
				SubOps.get(Subs.get(s).RecordType.Name).add(new selectoption(Subs.get(s).id,Subs.get(s).Name));
			else{
				SubOps.put(Subs.get(s).RecordType.Name,new List<selectoption> {new selectoption('--None--','--'+Subs.get(s).RecordType.Name+'--'),new selectoption(Subs.get(s).id,Subs.get(s).Name)});
				SubOpsSelected.put(Subs.get(s).RecordType.Name,'--None--');
			}
		for(String s:SubOps.keySet())
			SubOps.get(s).add(new selectoption('--Clear--','--Clear--'));
			
		mainflds = '';
		for(String s:types.values())
			if(s!=null){
				mainflds += s+','+s.replaceAll('__c','__r.name')+',';
			}
		mainflds = mainflds.left(mainflds.length()-1);

		Set <String> unitids = new Set <String>();
		RobotsUninit = new Map <String, AssetCheck>();
		RobotByUnit = new Map <String,String>();
		List <Asset__c> robotsqr = [select id,Name,Configuration__c,Product_name__r.name,Version__c from Asset__c where RecordType.Name='Robotic Unit' and Site__c=:curid Order by Name];
		Robots = new Map <String, AssetCheck>();
		for(Asset__c ass:robotsqr)
			Robots.put(ass.id, new AssetCheck(ass));

		for(AssetCheck ass:Robots.values())
			if(ass.asset.Configuration__c==null){
				status = '0';
				RobotsUninit.put(ass.asset.id,new AssetCheck(ass.asset));
				Robots.remove(ass.asset.id);
			}
			else{
				unitids.add(ass.asset.Configuration__c);
				RobotByUnit.put(ass.asset.Configuration__c,ass.asset.id);
			}

		MainOps = new List <selectoption>();
		MainOps.add(new selectoption('New','New'));
		List <Configuration__c> exstunits = Database.query('select id,Name,'+mainflds+' from Configuration__c where id in:unitids Order by Name');
		Units = new Map <String, Configuration__c>();
		for(Configuration__c unit:exstunits){
			Units.put(unit.id,unit);
			MainOps.add(new selectoption(unit.id,unit.name));
		}
		System.debug('Sub opts: '+SubOps);
		System.debug('Sub opts sel: '+SubOpsSelected);
		System.debug('Robots: '+Robots);
		System.debug('RobotsUninit: '+RobotsUninit);
		System.debug('qr flds: '+mainflds);
	}

	public void FinishMissing(){
		List <Asset__c> asset4up = new List <Asset__c>();
		Map <String, Configuration__c> asset4new = new Map <String, Configuration__c>();
		List <Configuration__c> main4ins = new List <Configuration__c>();
		for(String s:RobotsUninit.KeySet()){
			if(RobotsUninit.get(s).checked==true){
				Configuration__c newconf = new Configuration__c(Name=RobotsUninit.get(s).asset.Name+'-conf');
				if(RobotsUninit.get(s).asset.Product_name__r.name=='Robot - E4'){
					newconf.RecordTypeId = E4mainrecid;
					RobotsUninit.get(s).asset.Version__c = 'E4_04_01';
				}
				else{
					newconf.RecordTypeId = T4mainrecid;
					RobotsUninit.get(s).asset.Version__c = 'T4_01_01';
				}
				if(MissingMain!='New'){
					Configuration__c exstconf = Units.get(MissingMain);
					for(String fld:types.values())
						newconf.put(fld, exstconf.get(fld));
					asset4new.put(s,newconf);
					//asset4up.add(RobotsUninit.get(s).asset);
				}
				else{
					asset4new.put(s,newconf);
				}
			}
		}
		insert asset4new.values();
		for(String s:asset4new.keySet()){
			RobotsUninit.get(s).asset.Configuration__c = asset4new.get(s).id;
			asset4up.add(RobotsUninit.get(s).asset);
			Units.put(asset4new.get(s).id,asset4new.get(s));
			MainOps.add(new selectoption(asset4new.get(s).id,asset4new.get(s).name));
		}
		update asset4up;
		for(Asset__c ass:asset4up){
			RobotsUninit.remove(ass.id);
			Robots.put(ass.id,new AssetCheck(ass));
			RobotByUnit.put(ass.Configuration__c,ass.id);
		}
		if(RobotsUninit.size()==0)
			status = '1';
	}

	public void Search(){
		Set <String> unitids = new Set <String>();
		String qr ='select id,Name,Configuration__c from Asset__c where RecordType.Name=\'Robotic Unit\' and Site__c=:curid and Configuration__c!=null';
		msgs.clear();
		qr += AssetFilter.getQuery();
		qr += ' Order by Name';
		System.debug(qr);
		for(String s:SubOpsSelected.keyset())
			SubOpsSelected.put(s,'--None--');
		List <Asset__c> foundrobots = new List <Asset__c>();
		try{
			foundrobots = Database.query(qr);
		}
		catch(exception e){
			msgs.add(new msg('Bad Query, please fix Filters','danger'));
			System.debug(e.getMessage());
			return;
		}
		Robots = new Map <String, AssetCheck>();
		RobotByUnit = new Map <String, String>();
		for(Asset__c robot:foundrobots){
			Robots.put(robot.id,new AssetCheck(robot));
			unitids.add(robot.Configuration__c);
			RobotByUnit.put(robot.Configuration__c,robot.id);
		}

		List <Configuration__c> exstunits = Database.query('select id,Name,'+mainflds+' from Configuration__c where id in:unitids Order by Name');
		Units = new Map <String, Configuration__c>();
		for(Configuration__c unit:exstunits)
			Units.put(unit.id,unit);
		System.debug('Units: '+Units);
	}

	public void Assign(){
		Set <String> ids= new Set <String>();
		for(String k:SubOpsSelected.keySet())
			if(SubOpsSelected.get(k)!='--None--')
				for(String k2:Units.keySet())
					if(Robots.get(RobotByUnit.get(k2)).checked){
						if(SubOpsSelected.get(k)!='--Clear--'&&Units.get(k2).get(types.get(k))!=SubOpsSelected.get(k)){
							Units.get(k2).put(types.get(k), SubOpsSelected.get(k));
							ids.add(k2);
						}
						else if(SubOpsSelected.get(k)=='--Clear--'&&Units.get(k2).get(types.get(k))!=null){
							Units.get(k2).put(types.get(k), null);
							ids.add(k2);
						}
					}
		System.debug(ids.size());
		List <Configuration__c> conf4up = new List <Configuration__c>();
		for(String k:ids)
			conf4up.add(Units.get(k));
		update conf4up;
	}
}
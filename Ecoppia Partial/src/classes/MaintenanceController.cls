/*
 * @Author:                                                                                                                                                      
                                                       `/////                                                                                         
    smmmmmmmmmmmmmmmm-                                 .NMMMM`  :mmmmmd-      -dmmNmd-                                           /mmmNmmmmmmmmmmm:    
    yMMMMMMMMMMMMMMMM-                                 .NMMMM`   .dMMMMN+    +NMMMMy`                                            +MMMMMMMMMMMMMMM:    
    .--------+NMMMMMh`      `:+osyso+:`         :+syso/:MMMMM`    `sMMMMMy``hMMMMN/     `:/+osyysso/-`        `:+syso/.:::::`    +MMMMM/---------`    
           `sMMMMMm:      :hMMMMMMMMMMNs`    `oNMMMMMMMMMMMMM`      :mMMMMmmMMMMy`      .MMMMMMMMMMMMNo`    `sNMMMMMMMMMMMMM:    +MMMMM.              
          /NMMMMNo`     `yMMMMo-``.oMMMMd`  `dMMMMm+:-:+MMMMM`       `yMMMMMMMN/        `y+:.```-+mMMMMo   `dMMMMNo:--/NMMMM:    +MMMMMNNNNNNNNNs     
        -dMMMMMh.       +MMMMh::::::dMMMM+  yMMMMd`    .NMMMM`        `dMMMMMMs            `-:/+oodMMMMy   sMMMMN.     mMMMM:    +MMMMMMMMMMMMMMs     
      `sMMMMMm:         hMMMMMMMMMMMMMMMMs  mMMMMo     .NMMMM`       -mMMMMMMMMh.       .smMMMMMMMMMMMMy   yMMMMh      mMMMM:    +MMMMM:--------.     
     /NMMMMNo`          yMMMMh:::::::::::.  mMMMMs     .NMMMM`      oNMMMModMMMMN/     -NMMMMo:.` oMMMMy   sMMMMm`     mMMMM:    +MMMMM.              
   `dMMMMMNo/////////:  -NMMMMo.      ./s.  oMMMMN/`  `+MMMMM`    .hMMMMm: `sMMMMMy`   oMMMMd     sMMMMy   .NMMMMmo//oyMMMMM:    +MMMMM+/////////.    
   -MMMMMMMMMMMMMMMMMm   -dMMMMMNdddmNMMM-  `yMMMMMMNNMMMMMMM`   /NMMMMh`    /NMMMMm-  -NMMMMmyyymMMMMMy    -hMMMMMMMMNMMMMM:    +MMMMMMMMMMMMMMM:    
   .hddddddddddddddddy     -ohmNMMMMMNmho.    :ymMMMNh+-hdddh`  +hdddd+       .hddddh-  .odNMMNds:+ddddo      ./osys+:`MMMMM-    /ddddddddddddddd-    
                                `````            ```                                        ``              `+/-.` `.:hMMMMd                          
                                                                                                            `NMMMMMMMMMMMMy`                          
                                                                                                            `oyyhddddhyo/`                            
 * @Date: 2018-11-22 13:56:38 
 * @Last Modified by: ZedXagE - ITmyWay
 * @Last Modified time: 2018-11-22 14:12:11
 */

public class MaintenanceController {
	private String curid;
	private Site__c site;
	public Date dat{get;set;}
	public Map <String, AssetCheck> Robots{get;set;}
	public class msg{
		public String msg{get;set;}
		public String typ{get;set;}
		public msg(String m,String t){
			msg = m;
			typ = t;
		}
	}
	public List<msg> msgs{get;set;}
	public String planid{get;set;}
	public List <selectoption> planops{get;set;}
	public Map <String, Maintenance_Plan__c> Plans{get;set;}
	public Filters AssetFilter{get;set;}
	public class AssetCheck{
		public Asset__c asset{get;set;}
		public Boolean checked{get;set;}
		public AssetCheck(Asset__c ass){
			asset=ass;
			checked=false;
		}
	}
	public MaintenanceController(ApexPages.StandardController controller) {
		curid = ApexPages.currentPage().getParameters().get('id');
		site = [select id,Account__c,Main_Technician__r.RelatedRecordId from Site__c where id=:curid];
		AssetFilter = new Filters('Asset__c');
		init();
	}
	private void init(){
		dat = System.today();
		msgs = new List <msg>();
		planops = new List <selectoption>();
		planops.add(new selectoption('','--Plans--'));
		Plans = new Map <String, Maintenance_Plan__c>([select id,Name,Work_Type__c,(select id,Name,Description__c,Part_No__c,Part_No__r.Role__c from Maintenance_Plan_Lines__r) from Maintenance_Plan__c]);
		for(Maintenance_Plan__c plan:Plans.values())
			planops.add(new selectoption(plan.id,plan.Name));
		Search();
		System.debug('Robots: '+Robots);
	}
	public void Search(){
		msgs.clear();
		Set <String> unitids = new Set <String>();
		String qr ='select id,Name,Role__c,Product_Name__r.Name,Mode__c,Inventory_Status__c,Installation_Date__c from Asset__c where RecordType.Name=\'Robotic Unit\' and Site__c=:curid';
		qr += AssetFilter.getQuery();
		qr += ' Order by Name';
		System.debug(qr);
		List <Asset__c> foundrobots = new List <Asset__c>();
		try{
			foundrobots = Database.query(qr);
		}
		catch(exception e){
			msgs.add(new msg('Bad Query, please fix Filters','danger'));
			System.debug(e.getMessage());
			return;
		}
		Robots = new Map <String, AssetCheck>();
		for(Asset__c robot:foundrobots)
			Robots.put(robot.id,new AssetCheck(robot));
	}
	public void Generate(){
		msgs.clear();
		if(planid!=null&&planid!=''){
			List <Asset__c> checked = new List <Asset__c>();
			for(String k:Robots.keySet())
				if(Robots.get(k).checked){
					checked.add(Robots.get(k).asset);
					Robots.get(k).checked = false;
				}
			if(checked.size()>0){
				Maintenance__c maint = new Maintenance__c();
				maint.Site__c = site.id;
				maint.Maintenance_Plan__c = planid;
				maint.Start_Date__c = dat;
				insert maint;
				msgs.add(new msg('Successfully created <a href="/'+maint.id+'" target="_blank">Maintenance</a>','success'));
				try{
					Map <Integer ,WorkOrder> woks = new Map <Integer, WorkOrder>();
					Map <Integer,List <WorkOrderLineItem>> wokls = new Map <Integer,List <WorkOrderLineItem>>();
					integer i=0;
					for(Asset__c ass:checked){
						WorkOrder wok = new WorkOrder();
						wok.Asset__c = ass.id;
						wok.Maintenance__c = maint.id;
						wok.Site__c = site.id;
						wok.AccountId = site.Account__c;
						wok.Subject = Plans.get(planid).Name+'-'+ass.Name;
						wok.Priority = 'Low';
						wok.StartDate = dat;
						wok.WorkTypeId = Plans.get(planid).Work_Type__c;
						if(site.Main_Technician__r.RelatedRecordId!=null)
							wok.Ownerid = site.Main_Technician__r.RelatedRecordId;
						wok.Status = 'New';
						woks.put(i,wok);
						wokls.put(i,new List <WorkOrderLineItem>());
						for(Maintenance_Plan_Line__c planline:Plans.get(planid).Maintenance_Plan_Lines__r){
							WorkOrderLineItem wokl = new WorkOrderLineItem();
							wokl.Status = 'New';
							wokl.Description = planline.Name+'-'+planline.Description__c;
							wokl.Role__c = planline.Part_No__r.Role__c;
                            wokl.Required_Product__c = planline.Part_No__c;
							wokls.get(i).add(wokl);
						}
						i++;
					}
					insert woks.values();
					List <WorkOrderLineItem> wokls4ins = new List <WorkOrderLineItem>();
					for(integer j:woks.keySet())
						for(WorkOrderLineItem wokl:wokls.get(j)){
							wokl.WorkOrderId = woks.get(j).id;
							wokls4ins.add(wokl);
						}
					insert wokls4ins;
					maint.Auto_generate_work_orders__c = true;
					update maint;
				}
				catch(exception e){
					System.debug(e.getMessage());
				}					
			}
			else{
				msgs.add(new msg('Please check at least one','warning'));
			}
		}
		else{
			msgs.add(new msg('Please select Plan','warning'));
		}
	}
}
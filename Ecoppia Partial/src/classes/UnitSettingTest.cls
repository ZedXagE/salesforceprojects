/*
 * @Author:                                                                                                                                                      
                                                       `/////                                                                                         
    smmmmmmmmmmmmmmmm-                                 .NMMMM`  :mmmmmd-      -dmmNmd-                                           /mmmNmmmmmmmmmmm:    
    yMMMMMMMMMMMMMMMM-                                 .NMMMM`   .dMMMMN+    +NMMMMy`                                            +MMMMMMMMMMMMMMM:    
    .--------+NMMMMMh`      `:+osyso+:`         :+syso/:MMMMM`    `sMMMMMy``hMMMMN/     `:/+osyysso/-`        `:+syso/.:::::`    +MMMMM/---------`    
           `sMMMMMm:      :hMMMMMMMMMMNs`    `oNMMMMMMMMMMMMM`      :mMMMMmmMMMMy`      .MMMMMMMMMMMMNo`    `sNMMMMMMMMMMMMM:    +MMMMM.              
          /NMMMMNo`     `yMMMMo-``.oMMMMd`  `dMMMMm+:-:+MMMMM`       `yMMMMMMMN/        `y+:.```-+mMMMMo   `dMMMMNo:--/NMMMM:    +MMMMMNNNNNNNNNs     
        -dMMMMMh.       +MMMMh::::::dMMMM+  yMMMMd`    .NMMMM`        `dMMMMMMs            `-:/+oodMMMMy   sMMMMN.     mMMMM:    +MMMMMMMMMMMMMMs     
      `sMMMMMm:         hMMMMMMMMMMMMMMMMs  mMMMMo     .NMMMM`       -mMMMMMMMMh.       .smMMMMMMMMMMMMy   yMMMMh      mMMMM:    +MMMMM:--------.     
     /NMMMMNo`          yMMMMh:::::::::::.  mMMMMs     .NMMMM`      oNMMMModMMMMN/     -NMMMMo:.` oMMMMy   sMMMMm`     mMMMM:    +MMMMM.              
   `dMMMMMNo/////////:  -NMMMMo.      ./s.  oMMMMN/`  `+MMMMM`    .hMMMMm: `sMMMMMy`   oMMMMd     sMMMMy   .NMMMMmo//oyMMMMM:    +MMMMM+/////////.    
   -MMMMMMMMMMMMMMMMMm   -dMMMMMNdddmNMMM-  `yMMMMMMNNMMMMMMM`   /NMMMMh`    /NMMMMm-  -NMMMMmyyymMMMMMy    -hMMMMMMMMNMMMMM:    +MMMMMMMMMMMMMMM:    
   .hddddddddddddddddy     -ohmNMMMMMNmho.    :ymMMMNh+-hdddh`  +hdddd+       .hddddh-  .odNMMNds:+ddddo      ./osys+:`MMMMM-    /ddddddddddddddd-    
                                `````            ```                                        ``              `+/-.` `.:hMMMMd                          
                                                                                                            `NMMMMMMMMMMMMy`                          
                                                                                                            `oyyhddddhyo/`                            
 * @Date: 2018-11-22 13:56:38 
 * @Last Modified by: ZedXagE - ITmyWay
 * @Last Modified time: 2018-11-22 14:12:29
 */

@isTest(seealldata=true)
public class UnitSettingTest {
	static TestMethod void tesUnitSettings() {
		Site__c site = [select id from Site__c Order by CreatedDate Limit 1];
		Configuration__c conf = new Configuration__c(Name='testconf');
		insert conf;
		insert new Asset__c(Name='test',Site__c=site.id,Asset_id__c='rtest001',Configuration__c=conf.id);
		PageReference pgRef = Page.UnitSetting;
		Test.setCurrentPage(pgRef);
		ApexPages.currentPage().getParameters().put('id', site.id);
		ApexPages.StandardController controller = new ApexPages.StandardController(site);
		UnitSettingController con = new UnitSettingController(controller);
		con.AssetFilter.addFilter();
		con.AssetFilter.filters[0].field='id';
		con.AssetFilter.filters[0].changeField();
		con.AssetFilter.filters[0].condition='!=%s';
		con.AssetFilter.addFilter();
		con.AssetFilter.filters[1].field='createddate';
		con.AssetFilter.filters[1].changeField();
		con.AssetFilter.filters[1].condition='!=%s';
		con.AssetFilter.filters[1].dat=System.Today();
		con.AssetFilter.addFilter();
		con.AssetFilter.filters[2].field='name';
		con.AssetFilter.filters[2].changeField();
		con.AssetFilter.filters[2].freetxt='1234';
		con.AssetFilter.filters[2].condition='!=%s';
		con.search();
		for(String s:con.Robots.keyset())
			con.Robots.get(s).checked=true;
		con.sendRowHardConfig();
		con.sendRowSoftConfig();
		con.sendRowKeepAlive();
		con.sendRowStartClean();
		con.sendRowStopClean();
		con.sendRowReadLogs();
		con.sendRowReadConfig();
		con.sendCommunicationHardConfig();
		con.sendCommunicationSoftConfig();

		con.AssetFilter.curFil='';
		con.AssetFilter.deleteFilter();
	}
}
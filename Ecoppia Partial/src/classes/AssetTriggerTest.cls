/**
 * Created by nkarmi on 11/21/2018.
 */

@IsTest
private class AssetTriggerTest {
    @IsTest static void testUpdateAssetWithoutChangingSite() {
        List<Asset__c> assets = TestDataFactory.createAssets(1);
        assets = [SELECT Id, (SELECT Id, Transaction_Type__c FROM Transaction_Assets__r ORDER BY CreatedDate ASC) FROM Asset__c WHERE Id IN :assets];
        assets[0].Location_In_Meter__c = 500;
        update assets;
        for(Asset__c objAsset : assets){
            System.assertEquals(1, objAsset.Transaction_Assets__r.size());
            System.assertEquals('Created', objAsset.Transaction_Assets__r[0].Transaction_Type__c);
        }
    }

    @IsTest static void testCreateAsset() {
        List<Asset__c> assets = TestDataFactory.createAssets(150);
        assets = [SELECT Id, (SELECT Id, Transaction_Type__c FROM Transaction_Assets__r ORDER BY CreatedDate ASC) FROM Asset__c WHERE Id IN :assets];

        for(Asset__c objAsset : assets){
            System.assertEquals(1, objAsset.Transaction_Assets__r.size());
            System.assertEquals('Created', objAsset.Transaction_Assets__r[0].Transaction_Type__c);
        }
    }

    @IsTest static void testChangeAssetSiteFromSiteToWarehouseRecordType() {
        List<Asset__c> assets = TestDataFactory.createAssets(150);
        List<Site__c> warehouses = TestDataFactory.createSitesWithWarehouseRecordType(1);

        for(Asset__c objAsset : assets){
            objAsset.Site__c = warehouses[0].Id;
        }
        update assets;
        assets = [SELECT Id, (SELECT Id, Transaction_Type__c FROM Transaction_Assets__r ORDER BY CreatedDate ASC) FROM Asset__c WHERE Id IN :assets];
        for(Asset__c objAsset : assets){
            System.assertEquals(2, objAsset.Transaction_Assets__r.size());
            System.assertEquals('Removed', objAsset.Transaction_Assets__r[1].Transaction_Type__c);
        }
    }

    @IsTest static void testChangeAssetSiteFromWarehouseToWarehouseRecordType() {
        List<Asset__c> assets = TestDataFactory.createAssets(150);
        List<Site__c> warehouses = TestDataFactory.createSitesWithWarehouseRecordType(2);

        for(Asset__c objAsset : assets){
            objAsset.Site__c = warehouses[0].Id;
        }
        update assets;

        for(Asset__c objAsset : assets){
            objAsset.Site__c = warehouses[1].Id;
        }
        update assets;
        assets = [SELECT Id, (SELECT Id, Transaction_Type__c FROM Transaction_Assets__r ORDER BY CreatedDate ASC) FROM Asset__c WHERE Id IN :assets];
        for(Asset__c objAsset : assets){
            System.assertEquals(3, objAsset.Transaction_Assets__r.size());
            System.assertEquals('Transfer', objAsset.Transaction_Assets__r[2].Transaction_Type__c);
        }
    }

    @IsTest static void testChangeAssetSiteToSiteRecordType() {
        List<Asset__c> assets = TestDataFactory.createAssets(150);
        Integer lastSiteIndex = assets.size() - 1 ;
        for(Asset__c objAsset : assets){
            objAsset.Site__c = TestDataFactory.sites[lastSiteIndex].Id;
            lastSiteIndex--;
        }
        update assets;

        assets = [SELECT Id, (SELECT Id, Transaction_Type__c FROM Transaction_Assets__r ORDER BY CreatedDate ASC) FROM Asset__c WHERE Id IN :assets];
        for(Asset__c objAsset : assets){
            System.assertEquals(2, objAsset.Transaction_Assets__r.size());
            System.assertEquals('Installed', objAsset.Transaction_Assets__r[1].Transaction_Type__c);
        }
    }

    @IsTest static void testChangeAssetParentUnitFromNullToSet() {
        List<Asset__c> assets = TestDataFactory.createAssets(150);
        Integer lastAssetIndex = assets.size() - 1 ;
        for(Asset__c objAsset : assets){
            objAsset.Parent_Asset__c = assets[lastAssetIndex].Id;
            lastAssetIndex--;
        }
        update assets;

        assets = [SELECT Id, (SELECT Id, Transaction_Type__c FROM Transaction_Assets__r ORDER BY CreatedDate ASC) FROM Asset__c WHERE Id IN :assets];
        for(Asset__c objAsset : assets){
            System.assertEquals(2, objAsset.Transaction_Assets__r.size());
            System.assertEquals('Assembly', objAsset.Transaction_Assets__r[1].Transaction_Type__c);
        }
    }

    @IsTest static void testChangeAssetParentUnitFromSetToNull() {
        List<Asset__c> assets = TestDataFactory.createAssets(150);
        Integer lastAssetIndex = assets.size() - 1 ;
        for(Asset__c objAsset : assets){
            objAsset.Parent_Asset__c = assets[lastAssetIndex].Id;
            lastAssetIndex--;
        }
        update assets;

        delete [SELECT Id FROM Transaction_Asset__c];

        for(Asset__c objAsset : assets){
            objAsset.Parent_Asset__c = null;
            lastAssetIndex--;
        }
        update assets;

        assets = [SELECT Id, (SELECT Id, Transaction_Type__c FROM Transaction_Assets__r ORDER BY CreatedDate ASC) FROM Asset__c WHERE Id IN :assets];
        for(Asset__c objAsset : assets){
            System.assertEquals(1, objAsset.Transaction_Assets__r.size());
            System.assertEquals('Disassemble', objAsset.Transaction_Assets__r[0].Transaction_Type__c);
        }
    }

    @IsTest static void testChangeAssetParentUnit() {
        List<Asset__c> assets = TestDataFactory.createAssets(4);
        Integer lastAssetIndex = assets.size() - 1 ;
        for(Asset__c objAsset : assets){
            objAsset.Parent_Asset__c = assets[lastAssetIndex].Id;
            lastAssetIndex--;
        }
        update assets;

        delete [SELECT Id FROM Transaction_Asset__c];

        assets[0].Parent_Asset__c = assets[1].Id;
        assets[1].Parent_Asset__c = assets[2].Id;
        assets[2].Parent_Asset__c = assets[3].Id;
        assets[3].Parent_Asset__c = assets[0].Id;

        update assets;

        assets = [SELECT Id, (SELECT Id, Transaction_Type__c FROM Transaction_Assets__r ORDER BY CreatedDate ASC) FROM Asset__c WHERE Id IN :assets];
        for(Asset__c objAsset : assets){
            System.assertEquals(1, objAsset.Transaction_Assets__r.size());
            System.assertEquals('Re-Assembly', objAsset.Transaction_Assets__r[0].Transaction_Type__c);
        }
    }
}
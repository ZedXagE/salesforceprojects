/*
 * @Author:                                                                                                                                                      
                                                       `/////                                                                                         
    smmmmmmmmmmmmmmmm-                                 .NMMMM`  :mmmmmd-      -dmmNmd-                                           /mmmNmmmmmmmmmmm:    
    yMMMMMMMMMMMMMMMM-                                 .NMMMM`   .dMMMMN+    +NMMMMy`                                            +MMMMMMMMMMMMMMM:    
    .--------+NMMMMMh`      `:+osyso+:`         :+syso/:MMMMM`    `sMMMMMy``hMMMMN/     `:/+osyysso/-`        `:+syso/.:::::`    +MMMMM/---------`    
           `sMMMMMm:      :hMMMMMMMMMMNs`    `oNMMMMMMMMMMMMM`      :mMMMMmmMMMMy`      .MMMMMMMMMMMMNo`    `sNMMMMMMMMMMMMM:    +MMMMM.              
          /NMMMMNo`     `yMMMMo-``.oMMMMd`  `dMMMMm+:-:+MMMMM`       `yMMMMMMMN/        `y+:.```-+mMMMMo   `dMMMMNo:--/NMMMM:    +MMMMMNNNNNNNNNs     
        -dMMMMMh.       +MMMMh::::::dMMMM+  yMMMMd`    .NMMMM`        `dMMMMMMs            `-:/+oodMMMMy   sMMMMN.     mMMMM:    +MMMMMMMMMMMMMMs     
      `sMMMMMm:         hMMMMMMMMMMMMMMMMs  mMMMMo     .NMMMM`       -mMMMMMMMMh.       .smMMMMMMMMMMMMy   yMMMMh      mMMMM:    +MMMMM:--------.     
     /NMMMMNo`          yMMMMh:::::::::::.  mMMMMs     .NMMMM`      oNMMMModMMMMN/     -NMMMMo:.` oMMMMy   sMMMMm`     mMMMM:    +MMMMM.              
   `dMMMMMNo/////////:  -NMMMMo.      ./s.  oMMMMN/`  `+MMMMM`    .hMMMMm: `sMMMMMy`   oMMMMd     sMMMMy   .NMMMMmo//oyMMMMM:    +MMMMM+/////////.    
   -MMMMMMMMMMMMMMMMMm   -dMMMMMNdddmNMMM-  `yMMMMMMNNMMMMMMM`   /NMMMMh`    /NMMMMm-  -NMMMMmyyymMMMMMy    -hMMMMMMMMNMMMMM:    +MMMMMMMMMMMMMMM:    
   .hddddddddddddddddy     -ohmNMMMMMNmho.    :ymMMMNh+-hdddh`  +hdddd+       .hddddh-  .odNMMNds:+ddddo      ./osys+:`MMMMM-    /ddddddddddddddd-    
                                `````            ```                                        ``              `+/-.` `.:hMMMMd                          
                                                                                                            `NMMMMMMMMMMMMy`                          
                                                                                                            `oyyhddddhyo/`                            
 * @Date: 2018-11-22 13:56:38 
 * @Last Modified by: ZedXagE - ITmyWay
 * @Last Modified time: 2018-11-22 14:12:34
 */

public with sharing class WareHousesController {
	public String params{get;set{
			if(params!=value&&value!=null){
				params = value;
				String [] spl = params.split(';');
				fromtype = spl[0];
				fromid = spl[1]!='null'?spl[1]:null;
				totype = spl[2];
				toid = spl[3]!='null'?spl[3]:null;
				if(totype!='All'&&totype!='Site')	typeRobo=false;
				else	typeRobo=true;
				init();
			}
		}}
	public String fromtype{get;set;}
	public String totype{get;set;}
	public Boolean typeRobo{get;set;}
	public String fromid{get;set;}
	public String toid{get;set;}
	public Integer lim{get;set;}
	public Integer total{get;set;}
	public Integer pag{get;set;}
	public Map <String, Site__c> sites{get;set;}
	public Map <String, GroupAsset> groups{get;set;}
	public class msg{
		public String msg{get;set;}
		public String typ{get;set;}
		public msg(String m,String t){
			msg = m;
			typ = t;
		}
	}
	public List<msg> msgs{get;set;}
	public Filters AssetFilter{get;set;}
	public List <selectoption> fromops{get;set;}
	public List <selectoption> toops{get;set;}
	public class GroupAsset{
		public Map <String, AssetCheck> lines{get;set;}
		public String Role{get;set;}
		public String Name{get;set;}
		public Decimal Quan{get;set;}
		public Integer Num{get;set;}
		public GroupAsset(Asset__c ass){
			lines = new Map <String, AssetCheck>();
			Quan = ass.Quantity__c!=null?ass.Quantity__c:1;
			Num = 1;
			lines.put(ass.id,new AssetCheck(ass));
			Name = ass.Product_Name__r.Name;
			Role = ass.Role__c;
		}
	}
	public class AssetCheck{
		public Asset__c asset{get;set;}
		public Boolean checked{get;set;}
		public Boolean hassub{get;set;}
		public Boolean batch{get;set;}
		public Decimal move{get;set;}
		public Map <String, SubAssetCheck> subs{get;set;}
		public AssetCheck(Asset__c ass){
			asset=ass;
			batch = false;
			if(ass.Product_Name__r.Type__c=='Batch'){
				batch = true;
				move = ass.Quantity__c;
			}
			checked=false;
			hassub=false;
			subs = new Map <String, SubAssetCheck>();
		}
	}
	public class SubAssetCheck{
		public Asset__c asset{get;set;}
		public Boolean checked{get;set;}
		public Map <String, SubSubAssetCheck> subs{get;set;}
		public SubAssetCheck(Asset__c ass){
			asset=ass;
			checked=false;
			subs = new Map <String, SubSubAssetCheck>();
		}
	}
	public class SubSubAssetCheck{
		public Asset__c asset{get;set;}
		public SubSubAssetCheck(Asset__c ass){
			asset=ass;
		}
	}
	public WareHousesController() {
		AssetFilter = new Filters('Asset__c');
	}
	private void init(){
		lim = 10;
		pag = 0;
		total = 0;
		msgs = new List <msg>();
		sites = new Map <String,Site__c>();
		fromops = new List <selectoption>();
		fromops.add(new selectoption('','--From '+fromtype+'--'));
		toops = new List <selectoption>();
		toops.add(new selectoption('','--To '+totype+'--'));
		String fromqr = 'select id,Name,RecordType.Name from Site__c';
		if(fromtype!='All')	fromqr += ' where RecordType.Name=:fromtype';
		String toqr = 'select id,Name,RecordType.Name from Site__c';
		if(totype!='All')	toqr += ' where RecordType.Name=:totype';
		List <Site__c> froms = Database.query(fromqr);
		List <Site__c> tos = Database.query(toqr);
		for(Site__c site:froms){
			fromops.add(new selectoption(site.id,site.name));
			sites.put(site.id,site);
		}
		for(Site__c site:tos){
			toops.add(new selectoption(site.id,site.name));
			sites.put(site.id,site);
		}
		if(fromid!=null)
			Search();
	}
	public void Search(){
		total = 0;
		pag = 0;
		Set <String> unitids = new Set <String>();
		String qr;
		if(!typeRobo)
			qr = 'select id,RecordTypeId,Name,Product_Name__c,Product_Name__r.Name,Product_Name__r.Type__c,Role__c,Mode__c,Inventory_Status__c,Installation_Date__c,Site__c,Asset_ID__c,Quantity__c,(select id,RecordTypeId,Name,Product_Name__c,Product_Name__r.Name,Product_Name__r.Type__c,Role__c,Mode__c,Inventory_Status__c,Installation_Date__c,Site__c,Parent_Asset__c,Asset_ID__c,Quantity__c from Assets__r) from Asset__c where RecordType.Name=\'Asset line\' and Site__c=:fromid and Parent_Asset__c=null';
		else 
			qr = 'select id,RecordTypeId,Name,Product_Name__c,Product_Name__r.Name,Product_Name__r.Type__c,Role__c,Mode__c,Inventory_Status__c,Installation_Date__c,Site__c,Asset_ID__c,Quantity__c from Asset__c where RecordType.Name=\'Robotic Unit\' and Site__c=:fromid and Parent_Asset__c=null and Product_Name__c!=null';
		msgs.clear();
		qr += AssetFilter.getQuery();
		qr += ' Order by Name';
		System.debug(qr);
		List <Asset__c> foundlines = new List <Asset__c>();
		try{
			foundlines = Database.query(qr);
		}
		catch(exception e){
			msgs.add(new msg('Bad Query, please fix Filters','danger'));
			System.debug(e.getMessage());
			return;
		}
		groups = new Map <String, GroupAsset>();
		if(!typeRobo){
			for(Asset__c line:foundlines){
				if(!Groups.containsKey(line.Product_Name__c)){
					groups.put(line.Product_Name__c,new GroupAsset(line));
					total++;
				}
				else{
					groups.get(line.Product_Name__c).lines.put(line.id,new AssetCheck(line));
					groups.get(line.Product_Name__c).Quan = groups.get(line.Product_Name__c).Quan + (line.Quantity__c!=null?line.Quantity__c:1);
					groups.get(line.Product_Name__c).Num ++;
				}
				for(Asset__c subline:line.Assets__r){
					groups.get(line.Product_Name__c).lines.get(line.id).hassub = true;
					groups.get(line.Product_Name__c).lines.get(line.id).subs.put(subline.id,new SubAssetCheck(subline));
				}
			}
		}
		else{
			Set <String> pars = new Set <String>();
			for(Asset__c robo:foundlines){
				pars.add(robo.id);
				if(!Groups.containsKey(robo.Product_Name__c))
					groups.put(robo.Product_Name__c,new GroupAsset(robo));
				else{
					groups.get(robo.Product_Name__c).lines.put(robo.id,new AssetCheck(robo));
					groups.get(robo.Product_Name__c).Quan = groups.get(robo.Product_Name__c).Quan + (robo.Quantity__c!=null?robo.Quantity__c:1);
					groups.get(robo.Product_Name__c).Num ++;
				}
			}
			System.debug(pars);
			String subqr = 'select id,RecordTypeId,Parent_Asset__c,Parent_Asset__r.Product_Name__c,Name,Product_Name__c,Product_Name__r.Name,Product_Name__r.Type__c,Role__c,Mode__c,Inventory_Status__c,Installation_Date__c,Site__c,Asset_ID__c,Quantity__c,(select id,RecordTypeId,Name,Product_Name__c,Product_Name__r.Name,Product_Name__r.Type__c,Role__c,Mode__c,Inventory_Status__c,Installation_Date__c,Site__c,Parent_Asset__c,Asset_ID__c,Quantity__c from Assets__r) from Asset__c where Parent_Asset__c in: pars Order by Name';	
			List <Asset__c> foundsubs = Database.query(subqr);
			for(Asset__c line:foundsubs){
				if(Groups.containsKey(line.Parent_Asset__r.Product_Name__c)){
					groups.get(line.Parent_Asset__r.Product_Name__c).lines.get(line.Parent_Asset__c).subs.put(line.id,new SubAssetCheck(line));
					for(Asset__c subline:line.Assets__r)
						groups.get(line.Parent_Asset__r.Product_Name__c).lines.get(line.Parent_Asset__c).subs.get(line.id).subs.put(subline.id,new SubSubAssetCheck(subline));
				}
			}
		}
		System.debug(groups);
	}
	public void Move(){
		msgs.clear();
		if(toid!=null&&toid!=''&&fromid!=null&&fromid!=''&&fromid!=toid){
			List <Asset__c> checked = new List <Asset__c>();
			for(String g:groups.keySet()){
				for(String k:groups.get(g).lines.keySet()){
					if(!typeRobo){
						Boolean parcheck = false;
						if(groups.get(g).lines.get(k).checked){
							parcheck = true;
							System.debug(groups.get(g).lines.get(k).batch+' '+groups.get(g).lines.get(k).asset.Quantity__c+' '+groups.get(g).lines.get(k).move);
							if(groups.get(g).lines.get(k).batch==true&&groups.get(g).lines.get(k).asset.Quantity__c!=null&&groups.get(g).lines.get(k).move!=null&&groups.get(g).lines.get(k).asset.Quantity__c>groups.get(g).lines.get(k).move){
								Asset__c curass = groups.get(g).lines.get(k).asset;
								curass.Quantity__c -= groups.get(g).lines.get(k).move;
								Asset__c newass = new Asset__c();
								newass.Site__c = toid;
								newass.Name = curass.Name;
								newass.Inventory_Status__c = curass.Inventory_Status__c;
								newass.RecordTypeId = curass.RecordTypeId;
								newass.Product_Name__c = curass.Product_Name__c;
								newass.Quantity__c = groups.get(g).lines.get(k).move;
								newass.Batch__c = curass.id;
								checked.add(newass);
							}
							else
								groups.get(g).lines.get(k).asset.Site__c = toid;
							checked.add(groups.get(g).lines.get(k).asset);
						}
						for(String sk:groups.get(g).lines.get(k).subs.keySet()){
							if(groups.get(g).lines.get(k).subs.get(sk).checked){
								groups.get(g).lines.get(k).subs.get(sk).asset.Site__c = toid;
								if(!parcheck)	groups.get(g).lines.get(k).subs.get(sk).asset.Parent_Asset__c = null;
								checked.add(groups.get(g).lines.get(k).subs.get(sk).asset);
							}
						}
					}
					else{
						if(groups.get(g).lines.get(k).checked){
							groups.get(g).lines.get(k).asset.Site__c = toid;
							checked.add(groups.get(g).lines.get(k).asset);
							for(String sk:groups.get(g).lines.get(k).subs.keySet()){
								System.debug(sk);
								groups.get(g).lines.get(k).subs.get(sk).asset.Site__c = toid;
								checked.add(groups.get(g).lines.get(k).subs.get(sk).asset);
								for(String ssk:groups.get(g).lines.get(k).subs.get(sk).subs.keySet()){
									groups.get(g).lines.get(k).subs.get(sk).subs.get(ssk).asset.Site__c = toid;
									checked.add(groups.get(g).lines.get(k).subs.get(sk).subs.get(ssk).asset);
								}
							}
						}
					}
				}
			}
			upsert checked;
			Search();
		}
		else
			msgs.add(new msg('Please select Warehouses','warning'));
	}
}
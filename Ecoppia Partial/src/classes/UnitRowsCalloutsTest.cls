/**
 * Created by nkarmi on 11/10/2018.
 */

@IsTest
private class UnitRowsCalloutsTest {
    @isTest static  void testToMasterHardConfiguration() {
        // Set mock callout class
        Test.setMock(HttpCalloutMock.class, new SitesHttpCalloutMock());
        List<Asset__c> rows = TestDataFactory.createRowsWithURC(1, 2);
        List<String> assetIds = new List<String>();
        for(Asset__c objRow : rows){
            assetIds.add(objRow.Asset_ID__c);
        }
        UnitRowsCallouts.toMasterHardConfiguration(assetIds);
        List<Event__c> events = [SELECT Id, Status__c, Event__c, Type__c, Asset__c FROM Event__c];

        System.assert(events.size() > 0);
        System.assertEquals('Success', events[0].Status__c);
        System.assertEquals('Configuration', events[0].Type__c);
        System.assertEquals('SF Set Hard Config', events[0].Event__c);
        System.assertEquals(rows[0].Id, events[0].Asset__c);
    }

    @isTest static  void testToMasterSoftConfiguration() {
        // Set mock callout class
        Test.setMock(HttpCalloutMock.class, new SitesHttpCalloutMock());
        List<Asset__c> rows = TestDataFactory.createRowsWithURC(2, 2);
        rows[0].Version__c = 'E4_04_01';
        rows[1].Version__c = 'T4_01_01';
        update rows;

        List<String> assetIds = new List<String>();
        for(Asset__c objRow : rows){
            assetIds.add(objRow.Asset_ID__c);
        }
        UnitRowsCallouts.toMasterSoftConfiguration(assetIds);
        List<Event__c> events = [SELECT Id, Status__c, Event__c, Type__c, Asset__c FROM Event__c];

        System.assert(events.size() > 0);
        System.assertEquals('Success', events[0].Status__c);
        System.assertEquals('Configuration', events[0].Type__c);
        System.assertEquals('SF Set Soft Config', events[0].Event__c);
        System.assertEquals(rows[0].Id, events[0].Asset__c);
    }

    @isTest static  void testToMasterSiteStartCleanOperation() {
        // Set mock callout class
        Test.setMock(HttpCalloutMock.class, new SitesHttpCalloutMock());
        List<Asset__c> rows = TestDataFactory.createRowsWithURC(2, 2);
        rows[0].Version__c = 'E4_04_01';
        rows[1].Version__c = 'T4_01_01';
        update rows;

        List<String> assetIds = new List<String>();
        for(Asset__c objRow : rows){
            assetIds.add(objRow.Asset_ID__c);
        }


        UnitRowsCallouts.toMasterRowOperation(assetIds, 'startClean');
        List<Event__c> events = [SELECT Id, Status__c, Event__c, Type__c, Asset__c FROM Event__c];

        System.assert(events.size() > 0);
        System.assertEquals('Success', events[0].Status__c);
        System.assertEquals('Command', events[0].Type__c);
        System.assertEquals('SF startClean', events[0].Event__c);
        System.assertEquals(rows[0].Id, events[0].Asset__c);
    }

    @isTest static  void testToMasterSiteSetConfigOperation() {
        // Set mock callout class
        Test.setMock(HttpCalloutMock.class, new SitesHttpCalloutMock());
        List<Asset__c> rows = TestDataFactory.createRowsWithURC(2, 2);
        rows[0].Version__c = 'E4_04_01';
        update rows;

        List<String> assetIds = new List<String>();
        for(Asset__c objRow : rows){
            assetIds.add(objRow.Asset_ID__c);
        }


        UnitRowsCallouts.toMasterRowOperation(assetIds, 'setConfig');
        List<Event__c> events = [SELECT Id, Status__c, Event__c, Type__c, Asset__c FROM Event__c];

        System.assert(events.size() > 0);
        System.assertEquals('Success', events[0].Status__c);
        System.assertEquals('Command', events[0].Type__c);
        System.assertEquals('SF setConfig', events[0].Event__c);
        System.assertEquals(rows[0].Id, events[0].Asset__c);
    }
}
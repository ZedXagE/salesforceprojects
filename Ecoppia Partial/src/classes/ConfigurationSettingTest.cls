/*
 * @Author:                                                                                                                                                      
                                                       `/////                                                                                         
    smmmmmmmmmmmmmmmm-                                 .NMMMM`  :mmmmmd-      -dmmNmd-                                           /mmmNmmmmmmmmmmm:    
    yMMMMMMMMMMMMMMMM-                                 .NMMMM`   .dMMMMN+    +NMMMMy`                                            +MMMMMMMMMMMMMMM:    
    .--------+NMMMMMh`      `:+osyso+:`         :+syso/:MMMMM`    `sMMMMMy``hMMMMN/     `:/+osyysso/-`        `:+syso/.:::::`    +MMMMM/---------`    
           `sMMMMMm:      :hMMMMMMMMMMNs`    `oNMMMMMMMMMMMMM`      :mMMMMmmMMMMy`      .MMMMMMMMMMMMNo`    `sNMMMMMMMMMMMMM:    +MMMMM.              
          /NMMMMNo`     `yMMMMo-``.oMMMMd`  `dMMMMm+:-:+MMMMM`       `yMMMMMMMN/        `y+:.```-+mMMMMo   `dMMMMNo:--/NMMMM:    +MMMMMNNNNNNNNNs     
        -dMMMMMh.       +MMMMh::::::dMMMM+  yMMMMd`    .NMMMM`        `dMMMMMMs            `-:/+oodMMMMy   sMMMMN.     mMMMM:    +MMMMMMMMMMMMMMs     
      `sMMMMMm:         hMMMMMMMMMMMMMMMMs  mMMMMo     .NMMMM`       -mMMMMMMMMh.       .smMMMMMMMMMMMMy   yMMMMh      mMMMM:    +MMMMM:--------.     
     /NMMMMNo`          yMMMMh:::::::::::.  mMMMMs     .NMMMM`      oNMMMModMMMMN/     -NMMMMo:.` oMMMMy   sMMMMm`     mMMMM:    +MMMMM.              
   `dMMMMMNo/////////:  -NMMMMo.      ./s.  oMMMMN/`  `+MMMMM`    .hMMMMm: `sMMMMMy`   oMMMMd     sMMMMy   .NMMMMmo//oyMMMMM:    +MMMMM+/////////.    
   -MMMMMMMMMMMMMMMMMm   -dMMMMMNdddmNMMM-  `yMMMMMMNNMMMMMMM`   /NMMMMh`    /NMMMMm-  -NMMMMmyyymMMMMMy    -hMMMMMMMMNMMMMM:    +MMMMMMMMMMMMMMM:    
   .hddddddddddddddddy     -ohmNMMMMMNmho.    :ymMMMNh+-hdddh`  +hdddd+       .hddddh-  .odNMMNds:+ddddo      ./osys+:`MMMMM-    /ddddddddddddddd-    
                                `````            ```                                        ``              `+/-.` `.:hMMMMd                          
                                                                                                            `NMMMMMMMMMMMMy`                          
                                                                                                            `oyyhddddhyo/`                            
 * @Date: 2018-11-22 13:56:38 
 * @Last Modified by:   ZedXagE - ITmyWay 
 * @Last Modified time: 2018-11-22 13:56:38 
 */
/*AssetAssemblyTest,CheckUniqueRoleTest,ConfigurationSettingTest,HourlyMaintenanceTest,MaintenanceTest,UnitSettingTest,WareHousesTest*/
@isTest(seealldata=true)
public class ConfigurationSettingTest {
	static TestMethod void tesConfSettings() {
		Site__c site = [select id from Site__c Order by CreatedDate Limit 1];
		insert new Asset__c(Name='test',Site__c=site.id);
		PageReference pgRef = Page.ConfigurationSetting;
		Test.setCurrentPage(pgRef);
		ApexPages.currentPage().getParameters().put('id', site.id);
		ApexPages.StandardController controller = new ApexPages.StandardController(site);
		ConfigurationSettingController con = new ConfigurationSettingController(controller);
		con.MissingMain='New';
        for(String s:con.RobotsUninit.KeySet())
			con.RobotsUninit.get(s).checked=true;
		con.FinishMissing();
		con.AssetFilter.addFilter();
		con.AssetFilter.filters[0].field='id';
		con.AssetFilter.filters[0].changeField();
		con.AssetFilter.filters[0].condition='!=%s';
		con.AssetFilter.addFilter();
		con.AssetFilter.filters[1].field='createddate';
		con.AssetFilter.filters[1].changeField();
		con.AssetFilter.filters[1].condition='!=%s';
		con.AssetFilter.filters[1].dat=System.Today();
		con.AssetFilter.addFilter();
		con.AssetFilter.filters[2].field='name';
		con.AssetFilter.filters[2].changeField();
		con.AssetFilter.filters[2].freetxt='1234';
		con.AssetFilter.filters[2].condition='!=%s';
		con.search();
		for(String s:con.SubOpsSelected.KeySet())
			con.SubOpsSelected.put(s,con.SubOps.get(s)[con.SubOps.get(s).size()-1].getValue());
		con.Assign();
		con.AssetFilter.curFil='';
		con.AssetFilter.deleteFilter();
	}
	static TestMethod void tesConfSettingsTrigger() {
		Configuration_Map__mdt confm = [select Record_Type_Name__c,API_Field__c from Configuration_Map__mdt limit 1];
		String mainrecid = [select id from RecordType where Name like: '%Unit%' and IsActive=true and SobjectType='Configuration__c' limit 1].id;
		String subrecid = [select id from RecordType where Name=:confm.Record_Type_Name__c and IsActive=true and SobjectType='Configuration__c' limit 1].id;
		Configuration__c subconf = [select id from Configuration__c where RecordTypeId=:subrecid limit 1];
		Configuration__c mainconf = [select id from Configuration__c where RecordTypeId=:mainrecid limit 1];
		mainconf.put(confm.API_Field__c,null);
		update mainconf;
		mainconf.put(confm.API_Field__c,subconf.id);
		update mainconf;
		ConfigurationRefresh.Refresh(subconf.id);
	}
}
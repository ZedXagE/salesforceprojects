/*
 * @Author:                                                                                                                                                      
                                                       `/////                                                                                         
    smmmmmmmmmmmmmmmm-                                 .NMMMM`  :mmmmmd-      -dmmNmd-                                           /mmmNmmmmmmmmmmm:    
    yMMMMMMMMMMMMMMMM-                                 .NMMMM`   .dMMMMN+    +NMMMMy`                                            +MMMMMMMMMMMMMMM:    
    .--------+NMMMMMh`      `:+osyso+:`         :+syso/:MMMMM`    `sMMMMMy``hMMMMN/     `:/+osyysso/-`        `:+syso/.:::::`    +MMMMM/---------`    
           `sMMMMMm:      :hMMMMMMMMMMNs`    `oNMMMMMMMMMMMMM`      :mMMMMmmMMMMy`      .MMMMMMMMMMMMNo`    `sNMMMMMMMMMMMMM:    +MMMMM.              
          /NMMMMNo`     `yMMMMo-``.oMMMMd`  `dMMMMm+:-:+MMMMM`       `yMMMMMMMN/        `y+:.```-+mMMMMo   `dMMMMNo:--/NMMMM:    +MMMMMNNNNNNNNNs     
        -dMMMMMh.       +MMMMh::::::dMMMM+  yMMMMd`    .NMMMM`        `dMMMMMMs            `-:/+oodMMMMy   sMMMMN.     mMMMM:    +MMMMMMMMMMMMMMs     
      `sMMMMMm:         hMMMMMMMMMMMMMMMMs  mMMMMo     .NMMMM`       -mMMMMMMMMh.       .smMMMMMMMMMMMMy   yMMMMh      mMMMM:    +MMMMM:--------.     
     /NMMMMNo`          yMMMMh:::::::::::.  mMMMMs     .NMMMM`      oNMMMModMMMMN/     -NMMMMo:.` oMMMMy   sMMMMm`     mMMMM:    +MMMMM.              
   `dMMMMMNo/////////:  -NMMMMo.      ./s.  oMMMMN/`  `+MMMMM`    .hMMMMm: `sMMMMMy`   oMMMMd     sMMMMy   .NMMMMmo//oyMMMMM:    +MMMMM+/////////.    
   -MMMMMMMMMMMMMMMMMm   -dMMMMMNdddmNMMM-  `yMMMMMMNNMMMMMMM`   /NMMMMh`    /NMMMMm-  -NMMMMmyyymMMMMMy    -hMMMMMMMMNMMMMM:    +MMMMMMMMMMMMMMM:    
   .hddddddddddddddddy     -ohmNMMMMMNmho.    :ymMMMNh+-hdddh`  +hdddd+       .hddddh-  .odNMMNds:+ddddo      ./osys+:`MMMMM-    /ddddddddddddddd-    
                                `````            ```                                        ``              `+/-.` `.:hMMMMd                          
                                                                                                            `NMMMMMMMMMMMMy`                          
                                                                                                            `oyyhddddhyo/`                            
 * @Date: 2018-11-22 13:56:38 
 * @Last Modified by: ZedXagE - ITmyWay
 * @Last Modified time: 2018-11-22 14:11:44
 */

@isTest(seealldata=true)
public class AssetAssemblyTest {
	static TestMethod void tesAssetAssembly() {
		String rol='PLC battery 12V5Ah';
		String Roboid = [select id from RecordType where Name='Robotic Unit' and IsActive=true and SobjectType='Asset__c' limit 1].id;
		String lineid = [select id from RecordType where Name='Asset line' and IsActive=true and SobjectType='Asset__c' limit 1].id;
		String wareid = [select id from RecordType where Name='Warehouse' and IsActive=true and SobjectType='Site__c' limit 1].id;
		String siteid = [select id from RecordType where Name='Site' and IsActive=true and SobjectType='Site__c' limit 1].id;
		Product2 prod2 = [select id from Product2 where Name='Robot - E4'];
		Site__c site = new Site__c(name='testsite',RecordTypeid=siteid);
		insert site;
		Site__c ware = new Site__c(name='testware',RecordTypeid=wareid);
		insert ware;
		Asset__c ass = new Asset__c(Name='test',RecordTypeid=Roboid,Site__c=site.id,Product_Name__c=prod2.id);
		insert ass;
		WorkOrder wok = new WorkOrder(Asset__c=ass.id);
		insert wok;
		WorkOrderLineItem wokl = new WorkOrderLineItem(WorkOrderId=wok.id,Role__c=rol);
		insert wokl;
		List <Asset__c> assets = new List <Asset__c>();
		for(Product2 prod:[select id,Role__c,Quantity__c,Type__c,(select id,Role__c,Quantity__c,Type__c from Products__r where Role__c!=null and ((Type__c='Batch' and Quantity__c!=null) OR (Type__c!='Batch')) Order by Role__c) from Product2 where Parent_Product__c=:prod2.id and Role__c!=null and ((Type__c='Batch' and Quantity__c!=null) OR (Type__c!='Batch')) Order by Role__c]){
			Asset__C newass =new Asset__c();
			newass.Quantity__c = prod.Quantity__c!=null?prod.Quantity__c+100:null;
			newass.Name = prod.Role__c;
			newass.Product_Name__c = prod.id;
			newass.Site__c = ware.id;
			newass.RecordTypeId = lineid;
			assets.add(newass);
			Asset__C newass2 = newass.clone();
			assets.add(newass2);
		}
		insert assets;
		assets = [select id,Role__c from Asset__c where Site__c=:ware.id];
		List <Asset__c> subassets = new List <Asset__c>();
		for(Product2 prod:[select id,Role__c,Quantity__c,Type__c,(select id,Role__c,Quantity__c,Type__c from Products__r where Role__c!=null and ((Type__c='Batch' and Quantity__c!=null) OR (Type__c!='Batch')) Order by Role__c) from Product2 where Parent_Product__c=:prod2.id and Role__c!=null and ((Type__c='Batch' and Quantity__c!=null) OR (Type__c!='Batch')) Order by Role__c]){
			for(Product2 subprod:prod.Products__r){
				Asset__C newass = new Asset__c();
				newass.Quantity__c = subprod.Quantity__c!=null?subprod.Quantity__c+100:null;
				newass.Name = subprod.Role__c;
				newass.Product_Name__c = subprod.id;
				newass.Site__c = ware.id;
				newass.RecordTypeId = lineid;
				for(Asset__c asset:assets)
					if(asset.Role__c==prod.Role__c)
						newass.Parent_Asset__c = asset.id;
				subassets.add(newass);
				Asset__C newass2 = newass.clone();
				for(Asset__c asset:assets)
					if(asset.Role__c==prod.Role__c)
						newass2.Parent_Asset__c = asset.id;
				subassets.add(newass2);
			}
		}
		insert subassets;
		AssetAssemblyController con = new AssetAssemblyController();
		con.workorderid = wok.id;
		con.Warehouse = ware.id;
		con.WareHouseChange();
		System.debug(con.AssetLineByRole);
		Set <String> newvals = new Set <String>();
		for(String k:con.AssetLineByRole.keySet()){
			for(selectoption sel:con.AssetLineByRole.get(k).opts)
				if(sel.getValue()!=''&&!newvals.contains(sel.getValue())){
					con.AssetLineByRole.get(k).newval = sel.getValue();
					newvals.add(sel.getValue());
					break;
				}
			con.currentrole = k;
			con.changeParent();
		}
		con.Assign();
		for(String k:con.AssetLineByRole.keySet()){
			for(selectoption sel:con.AssetLineByRole.get(k).opts)
				if(sel.getValue()!=''&&!newvals.contains(sel.getValue())){
					con.AssetLineByRole.get(k).newval = sel.getValue();
					newvals.add(sel.getValue());
					break;
				}
			con.currentrole = k;
			con.changeParent();
		}
		con.Assign();
		for(String k:con.AssetLineByRole.keySet()){
			con.AssetLineByRole.get(k).newval = '';
			con.currentrole = k;
			con.changeParent();
		}
		con.Assign();
	}
}
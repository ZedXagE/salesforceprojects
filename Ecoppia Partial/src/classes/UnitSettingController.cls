/*
 * @Author:                                                                                                                                                      
                                                       `/////                                                                                         
    smmmmmmmmmmmmmmmm-                                 .NMMMM`  :mmmmmd-      -dmmNmd-                                           /mmmNmmmmmmmmmmm:    
    yMMMMMMMMMMMMMMMM-                                 .NMMMM`   .dMMMMN+    +NMMMMy`                                            +MMMMMMMMMMMMMMM:    
    .--------+NMMMMMh`      `:+osyso+:`         :+syso/:MMMMM`    `sMMMMMy``hMMMMN/     `:/+osyysso/-`        `:+syso/.:::::`    +MMMMM/---------`    
           `sMMMMMm:      :hMMMMMMMMMMNs`    `oNMMMMMMMMMMMMM`      :mMMMMmmMMMMy`      .MMMMMMMMMMMMNo`    `sNMMMMMMMMMMMMM:    +MMMMM.              
          /NMMMMNo`     `yMMMMo-``.oMMMMd`  `dMMMMm+:-:+MMMMM`       `yMMMMMMMN/        `y+:.```-+mMMMMo   `dMMMMNo:--/NMMMM:    +MMMMMNNNNNNNNNs     
        -dMMMMMh.       +MMMMh::::::dMMMM+  yMMMMd`    .NMMMM`        `dMMMMMMs            `-:/+oodMMMMy   sMMMMN.     mMMMM:    +MMMMMMMMMMMMMMs     
      `sMMMMMm:         hMMMMMMMMMMMMMMMMs  mMMMMo     .NMMMM`       -mMMMMMMMMh.       .smMMMMMMMMMMMMy   yMMMMh      mMMMM:    +MMMMM:--------.     
     /NMMMMNo`          yMMMMh:::::::::::.  mMMMMs     .NMMMM`      oNMMMModMMMMN/     -NMMMMo:.` oMMMMy   sMMMMm`     mMMMM:    +MMMMM.              
   `dMMMMMNo/////////:  -NMMMMo.      ./s.  oMMMMN/`  `+MMMMM`    .hMMMMm: `sMMMMMy`   oMMMMd     sMMMMy   .NMMMMmo//oyMMMMM:    +MMMMM+/////////.    
   -MMMMMMMMMMMMMMMMMm   -dMMMMMNdddmNMMM-  `yMMMMMMNNMMMMMMM`   /NMMMMh`    /NMMMMm-  -NMMMMmyyymMMMMMy    -hMMMMMMMMNMMMMM:    +MMMMMMMMMMMMMMM:    
   .hddddddddddddddddy     -ohmNMMMMMNmho.    :ymMMMNh+-hdddh`  +hdddd+       .hddddh-  .odNMMNds:+ddddo      ./osys+:`MMMMM-    /ddddddddddddddd-    
                                `````            ```                                        ``              `+/-.` `.:hMMMMd                          
                                                                                                            `NMMMMMMMMMMMMy`                          
                                                                                                            `oyyhddddhyo/`                            
 * @Date: 2018-11-22 13:56:38 
 * @Last Modified by: ZedXagE - ITmyWay
 * @Last Modified time: 2018-11-22 14:12:22
 */

public class UnitSettingController {
	private String curid;
	private String mainflds;
	public String fieldsort{get;set;}
	public String sortdir{get;set;}
	public Map <String, AssetCheck> Robots{get;set;}
	public list <String> robotssort{get;set;}
	public List <Control_Unit_Field__mdt> tabs {get;set;}
	public string tabfields;
	public class msg{
		public String msg{get;set;}
		public String typ{get;set;}
		public msg(String m,String t){
			msg = m;
			typ = t;
		}
	}
	public List<msg> msgs{get;set;}
	public Filters AssetFilter{get;set;}
	public class AssetCheck{
		public Asset__c asset{get;set;}
		public Boolean checked{get;set;}
		public AssetCheck(Asset__c ass){
			asset=ass;
			checked=false;
		}
	}
	public UnitSettingController(ApexPages.StandardController controller) {
		curid = ApexPages.currentPage().getParameters().get('id');
		AssetFilter = new Filters('Asset__c');
		AssetFilter.Showed = true;
		init();
	}
	private void init(){
		fieldsort = 'Name';
		sortdir = 'ASC';
		msgs = new List <msg>();
		tabs = [select Label,API_Field__c,Order__c from Control_Unit_Field__mdt Order by Order__c];
		tabfields = '';
		for(Control_Unit_Field__mdt cuf:tabs)
			tabfields += ','+cuf.API_Field__c;
		Search();
		AssetFilter.addFilter();
		AssetFilter.addFilter();
		AssetFilter.filters[0].field ='state__c';
		AssetFilter.filters[0].changefield();
		AssetFilter.filters[1].field = 'battery_at_end__c';
		AssetFilter.filters[1].changefield();
		System.debug('Robots: '+Robots);
	}

	public void Search(){
		System.debug(fieldsort+' '+sortdir);
		msgs.clear();
		Set <String> unitids = new Set <String>();
		String qr ='select id,Name,Asset_ID__c'+tabfields+' from Asset__c where RecordType.Name=\'Robotic Unit\' and Site__c=:curid and Asset_ID__c!=null';
		qr += AssetFilter.getQuery();
		qr += ' Order by '+fieldsort+' '+sortdir;
		System.debug(qr);
		List <Asset__c> foundrobots = new List <Asset__c>();
		try{
			foundrobots = Database.query(qr);
		}
		catch(exception e){
			msgs.add(new msg('Bad Query, please fix Filters','danger'));
			System.debug(e.getMessage());
			return;
		}
		robotssort = new List <String>();
		Robots = new Map <String, AssetCheck>();
		for(Asset__c robot:foundrobots){
			robotssort.add(robot.id);
			Robots.put(robot.id,new AssetCheck(robot));
		}
	}
	public void sendRowHardConfig() {
		msgs.clear();
		List < String > checked = new List < String > ();
		for (String k: Robots.keySet())
			if (Robots.get(k).checked)
				checked.add(Robots.get(k).asset.Asset_ID__c);
		if (checked.size() > 0) {
			if(!Test.isRunningTest())	UnitRowsCallouts.toMasterHardConfiguration(checked);
			suc();
		} else {
			sel();
		}
	}

	public void sendRowSoftConfig() {
		msgs.clear();
		List < String > checked = new List < String > ();
		for (String k: Robots.keySet())
			if (Robots.get(k).checked)
				checked.add(Robots.get(k).asset.Asset_ID__c);
		if (checked.size() > 0) {
			if(!Test.isRunningTest())	UnitRowsCallouts.toMasterSoftConfiguration(checked);
			suc();
		} else {
			sel();
		}
	}

	public void sendRowKeepAlive() {
		msgs.clear();
		List < String > checked = new List < String > ();
		for (String k: Robots.keySet())
			if (Robots.get(k).checked)
				checked.add(Robots.get(k).asset.Asset_ID__c);
		if (checked.size() > 0) {
			if(!Test.isRunningTest())	UnitRowsCallouts.toMasterRowOperation(checked, 'keepalive');
			suc();
		} else {
			sel();
		}
	}

	public void sendRowStartClean() {
		msgs.clear();
		List < String > checked = new List < String > ();
		for (String k: Robots.keySet())
			if (Robots.get(k).checked)
				checked.add(Robots.get(k).asset.Asset_ID__c);
		if (checked.size() > 0) {
			if(!Test.isRunningTest())	UnitRowsCallouts.toMasterRowOperation(checked, 'startClean');
			suc();
		} else {
			sel();
		}
	}

	public void sendRowStopClean() {
		msgs.clear();
		List < String > checked = new List < String > ();
		for (String k: Robots.keySet())
			if (Robots.get(k).checked)
				checked.add(Robots.get(k).asset.Asset_ID__c);
		if (checked.size() > 0) {
			if(!Test.isRunningTest())	UnitRowsCallouts.toMasterRowOperation(checked, 'stopClean');
			suc();
		} else {
			sel();
		}
	}

	public void sendRowReadLogs() {
		msgs.clear();
		List < String > checked = new List < String > ();
		for (String k: Robots.keySet())
			if (Robots.get(k).checked)
				checked.add(Robots.get(k).asset.Asset_ID__c);
		if (checked.size() > 0) {
			if(!Test.isRunningTest())	UnitRowsCallouts.toMasterRowOperation(checked, 'readLogs');
			suc();
		} else {
			sel();
		}
	}

	public void sendRowReadConfig() {
		msgs.clear();
		List < String > checked = new List < String > ();
		for (String k: Robots.keySet())
			if (Robots.get(k).checked)
				checked.add(Robots.get(k).asset.Asset_ID__c);
		if (checked.size() > 0) {
			if(!Test.isRunningTest())	UnitRowsCallouts.toMasterRowOperation(checked, 'readConfiguration');
			suc();
		} else {
			sel();
		}
	}
	public void sendUnitConfig() {
		msgs.clear();
		List < String > checked = new List < String > ();
		for (String k: Robots.keySet())
			if (Robots.get(k).checked)
				checked.add(Robots.get(k).asset.Asset_ID__c);
		if (checked.size() > 0) {
			if(!Test.isRunningTest())	 UnitRowsCallouts.toMasterRowOperation(checked, 'setConfig');
			suc();
		} else {
			sel();
		}
	}
	           
	public void sendCommunicationHardConfig() {
		msgs.clear();
		List < String > checked = new List < String > ();
		for (String k: Robots.keySet())
			if (Robots.get(k).checked)
				checked.add(Robots.get(k).asset.Asset_ID__c);
		if (checked.size() > 0) {
			if(!Test.isRunningTest())	CommunicationStationsCallouts.toMasterHardConfiguration(checked);
			suc();
		} else {
			sel();
		}
	}

	public void sendCommunicationSoftConfig() {
		msgs.clear();
		List < String > checked = new List < String > ();
		for (String k: Robots.keySet())
			if (Robots.get(k).checked)
				checked.add(Robots.get(k).asset.Asset_ID__c);
		if (checked.size() > 0) {
			if(!Test.isRunningTest())	CommunicationStationsCallouts.toMasterSoftConfiguration(checked);
			suc();
		} else {
			sel();
		}
	}

	public void sendMaintenance() {
		msgs.clear();
		List < String > checked = new List < String > ();
		for (String k: Robots.keySet())
			if (Robots.get(k).checked)
				checked.add(Robots.get(k).asset.Asset_ID__c);
		if (checked.size() > 0) {
			if(!Test.isRunningTest())	UnitRowsCallouts.toMasterRowOperation(checked, 'maintenance');
			suc();
		} else {
			sel();
		}
	}

	private void suc() {
		msgs.add(new msg(Label.Control_Panel_Succes,'success'));
	}
	private void sel() {
		msgs.add(new msg('Please check at least one','warning'));
	}
}
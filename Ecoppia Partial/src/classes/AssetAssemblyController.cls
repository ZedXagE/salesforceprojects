/*
 * @Author:                                                                                                                                                      
                                                       `/////                                                                                         
    smmmmmmmmmmmmmmmm-                                 .NMMMM`  :mmmmmd-      -dmmNmd-                                           /mmmNmmmmmmmmmmm:    
    yMMMMMMMMMMMMMMMM-                                 .NMMMM`   .dMMMMN+    +NMMMMy`                                            +MMMMMMMMMMMMMMM:    
    .--------+NMMMMMh`      `:+osyso+:`         :+syso/:MMMMM`    `sMMMMMy``hMMMMN/     `:/+osyysso/-`        `:+syso/.:::::`    +MMMMM/---------`    
           `sMMMMMm:      :hMMMMMMMMMMNs`    `oNMMMMMMMMMMMMM`      :mMMMMmmMMMMy`      .MMMMMMMMMMMMNo`    `sNMMMMMMMMMMMMM:    +MMMMM.              
          /NMMMMNo`     `yMMMMo-``.oMMMMd`  `dMMMMm+:-:+MMMMM`       `yMMMMMMMN/        `y+:.```-+mMMMMo   `dMMMMNo:--/NMMMM:    +MMMMMNNNNNNNNNs     
        -dMMMMMh.       +MMMMh::::::dMMMM+  yMMMMd`    .NMMMM`        `dMMMMMMs            `-:/+oodMMMMy   sMMMMN.     mMMMM:    +MMMMMMMMMMMMMMs     
      `sMMMMMm:         hMMMMMMMMMMMMMMMMs  mMMMMo     .NMMMM`       -mMMMMMMMMh.       .smMMMMMMMMMMMMy   yMMMMh      mMMMM:    +MMMMM:--------.     
     /NMMMMNo`          yMMMMh:::::::::::.  mMMMMs     .NMMMM`      oNMMMModMMMMN/     -NMMMMo:.` oMMMMy   sMMMMm`     mMMMM:    +MMMMM.              
   `dMMMMMNo/////////:  -NMMMMo.      ./s.  oMMMMN/`  `+MMMMM`    .hMMMMm: `sMMMMMy`   oMMMMd     sMMMMy   .NMMMMmo//oyMMMMM:    +MMMMM+/////////.    
   -MMMMMMMMMMMMMMMMMm   -dMMMMMNdddmNMMM-  `yMMMMMMNNMMMMMMM`   /NMMMMh`    /NMMMMm-  -NMMMMmyyymMMMMMy    -hMMMMMMMMNMMMMM:    +MMMMMMMMMMMMMMM:    
   .hddddddddddddddddy     -ohmNMMMMMNmho.    :ymMMMNh+-hdddh`  +hdddd+       .hddddh-  .odNMMNds:+ddddo      ./osys+:`MMMMM-    /ddddddddddddddd-    
                                `````            ```                                        ``              `+/-.` `.:hMMMMd                          
                                                                                                            `NMMMMMMMMMMMMy`                          
                                                                                                            `oyyhddddhyo/`                            
 * @Date: 2018-11-22 13:56:38 
 * @Last Modified by: ZedXagE - ITmyWay
 * @Last Modified time: 2018-11-22 14:11:38
 */

public with sharing class AssetAssemblyController {
	public List<msg> msgs{get;set;}
	public class msg{
		public String msg{get;set;}
		public String typ{get;set;}
		public msg(String m,String t){
			msg = m;
			typ = t;
		}
	}
	public String wareid;
	public String lineid;
	public Asset__c asset{get;set;}
	public WorkOrder workorder{get;set;}
	public Map <String, Code> RolesByCode{get;set;}
	public class Code{
		public Map <String,String> roles{get;set;}
		public Code(String role,String prod){
			roles = new Map <String,String>{role=>prod};
		}
	}
	public Map <String, WorkOrderLineItem> workorderlines{get;set;}
	public String warehouse{get;set;}
	public String currentrole{get;set;}
	public String condition{get;set;}
	public String defaultwarehouse{get;set;}
	public String workorderStatus{get;set;}
	public list <selectoption> warehouses{get;set;}
	private list <selectoption> workingpick;
	public List <String> ordr{get;set;}
	public Map <String, AssetOptions> AssetLineByRole{get;set;}
	public String assetid{get;
		set{
			if(assetid!=value&&value!=null){
				assetid = value;
				getAsset(value);
			}
		}
	}
	public String workorderid{get;
		set{
			if(workorderid!=value&&value!=null){
				workorderid = value;
				getWorkOrder(value);
			}
		}
	}
	public class AssetOptions{
		public String Role{get;set;}
		public Map <String, Asset__c> assets{get;set;}
		public List <selectoption> opts{get;set;}
		public List <selectoption> modes{get;set;}
		public String mode{get;set;}
		public String current{get;set;}
		public Boolean todo{get;set;}
		public Decimal minQuantity{get;set;}
		public String newval{get;set;}
		public Boolean hassub{get;set;}
		public Map <String, SubAssetOptions> subs{get;set;}
		public AssetOptions(String rol,Decimal quan,List <selectoption> mds,Boolean tdo){
			hassub = false;
			Role = rol;
			minQuantity = quan;
			assets = new Map <String, Asset__c>();
			subs = new Map <String, SubAssetOptions>();
			opts= new List <selectoption>();
			modes = mds;
			todo = tdo;
		}
	}
	public class SubAssetOptions{
		public String Role{get;set;}
		public Map <String, Asset__c> assets{get;set;}
		public List <selectoption> opts{get;set;}
		public List <selectoption> modes{get;set;}
		public String mode{get;set;}
		public String current{get;set;}
		public Boolean todo{get;set;}
		public Decimal minQuantity{get;set;}
		public String newval{get;set;}
		public SubAssetOptions(String rol,Decimal quan,List <selectoption> mds,Boolean tdo){
			Role = rol;
			minQuantity = quan;
			assets = new Map <String, Asset__c>();
			opts= new List <selectoption>();
			modes = mds;
			todo = tdo;
		}
	}
	public void changeParent(){
		if(currentrole!=null&&currentrole!=''){
			if(AssetLineByRole.get(currentrole).newval!=null&&AssetLineByRole.get(currentrole).newval!=''){
				for(Asset__c subass:AssetLineByRole.get(currentrole).assets.get(AssetLineByRole.get(currentrole).newval).Assets__r){
					if(AssetLineByRole.get(currentrole).subs.containsKey(subass.Role__c)&&AssetLineByRole.get(currentrole).subs.get(subass.Role__c).assets.containsKey(subass.id))
						AssetLineByRole.get(currentrole).subs.get(subass.Role__c).newval = subass.id;
				}
			}
			else{
				for(String k:AssetLineByRole.get(currentrole).subs.KeySet()){
					AssetLineByRole.get(currentrole).subs.get(k).newval = null;
				}
			}
		}
	}
	private void getAsset(String val){
		workorderlines = new Map <String, WorkOrderLineItem>();
		if(workorderid!=null&&workorderid!=''){
			for(WorkOrderLineItem lin:workorder.WorkOrderLineItems)
				workorderlines.put(lin.Role__c,lin);
		}
		lineid = [select id from RecordType where Name='Asset line' and IsActive=true and SobjectType='Asset__c' limit 1].id;
		asset = [select id,Name,Product_Name__c,Product_Name__r.Name,Site__c from Asset__c where id=:val];
		ordr = new List <String>();
		AssetLineByRole = new Map <String, AssetOptions>();
		RolesByCode = new Map <String, Code>();
		workingpick = new List <selectoption>();
		msgs = new List <msg>();
		workingpick.add(new selectoption('','--None--'));
		String[] types = new String[]{'Asset__c'};
		Schema.DescribeSobjectResult[] results = Schema.describeSObjects(types);
		for(Schema.DescribeSobjectResult res : results)
			for (Schema.PicklistEntry entry : res.fields.getMap().get('Mode__c').getDescribe().getPicklistValues())
				if(entry.isActive())
					workingpick.add(new selectoption(entry.getValue(),entry.getLabel()));
		if(asset.Product_Name__c!=null){
			for(Product2 prod:[select id,Role__c,ProductCode,Quantity__c,Type__c,Assembly__c,(select id,Role__c,ProductCode,Quantity__c,Type__c from Products__r where Role__c!=null and ProductCode!=null and ((Type__c='Batch' and Quantity__c!=null) OR (Type__c!='Batch')) Order by Role__c) from Product2 where Parent_Product__c=:asset.Product_Name__c and Role__c!=null and ProductCode!=null and ((Type__c='Batch' and Quantity__c!=null) OR (Type__c!='Batch')) Order by Assembly__c desc,Role__c]){
				AssetLineByRole.put(prod.Role__c,new AssetOptions(prod.Role__c,prod.Type__c=='Batch'?prod.Quantity__c:1,workingpick,workorderlines.containskey(prod.Role__c)?true:false));
				if(!RolesByCode.containsKey(prod.ProductCode))
					RolesByCode.put(prod.ProductCode,new code(prod.Role__c,prod.id));
				else
					RolesByCode.get(prod.ProductCode).roles.put(prod.Role__c,prod.id);
				if(!ordr.contains(prod.Role__c))
					ordr.add(prod.Role__c);
				for(Product2 subprod:prod.Products__r){
					AssetLineByRole.get(prod.Role__c).hassub=true;
					AssetLineByRole.get(prod.Role__c).subs.put(subprod.Role__c,new SubAssetOptions(subprod.Role__c,subprod.Type__c=='Batch'?subprod.Quantity__c:1,workingpick,workorderlines.containskey(subprod.Role__c)?true:false));
					if(!RolesByCode.containsKey(subprod.ProductCode))
						RolesByCode.put(subprod.ProductCode,new code(subprod.Role__c,subprod.id));
					else
						RolesByCode.get(subprod.ProductCode).roles.put(subprod.Role__c,subprod.id);
				}
			}
			System.debug(ordr);
			for(String k:RolesByCode.keySet())
				System.debug(k+' - '+RolesByCode.get(k));
			getWareHouses();
		}
	}
	private void getCurrentAssets(){
		for(String k:AssetLineByRole.keySet()){
			AssetLineByRole.get(k).newval = null;
			AssetLineByRole.get(k).current = null;
			AssetLineByRole.get(k).assets.clear();
			AssetLineByRole.get(k).opts.clear();
			AssetLineByRole.get(k).mode=null;
			for(String k2:AssetLineByRole.get(k).subs.keySet()){
				AssetLineByRole.get(k).subs.get(k2).newval = null;
				AssetLineByRole.get(k).subs.get(k2).current = null;
				AssetLineByRole.get(k).subs.get(k2).assets.clear();
				AssetLineByRole.get(k).subs.get(k2).opts.clear();
				AssetLineByRole.get(k).subs.get(k2).mode=null;
			}
		}
		for(Asset__c ass:[select id,Role__c,Product_Name__r.ProductCode,Name,Asset_ID__c,Mode__c,Installation_Date__c,Quantity__c,Batch__c,Product_Name__c,Product_Name__r.Name,Parent_Asset__c,Inventory_Status__c,Product_Name__r.Assembly__c,Picklist_Name__c,(select id,Role__c,Product_Name__r.ProductCode,Name,Asset_ID__c,Mode__c,Installation_Date__c,Quantity__c,Batch__c,Product_Name__c,Product_Name__r.Name,Parent_Asset__c,Inventory_Status__c,Picklist_Name__c from Assets__r where RecordTypeId=:lineid and Role__c!=null and Product_Name__r.ProductCode!=null Order by Role__c) from Asset__c where Parent_Asset__c=:asset.id and RecordTypeid=:lineid and Role__c!=null and Product_Name__r.ProductCode!=null Order by Product_Name__r.Assembly__c desc,Role__c]){
			if(AssetLineByRole.containsKey(ass.Role__c)){
				AssetLineByRole.get(ass.Role__c).current=ass.id;
				AssetLineByRole.get(ass.Role__c).newval=ass.id;
				AssetLineByRole.get(ass.Role__c).assets.put(ass.id,ass);
				for(Asset__c subass:ass.Assets__r)
					if(AssetLineByRole.get(ass.Role__c).subs.containsKey(subass.Role__c)){
						AssetLineByRole.get(ass.Role__c).subs.get(subass.Role__c).current=subass.id;
						AssetLineByRole.get(ass.Role__c).subs.get(subass.Role__c).newval=subass.id;
						AssetLineByRole.get(ass.Role__c).subs.get(subass.Role__c).assets.put(subass.id,subass);
					}
			}
		}
	}
	private void getWorkOrder(String val){
		workorder = [select id,Asset__c,Status,(select id,WorkOrderId,Role__c,Installed_Asset__c,Removed_Asset__c,Status,Completed__c from WorkOrderLineItems where Status!='Completed' and Role__c!=null) from WorkOrder where id=:val];
		System.debug(workorder.Asset__c);
		if(workorder.Asset__c!=null)
			assetid = workorder.Asset__c;
	}
	private void getWareHouses(){
		wareid = [select id from RecordType where Name='Warehouse' and IsActive=true and SobjectType='Site__c' limit 1].id;
		warehouses = new List <selectoption>();
		warehouses.add(new selectoption('','--Warehouses--'));
		warehouse = '';
		defaultwarehouse = null;
		condition = 'All';
		for(ServiceResource res:[select Default_warehouse__c from ServiceResource where RelatedRecordId=:Userinfo.getUserId() limit 1])
			defaultwarehouse = res.Default_warehouse__c;
		for(Site__c ware:[select id,Name from Site__c where RecordTypeId=:wareid  Order by Name])
			warehouses.add(new selectoption(ware.id,ware.name));
		if(defaultwarehouse!=null)
			warehouse = defaultwarehouse;
		WareHouseChange();
	}
	public void WareHouseChange(){
		getCurrentAssets();	
		if(warehouse!=''){
			String qr = 'select id,Role__c,Product_Name__r.ProductCode,Name,Asset_ID__c,Inventory_Status__c,Mode__c,Installation_Date__c,Quantity__c,Batch__c,Product_Name__c,Product_Name__r.Name,Product_Name__r.Assembly__c,Parent_Asset__c,Picklist_Name__c,(select id,Role__c,Product_Name__r.ProductCode,Name,Asset_ID__c,Inventory_Status__c,Mode__c,Installation_Date__c,Quantity__c,Batch__c,Product_Name__c,Product_Name__r.Name,Parent_Asset__c,Picklist_Name__c from Assets__r where Role__c!=null and Product_Name__r.ProductCode!=null) from Asset__c where Site__c=:warehouse and RecordTypeId=:lineid and Parent_Asset__c=null and Role__c!=null and Product_Name__r.ProductCode!=null';
			if(condition!='All')
				qr+=' and Inventory_Status__c=\''+condition+'\'';
			qr +=' Order by Product_Name__r.Assembly__c desc,Role__c';
			List <Asset__c> wareassets = Database.query(qr);
			for(Asset__c ass:wareassets){
				System.debug(ass.Product_Name__r.ProductCode);
				if(RolesByCode.containsKey(ass.Product_Name__r.ProductCode)){
					for(String Role:RolesByCode.get(ass.Product_Name__r.ProductCode).roles.keySet()){
						if(AssetLineByRole.containsKey(Role)){
							if(AssetLineByRole.get(Role).minQuantity==1||(AssetLineByRole.get(Role).minQuantity!=1&&ass.Quantity__c!=null&&AssetLineByRole.get(Role).minQuantity<=ass.Quantity__c)){
								AssetLineByRole.get(Role).assets.put(ass.id,ass);
								for(Asset__c asspar:ass.Assets__r){
									if(RolesByCode.containsKey(asspar.Product_Name__r.ProductCode)){
										for(String subRole:RolesByCode.get(asspar.Product_Name__r.ProductCode).roles.keySet())
											if(AssetLineByRole.get(Role).subs.containsKey(subRole)&&(AssetLineByRole.get(Role).subs.get(subRole).minQuantity==1||(AssetLineByRole.get(Role).subs.get(subRole).minQuantity!=1&&asspar.Quantity__c!=null&&AssetLineByRole.get(Role).subs.get(subRole).minQuantity<=asspar.Quantity__c)))
												AssetLineByRole.get(Role).subs.get(subRole).assets.put(asspar.id,asspar);
									}
								}
							}
						}
						else 
							for(String k:AssetLineByRole.keySet()){
								if(AssetLineByRole.get(k).subs.containsKey(Role))
									if(AssetLineByRole.get(k).subs.get(Role).minQuantity==1||(AssetLineByRole.get(k).subs.get(Role).minQuantity!=1&&ass.Quantity__c!=null&&AssetLineByRole.get(k).subs.get(Role).minQuantity<=ass.Quantity__c))
										AssetLineByRole.get(k).subs.get(Role).assets.put(ass.id,ass);
							}
					}
				}
			}
		}
		genOpts();
	}
	public void genOpts(){
		for(String k:AssetLineByRole.keySet()){
			AssetLineByRole.get(k).opts.add(new selectoption('','--None--'));
			for(String ak:AssetLineByRole.get(k).assets.keySet())
				AssetLineByRole.get(k).opts.add( new selectoption(ak,AssetLineByRole.get(k).assets.get(ak).Picklist_Name__c));
			AssetLineByRole.get(k).opts.sort();
			for(String k2:AssetLineByRole.get(k).subs.keySet()){
				AssetLineByRole.get(k).subs.get(k2).opts.add(new selectoption('','--None--'));
				for(String ak:AssetLineByRole.get(k).subs.get(k2).assets.keySet())
					AssetLineByRole.get(k).subs.get(k2).opts.add( new selectoption(
						ak,
						(AssetLineByRole.get(k).subs.get(k2).assets.get(ak).Parent_Asset__c!=null&&AssetLineByRole.get(k).assets.containsKey(AssetLineByRole.get(k).subs.get(k2).assets.get(ak).Parent_Asset__c)?(AssetLineByRole.get(k).assets.get(AssetLineByRole.get(k).subs.get(k2).assets.get(ak).Parent_Asset__c).Picklist_Name__c)+' → ':'')
						+(AssetLineByRole.get(k).subs.get(k2).assets.get(ak).Picklist_Name__c)
						,AssetLineByRole.get(k).subs.get(k2).assets.get(ak).Parent_Asset__c!=null)
						);
				AssetLineByRole.get(k).subs.get(k2).opts.sort();
			}
		}
	}
	private WorkOrderLineItem getWorkLine(String status,Boolean cut,String role,String toass,String fromass){
		WorkOrderLineItem workl = new WorkOrderLineItem();
		workl.WorkOrderId=workorderid;
		workl.Status=status;
		workl.Role__c=role;
		workl.Installed_Asset__c=toass;
		workl.Removed_Asset__c=fromass;
		if(cut!=true)
			setDescription(workl);
		return workl;
	}
	private void setDescription(WorkOrderLineItem workl){
		if(AssetLineByRole.containsKey(workl.Role__c)){
			if(workl.Status=='new'){
				workl.Description=AssetLineByRole.get(workl.Role__c).assets.get(workl.Installed_Asset__c).Product_Name__r.Name+' ('+workl.Role__c+') Was Installed';
			}
			else if(workl.Status=='replace'){
				workl.Description=AssetLineByRole.get(workl.Role__c).assets.get(workl.Removed_Asset__c).Product_Name__r.Name+' ('+workl.Role__c+') Was Replaced from '+(AssetLineByRole.get(workl.Role__c).assets.get(workl.Removed_Asset__c).Asset_ID__c!=null?AssetLineByRole.get(workl.Role__c).assets.get(workl.Removed_Asset__c).Asset_ID__c:'')+' to '+(AssetLineByRole.get(workl.Role__c).assets.get(workl.Installed_Asset__c).Asset_ID__c!=null?AssetLineByRole.get(workl.Role__c).assets.get(workl.Installed_Asset__c).Asset_ID__c:'');

			}
			else if(workl.Status=='delete'){
				workl.Description=AssetLineByRole.get(workl.Role__c).assets.get(workl.Removed_Asset__c).Product_Name__r.Name+' ('+workl.Role__c+') Was Removed';
			}
		}
		else{
			for(String k:AssetLineByRole.KeySet()){
				if(AssetLineByRole.get(k).subs.containsKey(workl.Role__c)){
						if(workl.Status=='new'){
							workl.Description=AssetLineByRole.get(k).subs.get(workl.Role__c).assets.get(workl.Installed_Asset__c).Product_Name__r.Name+' ('+workl.Role__c+') Was Installed';
						}
						else if(workl.Status=='replace'){
							workl.Description=AssetLineByRole.get(k).subs.get(workl.Role__c).assets.get(workl.Removed_Asset__c).Product_Name__r.Name+' ('+workl.Role__c+') Was Replaced from '+(AssetLineByRole.get(k).subs.get(workl.Role__c).assets.get(workl.Removed_Asset__c).Asset_ID__c!=null?AssetLineByRole.get(k).subs.get(workl.Role__c).assets.get(workl.Removed_Asset__c).Asset_ID__c:'')+' to '+(AssetLineByRole.get(k).subs.get(workl.Role__c).assets.get(workl.Installed_Asset__c).Asset_ID__c!=null?AssetLineByRole.get(k).subs.get(workl.Role__c).assets.get(workl.Installed_Asset__c).Asset_ID__c:'');
						}
						else if(workl.Status=='delete'){
							workl.Description=AssetLineByRole.get(k).subs.get(workl.Role__c).assets.get(workl.Removed_Asset__c).Product_Name__r.Name+' ('+workl.Role__c+') Was Removed';
						}
					break;
				}
			}
		}
		workl.Completed__c = true;
		workl.Status='Completed';
	}
	public void Assign(){
		msgs.clear();
		Map <String, Asset__c> assets4upsert = new Map <String, Asset__c>();
		Map <String,WorkOrderLineItem> worklines = new Map <String,WorkOrderLineItem>();
		Set <String> newvals = new Set <String>();
		for(String k:AssetLineByRole.keySet()){
			if(!newvals.contains(AssetLineByRole.get(k).newval)){
				if(AssetLineByRole.get(k).newval!=null)
					newvals.add(AssetLineByRole.get(k).newval);
			}
			else{
				msgs.add(new msg('Same Asset chosen for more than 1 role','danger'));
				return;
			}
			assets4upsert.putAll(checkScenario(k,null,worklines));
			for(String k2:AssetLineByRole.get(k).subs.keySet()){
				if(!newvals.contains(AssetLineByRole.get(k).subs.get(k2).newval)){
					if(AssetLineByRole.get(k).subs.get(k2).newval!=null)
						newvals.add(AssetLineByRole.get(k).subs.get(k2).newval);
				}
				else{
					msgs.add(new msg('Same Asset chosen for more than 1 role','danger'));
					return;
				}
				assets4upsert.putAll(checkScenario(k,k2,worklines));
			}
		}
		upsert assets4upsert.values();
		WareHouseChange();
		if(workorderid!=null&&workorderid!=''){
			if(workorderStatus!=null&&workorderStatus!=workorder.Status){
				workorder.Status=workorderStatus;
				update workorder;
			}
			workorderStatus = null;
			List <WorkOrderLineItem> line4up = new List <WorkOrderLineItem>();
			for(String k:worklines.KeySet())
				if(workorderlines.containsKey(k)){
					workorderlines.get(k).Status='Completed';
					workorderlines.get(k).Completed__c = true;
					worklines.remove(k);
					line4up.add(workorderlines.get(k));
				}
				else if(worklines.get(k).Status!='Completed'){
					for(String k2:assets4upsert.keyset())
						if(k2.contains(k)){
							worklines.get(k).Installed_Asset__c = assets4upsert.get(k2).id;
							break;
						}
					setDescription(worklines.get(k));
					worklines.get(k).Completed__c = true;
				}
			insert worklines.values();
			update line4up;
			for(String w:workorderlines.KeySet())
				if(workorderlines.get(w).Status=='Completed'){
					workorderlines.remove(w);
					for(String k:AssetLineByRole.keySet()){
						if(k==w){
							AssetLineByRole.get(k).todo=false;
							break;
						}
						for(String k2:AssetLineByRole.get(k).subs.keySet()){
							if(k2==w){
								AssetLineByRole.get(k).subs.get(k2).todo=false;
								break;
							}
						}
					}
				}
		}
		System.debug(worklines.size());
		System.debug(assets4upsert);
	}
	private void clearChildsLkp(String role,String assid,Map <String, Asset__c> changedassets){
		for(Asset__c subass:AssetLineByRole.get(role).assets.get(assid).Assets__r){
			for(String k:AssetLineByRole.get(role).subs.keyset())
				if(k==subass.Role__c){
					if(AssetLineByRole.get(role).subs.get(k).newval!=subass.id){
						subass.Parent_Asset__c = null;
						changedassets.put('Releaseit;'+subass.id,subass);
					}
					break;
				}
		}
	}
	public Map <String, Asset__c> checkScenario(String role, String subrole, Map <String,WorkOrderLineItem> worklines){
		Map <String, Asset__c> changedassets = new Map <String, Asset__c>();
		if(subrole==null){
			//assign new
			if(AssetLineByRole.get(role).current==null&&AssetLineByRole.get(role).newval!=null&&AssetLineByRole.get(role).newval!=''){
				Asset__c curass = AssetLineByRole.get(role).assets.get(AssetLineByRole.get(role).newval);
				Decimal minQ = AssetLineByRole.get(role).minQuantity;
				if(minQ > 1&&curass.Quantity__c > minQ){
					curass.Quantity__c -= minQ;
					Asset__c newass = new Asset__c();
					newass.Quantity__c = minQ;
					newass.Name = curass.Name;
					newass.Product_Name__c = RolesByCode.get(curass.Product_Name__r.ProductCode).roles.get(role);
					newass.Site__c = asset.Site__c;
					newass.Parent_Asset__c = asset.id;
					newass.Batch__c = curass.id;
					newass.Installation_Date__c = System.Today();
					newass.Inventory_Status__c = curass.Inventory_Status__c;
					newass.RecordTypeId = lineid;
					changedassets.put('AssignCutNew;'+role,newass);
					changedassets.put('AssignCutFrom;'+role,curass);
					worklines.put(role,getWorkLine('new',true,role,null,null));
				}
				else{
					curass.Parent_Asset__c = asset.id;
					curass.Site__c = asset.Site__c;
					curass.Installation_Date__c = System.Today();
					curass.Product_Name__c = RolesByCode.get(curass.Product_Name__r.ProductCode).roles.get(role);
					changedassets.put('AssignNew;'+role,curass);
					worklines.put(role,getWorkLine('new',false,role,curass.id,null));
					clearChildsLkp(role,curass.id,changedassets);
				}
			}
			//replace exsiting
			else if(AssetLineByRole.get(role).current!=null&&AssetLineByRole.get(role).newval!=null&&AssetLineByRole.get(role).newval!=''&&AssetLineByRole.get(role).current!=AssetLineByRole.get(role).newval){
				Asset__c oldass = AssetLineByRole.get(role).assets.get(AssetLineByRole.get(role).current);
				Asset__c curass = AssetLineByRole.get(role).assets.get(AssetLineByRole.get(role).newval);
				Decimal minQ = AssetLineByRole.get(role).minQuantity;
				if(minQ > 1&&curass.Quantity__c > minQ){
					oldass.Parent_Asset__c = null;
					oldass.Site__c = warehouse;
					oldass.Mode__c = AssetLineByRole.get(role).mode;
					oldass.Inventory_Status__c = 'Used';
					changedassets.put('ReplacedOld;'+role,oldass);

					curass.Quantity__c -= minQ;
					changedassets.put('ReplacedCutFrom;'+role,curass);

					Asset__c newass = new Asset__c();
					newass.Quantity__c = minQ;
					newass.Name = curass.Name;
					newass.Product_Name__c = RolesByCode.get(curass.Product_Name__r.ProductCode).roles.get(role);
					newass.Site__c = asset.Site__c;
					newass.Parent_Asset__c = asset.id;
					newass.Batch__c = curass.id;
					newass.Installation_Date__c = System.Today();
					newass.Inventory_Status__c = curass.Inventory_Status__c;
					newass.RecordTypeId = lineid;
					changedassets.put('ReplacedCutNew;'+role,newass);
					worklines.put(role,getWorkLine('replace',true,role,null,oldass.id));
				}
				else{
					oldass.Parent_Asset__c = null;
					oldass.Site__c = warehouse;
					oldass.Mode__c = AssetLineByRole.get(role).mode;
					oldass.Inventory_Status__c = 'Used';
					changedassets.put('ReplacedOld;'+role,oldass);

					curass.Parent_Asset__c = asset.id;
					curass.Site__c = asset.Site__c;
					curass.Installation_Date__c = System.Today();
					curass.Product_Name__c = RolesByCode.get(curass.Product_Name__r.ProductCode).roles.get(role);
					changedassets.put('ReplacedNew;'+role,curass);
					worklines.put(role,getWorkLine('replace',false,role,curass.id,oldass.id));
					clearChildsLkp(role,curass.id,changedassets);
				}
			}
			//delete exsiting
			else if(AssetLineByRole.get(role).current!=null&&(AssetLineByRole.get(role).newval==null||AssetLineByRole.get(role).newval=='')){
				Asset__c oldass = AssetLineByRole.get(role).assets.get(AssetLineByRole.get(role).current);
				oldass.Parent_Asset__c = null;
				oldass.Site__c = warehouse;
				oldass.Mode__c = AssetLineByRole.get(role).mode;
				oldass.Inventory_Status__c = 'Used';
				changedassets.put('DeleteOld;'+role,oldass);
				worklines.put(role,getWorkLine('delete',false,role,null,oldass.id));
			}
		}
		else{
			//assign new
			if(AssetLineByRole.get(role).subs.get(subrole).current==null&&AssetLineByRole.get(role).subs.get(subrole).newval!=null&&AssetLineByRole.get(role).subs.get(subrole).newval!=''){
				Asset__c curass = AssetLineByRole.get(role).subs.get(subrole).assets.get(AssetLineByRole.get(role).subs.get(subrole).newval);
				Decimal minQ = AssetLineByRole.get(role).subs.get(subrole).minQuantity;
				if(minQ > 1&&curass.Quantity__c > minQ){
					curass.Quantity__c -= minQ;
					Asset__c newass = new Asset__c();
					newass.Quantity__c = minQ;
					newass.Name = curass.Name;
					newass.Product_Name__c = RolesByCode.get(curass.Product_Name__r.ProductCode).roles.get(subrole);
					newass.Site__c = asset.Site__c;
					newass.Batch__c = curass.id;
					newass.Installation_Date__c = System.Today();
					newass.Inventory_Status__c = curass.Inventory_Status__c;
					newass.Parent_Asset__c = AssetLineByRole.get(role).newval!=''?AssetLineByRole.get(role).newval:null;
					changedassets.put('AssignCutNew;'+subrole,newass);
					changedassets.put('AssignCutFrom;'+subrole,curass);
					worklines.put(subrole,getWorkLine('new',true,subrole,null,null));
				}
				else{
					curass.Site__c = asset.Site__c;
					curass.Installation_Date__c = System.Today();
					curass.Parent_Asset__c = AssetLineByRole.get(role).newval!=''?AssetLineByRole.get(role).newval:null;
					curass.Product_Name__c = RolesByCode.get(curass.Product_Name__r.ProductCode).roles.get(subrole);
					changedassets.put('AssignNew;'+subrole,curass);
					worklines.put(subrole,getWorkLine('new',false,subrole,curass.id,null));
				}
			}
			//replace exsiting
			else if(AssetLineByRole.get(role).subs.get(subrole).current!=null&&AssetLineByRole.get(role).subs.get(subrole).newval!=null&&AssetLineByRole.get(role).subs.get(subrole).newval!=''&&AssetLineByRole.get(role).subs.get(subrole).current!=AssetLineByRole.get(role).subs.get(subrole).newval){
				Asset__c oldass = AssetLineByRole.get(role).subs.get(subrole).assets.get(AssetLineByRole.get(role).subs.get(subrole).current);
				Asset__c curass = AssetLineByRole.get(role).subs.get(subrole).assets.get(AssetLineByRole.get(role).subs.get(subrole).newval);
				Decimal minQ = AssetLineByRole.get(role).subs.get(subrole).minQuantity;
				if(minQ > 1&&curass.Quantity__c > minQ){
					oldass.Parent_Asset__c = null;
					oldass.Site__c = warehouse;
					oldass.Mode__c = AssetLineByRole.get(role).subs.get(subrole).mode;
					oldass.Inventory_Status__c = 'Used';
					changedassets.put('ReplacedOld;'+subrole,oldass);

					curass.Quantity__c -= minQ;
					changedassets.put('ReplacedCutFrom;'+subrole,curass);

					Asset__c newass = new Asset__c();
					newass.Quantity__c = minQ;
					newass.Name = curass.Name;
					newass.Product_Name__c = RolesByCode.get(curass.Product_Name__r.ProductCode).roles.get(subrole);
					newass.Site__c = asset.Site__c;
					newass.Parent_Asset__c = AssetLineByRole.get(role).newval!=''?AssetLineByRole.get(role).newval:null;
					newass.Batch__c = curass.id;
					newass.Installation_Date__c = System.Today();
					newass.Inventory_Status__c = curass.Inventory_Status__c;
					newass.RecordTypeId = lineid;
					changedassets.put('ReplacedCutNew;'+subrole,newass);
					worklines.put(subrole,getWorkLine('replace',true,subrole,null,oldass.id));
				}
				else{
					oldass.Parent_Asset__c = null;
					oldass.Site__c = warehouse;
					oldass.Mode__c = AssetLineByRole.get(role).subs.get(subrole).mode;
					oldass.Inventory_Status__c = 'Used';
					changedassets.put('ReplacedOld;'+subrole,oldass);

					curass.Parent_Asset__c = AssetLineByRole.get(role).newval!=''?AssetLineByRole.get(role).newval:null;
					curass.Site__c = asset.Site__c;
					curass.Installation_Date__c = System.Today();
					curass.Product_Name__c = RolesByCode.get(curass.Product_Name__r.ProductCode).roles.get(subrole);
					changedassets.put('ReplacedNew;'+subrole,curass);
					worklines.put(subrole,getWorkLine('replace',false,subrole,curass.id,oldass.id));
				}
			}
			//delete exsiting
			else if(AssetLineByRole.get(role).subs.get(subrole).current!=null&&(AssetLineByRole.get(role).subs.get(subrole).newval==null||AssetLineByRole.get(role).subs.get(subrole).newval=='')){
				Asset__c oldass = AssetLineByRole.get(role).subs.get(subrole).assets.get(AssetLineByRole.get(role).subs.get(subrole).current);
				oldass.Parent_Asset__c = null;
				oldass.Site__c = warehouse;
				oldass.Mode__c = AssetLineByRole.get(role).mode;
				oldass.Inventory_Status__c = 'Used';
				changedassets.put('DeleteOld;'+subrole,oldass);
				worklines.put(subrole,getWorkLine('delete',false,subrole,null,oldass.id));
			}
		}
		return changedassets;
	}
	public AssetAssemblyController() {
	}
}
/*
 * @Author: ZedXagE - ITmyWay 
 * @Date: 2018-05-06 11:47:07 
 * @Last Modified by: ZedXagE - ITmyWay
 * @Last Modified time: 2018-05-06 11:55:28
 */

public class GDPR_Handler {
	
	public static Set <String> data0;
	public static Set <String> data1;
	public static Set <String> data2;
	public static Set <String> data3;
	public static Set <String> data4;
	public static Set <String> data5;
	public static Set <String> data6;
	public static Set <String> data7;
	public static Set <String> data8;
	public static Set <String> data9;
	public static void setData(Integer n, Set<String> data) {
			 if (n == 0) data0 = data;
		else if (n == 1) data1 = data;
		else if (n == 2) data2 = data;
		else if (n == 3) data3 = data;
		else if (n == 4) data4 = data;
		else if (n == 5) data5 = data;
		else if (n == 6) data6 = data;
		else if (n == 7) data7 = data;
		else if (n == 8) data8 = data;
		else if (n == 9) data9 = data;
	}

	//insert default Records to CS
	public static void DefaultsCreate () {
		List <ITGDPR_Settings__c> gdpr_settings = new List <ITGDPR_Settings__c>();
		gdpr_settings.add(new ITGDPR_Settings__c(Name='1',Copy__c=true,Lead_API_Field__c='LastName',Contact_API_Field__c='LastName',Individual_API_Field__c='LastName'));
		gdpr_settings.add(new ITGDPR_Settings__c(Name='2',Copy__c=true,Lead_API_Field__c='FirstName',Contact_API_Field__c='FirstName',Individual_API_Field__c='FirstName'));
		gdpr_settings.add(new ITGDPR_Settings__c(Name='3',Copy__c=true,Lead_API_Field__c='Salutation',Contact_API_Field__c='Salutation',Individual_API_Field__c='Salutation'));
		gdpr_settings.add(new ITGDPR_Settings__c(Name='4',Connect__c=true,Delete_On_Create__c=true,Copy__c=true,Lead_API_Field__c='Email',Contact_API_Field__c='Email',Individual_API_Field__c='Email__c'));
		gdpr_settings.add(new ITGDPR_Settings__c(Name='5',Copy__c=true,Delete_On_Create__c=true,Lead_API_Field__c='Phone',Contact_API_Field__c='Phone',Individual_API_Field__c='Phone__c'));
		gdpr_settings.add(new ITGDPR_Settings__c(Name='6',Copy__c=true,Delete_On_Create__c=true,Lead_API_Field__c='MobilePhone',Contact_API_Field__c='MobilePhone',Individual_API_Field__c='Mobile__c'));
		insert gdpr_settings;
	}
	//connect relevant record on create of lead, contact
	public static void ConnectOrCreate (List <SObject> sobs, String rectype) {
		List <ITGDPR_Settings__c> gdpr_settings = getGDPRSettings(rectype);
		if(gdpr_settings.size()>0){
			Map <String, String> connects = new Map <String, String>();
			Map <String, String> copies = new Map <String, String>();
			Set <String> deletes = new Set <String>();

			String field=rectype+'_API_Field__c';
			for(ITGDPR_Settings__c itg:gdpr_settings)
				if((String) itg.get(field)!=null){
					if(itg.Individual_API_Field__c!=null){
						if(itg.Connect__c==true)
							connects.put((String) itg.get(field),itg.Individual_API_Field__c);
						if(itg.Copy__c==true)
							copies.put((String) itg.get(field),itg.Individual_API_Field__c);
					}
					if(itg.Delete_On_Create__c==true)
						deletes.add((String) itg.get(field));
				}

			Map <String,Set <String>> fieldsCheck = new Map <String,Set <String>>();
			String qr='select id';
			for(String f:connects.keyset()){
				qr+=', '+connects.get(f);
				fieldsCheck.put(f, new Set <String>());
			}
			qr+=' from Individual where ';
			for(SObject sob:sobs)
				for(String f:connects.keyset())
					fieldsCheck.get(f).add((String) sob.get(f));
			List <String> keyList = new List <String>(connects.keyset());
			List <String> valList = new List <String>(connects.values());
			for(Integer i=0;i<keyList.size();i++)
				setData(i,fieldsCheck.get(keyList[i]));
			for(integer i=0;i<keyList.size();i++) {
				if(i!=keyList.size()-1)
					qr+=valList[i]+' in : data'+i+' and ';
				else
					qr+=valList[i]+' in : data'+i;
			}

			System.debug(qr);
			List <Individual> allind = new List <Individual>();
			List <Individual> exstind = Database.query(qr);
			List <Individual> newind = new List <Individual>();
			for(Sobject sob:sobs){
				Boolean Found=false;
				Boolean nullConnect=false;
				for(String s:connects.keyset())
					if(sob.get(s)==null)
						nullConnect=true;
				System.debug(nullConnect);
				for(Individual ind:exstind){
					integer i=0;
					for(String s:connects.keyset())
						if(sob.get(s)==ind.get(connects.get(s)))
							i++;
					if(i==connects.keyset().size()){
						allind.add(ind);
						Found=true;
						break;
					}
				}
				if(Found==False){
					Individual ind = new Individual();
					for(String f:copies.keyset())
						ind.put(copies.get(f),sob.get(f));
					if(nullConnect==false)
						newind.add(ind);
					System.debug(ind);
					allind.add(ind);
				}
				for(String f:deletes)
					sob.put(f,null);
			}
			insert newind;

			for(integer i=0;i<sobs.size();i++)
				sobs[i].put('IndividualId', allind[i].id);

		}
	}

	//get Relevant Data from CS
	public static List <ITGDPR_Settings__c>	getGDPRSettings	(String rectype) {
		String qr = 'select Individual_API_Field__c, '+(rectype!=null?rectype+'_API_Field__c':'Lead_API_Field__c, Contact_API_Field__c')+', Connect__c, Copy__c, Delete_On_Create__c, Delete_On_Delete__c from ITGDPR_Settings__c where '+(rectype!=null?rectype+'_API_Field__c!=null':'Lead_API_Field__c!=null OR Contact_API_Field__c!=null')+' OR Individual_API_Field__c!=null';
		return Database.query(qr);
	}

	//delete relevant fields from lead, contact and turning on checkbox
	public static void DeleteInds (List <Individual> inds) {
		List <ITGDPR_Settings__c> gdpr_settings = getGDPRSettings(null);
		if(gdpr_settings.size()>0){
			Set <String> ids = new Set <String>();
			for(Individual ind:inds)
				ids.add(ind.id);
			Set <String> deletesCons = new Set <String>();
			Set <String> deletesLds = new Set <String>();
			for(ITGDPR_Settings__c itg:gdpr_settings)
				if(itg.Delete_On_Delete__c==true) {
					if(itg.Lead_API_Field__c!=null)
						deletesLds.add(itg.Lead_API_Field__c);
					if(itg.Contact_API_Field__c!=null)
						deletesCons.add(itg.Contact_API_Field__c);
				}
			String qrcons='select id';
			for(String f:deletesCons)
				qrcons+=', '+f;
			String qrlds='select id';
			for(String f:deletesLds)
				qrlds+=', '+f;
			qrcons+=' from Contact where IndividualId in: ids';
			qrlds+=' from Lead where IndividualId in: ids';
			List <Contact> cons = Database.query(qrcons);
			List <Lead> lds = Database.query(qrlds);
			for(Contact con:cons){
				for(String f:deletesCons)
					con.put(f,null);
				con.put('IndividualId',null);
			}
			for(Lead ld:lds){
				for(String f:deletesLds)
					ld.put(f,null);
				ld.put('IndividualId',null);
			}
			update cons;
			update lds;
		}
	}
}
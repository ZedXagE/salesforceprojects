trigger GDPR_Lead on Lead (before Insert) {
    GDPR_Handler.ConnectOrCreate(Trigger.New,'Lead');
}
public class SearchClientController {
    public Flow.Interview.Client_Search myFlow { get; set; }
    public String getmyID() {
        if (myFlow==null) return '';
        else return myFlow.AccountID;
    }
    public PageReference getAID(){
        String accid=getmyID();
        System.debug(accid);
        PageReference p = new PageReference('/apex/SearchClient?accid='+accid);
        p.setRedirect(true);
        return p;
    }
}
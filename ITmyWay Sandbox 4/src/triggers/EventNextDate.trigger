trigger EventNextDate on Event (after insert,after update,before delete) {
    if(Trigger.IsInsert||Trigger.IsUpdate){
        for(Event rec:Trigger.New)
            if(rec.WhoID!=null){
                Flow.Interview.Next_Activity_Data myFlow = new Flow.Interview.Next_Activity_Data( new Map<String, Object>{'WhoID'=>rec.WhoID} );
                myFlow.start();
            }
    }
    else{
        for(Event rec:Trigger.Old)
            if(rec.WhoID!=null){
                Flow.Interview.Next_Activity_Data myFlow = new Flow.Interview.Next_Activity_Data( new Map<String, Object>{'WhoID'=>rec.WhoID} );
                myFlow.start();
            }
    }
}
trigger TaskNextDate on Task (after insert,after update,before delete) {
    if(Trigger.IsInsert||Trigger.IsUpdate){
        for(Task rec:Trigger.New)
            if(rec.WhoID!=null){
                Flow.Interview.Next_Activity_Data myFlow = new Flow.Interview.Next_Activity_Data( new Map<String, Object>{'WhoID'=>rec.WhoID} );
                myFlow.start();
            }
    }
    else{
        for(Task rec:Trigger.Old)
            if(rec.WhoID!=null){
                Flow.Interview.Next_Activity_Data myFlow = new Flow.Interview.Next_Activity_Data( new Map<String, Object>{'WhoID'=>rec.WhoID} );
                myFlow.start();
            }
    }
}
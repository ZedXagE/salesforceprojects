public class IOLoaderController implements Queueable {
	public static Map<String,String> getUsers(){
		Map<String,String> users=new Map<String,String>();
		for(User u:[select id,FirstName from User])
			users.put(u.FirstName,u.id);
		return users;
	}
	public static Map<String,String> getAccRecTypes(){
		Map<String,String> recs=new Map<String,String>();
		for(RecordType rec:[select id,Name from RecordType where SobjectType='Account'])
			recs.put(rec.Name,rec.id);
		return recs;
	}
	public static Map<String,String> getOppRecTypes(){
		Map<String,String> recs=new Map<String,String>();
		for(RecordType rec:[select id,Name from RecordType where SobjectType='Opportunity'])
			recs.put(rec.Name,rec.id);
		return recs;
	}
	public static String DeepCheckMap(Map <String,String> ma,String val){
		for(String Key:ma.keySet()){
			if(key!=null){
				if(val.contains(' ')){
					String [] NoSpace=val.split(' ');
					integer i=0;
					for(String s:NoSpace)
						if(Key.contains(s))
							i++;
					if(i==NoSpace.size())
						return Key;
				}
				else{
					if(Key.contains(val))
						return Key;
				}
			}
		}
		return null;
	}
	public static String checkMap(Map <String,String> ma,String val){
		return ma.containsKey(val)?ma.get(val):null;
	}
	public class IOLead{
		String id;
		String display_name;
		String description;
		String url;
		String status_label;
		Map <String,String> custom;
		String created_by;
		String created_by_name;
		String updated_by;
		String updated_by_name;
		DateTime date_created;
		DateTime first_communication_date;
		String first_Communication_Type;
		DateTime last_activity_date;
		DateTime last_communication_date;
		String last_communication_type;
		String last_email_subject;
		List <IOAddress> addresses;
		List <IOOpportunity> opportunities;
		List <IOContact> contacts;
		List <IOTask> Tasks;
		//List <IOActivity> Activities;
	}
	public class IOAddress{
		String city;
        String country;
        String zipcode;
        String label;
        String state;
        String address_1;
        String address_2;
	}
	public class IOContact{
		String lead_id;
		String id;
		String name;
		String first_name;
		String last_name;
		String title;
		String primary_phone;
		String primary_phone_type;
		String other_phones;
		String primary_email;
		String primary_email_type;
		String other_emails;
		String primary_contact_url;
		String other_contact_urls;
		String created_by;
		String updated_by;
		DateTime date_created;
		DateTime date_updated;

	}
	public class IOOpportunity{
		String id;
		String user_id;
		String user_name;
		Decimal confidence;
		Date date_won;
		String status_label;
		String status_type;
		String contact_id;
		String contact_name;
		String note;
		String lead_id;
		String lead_name;
		String created_by;
		String created_by_name;
		String updated_by;
		String updated_by_name;
		DateTime date_created;
		DateTime date_updated;
		String opportunity_type;
	}
	/*public class IOActivity{
		List <IOAttachment> atts;
	}*/
	public class IOTask{
		String id;
		DateTime date_updated;
		String text;
		String created_by;
		Date due_date;
		String updated_by;
		String lead_id;
		String assigned_to;
		DateTime date_created;
		Boolean is_complete;
		String contact_id;
	}
	public class IOAttachment{
		
	}
	public class SFAccount{
		//Standard
		String Name;
		String RecordTypeId;
		String Description;
		String Status;
		String BillingStreet;
		String BillingCity;
		String BillingPostalCode;
		String BillingCountry;
		String BillingState;
		String Ownerid;
		String LeadSource;
		String LastModifiedBy;
		DateTime LastModifiedDate;
		DateTime CreatedDate;
		String CreatedBy;
		//Customs
		String URL_c;
		String Closeio_ID_c;
		String Addional_Owner_c;
		String Conference_Name_c;
		String Channel_c;
		String Channel_Type_c;
		String Advertiser_Publisher_c;
		String Tier_c;
		String Link_to_App_Store_c;
		String Relevant_Action_c;
		String App_category_c;
		String App_Name_c;
		String Appnext_account_e_mail_c;
		String Main_App_c;
		String MW_Av_Monthly_Visitors_c;
		String CreatedBy_close_io_ID_c;
		String UpdatedBy_close_io_ID_c;
		DateTime First_Communication_Date_c;
		String First_Communication_Type_c;
		DateTime Last_Activity_Date_c;
		DateTime Last_Communication_Date_c;
		String Last_Communication_Type_c;
		String Last_Email_Subject_c;
		String Intercom_ID_c;
		List <SFOpportunity> Opportunities;
		List <SFContact> Contacts;
		List <SFTask> Tasks;
		//List <SFEmail> Emails;
	}
	public class SFContact{
		//Standard
		String Title;
		String Phone;
		String MobilePhone;
		String Fax;
		SFAccount Account;
		String Ownerid;
		String OtherPhone;
		String Email;
		String LastModifiedBy;
		DateTime CreatedDate;
		DateTime LastModifiedDate;
		String CreatedBy;
		String FirstName;
		String LastName;
		//Custom
		String Lead_Closeio_ID_c;
		String Contact_Closeio_ID_c;
		String primary_phone_type_c;
		String primary_email_type_c;
		String OtherEmails_c;
		String URL_c;
		String OtherURLs_c;
		String Created_By_close_io_ID_c;
		String Updated_By_close_io_ID_c;
	}
	public class SFOpportunity{
		//Standard
		String Name;
		String LastModifiedBy;
		DateTime LastModifiedDate;
		String CreatedBy;
		DateTime CreatedDate;
		String Ownerid;
		Decimal Probability;
		Date CloseDate;
		String StageName;
		String RecordTypeId;
		String Contact_Closeio_ID_c;
		String Description;
		SFAccount Account;
		//custom
		String Closeio_ID_c;
		String Closeio_User_ID_c;
		String Lead_Closeio_ID_c;
		String CreatedBy_close_io_ID_c;
		String UpdatedBy_close_io_ID_c;
		String Opportunity_Type_c;
		String Stage_Type_c;
	}
	/*public class SFEmail{
		List <SFAttachment> atts;
	}*/
	public class SFTask{
		//Standard
		String id;
		DateTime LastModifiedDate;
		DateTime CreatedDate;
		Date ActivityDate;
		String Status;
		SFAccount Account;
		String OwnerId;
		String Subject;
		String CreatedBy;
		String LastModifiedBy;
		String Closeio_ID_c;
		SFContact Contact;
	}
	public class SFAttachment{
		
	}
	public static void IOLeads() {
		List<Account> tempaccs = new List <Account>();
		List<Opportunity> tempopps = new List <Opportunity>();
		List<Contact> tempcons = new List <Contact>();
		List<OpportunityContactRole> tempoppcons = new List <OpportunityContactRole>();
		List<Task> temptasks = new List <Task>();

		StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'ioleads' LIMIT 1];
		List <IOLead> iolds =(List <IOLead>)JSON.deserialize(sr.Body.toString(), List<IOLead>.class);
		System.debug(iolds.size());
		System.debug(iolds);
		Map <String,String> users=getUsers();
		Map <String,String> accrecs=getAccRecTypes();
		Map <String,String> opprecs=getOppRecTypes();
		List <SFAccount> newSFAccounts=new List <SFAccount>();
		for(IOLead iold:iolds){
			SFAccount accref = new SFAccount();
			accref.Closeio_ID_c=iold.id;
			SFAccount sfacc=new SFAccount();
			if(iold.status_label=='1. Cold'){
				sfacc.RecordTypeId=DeepCheckMap(accrecs,'Qualified Lead');
				sfacc.Status='Cold';
			}
			else if(iold.status_label=='2. Reached Out'){
				sfacc.RecordTypeId=DeepCheckMap(accrecs,'Qualified Lead');
				sfacc.Status='Reached Out';
			}
			else if(iold.status_label=='3. In Contact'){
				sfacc.RecordTypeId=DeepCheckMap(accrecs,'Qualified Lead');
				sfacc.Status='In Contact';
			}
			else if(iold.status_label=='4. Account Created'){
				sfacc.RecordTypeId=DeepCheckMap(accrecs,'Qualified Lead');
				sfacc.Status='Account Created (convert)';
			}
			else if(iold.status_label=='5. Testing / Qualification Process'){
				sfacc.RecordTypeId=DeepCheckMap(accrecs,'Account');
				sfacc.Status='Live - Evaluation';
			}
			else if(iold.status_label=='6. Active'){
				sfacc.RecordTypeId=DeepCheckMap(accrecs,'Account');
				sfacc.Status='Live - Approved';
			}
			else if(iold.status_label=='Not Interested'){
				sfacc.RecordTypeId=DeepCheckMap(accrecs,'Qualified Lead');
				sfacc.Status='Not Interested';
			}
			else if(iold.status_label=='Unqualified'){
				sfacc.RecordTypeId=DeepCheckMap(accrecs,'Qualified Lead');
				sfacc.Status='Unqualified';
			}
			else if(iold.status_label=='Churn - Renew Partnership'){
				sfacc.RecordTypeId=DeepCheckMap(accrecs,'Account');
				sfacc.Status='Churned';
			}
			else if(iold.status_label=='Legacy Leads'){
				sfacc.RecordTypeId=DeepCheckMap(accrecs,'Qualified Lead');
				sfacc.Status='Cold';
			}
			else if(iold.status_label=='Intercom'){
				sfacc.RecordTypeId=DeepCheckMap(accrecs,'Qualified Lead');
				sfacc.Status='Intercom Lead (Corporate site)';
			}
			else{
				System.debug('Error mapping: '+iold);
				break;
			}
			sfacc.Closeio_ID_c=iold.id;
			sfacc.Name=iold.display_name;
			sfacc.Description=iold.description;
			sfacc.URL_c=iold.url;
			sfacc.BillingStreet=iold.addresses[0].address_1+' '+iold.addresses[0].address_2;
			sfacc.BillingCity=iold.addresses[0].city;
			sfacc.BillingPostalCode=iold.addresses[0].zipcode;
			sfacc.BillingCountry=iold.addresses[0].country;
			sfacc.BillingState=iold.addresses[0].state;
			sfacc.Ownerid=checkMap(users,checkMap(iold.custom,'1. Owner'));
			sfacc.Addional_Owner_c=checkMap(users,checkMap(iold.custom,'2. Addional Owner'));
			sfacc.LeadSource=checkMap(iold.custom,'3. Lead Source');
			sfacc.Conference_Name_c=checkMap(iold.custom,'4. Conference Name');
			sfacc.Channel_c=checkMap(iold.custom,'5. Channel');
			sfacc.Channel_Type_c=checkMap(iold.custom,'6. Channel Type');
			sfacc.Advertiser_Publisher_c=checkMap(iold.custom,'7. Advertiser / Publisher');
			String tier=checkMap(iold.custom,'8. Tier');
			if(tier=='< 500K Installs')
				sfacc.Tier_c='100K-500K';
			else if(tier=='>100M Installs')
				sfacc.Tier_c='100M-500M';
			else
				sfacc.Tier_c=tier;
			sfacc.Link_to_App_Store_c=checkMap(iold.custom,'9. Link to App Store');
			String relevant=checkMap(iold.custom,'9.1 Relevant Action');
			if(relevant=='Play / Games')
				sfacc.Relevant_Action_c='Play a game';
			else if(relevant=='Dating')
				sfacc.Relevant_Action_c='Meet People';
			else
				sfacc.Relevant_Action_c=relevant;
			sfacc.App_category_c=checkMap(iold.custom,'App category');
			sfacc.App_Name_c=checkMap(iold.custom,'App Name');
			sfacc.Appnext_account_e_mail_c=checkMap(iold.custom,'Appnext account (e-mail)');
			sfacc.Main_App_c=checkMap(iold.custom,'Main App');
			sfacc.MW_Av_Monthly_Visitors_c=checkMap(iold.custom,'MW -  Av. Monthly Visitors');
			sfacc.CreatedBy_close_io_ID_c=iold.created_by;
			sfacc.CreatedBy=checkMap(users,iold.created_by);
			sfacc.UpdatedBy_close_io_ID_c=iold.updated_by;
			sfacc.LastModifiedBy=checkMap(users,iold.updated_by);
			sfacc.CreatedDate=iold.date_created;
			sfacc.First_Communication_Date_c=iold.first_communication_date;
			sfacc.First_Communication_Type_c=iold.first_communication_type;
			sfacc.Last_Activity_Date_c=iold.last_activity_date;
			sfacc.Last_Communication_Date_c=iold.last_communication_date;
			sfacc.Last_Communication_Type_c=iold.last_communication_type;
			sfacc.Last_Email_Subject_c=iold.last_email_subject;
			newSFAccounts.add(sfacc);
			sfacc.Contacts=new List <SFContact>();
			//contacts
			for(IOContact iocon:iold.contacts)
			{
				SFContact sfcon = new SFContact();
				sfcon.Account=accref;
				sfcon.Lead_Closeio_ID_c=iocon.lead_id;
				sfcon.Contact_Closeio_ID_c=iocon.id;
				sfcon.FirstName=iocon.first_name;
				sfcon.LastName=iocon.name;
				sfcon.Title=iocon.title;
				if(iocon.primary_phone_type=='Mobile')
					sfcon.MobilePhone=iocon.primary_phone;
				else if(iocon.primary_phone_type=='Fax')
					sfcon.Fax=iocon.primary_phone;
				else
					sfcon.Phone=iocon.primary_phone;
				sfcon.Primary_Phone_Type_c=iocon.primary_phone_type;
				sfcon.OtherPhone=iocon.other_phones;
				sfcon.Email=iocon.primary_email;
				sfcon.primary_email_type_c=iocon.primary_email_type;
				sfcon.OtherEmails_c=iocon.other_emails;
				sfcon.URL_c=iocon.primary_contact_url;
				sfcon.OtherURLs_c=iocon.other_contact_urls;
				sfcon.Created_By_close_io_ID_c=iocon.created_by;
				sfcon.CreatedBy=checkMap(users,iocon.created_by);
				sfcon.Updated_By_close_io_ID_c=iocon.updated_by;
				sfcon.LastModifiedBy=checkMap(users,iocon.updated_by);
				sfcon.CreatedDate=iocon.date_created;
				sfcon.LastModifiedDate=iocon.date_updated;
				sfcon.Ownerid=sfacc.Ownerid;
				sfacc.Contacts.add(sfcon);
			}
			sfacc.Opportunities=new List <SFOpportunity>();
			//opportunities
			for(IOOpportunity ioopp:iold.opportunities)
			{
				SFOpportunity sfopp = new SFOpportunity();
				sfopp.Account=accref;
				sfopp.Closeio_ID_c=ioopp.id;
				sfopp.Closeio_User_ID_c=ioopp.user_id;
				sfopp.Ownerid=checkMap(users,ioopp.user_name);
				sfopp.Probability=ioopp.confidence;
				sfopp.CloseDate=ioopp.date_won;
				sfopp.Contact_Closeio_ID_c=ioopp.contact_id;
				//sfopp.=ioopp.contact_name;
				sfopp.Description=ioopp.note;
				sfopp.Lead_Closeio_ID_c=ioopp.lead_id;
				//sfopp.=ioopp.lead_name;
				sfopp.CreatedBy_close_io_ID_c=ioopp.created_by;
				sfopp.CreatedBy=checkMap(users,ioopp.created_by_name);
				sfopp.UpdatedBy_close_io_ID_c=ioopp.updated_by;
				sfopp.LastModifiedBy=checkMap(users,ioopp.updated_by_name);
				sfopp.CreatedDate=ioopp.date_created;
				sfopp.LastModifiedDate=ioopp.date_updated;
				sfopp.Opportunity_Type_c=ioopp.opportunity_type;
				sfopp.Name=ioopp.lead_name+' ';
				if(sfopp.CloseDate!=null)
					sfopp.Name+=String.valueof(sfopp.CloseDate.day())+'/'+String.valueof(sfopp.CloseDate.month())+'/'+String.valueof(sfopp.CloseDate.year());
				else if(sfopp.CreatedDate!=null)
					sfopp.Name+=String.valueof(sfopp.CreatedDate.day())+'/'+String.valueof(sfopp.CreatedDate.month())+'/'+String.valueof(sfopp.CreatedDate.year());
				if(ioopp.status_label=='Introduction (active)'||ioopp.status_label=='API Integration (active)'||ioopp.status_label=='Integration mode (active)'||ioopp.status_label=='SDK Integration')	
					sfopp.RecordTypeId=DeepCheckMap(opprecs,'Monetization');
				else if(checkMap(iold.custom,'7. Advertiser / Publisher')=='Advertiser')
					sfopp.RecordTypeId=DeepCheckMap(opprecs,'Advertising');
				else
					sfopp.RecordTypeId=DeepCheckMap(opprecs,'Monetization');

				if(ioopp.status_label=='Introduction (active)'||ioopp.status_label=='Launch (won)'||ioopp.status_label=='Onboarding')		
					sfopp.StageName='Live';
				else if(ioopp.status_label=='API Integration (active)'||ioopp.status_label=='Integration mode (active)'||ioopp.status_label=='SDK Integration')		
					sfopp.StageName='Integration';
				else if(ioopp.status_label=='Lost (lost)')
					sfopp.StageName='Close lost';
				else if(ioopp.status_label=='UA Test Campaign')
					sfopp.StageName='Testing';
				else
					sfopp.StageName='Unknown';
				if(ioopp.status_type=='active'||ioopp.status_type=='won')
					sfopp.Stage_Type_c='Closed Won';
				else if(ioopp.status_type=='lost')
					sfopp.Stage_Type_c='Closed Lost';
				else
					sfopp.Stage_Type_c='Open';

				sfacc.Opportunities.add(sfopp);
			}

			/*for(IOActivity ioact:iold.activities)
			{
				SFEmail sfema = new SFEmail();
				sfacc.Emails.add(sfema);
				for(IOAttachment ioatt:ioact.atts)
				{
					SFAttachment sfatt = new SFAttachment();
					sfema.atts.add(sfatt);
				}
			}*/
			sfacc.tasks=new List <SFTask>();

			for(IOTask iotas:iold.tasks)
			{
				SFTask sftas = new SFTask();
				sftas.Closeio_ID_c=iotas.id;
				sftas.Account=Accref;
				sftas.Subject=iotas.text;
				sftas.CreatedDate=iotas.date_created;
				sftas.Ownerid=checkMap(users,iotas.assigned_to);
				sftas.LastModifiedBy=checkMap(users,iotas.updated_by);
				sftas.ActivityDate=iotas.due_date;
				sftas.CreatedBy=CheckMap(users,iotas.created_by);
				sftas.LastModifiedDate=iotas.date_updated;
				if(iotas.is_complete)
					sftas.Status='Complete';
				if(iotas.contact_id!=null){
					for(SFContact sfcon:sfacc.contacts){
						if(iotas.contact_id==sfcon.Contact_Closeio_ID_c){
							SFContact conref=new SFContact();
							conref.Contact_Closeio_ID_c=sfcon.Contact_Closeio_ID_c;
							sftas.Contact=conref;
						}
					}
				}
				sfacc.tasks.add(sftas);
			}
		}
		System.debug('Success Account Maps size: '+newSFAccounts.size());
		System.debug('Success Account Maps: '+newSFAccounts);
		for(SFAccount sfacc:newSFAccounts){
			Account acc=new Account();
			Account accref = new Account(Closeio_ID__c=sfacc.Closeio_ID_c);
			acc.Name=sfacc.Name;
			acc.Closeio_ID__c=sfacc.Closeio_ID_c;
			for(SFContact sfcon:sfacc.contacts){
				Contact con=new Contact();
				con.Account=accref;
				con.LastName=sfcon.LastName;
				con.FirstName=sfcon.FirstName;
				con.Closeio_ID__c=sfcon.Contact_Closeio_ID_c;
				con.Lead_Closeio_ID__c=sfacc.Closeio_ID_c;
				tempcons.add(con);
			}
			for(SFOpportunity sfopp:sfacc.opportunities){
				Opportunity oppref=new Opportunity(Closeio_ID__c=sfopp.Closeio_ID_c);
				Opportunity opp=new Opportunity();
				opp.CloseDate=sfopp.CloseDate;//must
				opp.StageName='Closed Won';//must
				opp.Closeio_ID__c=sfopp.Closeio_ID_c;
				opp.Lead_Closeio_ID__c=sfopp.Lead_Closeio_ID_c;
				opp.Name=sfopp.Name;
				opp.Account=accref;
				tempopps.add(opp);
				if(sfopp.Contact_Closeio_ID_c!=null&&sfopp.Contact_Closeio_ID_c!=''){
					for(Contact con:tempcons)
						if(sfopp.Contact_Closeio_ID_c==con.Closeio_ID__c){
							Contact conref=new Contact(Closeio_ID__c=con.Closeio_ID__c);
							OpportunityContactRole oppcon =new OpportunityContactRole();
							oppcon.Opportunity=oppref;
							oppcon.Contact=conref;
							tempoppcons.add(oppcon);
						}
				}

			}
			System.debug(acc);
			tempaccs.add(acc);
		}

		ID jobID = System.enqueueJob(new IOLoaderController(tempaccs,tempcons,tempopps,tempoppcons));
	}

	public List <Account> accs;
	public List <Contact> cons;
	public List <Opportunity> opps;
	public List <OpportunityContactRole> oppcons;

	public IOLoaderController(List <Account> acs,List <Contact> cos,List <Opportunity>ops,List <OpportunityContactRole> opcon){
		accs=acs;
		cons=cos;
		opps=ops;
		oppcons=opcon;
	}
    public void execute(QueueableContext context) {
		if(accs.size()>0){
			System.debug(accs[0]);
			//Account accref = new Account(Closeio_ID__c=accs[0].Closeio_ID__c);
			List <Contact> relatecons=new List <Contact> ();
			List <Opportunity> relateopps=new List <Opportunity> ();
			List <OpportunityContactRole> relateoppcons=new List <OpportunityContactRole> ();
			for(integer i=0;i<cons.Size();i++)
				if(cons[i].Lead_Closeio_ID__c==accs[0].Closeio_ID__c){
					relatecons.add(cons[i]);
					cons.remove(i);
					i--;
				}
			for(integer i=0;i<opps.Size();i++)
				if(opps[i].Lead_Closeio_ID__c==accs[0].Closeio_ID__c){
					relateopps.add(opps[i]);
					for(integer j=0;j<oppcons.Size();j++)
						if(oppcons[j].Opportunity.Closeio_ID__c==opps[i].Closeio_ID__c){
							relateoppcons.add(oppcons[j]);
							oppcons.remove(j);
							j--;
						}
					opps.remove(i);
					i--;
				}
			insert accs[0];
			insert relatecons;
			insert relateopps;
			insert relateoppcons;
			accs.remove(0);
			ID jobID = System.enqueueJob(new IOLoaderController(accs,cons,opps,oppcons));
		}
  
       
    }
}
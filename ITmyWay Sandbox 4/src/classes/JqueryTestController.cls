public class JqueryTestController {
	private String Sobjectname;
	private String fields;
	private Map <String, Schema.SObjectField> fieldMap;
	public String jsonData{get;set;}
	public String jsonFields{get;set;}
	private List<Field> Dfields;
	public class Field{
	public String api;
	public String label;
	public Boolean perm;
		public Field(String ap,String lab,Boolean per){
			api=ap;label=lab;perm=per;
		}
	}
	public JqueryTestController() {
		//get sobjectname
		Sobjectname=ApexPages.currentPage().getParameters().get('obj');
		if(Sobjectname==null||Sobjectname=='')
			Sobjectname='Account';
		
		//get field map
		fieldMap = Schema.getGlobalDescribe().get(Sobjectname).getDescribe().fields.getMap();

		//get fields
		fields=getAllFields();
		fields=fields.toLowerCase();

		//get records
		jsonData = getRecords();
	}
	private String getRecords()
	{
		initFieldLabelsAndPerm(fields);
		jsonFields=JSON.serialize(Dfields);
		String soqlQuery = 'SELECT '+fields+' FROM '+Sobjectname+'';
		List <Map <String,object>> objsFields = new List <Map <String,object>>();
		for(Sobject sob:Database.query(soqlQuery)){
			Map <String,object> objFields = new Map <String,object>();
			for(String fil:fieldMap.keySet())
				if(fields.contains(fil)&&fieldMap.get(fil).getDescribe().isAccessible())
					objFields.put(fil,sob.get(fil));
			objsFields.add(objFields);
		}
		return JSON.serialize(objsFields);
	}
	private String getAllFields(){
		String allfields='';
		for (String fil: fieldMap.keySet())
			if(fieldMap.get(fil).getDescribe().isAccessible())
				allfields+=allfields==''?fil:','+fil;
		return allfields;
	}
	private void initFieldLabelsAndPerm(String fields){
		Dfields = new List<Field>();
		for (String fil: fieldMap.keySet())
			if(fields.contains(fil)&&fieldMap.get(fil).getDescribe().isAccessible())
				Dfields.add(new Field(fil,fieldMap.get(fil).getDescribe().getLabel(),fieldMap.get(fil).getDescribe().isUpdateable()));
	}
	public void Up(){
		try{
			Type t= Type.forName('List<'+Sobjectname+'>');
			List <Sobject> sobs=(List <Sobject>)JSON.deserialize(jsonData,t);
			update sobs;
		}
		catch(exception e){
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,e.getMessage()));
		}
	}
}
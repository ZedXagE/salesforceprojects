@isTest
public class Test_EventAndTaskNextDate {
	static TestMethod void Test() {
		Lead ld = new Lead(LastName='test',Company='test');
		insert ld;
		Task ts=new Task(Whoid=ld.id,ActivityDate=System.Today());
		insert ts;
		Event ev=new Event(Whoid=ld.id,ActivityDateTime=System.Now(),DurationInMinutes=3);
		insert ev;
		delete ts;
		delete ev;
	}
}
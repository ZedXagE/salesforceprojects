trigger WorkOrderTrigger on Account (after update) {
    Savepoint sp = Database.setSavepoint(); try {

        AssignmentController.onAssign(Trigger.new, Trigger.oldMap);

    } catch (Exception e) { Database.rollback(sp); throw e; }
}
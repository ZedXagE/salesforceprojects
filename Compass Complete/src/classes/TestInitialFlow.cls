@isTest
public class TestInitialFlow {
    static testMethod void TestInitialFlow4() {
		task tas=new task(subject='Inbound rule4',Tnfld_caller_id__c='8662192176');
		insert tas;
        user use = [select id, Name, Company__c, CommunityNickname from user where id =: UserInfo.getUserId()];
		PageReference pgRef = Page.initialflow; //Create Page Reference - 'Appt_New' is the name of Page
		Test.setCurrentPage(pgRef); //Set the page for Test Method
		ApexPages.currentPage().getParameters().put('id', tas.id);//Pass Id to page
		InitialFlowController sc = new InitialFlowController();
        BackandnewtabController bn = new BackandnewtabController();
		Insurance_Account__c insacc=new Insurance_Account__c(name='test',CallerID__c='8662192176',Operation__c=use.Company__c);
		insert insacc;
		sc.start(); 
	}
	static testMethod void TestInitialFlow() {
		task tas=new task(subject='Inbound rule4',Tnfld_caller_id__c='8662192176');
		insert tas;
		PageReference pgRef = Page.initialflow; //Create Page Reference - 'Appt_New' is the name of Page
		Test.setCurrentPage(pgRef); //Set the page for Test Method
		ApexPages.currentPage().getParameters().put('id', tas.id);//Pass Id to page
		InitialFlowController sc = new InitialFlowController();
        BackandnewtabController bn = new BackandnewtabController();
		sc.start();
		Insurance_Account__c insacc=new Insurance_Account__c(name='test',CallerID__c='8662192176');
		insert insacc;
		sc.start();
		Account acc=new Account(Insurance_Account__c=insacc.id,name='test');
		insert acc;
		sc.start(); 
		sc.create();
		ApexPages.currentPage().getParameters().put('curaccidcheck', acc.id);//Pass Id to page
		sc.checkETA();
		ApexPages.currentPage().getParameters().put('curaccidrule4', acc.id);//Pass Id to page
		sc.rule4click();
		ApexPages.currentPage().getParameters().put('curaccidrule2', acc.id);//Pass Id to page
		sc.rule2click();
		sc.redirect(acc.id);
	}
	static testMethod void TestInitialFlow2() {
		task tas=new task(subject='Inbound rule4',Tnfld_caller_id__c='8662192176',Source__C='Returning');
		insert tas;
		PageReference pgRef = Page.initialflow; //Create Page Reference - 'Appt_New' is the name of Page
		Test.setCurrentPage(pgRef); //Set the page for Test Method
		ApexPages.currentPage().getParameters().put('id', tas.id);//Pass Id to page
		InitialFlowController sc = new InitialFlowController();
		BackandnewtabController bn = new BackandnewtabController();
		sc.start();
		Insurance_Account__c insacc=new Insurance_Account__c(name='test',CallerID__c='8662192176');
		insert insacc;
		sc.start();
		Account acc=new Account(Insurance_Account__c=insacc.id,name='test');
		insert acc;
		sc.start();
		sc.create();
		ApexPages.currentPage().getParameters().put('curaccidcheck', acc.id);//Pass Id to page
		sc.checkETA();
		ApexPages.currentPage().getParameters().put('curaccidrule4', acc.id);//Pass Id to page
		sc.rule4click();
		ApexPages.currentPage().getParameters().put('curaccidrule2', acc.id);//Pass Id to page
		sc.rule2click();
		sc.redirect(acc.id);
	}
	static testMethod void TestInitialFlow3() {
		task tas=new task(subject='Inbound rule4',Tnfld_caller_id__c='8662192176',Source__C='Returning');
		insert tas;
		PageReference pgRef = Page.initialflow; //Create Page Reference - 'Appt_New' is the name of Page
		Test.setCurrentPage(pgRef); //Set the page for Test Method
		ApexPages.currentPage().getParameters().put('id', tas.id);//Pass Id to page
		InitialFlowController sc = new InitialFlowController();
		BackandnewtabController bn = new BackandnewtabController();
		sc.start();
		Account acc=new Account(Alt_phone__c='8662192176',name='test');
		insert acc;
		sc.start();
		sc.create();
		ApexPages.currentPage().getParameters().put('curaccidcheck', acc.id);//Pass Id to page
		sc.checkETA();
		ApexPages.currentPage().getParameters().put('curaccidrule4', acc.id);//Pass Id to page
		sc.rule4click();
		ApexPages.currentPage().getParameters().put('curaccidrule2', acc.id);//Pass Id to page
		sc.rule2click();
		sc.redirect(acc.id);
	}
	static testMethod void TestInitialFlow5() {
		task tas=new task(subject='Outbound',Tnfld_caller_id__c='8662192176',Source__C='Returning');
		insert tas;
		PageReference pgRef = Page.initialflow; //Create Page Reference - 'Appt_New' is the name of Page
		Test.setCurrentPage(pgRef); //Set the page for Test Method
		ApexPages.currentPage().getParameters().put('id', tas.id);//Pass Id to page
		InitialFlowController sc = new InitialFlowController();
		BackandnewtabController bn = new BackandnewtabController();
		sc.start();
		Account acc=new Account(Alt_phone__c='8662192176',name='test');
		insert acc;
		sc.start();
	}
}
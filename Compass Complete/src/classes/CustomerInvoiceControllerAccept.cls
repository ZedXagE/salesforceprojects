public class CustomerInvoiceControllerAccept {
    public String signatureData {get;set;}
    public String signaturepic {get;set;}
    public String signatureid {get;set;}
    public id Curid {get;set;}
    public account acc {get;set;}
    public Technician__c ser {get;set;}
    public String Credat {get;set;}
    public String Cusdat {get;set;}
    public String badd {get;set;}
    public String sadd {get;set;}
    public String badd1 {get;set;}
    public String sadd1 {get;set;}
    public String badd2 {get;set;}
    public String sadd2 {get;set;}
    public String Vec {get;set;}
    public String recty {get;set;}
    public String companyn {get;set;}
    public String companyt {get;set;}
    public String atid {get;set;}
    public String whereto {get;set;}
    public String torogen {get;set;}
    public String toroatid {get;set;}
    public String torour {get;set;}
    public Document doc {get;set;}
    public Boolean noaccess {get;set;}
    public Boolean ContinueFields {get;set;}
    public Invoice_Parameters__c inv {get;set;}
    public Double offset {
        get {
            TimeZone tz = UserInfo.getTimeZone();
            //Milliseconds to Day
            return tz.getOffset(DateTime.now()) / (1000 * 3600 * 24.0);
        }
    }
    public CustomerInvoiceControllerAccept(ApexPages.StandardController controller) {
        ContinueFields = false;
        recty = '';
        Curid = ApexPages.currentPage().getParameters().get('id');
        torour = ApexPages.currentPage().getParameters().get('toro');
        torogen = ApexPages.currentPage().getParameters().get('torogen');
        toroatid = '';
        atid = '';
        companyn = '';
        companyt = '';
        noaccess = true;
        signatureid = ApexPages.currentPage().getParameters().get('sigpic');
        signaturepic = '';
        set < id > subsl = new set < id > ();
        for (recordtype rec: [select id, name from recordtype])
            if (rec.name.contains('Sub'))
                subsl.add(rec.id);
        if (signatureid != null) {
            doc = [select id, body from document where id =: signatureid];
            signaturepic = '/servlet/servlet.FileDownload?file=' + signatureid;
        }
        Vec = '';
        badd = '';
        sadd = '';
        badd1 = '';
        sadd1 = '';
        badd2 = '';
        sadd2 = '';
        acc = [select id, Canada__c, Customer_Email__c, Invoice_Phone__c, Signature__c, Toro__c, Time_Stamp_Finished__c, Name_Of_Location__c, RecordType.Name, name, vin__c, year_vehic_toro__c, car_makers__c, model_trr__c, color__c, plate__c, job_type__c, phone, alt_phone__c, Finish_Date__c, Item_List_Toro_1__c, Item_List_Toro_2__c, Item_List_Toro_3__c, Quantity_Toro_1__c, Quantity_Toro_2__c, Quantity_Toro_3__c, Rate_Toro_1__c, Rate_Toro_2__c, Rate_Toro_3__c, Amount_Toro_1__c, Amount_Toro_2__c, Amount_Toro_3__c, Total_Amount_Toro_v1__c, Payment_Type_Toro_1__c, Payment_Type_Toro_2__c, Payment_Type_Toro_3__c, Payment_Amount_1__c, Payment_Amount_2__c, Payment_Amount_3__c, Direction__c, Time_Stamp_Assigned__c, Owner.Alias, Customer_Type__c, Insurance_account__c, Insurance_account__r.name, Insurance_account__r.BillingAddress__c, Billing_Zip__c, Operation__c, Total_Amount__c, Customer_Invoice_Date__c, Customer_Invoice__c, PO__c, First_Name__c, Billing_Address__c, Notes3__c, Services_Type__c, CreatedDate, Reference__c, Service_Call__c, Year__c, Make__c, Model__c, Hookup_Fee__c, BillingCountry, BillingState, BillingCity, BillingPostalCode, BillingStreet, Extra_Milage__c, ShippingCountry, ShippingState, ShippingCity, ShippingPostalCode, ShippingStreet, Extra_Labor__c, Special_Equipment__c, Lock_Out_Fee__c, Flat_Tire_Fee__c, Jump_Start_Fee__c, Connivant_DropOff_Fee__c, HWY_PickUp_Fee__c, Cleaning_Fee__c, Material2__c, Sales_Tax__c, Invoice_Total__c, Service_Technician__c, Invoice_Hookup_Fee__c, Invoice_Service_Call__c, Invoice_Mileage__c, Car_Details__c from account where id =: Curid];
        if (acc.RecordType.Name.contains('Toro') || acc.Toro__c)
            recty = 'toro';
        else
            recty = 'green';
        String invname;
        if(acc.Canada__c) invname = 'Canada';
        else invname = acc.Operation__c;
        list < Invoice_Parameters__c > invs = [select Logo_URL__c from Invoice_Parameters__c where Name =: invname limit 1];
        if (invs.size() > 0)
            inv = invs[0];
        list < Technician__c > serl = [select id, First_Name__c, Last_Name__c, Company_Name__c, Phone__c, Email__c, Company_Phone__c, RecordTypeId from Technician__c where id =: acc.Service_Technician__c];
        if (serl.size() > 0)
            ser = serl[0];
        else
            ser = new Technician__c();
        list < Technician__c > techcompname = [select id, Company_Name__c, Company_Phone__c from Technician__c where User__c =: UserInfo.getUserId() and recordTypeid in: subsl];
        if (techcompname.size() > 0) {
            noaccess = false;
            companyn = techcompname[0].Company_Name__c;
            companyt = techcompname[0].Company_Phone__c;
            list < attachment > attach = [select id from attachment where ParentId =: ser.id and name =: 'logo.jpg'];
            if (attach.size() > 0)
                atid = attach[0].id;
        }
        Credat = '';
        Cusdat = '';
        if (acc.CreatedDate != null)
            Credat = acc.CreatedDate.format();
        if (acc.Customer_Invoice_Date__c != null)
            Cusdat = acc.Customer_Invoice_Date__c.format();
        if (acc.BillingStreet != null)
            badd += acc.BillingStreet + ', ';
        badd1 = badd;
        if (acc.BillingCity != null) {
            badd += acc.BillingCity + ', ';
            badd2 += acc.BillingCity + ', ';
        }
        if (acc.BillingState != null) {
            badd += acc.BillingState;
            badd2 += acc.BillingState;
        }
        if (acc.BillingPostalCode != null) {
            badd += acc.BillingPostalCode + ' , ';
            badd2 += acc.BillingPostalCode;
        } else {
            if (acc.BillingPostalCode == null && acc.BillingState != null)
                badd += ', ';
        }
        if (acc.BillingCountry != null)
            badd += acc.BillingCountry;
        sadd = '';
        if (acc.ShippingStreet != null)
            sadd += acc.ShippingStreet + ', ';
        sadd1 = sadd;
        if (acc.ShippingCity != null) {
            sadd += acc.ShippingCity + ', ';
            sadd2 += acc.ShippingCity + ', ';
        }
        if (acc.ShippingState != null) {
            sadd += acc.ShippingState;
            sadd2 += acc.ShippingState;
        }
        if (acc.ShippingPostalCode != null) {
            sadd += acc.ShippingPostalCode + ' , ';
            sadd2 += acc.ShippingPostalCode;
        } else {
            if (acc.ShippingPostalCode == null && acc.ShippingState != null)
                sadd += ', ';
        }
        if (acc.ShippingCountry != null)
            sadd += acc.ShippingCountry;
        if (acc.Year__c != null)
            Vec += acc.Year__c;
        if (acc.Make__c != null)
            Vec += '/' + acc.Make__c;
        if (acc.Model__c != null)
            Vec += '/' + acc.Model__c;
        String url = ApexPages.currentPage().getURL();
        Apexpages.currentPage().getHeaders().put('content-disposition', 'filename=' + recty.capitalize() + '-customerinvoice-' + acc.Reference__c + '.pdf');
    }
    public static void SaveURLpdf(String accid, String attid) {
        String ur = Label.Invoice_site_url;
        ur += '?id=' + attid;
        update new Account(id = accid, Last_Invoice_URL__c = ur);
    }
    public pagereference GeneratePDFSMS() {
        PageReference pageRef = GeneratePDF('sms');
        pageRef.setRedirect(True);
        return pageRef;
    }
    public pagereference GeneratePDF() {
        PageReference pageRef = GeneratePDF('email');
        pageRef.setRedirect(True);
        return pageRef;
    }
    public pagereference GeneratePDF(String wher) {
        pagereference Pg = Page.CustomerInvoice1;
        if (signatureid != null)
            Pg.getParameters().put('sigpic', doc.id);
        attachment at = new Attachment();
        if (acc.name != 'testspec') {
            Blob pdf1 = pg.getcontentAsPdf();
            at.Body = pdf1;
        } else {
            at.body = Blob.valueOf('Unit Test Attachment Body');
        }
        at.OwnerId = UserInfo.getUserId();
        at.ParentId = acc.Id;
        at.Name = acc.Customer_Invoice__c + '.pdf';
        at.ContentType = 'application/pdf';
        insert at;
        SaveURLpdf(acc.id, at.id);
        if (signatureid != null){
            try{
                delete doc;
            }
            catch(exception e){}
        }
        String ur = '';
        if (wher == 'sms')
            ur = '/apex/Invoice_via_SMS';
        else
            ur = '/apex/InvoiceEmail';
        ur += '?id=' + acc.id + '&Atid=' + at.id;
        PageReference pageRef = new PageReference(ur);
        pageRef.setRedirect(True);
        return pageRef;
    }
    public pagereference ToroTechFields() {
        update acc;
        ContinueFields=true;
        return null;
    }
    public pagereference GenerateToroPDF() {
        if (torour == '1') {
            pagereference Pg = Page.CustomerInv;
            Pg.getParameters().put('id', Curid);
            Pg.getHeaders().put('content-disposition', 'filename=' + recty.capitalize() + '-customerinvoice-' + acc.Reference__c + '.pdf');
            attachment at = new Attachment();
            if (acc.name != 'testspec') {
                Blob pdf1 = pg.getcontentAsPdf();
                at.Body = pdf1;
            } else {
                at.body = Blob.valueOf('Unit Test Attachment Body');
            }
            at.OwnerId = UserInfo.getUserId();
            at.ParentId = acc.Id;
            at.Name = recty.capitalize() + '-customerinvoice-' + acc.Reference__c + '.pdf';
            at.ContentType = 'application/pdf';
            insert at;
            SaveURLpdf(acc.id, at.id);
            toroatid = at.id;
        }
        return null;

    }
    public pagereference GenerateToroTechPDF() {
        if (torogen == '1') {
            String wher = ApexPages.currentPage().getParameters().get('where');
            PageReference pageRef = GenerateToroPDFOp(wher);
            pageRef.setRedirect(True);
            return pageRef;
        }
        return null;
    }
    public pagereference GenerateToroPDFOp(String wher) {
        if (doc.id != null) {
            PageReference Pg;
            pg = Page.CustomerInvTechGen;
            Pg.getParameters().put('id', Curid);
            Pg.getParameters().put('sigpic', doc.id);
            Pg.getHeaders().put('content-disposition', 'filename=' + recty.capitalize() + '-customerinvoice-' + acc.Reference__c + '.pdf');
            attachment at = new Attachment();
            if (acc.name != 'testspec') {
                Blob pdf1 = pg.getcontentAsPDF();
                at.Body = pdf1;
            } else {
                at.body = Blob.valueOf('Unit Test Attachment Body');
            }
            at.OwnerId = UserInfo.getUserId();
            at.ParentId = acc.Id;
            at.Name = recty.capitalize() + '-customerinvoice-' + acc.Reference__c + '.pdf';
            at.ContentType = 'application/pdf';
            insert at;
            SaveURLpdf(acc.id, at.id);
            try{
                delete doc;
            }
            catch(exception e){}
            String ur = '';
            if (wher != '' && wher != null) {
                if (wher == 'sms')
                    ur = '/apex/Invoice_via_SMS';
                else
                    ur = '/apex/InvoiceEmail';
                ur += '?id=' + acc.id + '&Atid=' + at.id;
                PageReference pageRef = new PageReference(ur);
                pageRef.setRedirect(True);
                return pageRef;
            } else {
                PageReference pageRef = new PageReference('/' + CurId);
                pageRef.setRedirect(True);
                return pageRef;
            }
        }
        return null;
    }
    public pagereference GoToro() {
        if (signatureData != null && signatureData != '') {
            acc.Signature__c = '<img style="width: 100%;height:150px;" src="' + signatureData + '"/>';
            update acc;
            String[] b64 = signatureData.split(',');
            if (b64.size() > 1) {
                Document doc = new Document();
                doc.Name = acc.name;
                doc.AuthorId = UserInfo.getUserId();
                doc.FolderId = UserInfo.getUserId();
                doc.ContentType = 'image/jpeg';
                doc.Type = 'jpg';
                doc.IsPublic = true;
                doc.Body = EncodingUtil.base64Decode(b64[1]);
                insert doc;
                String ur = '/apex/CustomerInvTechGen?id=' + acc.id + '&torogen=1&where=' + whereto + '&sigpic=' + doc.id;
                PageReference pageRef = new PageReference(ur);
                pageRef.setRedirect(True);
                return pageRef;
            }
        }
        return null;
    }
    public pagereference Back() {
        if (signatureid != null && acc.name != 'testspec'){
            try{
                delete doc;
            }
            catch(exception e){}
        }
        String ur = '/apex/CustomerInvoice?id=' + acc.id;
        PageReference newPage = new PageReference(ur);
        newPage.setRedirect(true);
        return newPage;
    }
}
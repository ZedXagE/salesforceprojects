//ZedXagE -ITmyWay 25/12/17
// start with  -  System.schedule('Insurance Bypass', '0 45 * * * ?', new InsuranceBypassCallsScheduler());
global class InsuranceBypassCallsScheduler implements Schedulable {
	global void execute(SchedulableContext sc) {
		List <Account> accs4check = [select id,ID_Number__c,recordTypeid from Account where (Source_Code__c Like '%%CHP%%' OR Source_Code__c Like '%%Insurance%%') AND createdDate>=:System.Now().Addhours(-1)];
		ID jobID = System.enqueueJob(new InsuranceBypassCallsQueue(accs4check));
   	}
}
public without sharing class AssignmentController {
    private Map<String, String> param = ApexPages.currentPage().getParameters();
    private Account workOrder;
    private Calendar_Item__c event;
    private Event activity;

    public String startDT { get; set; }
    public String endDT { get; set; }
    public String technicianId { get; set; }

    public String areaStr { get; set; }
    public String serviceStr { get; set; }
    public String originStr { get; set; }
    public String destinationStr { get; set; }

    public AssignmentController(ApexPages.StandardController controller) {
        String id = param.get('id');
        workOrder = [SELECT Name, Source_Area__c, Services_Type__c, Job_Type__c,
            BillingStreet,
            BillingCity,
            BillingPostalCode,
            BillingCountry,
            BillingState,
            ShippingStreet,
            ShippingCity,
            ShippingPostalCode,
            ShippingCountry,
            ShippingState
        FROM Account WHERE Id=:id];
        event = new Calendar_Item__c(Work_Order__c=id);
        activity = new Event(Work_Order__c=id);

        areaStr = workOrder.Source_Area__c;
        if (areaStr == 'DLS' || areaStr == 'FW')
            areaStr = 'DLS;FW';
        else if (areaStr == 'SF' || areaStr == 'SJ' || areaStr == 'SM' || areaStr == 'Tri.V' || areaStr == 'SR')
            areaStr = 'EB';
        else if (areaStr == 'GTA' || areaStr == 'WGTA')
            areaStr = 'GTA;WGTA';
        serviceStr = workOrder.Services_Type__c;
        if (serviceStr == 'Car Special Services')
            serviceStr = 'RSA';

        for (String addrPart : new List<String>{'Billing', 'Shipping'}) {
            List<String> address = new List<String>();
            String street, city, state, postalcode, country;
            street = (String)workOrder.get(addrPart + 'Street');
            city = (String)workOrder.get(addrPart + 'City');
            state = (String)workOrder.get(addrPart + 'State');
            postalcode = (String)workOrder.get(addrPart + 'PostalCode');
            country = (String)workOrder.get(addrPart + 'Country');

            if (street != null) address.add(street);
            if (city != null) address.add(city);
            if (state != null) {
                if (postalcode == null) address.add(state);
                else address.add(state + ' ' + postalcode);
            } else if (postalcode != null) address.add(postalcode);
            if (country != null) address.add(country);

            if (addrPart == 'Billing')
                originStr = String.join(address, ', ');
            else
                destinationStr = String.join(address, ', ');
        }
    }

    public void assign() {
        event.Start_Date_and_Time__c = Datetime.valueOfGmt(startDT);
        event.End_Date_and_Time__c = Datetime.valueOfGmt(endDT);
        event.Technician__c = technicianId;
        event.Name = workOrder.Job_Type__c != null ? workOrder.Job_Type__c : workOrder.Services_Type__c;
        insert event;
        workOrder.Status__c = 'Assigned';
        workOrder.Service_Technician__c = technicianId;
        update workOrder;
        String userId = [SELECT User__c FROM Technician__c WHERE Id=:technicianId].User__c;
        if (userId != null) {
            activity.StartDateTime = Datetime.valueOfGmt(startDT);
            activity.EndDateTime = Datetime.valueOfGmt(endDT);
            activity.OwnerId = userId;
            activity.Subject = workOrder.Name;
            insert activity;
            ConnectApiHelper.postFeedItemWithMentions(Network.getNetworkId(), workOrder.Id, '{' + userId + '} Youv\'e been assigned to a new Work Order');//' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + workOrder.Id);
        }
    }

    public static void onAssign(List<Account> NworkOrders, Map<Id, Account> OworkOrderById) {
        List<Account> workOrders = new List<Account>();
        for (Account workOrder : NworkOrders)
            if (workOrder.Service_Technician__c != OworkOrderById.get(workOrder.Id).Service_Technician__c)
                workOrders.add(workOrder);
        if (workOrders.isEmpty())
            return;

        List<String> workOrdersIds = new List<String>();
        List<String> techniciansIds = new List<String>();
        for (Account workOrder : workOrders) {
            workOrdersIds.add(workOrder.Id);
            techniciansIds.add(workOrder.Service_Technician__c);
        }

        Map<String, String> userIdByTechnicianId = new Map<String, String>();
        for (Technician__c technician : [SELECT Id, User__c FROM Technician__c WHERE Id IN :techniciansIds])
            userIdByTechnicianId.put(technician.Id, technician.User__c);

        delete [SELECT Id FROM AccountShare WHERE AccountId IN :workOrdersIds AND RowCause='Manual'];
        List<AccountShare> shares = new List<AccountShare>();
        for (Account workOrder : workOrders) {
            String userId = userIdByTechnicianId.get(workOrder.Service_Technician__c);
            if (userId != null && userId != workOrder.OwnerId) { // owner doesnt need manual share rules. it can accutally limit him!!!
                shares.add(new AccountShare(
                    AccountId=workOrder.Id,
                    UserOrGroupId=userId,
                    AccountAccessLevel='Edit',
                    OpportunityAccessLevel='Edit'
                ));
            }
        }
        upsert shares;
    }
}
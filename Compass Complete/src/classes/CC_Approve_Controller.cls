public class CC_Approve_Controller {
    private Attachment pdfAttach;
    public Signature__c sig{get;set;}
    public Attachment License{get;set;}
    public Attachment Card{get;set;}

    public String Timeout{get;set;}

    public String FullName{get;set;}

    public String Success{get;set;}

    public Boolean PicsNeeded{get;set;}
    public Boolean Pdf{get;set;}
    public Boolean gen{get;set;}

    public Account Account{get;set;}
    public Map<String, Boolean> errMsgs {get;set;}

    private void resetErrMsgs() {
        errMsgs = new Map<String, Boolean>();
        errMsgs.put('Name', false);
        errMsgs.put('Pics', false);
        errMsgs.put('Sig', false);
    }
    public CC_Approve_Controller() {
        resetErrMsgs();
        gen=ApexPages.currentPage().getParameters().get('gen')=='1'?true:false;
        Success=ApexPages.currentPage().getParameters().get('success')=='1'?'1':'0';
        PicsNeeded=ApexPages.currentPage().getParameters().get('pics')=='1'?true:false;
        Pdf=ApexPages.currentPage().getParameters().get('pdf')=='1'?true:false;
        FullName=ApexPages.currentPage().getParameters().get('fname')!=null?ApexPages.currentPage().getParameters().get('fname'):'';
        String curid=ApexPages.currentPage().getParameters().get('id');
        try{
            Account = [select id,Name,Ownerid,Operation__c,Toro__c,Operation_Name__c,CC_Authorization_Date__c,CreatedDate,Card_Holder_Name__c,Flat_Rate__c,Office_Credit__c,Job_Type__c,Billing_Address__c,Card_Holder_Phone__c,Customer_Email__c,Last_Four__c,Expiration_Date__c,Security_Code__c From Account where id=:curid];
            if(Account.CC_Authorization_Date__c!=null&&Account.CC_Authorization_Date__c+1<System.now())
                success='-1';
        }
        catch(exception e){
            Account =new Account();
        }
        initAttach();
        if(ApexPages.currentPage().getParameters().get('sig')!=null){
            sig=[select id,data__c,Ownerid,Account__c from Signature__c where id=:ApexPages.currentPage().getParameters().get('sig')];
        }
    }

    private void initAttach(){
        License = new Attachment(Ownerid=Account.Ownerid,ParentId=Account.id);
        Card = new Attachment(Ownerid=Account.Ownerid,ParentId=Account.id);
        pdfAttach= new Attachment(Ownerid=Account.Ownerid,ParentId=Account.id,ContentType='application/pdf');
        sig= new Signature__c(Ownerid=Account.Ownerid,Account__c=Account.id);
    }
    public void sendEmail(){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {Account.Customer_Email__c};
        mail.setToAddresses(toAddresses);
        mail.setSenderDisplayName(Account.Operation_Name__c);
        mail.setSubject('CC Authorization');
        String message='Here is a copy of CC Authorization';
        mail.setHtmlBody(message);
        Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
        efa.setFileName(pdfattach.Name);
        efa.setBody(pdfattach.Body);
        mail.setFileAttachments(new List<Messaging.Emailfileattachment>{efa});
        if(!Test.isRunningTest())
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    public pagereference checkGenerate(){
        if(pdf&&gen){
            pagereference Pg = Page.CC_Approve;
            Pg.getParameters().put('id', Account.id);
            Pg.getParameters().put('pdf', '1');
            Pg.getParameters().put('sig', sig.id);
            Pg.getParameters().put('fname', FullName);
            try{
                Blob pdf1 = Pg.getcontentASPDF();
                pdfattach.Body=pdf1;
                pdfattach.Name=Account.Name+' Authentication.pdf';
                insert pdfattach;
                if(Account.Customer_Email__c!=null){
                    sendEmail();
                }
                delete sig;
                String ur='/CCApprove?id='+Account.id+'&success=1';
                PageReference pageRef = new PageReference(ur);
                pageRef.setRedirect(true);
                return pageRef;
            }
            catch(exception e){}
            finally{
                initAttach();
            }
        }
        return null;
    }
    private String enimage(String data){
        return '<img alt="error" width="100px" height="70px" src="'+data+'"></img>';
    }
    //submit form
    public pagereference Submit(){
        resetErrMsgs();
        if(FullName!=''){
            if(sig.data__c!=''&&sig.data__c!=null){
                if(PicsNeeded){
                    if(Card.Body!=null&&License.Body!=null){
                        try{
                            insert License;
                            insert Card;
                        }
                        catch(exception e){}
                    }
                    else{
                        errMsgs.put('Pics', true);
                        return null;
                    }
                }
                try{
                    String sid=sig.id;
                    if(sid==null){
                        sig.data__c=enimage(sig.data__c);
                        upsert sig;
                    }
                }
                catch(exception e){}
                String ur='/CCApprove?id='+Account.id+'&pdf=1&gen=1&fname='+FullName+'&sig='+sig.id;
                PageReference pageRef = new PageReference(ur);
                pageRef.setRedirect(true);
                return pageRef;
            }
            else{
                errMsgs.put('Sig', true);
                return null;
            }
        }
        else{
            errMsgs.put('Name', true);
            return null;
        }
    }
}
public class Account_Actions_VZ {
    public String Lat{get;set;}
    public String Lon{get;set;}
    public String uid{get;set;}
    public String aid{get;set;}
	public String reason{get;set;}
    public String credit{get;set;}
	public String cash{get;set;}
	public String check{get;set;}
	public String approval{get;set;}
	public String remarks{get;set;}
    public String eta {get; set;} 

    public static void mentionFeed(String message, String subjectid, String mentionid){
        message=' '+message;
        ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
        ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
        ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
        ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
        messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
        mentionSegmentInput.id = mentionid;
        messageBodyInput.messageSegments.add(mentionSegmentInput);
        textSegmentInput.text = message;
        messageBodyInput.messageSegments.add(textSegmentInput);
        feedItemInput.body = messageBodyInput;
        feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
        feedItemInput.subjectId = subjectid;
        ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput);
    }

    @future
    public static void NewLocation(String uid, String Lon, String Lat){
        System.debug('Here Future 1: '+uid+' '+Lon+' '+Lat);
        if(uid!=null&&Lon!=null&&Lat!=null&&Lon!=''&&Lat!=''){
            try{
                Technician__c tech = [select id from Technician__c where User__c=:uid];
                tech.Last_Seen_Location__longitude__s=Decimal.valueof(Lon);
                tech.Last_Seen_Location__latitude__s=Decimal.valueof(Lat);
                tech.Last_Seen_Datetime__c=System.Now();
                update tech;
                if(Test.isRunningTest())
                    insert new Lead();
            }
            catch(exception e){}
        }
    }

	@future
	public static void FutureStatus(String aid, String status){
		Account acc = [select id, OwnerId from Account where id=:aid];
		try{
			acc.Status__c=status;
			update acc; 
            if(Test.isRunningTest())
                insert new Lead();
		}
		catch(exception e){
			mentionFeed(e.getMessage(), aid, acc.Ownerid);              
		}
    }
	public Account_Actions_VZ(ApexPages.StandardController controller) {
		uid = UserInfo.getUserId();
        aid = controller.getId();
	}
	public void UpdateLocation(){
        System.debug('Here: '+uid+' '+Lon+' '+Lat);
        if(Lat!=''&&Lon!='')
            NewLocation(uid,Lon,Lat);
    }
    //start of accept
    public Pagereference Accept(){
        System.debug('Here: '+uid+' '+Lon+' '+Lat);
        if(Lat!=''&&Lon!='')
            NewLocation(uid,Lon,Lat);
        ConfirmedNewETA(aid,Lon,Lat);
        Pagereference newPage = new Pagereference('/'+aid);
        newPage.setRedirect(true);
        return newPage;
    }

    @Future(callout=true)
    public static void ConfirmedNewETA(String aid, String Lon, String Lat){
        Account acc = [select id, OwnerId, Geolocation1__Longitude__s,Geolocation1__Latitude__s,Tech_ETA_to_Pick_Up__c from Account where id=:aid];
        if(aid!=null){
            System.debug('Here Future 2: '+aid+' '+Lon+' '+Lat+' '+acc.Geolocation1__Longitude__s+' '+acc.Geolocation1__Latitude__s);
            try{
                acc.Status__c='Confirmed';
        		if(Lon!=null&&Lat!=null&&Lon!=''&&Lat!=''&&acc.Geolocation1__Longitude__s!=null&&acc.Geolocation1__Latitude__s!=null){
                    integer duration = GoogleRoutesHandler.getGoogleETA(Decimal.valueof(Lon),Decimal.valueof(Lat),acc.Geolocation1__Longitude__s,acc.Geolocation1__Latitude__s);
                    if(duration!=null)
                    	acc.Tech_ETA_to_Pick_Up__c=System.now().addSeconds(duration);
                }
				update acc;
                if(Test.isRunningTest())
                    insert new Lead();
            }
            catch(exception e){
				mentionFeed(e.getMessage(), aid, acc.Ownerid);                
            }
        }
    }
    //end of accept

    //start of canceled
	public Pagereference Canceled(){
        System.debug('Here: '+uid+' '+Lon+' '+Lat);
        FutureCanceled(aid,reason);
        Pagereference newPage = new Pagereference('/'+aid);
        newPage.setRedirect(true);
        return newPage;
    }
	@future
	public static void FutureCanceled(String aid, String reason){
		Account acc = [select id, OwnerId from Account where id=:aid];
		try{
			acc.Status__c='Canceled';
			acc.Cancelled_By__c='Technician';
			acc.Cancel_Reason__c=reason;
			update acc;
            if(Test.isRunningTest())
                insert new Lead();
		}
		catch(exception e){
			mentionFeed(e.getMessage(), aid, acc.Ownerid);                
		}
    }
    //end of canceled

    //start of enroute
	public Pagereference EnRoute(){
        System.debug('Here: '+uid+' '+Lon+' '+Lat);
        if(Lat!=''&&Lon!='')
            NewLocation(uid,Lon,Lat);
        FutureStatus(aid,'En Route');
        Pagereference newPage = new Pagereference('/'+aid);
        newPage.setRedirect(true);
        return newPage;
    }
    //end of enroute


    //start of Finished Action

    public Pagereference Finished(){
        FutureFinished(aid,credit,cash,check,approval,remarks);
        Pagereference newPage = new Pagereference('/'+aid);
        newPage.setRedirect(true);
        return newPage;
    }
    @future
    public static void FutureFinished(String aid, String credit, String cash, String check, String approval, String remarks){
        Account acc = [select id, OwnerId from Account where id=:aid];
		try{
			acc.Status__c='Finished';
            acc.CC_Approval_Code__c=approval;
            acc.Remarks__c=remarks;
            acc.Office_Credit__c=(credit!=''?Decimal.valueof(credit):null);
            acc.Technician_Cash_Check__c=(cash!=''?Decimal.valueof(cash):null);
            acc.Check_Amount__c=(check!=''?Decimal.valueof(check):null);
			update acc;
            if(Test.isRunningTest())
                insert new Lead();
		}
		catch(exception e){
			mentionFeed(e.getMessage(), aid, acc.Ownerid);               
		}
    }
    //end of Finished Action

    //start of Loaded Action
    public String Loaded_ShippingStreet {get; set;}
    public String Loaded_ShippingCity {get; set;}
    public String Loaded_ShippingState {get; set;}
    public String Loaded_ShippingPostalCode {get; set;}
    public String Loaded_ShippingCountry {get; set;} 
    public String Loaded_Geolocation2_Latitude_s {get; set;} 
    public String Loaded_Geolocation2_Longitude_s {get; set;} 
    
    public void Loaded_UpdateShipping() {
        Future_Loaded_UpdateShipping(aid, Loaded_ShippingStreet, Loaded_ShippingCity, Loaded_ShippingState, Loaded_ShippingPostalCode, Loaded_ShippingCountry);
    }
    @future
    public static void Future_Loaded_UpdateShipping(String aid, String sstreet, String scity, String sstate, String spostal, String scountry) {
        Account acc = [select id, OwnerId from Account where id=:aid];
        try{
            acc.ShippingStreet=sstreet;
            acc.ShippingCity=scity;
            acc.ShippingState=sstate;
            acc.ShippingPostalCode=spostal;
            acc.ShippingCountry=scountry;
            update acc;
            if(Test.isRunningTest())
                insert new Lead();
        }
        catch(exception e){
			mentionFeed(e.getMessage(), aid, acc.Ownerid);                
		}
    }
    
    public Pagereference Loaded_UpdateStatus() {
        Future_Loaded_UpdateStatus(aid,Loaded_Geolocation2_Latitude_s,Loaded_Geolocation2_Longitude_s,eta);
        Pagereference newPage = new Pagereference('/'+aid);
        newPage.setRedirect(true);
        return newPage;
    }
    @future
    public static void Future_Loaded_UpdateStatus(String aid, String llat, String llon, String eta) {
        Account acc = [select id, OwnerId from Account where id=:aid];
        try{
            acc.Status__c='Loaded';
            acc.Geolocation2__Latitude__s=(llat!=''?Decimal.valueof(llat):null);
            acc.Geolocation2__Longitude__s=(llon!=''?Decimal.valueof(llon):null);
            acc.Tech_ETA__c=(eta!=''?Datetime.parse(eta):null);
            update acc;
            if(Test.isRunningTest())
                insert new Lead();
        }
        catch(exception e){
			mentionFeed(e.getMessage(), aid, acc.Ownerid);              
		}
    }
    //end of Loaded

    //start of UpdateETA Action

    public Pagereference UpdateETA() {
        FutureUpdateETA(aid, eta);
        Pagereference newPage = new Pagereference('/'+aid);
        newPage.setRedirect(true);
        return newPage;
    }

    @future
    public static void FutureUpdateETA(String aid, String eta) {
        Account acc = [select id, OwnerId from Account where id=:aid];
        try{
            acc.Tech_ETA__c=(eta!=''?Datetime.parse(eta):null);
            update acc;
            if(Test.isRunningTest())
                insert new Lead();
        }
        catch(exception e){
			mentionFeed(e.getMessage(), aid, acc.Ownerid);               
		}
    }
    //end of UpdateETA
}
@isTest(seealldata=true)
public class TestSummaryJob {
	static testMethod void Test() {
		PageReference pgRef = Page.summaryjob; //Create Page Reference - 'Appt_New' is the name of Page
		Test.setCurrentPage(pgRef); //Set the page for Test Method
		SummaryJobController sc = new SummaryJobController();
        sc.TechType='All Techs';
        sc.CheckPDF();
		sc.FleetWorkWeek='12-2018';
		sc.GenerateFleet();
        sc.FleetWrap.upFleet();    
		sc.FleetWrap.FileType='PDF';
        sc.DownFleet();
        sc.FleetWrap.FileType='Excel';
        sc.DownFleet();
		sc.WorkWeek='12-2018';
		sc.Generate();
        
        sc.use='NG';
        sc.technamesfill();
        sc.Generate();
        sc.use='Toro';
        sc.technamesfill();
        sc.Generate();
        sc.use='Trio';
        sc.technamesfill();
        sc.Generate();
        sc.use='360TS';
        sc.technamesfill();
        sc.Generate();
        sc.go();
        if(sc.WorkOrdersByTech.size()>0){
            sc.WorkOrdersByTech[0].SaveInv();
            sc.WorkOrdersByTech[0].FileType='PDF';
            sc.WorkOrdersByTech[0].Down();
            sc.WorkOrdersByTech[0].FileType='Excel';
            sc.WorkOrdersByTech[0].Down();
        }
		sc.SaveInvs();
        sc.FileType='PDF';
        sc.downloc=0;
        sc.DownloadAll();
        sc.downloc=0;
        sc.downtype='all';
        sc.DownloadAll();
        sc.use='NG';
        sc.Generate();
	}
}
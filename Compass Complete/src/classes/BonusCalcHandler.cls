public class BonusCalcHandler {
	public class Bonus{
		public Decimal Finished;
		public Decimal Opened;
		public Decimal Rate;
		public Decimal BonusRate;
		public Decimal TotalSales;
		public String Operation;
		public Bonus(){
			Finished = 0;
			Opened = 0;
			Rate = 0;
			BonusRate = 0;
			TotalSales = 0;
		}
	}
	private DateTime StartWeek;
	private DateTime EndWeek;
	private Date TodayDate;
	private Map <String,Bonus> Bonuses;
	private Map <String,Integer> Tmzn;
	public BonusCalcHandler(){
		
	}
	//initialize Dates needed for class
	private void getDates(){
		TodayDate = System.Today();
		if(TodayDate != TodayDate.toStartofWeek())
			StartWeek = TodayDate.toStartofWeek();
		else
			StartWeek = TodayDate.addDays(-1).toStartofWeek();
		EndWeek = StartWeek.addDays(7);
		System.debug(StartWeek+' to '+EndWeek);
	}
	//initialize bonuses for every user
	private void InitBonuses(){
		Bonuses = new Map <String,Bonus>();
		Tmzn = new Map <String,Integer>();
		for(User us:[select id,TimeZoneSidKey from User where IsActive=true]){
			Bonuses.put(us.id,new Bonus());
			if(!Tmzn.containsKey(us.TimeZoneSidKey))
				Tmzn.put(us.TimeZoneSidKey,Integer.valueof(Timezone.getTimeZone(us.TimeZoneSidKey).getOffset(StartWeek)/1000*(-1)));
		}
	}
	//get data of weekly finished and opened jobs
	private void GetData(){
		List <Account> Works = [select Owner.TimeZoneSidKey,OwnerId,Operation__c,Total_Amount__c,Converted_Amount_to_USD__c,CreatedDate,Calles__c,Financial_Hold_Finished_Date__c from Account Where ((CreatedDate>=:StartWeek.addDays(-1) and CreatedDate<:EndWeek.addDays(1)) OR (Financial_Hold_Finished_Date__c>=:StartWeek.addDays(-1) and Financial_Hold_Finished_Date__c<:EndWeek.addDays(1))) and OwnerId in:Bonuses.KeySet()];
		for(Account Work:Works){
			//set Operation
			if(Bonuses.get(Work.OwnerId).Operation==null)
				Bonuses.get(Work.OwnerId).Operation=Work.Operation__c;
			//Closed This Week
			if(Work.Financial_Hold_Finished_Date__c>=StartWeek.addSeconds(Tmzn.get(Work.Owner.TimeZoneSidKey))&&Work.Financial_Hold_Finished_Date__c<EndWeek.addSeconds(Tmzn.get(Work.Owner.TimeZoneSidKey))){
				Bonuses.get(Work.OwnerId).Finished++;
				Bonuses.get(Work.OwnerId).TotalSales+=Work.Converted_Amount_to_USD__c;
			}
			//Opened This Week
			if(Work.CreatedDate>=StartWeek.addSeconds(Tmzn.get(Work.Owner.TimeZoneSidKey))&&Work.CreatedDate<EndWeek.addSeconds(Tmzn.get(Work.Owner.TimeZoneSidKey))&&Work.Calles__c==1){
				Bonuses.get(Work.OwnerId).Opened++;
			}
		}
	}
	//calculation rate and delete no jobs users
	private void CalcAndDelete(){
		for(String uid:Bonuses.KeySet()){
			if(Bonuses.get(uid).Finished==0&&Bonuses.get(uid).Opened==0)
				Bonuses.remove(uid);
			else
				Bonuses.get(uid).Rate=(Bonuses.get(uid).Opened!=0?Bonuses.get(uid).Finished/Bonuses.get(uid).Opened:Bonuses.get(uid).Finished)*100;
		}
	}
	public void StartBonusCalc(DateTime Startd,DateTime Endd){
		StartWeek=Startd;
		EndWeek=Endd;
		TodayDate=System.Today();
		Initialize();
	}
	//init
	public void StartBonusCalc() {
		getDates();
		Initialize();
	}
	private void Initialize(){
		InitBonuses();
		GetData();
		CalcAndDelete();
		insertLines();
	}
	private void insertLines() {
		List <Bonus__c> Bonuses4Create = new List <Bonus__c>();
		for(String uid:Bonuses.KeySet())
			Bonuses4Create.add(new Bonus__c(Ownerid=uid,Date__c=TodayDate.addDays(-1),Total_Sales__c=Bonuses.get(uid).TotalSales,Rate__c=Bonuses.get(uid).Rate,Operation__c=Bonuses.get(uid).Operation,Calls__c=Bonuses.get(uid).Opened,Jobs__c=Bonuses.get(uid).Finished));
		insert Bonuses4Create;

		//Calc Bonuses
		Set <String> ids = new Set <String>();
		for(Bonus__c bns:Bonuses4Create)
			ids.add(bns.id);
		Bonuses4Create = [select id,Bonus_Rate__c,Rate_Step__c,Operation__c,Amount_Step__c from Bonus__c where id in:ids];
		if(Bonuses4Create.size()>0){
			//Init Sets of options to check in bonus table and init bonus rate as 0
			Set <String> ops=new Set <String>();
			Set <Decimal> astps=new Set <Decimal>();
			Set <Decimal> rstps=new Set <Decimal>();
			for(Bonus__c bns:Bonuses4Create){
				rstps.add(bns.Rate_Step__c);
				ops.add(bns.Operation__c);
				astps.add(bns.Amount_Step__c);
				bns.Bonus_Rate__c=0;
			}
			//check if bonus rate exist in tables and changes 0 to required one
			for(Bonus_Table__c bnsTab:[select Rate__c,Rate_Step__c,Operation__c,Amount_Step__c from Bonus_Table__c where Rate_Step__c in: rstps and Operation__c in: ops and Amount_Step__c in: astps])
				for(Bonus__c bns:Bonuses4Create)
					if(bns.Rate_Step__c==bnsTab.Rate_Step__c&&bns.Operation__c==bnsTab.Operation__c&&bns.Amount_Step__c==bnsTab.Amount_Step__c)
						bns.Bonus_Rate__c=bnsTab.Rate__c;
			update Bonuses4Create;
		}
		System.debug(Bonuses4Create);
	}
}
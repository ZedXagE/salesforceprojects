//ZedXagE - ITmyWay 19/3/18
global class BonusSnapshotsHandler implements Schedulable {
	global void execute(SchedulableContext SC) {
		//Get All Bonuses without Bonus Rate
		list <Bonus__c> bnss = [select id,Bonus_Rate__c,Rate_Step__c,Operation__c,Amount_Step__c from Bonus__c where Bonus_Rate__c=null];
		if(bnss.size()>0){

			//Init Sets of options to check in bonus table and init bonus rate as 0
			Set <String> ops=new Set <String>();
			Set <Decimal> astps=new Set <Decimal>();
			Set <Decimal> rstps=new Set <Decimal>();
			for(Bonus__c bns:bnss){
				rstps.add(bns.Rate_Step__c);
				ops.add(bns.Operation__c);
				astps.add(bns.Amount_Step__c);
				bns.Bonus_Rate__c=0;
			}

			//check if bonus rate exist in tables and changes 0 to required one
			for(Bonus_Table__c bnsTab:[select Rate__c,Rate_Step__c,Operation__c,Amount_Step__c from Bonus_Table__c where Rate_Step__c in: rstps and Operation__c in: ops and Amount_Step__c in: astps])
				for(Bonus__c bns:bnss)
					if(bns.Rate_Step__c==bnsTab.Rate_Step__c&&bns.Operation__c==bnsTab.Operation__c&&bns.Amount_Step__c==bnsTab.Amount_Step__c)
						bns.Bonus_Rate__c=bnsTab.Rate__c;

			update bnss;
		}
   	}          
}
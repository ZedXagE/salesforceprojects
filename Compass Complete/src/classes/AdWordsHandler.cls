/*
 * @Author: ZedXagE - ITmyWay 
 * @Date: 2018-07-05 14:09:53 
 * @Last Modified by: ZedXagE - ITmyWay
 * @Last Modified time: 2018-07-05 14:19:00
 */
public class AdWordsHandler {
	public String weekformat;
	public class AdWord{
		public String LcpId;
		public String LcpName;
		public Decimal Finished;
		public Decimal Opened;
		public Decimal TotalSales;
		public AdWord(String lid,String lname){
			LcpId = lid;
			LcpName = lname;
			Finished = 0;
			Opened = 0;
			TotalSales = 0;
		}
	}
	private DateTime StartWeek;
	private DateTime EndWeek;
	private Date TodayDate;
	private Map <String,String> SourceIds;
	private Map <String,Adword> AdWords;
	private Map <String,Integer> Tmzn;
	public AdWordsHandler() {
		
	}
	//initialize Dates needed for class
	private void getDates(){
		TodayDate = System.Today();
		if(TodayDate != TodayDate.toStartofWeek())
			StartWeek = TodayDate.toStartofWeek();
		else
			StartWeek = TodayDate.addDays(-1).toStartofWeek();
		EndWeek = StartWeek.addDays(7);
		System.debug(StartWeek+' to '+EndWeek);
	}
	//initialize AdWords for every lcp
	private void InitAdWords(){
		AdWords = new Map <String,Adword>();
		Tmzn = new Map <String,Integer>();
		SourceIds =  new Map <String,String>();
		for(Local_City_Page__c lcp:[select id,Name,Source__c,Operation__c from Local_City_Page__c where Source_Campaign__c='PPC' and Source__c!=null]){
			SourceIds.put(lcp.Source__c.toLowerCase(),lcp.id);
			AdWords.put(lcp.Source__c.toLowerCase(),new Adword(lcp.id,lcp.Name));
		}
		for(User us:[select id,TimeZoneSidKey from User where IsActive=true]){
			if(!Tmzn.containsKey(us.TimeZoneSidKey))
				Tmzn.put(us.TimeZoneSidKey,Integer.valueof(Timezone.getTimeZone(us.TimeZoneSidKey).getOffset(StartWeek)/1000*(-1)));
		}
	}
	//get data of weekly finished and opened jobs
	private void GetData(){
		List <Account> Works = [select Owner.TimeZoneSidKey,OwnerId,Source_Code__c,Total_Amount__c,Converted_Amount_to_USD__c,CreatedDate,Calles__c,Financial_Hold_Finished_Date__c from Account Where ((CreatedDate>=:StartWeek.addDays(-1) and CreatedDate<:EndWeek.addDays(1)) OR (Financial_Hold_Finished_Date__c>=:StartWeek.addDays(-1) and Financial_Hold_Finished_Date__c<:EndWeek.addDays(1))) and Source_Code__c in:AdWords.KeySet() and Source_Code__c!=null];
		for(Account Work:Works){
			//Closed This Week
			if(Work.Financial_Hold_Finished_Date__c>=StartWeek.addSeconds(Tmzn.get(Work.Owner.TimeZoneSidKey))&&Work.Financial_Hold_Finished_Date__c<EndWeek.addSeconds(Tmzn.get(Work.Owner.TimeZoneSidKey))){
				AdWords.get(Work.Source_Code__c.toLowerCase()).Finished++;
				AdWords.get(Work.Source_Code__c.toLowerCase()).TotalSales+=Work.Total_Amount__c;
			}
			//Opened This Week
			if(Work.CreatedDate>=StartWeek.addSeconds(Tmzn.get(Work.Owner.TimeZoneSidKey))&&Work.CreatedDate<EndWeek.addSeconds(Tmzn.get(Work.Owner.TimeZoneSidKey))&&Work.Calles__c==1){
				AdWords.get(Work.Source_Code__c.toLowerCase()).Opened++;
			}
		}
	}
	public void StartCalc(DateTime Startd,DateTime Endd){
		StartWeek=Startd;
		EndWeek=Endd;
		TodayDate=System.Today();
		Initialize();
	}
	//init
	public void StartCalc() {
		getDates();
		Initialize();
	}
	private void Initialize(){
		Date StartDate = Date.newinstance(StartWeek.year(),StartWeek.month(),StartWeek.day()+1);
		List <Week_Number__c> Week=[Select Week__c From Week_Number__c Where Start_Date__c=:StartDate Limit 1];
		weekformat = 'Week ';
		System.debug(StartDate);
		if(Week.size()>0)
			weekformat+=String.valueof(week[0].Week__c);
		weekformat+=' - ';
		InitAdWords();
		GetData();
		insertLines();
	}
	private void insertLines() {
		List <Adwords__c> AdWords4Create = new List <Adwords__c>();
		for(Adword ad:AdWords.Values())
			AdWords4Create.add(new AdWords__c(Name=weekformat+ad.LcpName,Local_City_Page__c=ad.LcpId,Calls__c=ad.Opened,Jobs__c=ad.Finished,Income__c=ad.TotalSales));
		insert AdWords4Create;
		System.debug(AdWords4Create);
	}
}
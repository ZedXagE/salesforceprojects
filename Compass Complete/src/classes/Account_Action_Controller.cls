public class Account_Action_Controller {

    public Account_Action_Controller(ApexPages.StandardController controller) {}

    //start of Accept Action
    public String Accept_Id {get; set;}
    public String Accept_Status_c {get; set;} 
    public String Accept_Tech_ETA_to_Pick_Up_c {get; set;} 

    public void Accept_UpdateStatusNew() {
        Account a = new Account(
            Id=Accept_Id,
            Status__c=Accept_Status_c
        );
        a.Tech_ETA_to_Pick_Up__c=(Accept_Tech_ETA_to_Pick_Up_c!=''?Datetime.parse(Accept_Tech_ETA_to_Pick_Up_c):null);
        update a;
    }
    public void Accept_UpdateStatus() {
        Account a = new Account(
            Id=Accept_Id,
            Status__c=Accept_Status_c
        );
        update a;
    }
    @future
    public static void Future_Accept_UpdateStatus(String accid, String status, String eta) {
        Account a = new Account(
            Id=accid,
            Status__c=status
        );
        if(eta!=null)
            a.Tech_ETA_to_Pick_Up__c=(eta!=''?Datetime.parse(eta):null);
        update a;
    }
    //end of Accept


    //start of Canceled Action
    public String Canceled_Id {get; set;}
    public String Canceled_Cancel_Reason_c {get; set;} 
    public String Canceled_Cancelled_By_c {get; set;} 
    public String Canceled_Status_c {get; set;} 

    public void Canceled_UpdateStatus() {
        Account a = new Account(
            Id=Canceled_Id,
            Cancel_Reason__c=Canceled_Cancel_Reason_c,
            Cancelled_By__c=Canceled_Cancelled_By_c,
            Status__c=Canceled_Status_c
        );
        update a;
    }
    //end of Canceled

    //start of Decline Action
    public String Decline_Id {get; set;}
    public String Decline_Status_c {get; set;} 

    public void Decline_UpdateStatus() {
        Account a = new Account(
            Id=Decline_Id,
            Status__c=Decline_Status_c
        );
        update a;
    }
    //end of Decline

    //start of En_Route Action
    public String En_Route_Id {get; set;}
    public String En_Route_Status_c {get; set;} 

    public void En_Route_UpdateStatus() {
        Account a = new Account(
            Id=En_Route_Id,
            Status__c=En_Route_Status_c
        );
        update a;
    }    
    //end of En_Route

    //start of UpdateETA Action
    public String UpdateETA_Id {get; set;}
    public String UpdateETA_Tech_ETA_c {get; set;} 

    public void UpdateETA_UpdateStatus() {
        Account a = new Account(
            Id=UpdateETA_Id
        );
        a.Tech_ETA__c=(UpdateETA_Tech_ETA_c!=''?Datetime.parse(UpdateETA_Tech_ETA_c):null);
        update a;
    }
    //end of UpdateETA

    //start of Finished Action
    public String Finished_Id {get; set;}
    public String Finished_Status_c {get; set;} 
    public String Finished_Office_Credit_c {get; set;} 
    public String Finished_Technician_Cash_Check_c {get; set;} 
    public String Finished_Check_Amount_c {get; set;} 
    public String Finished_CC_Approval_Code_c {get; set;} 
    public String Finished_Remarks_c {get; set;} 
    
    public void Finished_UpdateStatus() {
        Account a = new Account(
            Id=Finished_Id,
            Status__c=Finished_Status_c,
            CC_Approval_Code__c=Finished_CC_Approval_Code_c,
            Remarks__c=Finished_Remarks_c
        );
        a.Office_Credit__c=(Finished_Office_Credit_c!=''?Decimal.valueof(Finished_Office_Credit_c):null);
        a.Technician_Cash_Check__c=(Finished_Technician_Cash_Check_c!=''?Decimal.valueof(Finished_Technician_Cash_Check_c):null);
        a.Check_Amount__c=(Finished_Check_Amount_c!=''?Decimal.valueof(Finished_Check_Amount_c):null);
        update a;
    }
    //end of Finished

    //start of Loaded Action
    public String Loaded_Id {get; set;}
    public String Loaded_ShippingStreet {get; set;}
    public String Loaded_ShippingCity {get; set;}
    public String Loaded_ShippingState {get; set;}
    public String Loaded_ShippingPostalCode {get; set;}
    public String Loaded_ShippingCountry {get; set;} 
    public String Loaded_Status_c {get; set;} 
    public String Loaded_Geolocation2_Latitude_s {get; set;} 
    public String Loaded_Geolocation2_Longitude_s {get; set;} 
    public String Loaded_Tech_ETA_c {get; set;} 
    
    public void Loaded_UpdateShipping() {
        Account a = new Account(
            Id=Loaded_Id,
            ShippingStreet=Loaded_ShippingStreet,
            ShippingCity=Loaded_ShippingCity,
            ShippingState=Loaded_ShippingState,
            ShippingPostalCode=Loaded_ShippingPostalCode,
            ShippingCountry=Loaded_ShippingCountry
        );
        update a;
    }
    
    public void Loaded_UpdateStatus() {
        Account a = new Account(
            Id=Loaded_Id,
            Status__c=Loaded_Status_c
        );
        a.Geolocation2__Latitude__s=(Loaded_Geolocation2_Latitude_s!=''?Decimal.valueof(Loaded_Geolocation2_Latitude_s):null);
        a.Geolocation2__Longitude__s=(Loaded_Geolocation2_Longitude_s!=''?Decimal.valueof(Loaded_Geolocation2_Longitude_s):null);
        a.Tech_ETA__c=(Loaded_Tech_ETA_c!=''?Datetime.parse(Loaded_Tech_ETA_c):null);
        update a;
    }
    //end of Loaded
    
}
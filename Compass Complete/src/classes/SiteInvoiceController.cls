public class SiteInvoiceController {
	Public String Name {get;set;}
	Public String Data {get;set;}
	Public String ContentType {get;set;}
	public SiteInvoiceController() {
		try{
			Attachment att = [select Name,Body,ContentType from Attachment where id=:ApexPages.currentPage().getParameters().get('id')];
			Name = att.Name;
			Data = EncodingUtil.Base64Encode(att.Body);
			ContentType = att.ContentType;
		}
		catch(exception e){}
	}
}
public class GoogleRoutesHandler {
    public class googleResponse{
        public List<route> routes;
    }
    public class route{
        public List<leg> legs;
    }
    public class leg{
        public duration duration;
    }
    public class duration{
        public Integer value;
    }
    public static integer getGoogleETA(Decimal tLon, Decimal tLat, Decimal dLon, Decimal dLat){
        String data='origin='+tLat+','+tLon+'&destination='+dLat+','+dLon;
        googleResponse gres = getGoogle(Data);
        integer duration=0;
        try{
            for(leg leg:gres.routes[0].legs)
				duration+=leg.duration.value;
            return duration;
    	}
        catch(exception e){
            return null;
        }
    }
    public static googleResponse getGoogle(String data){
		Http http=new Http();
		HttpRequest req=new HttpRequest();
		req.setendpoint('https://maps.googleapis.com/maps/api/directions/json?key='+Label.GoogleAPIKey+'&'+data);
		req.setmethod('GET');
		req.setHeader('Content-Type', 'application/json');
		HttpResponse res = new HttpResponse();
        googleResponse gres = new googleResponse();
        if(!Test.isRunningTest()){
			res = http.send(req);
            gres = (googleResponse)JSON.deserialize(res.getBody(),googleResponse.class);
        }
		return gres;
	}
}
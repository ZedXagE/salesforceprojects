/*
 * @Author: ZedXagE - ITmyWay 
 * @Date: 2018-07-05 14:09:56 
 * @Last Modified by:   ZedXagE - ITmyWay 
 * @Last Modified time: 2018-07-05 14:09:56 
 */
global class AdWordsScheduler implements Schedulable {
	global AdWordsScheduler() {
		
	}
	//execute and insert
	global void execute(SchedulableContext SC) {
		AdWordsHandler aws = new AdWordsHandler();
		aws.StartCalc();
	}
}
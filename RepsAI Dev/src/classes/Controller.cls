//ZedXagE - ITmyWay, Issue-000001825 , 4/4/18
public class Controller {
    public class jsObject{
		public String token;
        public String requesterName;
        public String requesterId;
        public String ticketSubject;
        public String ticketId;
        public String representativeId;
        public String representativeName;
        public String ticketText;
        public List <organizationTag> organizationTags;
        public String organizationId;
        public DateTime organizationCreated;
        public jsObject(){
            organizationTags = new List <organizationTag>();
        }
    }
    public class organizationTag{
        public String id;
        public String value;
        public organizationTag(String d,String t){
            this.id=d;
            this.value=t;
        }
    }
    public class response{
        public resData data{get;set;}
        public response(){
            data = new resData();
        }
    }
    public class resData{
        public List <possibleResponses> possibleResponses{get;set;}
        public List <possibleActions> possibleActions{get;set;}
        public resData(){
            possibleResponses = new List <possibleResponses>();
            possibleActions = new List <possibleActions>();
        }
    }
    public class possibleResponses{
        public Integer row{get;set;}
        public Decimal confidence;
        public Boolean isSignature;
        public String title{get;set;}
        public String titleURL{get;set;}
        public String response{get;set;}
        public String responseHTML{get;set;}
        public String responseURL{get;set;}
        public Boolean isEnabled;
        public String ticketStatus{get;set;}
        public List <String> tags{get;set;}
    }
    public class possibleActions{
        
    }
    private final Case cas;
    public String curid{get;set;}
    public Integer prRow{get;set;}
    public String body{get;set;}
    public String subject{get;set;}
    public response Suggestions{get;set;}
    public Controller(ApexPages.StandardController stdController) {
        body = ApexPages.currentPage().getParameters().get('RepsAIbody');
        if(body!=null)
            body=EncodingUtil.urlDecode(body, 'UTF-8');
        subject = ApexPages.currentPage().getParameters().get('RepsAIsubject');
        if(subject!=null)
            subject=EncodingUtil.urlDecode(subject, 'UTF-8');
    }
    public Controller(){
        curid = ApexPages.currentPage().getParameters().get('id');
        this.cas = [select id,Description,Subject,ContactId,Contact.Name,OwnerId,Owner.Name,AccountId,Account.CreatedDate,(select TopicId,Topic.Name from TopicAssignments) from Case where id=:curid];
        Suggestions = new response();
    }
    public void getData() {
		try{
			jsObject js = new jsObject();
			js.token=[select Token__c from Settings__c where Token__c!=null limit 1].Token__c;
			js.requesterName = cas.Contact.Name;
			js.requesterId = cas.ContactId;
			js.ticketSubject = cas.Subject;
			js.ticketId = cas.id;
			js.representativeId = cas.OwnerId;
			js.representativeName = cas.Owner.Name;
			js.ticketText = cas.Description;
			js.organizationId = cas.AccountId;
			js.organizationCreated = cas.Account.CreatedDate;
			for(TopicAssignment tpa:cas.TopicAssignments)
				js.organizationTags.add(new organizationTag(tpa.TopicId,tpa.Topic.Name));
			String jsondata = JSON.serialize(js);
			System.debug(jsondata);
			HttpResponse res = zedHTTP('http://api.reps.ai/accounts/repsai/suggestions','POST',jsondata);
			if(res!=null){
				System.debug(res.getBody());
				if(!Test.isRunningTest())
					Suggestions = (response)JSON.deserialize(res.getBody(), response.class);
				if(Suggestions.data.possibleResponses!=null)
					for(Integer i=0;i<Suggestions.data.possibleResponses.size();i++){
						Suggestions.data.possibleResponses[i].row=i;
						Suggestions.data.possibleResponses[i].responseHTML=Suggestions.data.possibleResponses[i].response!=null?Suggestions.data.possibleResponses[i].response.replaceAll('\n','<br/>'):null;
						Suggestions.data.possibleResponses[i].responseURL=Suggestions.data.possibleResponses[i].responseHTML!=null?EncodingUtil.urlEncode(Suggestions.data.possibleResponses[i].responseHTML, 'UTF-8'):null;
						Suggestions.data.possibleResponses[i].titleURL=Suggestions.data.possibleResponses[i].title!=null?EncodingUtil.urlEncode(Suggestions.data.possibleResponses[i].title, 'UTF-8'):null;
					}
				System.debug(Suggestions.data.possibleResponses);
			}
		}
		catch(exception e){
			Suggestions.data.possibleResponses=null;
		}
    }
    public void Tag(){
        if(Suggestions.data.possibleResponses[prRow].tags!=null){
            Set <String> tags = new Set <String>();
            for(String tag:Suggestions.data.possibleResponses[prRow].tags)
                tags.add(tag);
            List <Topic> tpsExists = [select id,name from Topic where name in:tags];
            List <Topic> tps = new List <Topic>();
            List <TopicAssignment> tpAss = new List <TopicAssignment>();
            for(String tag:Suggestions.data.possibleResponses[prRow].tags){
                Boolean exst=false;
                for(Topic tp:tpsExists){
                    if(tp.name==tag){
                        exst=true;
                        tps.add(new Topic(id=tp.id,name=tag));
                        break;
                    }
                }
                if(!exst)
                    tps.add(new Topic(name=tag));
            }
            Database.SaveResult[] restps = Database.insert(tps, false);
            for(Topic tp:tps)
                if(tp.id!=null){
                    tpAss.add(new TopicAssignment(TopicId=tp.id,EntityId=cas.id));
                }
            Database.SaveResult[] restpass = Database.insert(tpass, false);
        }
    }
    public static HttpResponse zedHTTP(String endpoint,String method,String data){
        Http http=new Http();
        HttpRequest req=new HttpRequest();
        req.setendpoint(endpoint);
        req.setmethod(method);
        req.setHeader('Content-Type', 'application/json');
        req.setbody(data);
		req.setTimeout(6000);
        HttpResponse res = new HttpResponse();
        if(!Test.isRunningTest())
            res = http.send(req);
        return res;
    }
}
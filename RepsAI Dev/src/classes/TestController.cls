//ZedXagE - ITmyWay, Issue-000001825 , 4/4/18
@isTest
public class TestController {
    static testMethod void Test() {
        Account acc = new Account (Name='test');
        insert acc;
        Contact con = new Contact (LastName='test',Accountid=acc.id,Email='test@test.com');
        insert con;
        Case cas = new Case (Accountid=acc.id,Contactid=con.id,subject='test',Description='test');
        insert cas;
        Settings__c sett = new Settings__c(Name='test',Token__c='test');
        insert sett;
        ApexPages.StandardController sc = new ApexPages.StandardController(cas);
        Controller scc = new Controller(sc);
        PageReference pgRef = Page.Classic;
		Test.setCurrentPage(pgRef);
		ApexPages.currentPage().getParameters().put('id', cas.id);
        Controller cc = new Controller();
        Controller.possibleResponses pr = new Controller.possibleResponses();
        pr.tags=new List <String>{'test'};
        cc.Suggestions.data.possibleResponses.add(pr);
        cc.getData();
        cc.prRow=0;
        cc.Tag();
    }
}
public class InsuranceBypassCallsQueue implements Queueable {
	private List <Account> accs;
	public InsuranceBypassCallsQueue(List <Account> ac) {
		accs=ac;
	}
	public void execute(QueueableContext context) {
		String errors='';
		for(integer i=0;i<20;i++){
			if(accs.size()>0){
				List <account> source = [select id,Source_Code__c from Account where (NOT Source_Code__c Like '%%CHP%%') AND (NOT Source_Code__c Like '%%Insurance%%') AND recordTypeid=:accs[0].recordTypeid AND ID_Number__c=:accs[0].ID_Number__c LIMIT 1];
				if(source.size()>0){
					accs[0].Source_Code__c=source[0].Source_Code__c;
					try{
						update accs[0];
					}
					catch(exception e){
						errors+=accs[0].id+';';
					}
				}
				accs.remove(0);
			}
		}
		System.debug(errors);
		if(accs.size()>0)
			ID jobID = System.enqueueJob(new InsuranceBypassCallsQueue(accs));
    }
}
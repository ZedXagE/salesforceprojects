from random import *
 
def Min_element_in_array (a,n):
    min=a[0]
    j=1
    for i in range(1,n-1):
        if(a[i]<min):
            min=a[i]
            j+=1
    return j

def createRandArr (n):
    a=[]
    for i in range(0,n-1):
        a.append(randint(0,32767))
    return a

a=[]
j=0
num=0
for i in range(1,10000):
    a=createRandArr(10000)
    j+=Min_element_in_array(a,10000)
    num+=1
print(str(j/num))
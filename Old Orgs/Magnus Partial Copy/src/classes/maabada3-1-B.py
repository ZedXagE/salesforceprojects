from random import *
 
def Min_element_in_array (a,n):
    min=a[0]
    j=1
    for i in range(1,n-1):
        if(a[i]<min):
            min=a[i]
            j+=1
    return j

def createArr (n):
    a=[]
    for i in range(0,n-1):
        a.append(i)
    return a

def shuffleArr (a):
    shuffle(a)
    return a

a=[]
j=0
num=0
a=createArr(10000)
for i in range(1,10000):
    a=shuffleArr(a)
    j+=Min_element_in_array(a,10000)
    num+=1
print(str(j/num))
@isTest(seealldata=true)
public class InsuranceBypassCallsTest {
	static testMethod void Test() {
        Test.startTest();
        List <Account> accs4check = [select id,ID_Number__c,recordTypeid from Account where (Source_Code__c Like '%%CHP%%' OR Source_Code__c Like '%%Insurance%%') limit 21];
		InsuranceBypassCallsQueue sc=new InsuranceBypassCallsQueue(accs4check);
        sc.execute(null);
        new InsuranceBypassCallsScheduler().execute(null);
        Test.stopTest();
    }
}
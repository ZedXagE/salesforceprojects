@RestResource(urlMapping='/TrioAlerts')
//ITmyWay ZedXagE - 5/11/17 - issue 1339
global without sharing class TrioAlerts {
    @HttpPut
    global static String Flow() {
        //single message for errors
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {'keynan@itmyway.com'};
        mail.setToAddresses(toAddresses);
        mail.setSenderDisplayName('Compass Non Integrated Error');
        mail.setSubject('Compass Non Integrated Flow Error');
        String message='';

        //get data from call
        String pbxid=RestContext.request.params.get('pbxid');
        String ibnum=RestContext.request.params.get('ibnum');
        String callerid=RestContext.request.params.get('callerid');
        String calluniqueid=RestContext.request.params.get('calluniqueid');
        String source=RestContext.request.params.get('source');
        String agent=RestContext.request.params.get('agent');
        String durationText=RestContext.request.params.get('duration');
        String agentdurText=RestContext.request.params.get('agentdur');
        Decimal duration=0;
        Decimal agentdur=0;
        if(durationText!=null&&durationText!='')
            duration=Decimal.valueof(durationText);
        if(agentdurText!=null&&agentdurText!='')
            agentdur=Decimal.valueof(agentdurText);
        String data='pbxid='+pbxid+'&ibnum='+ibnum+'&callerid='+callerid+'&calluniqueid='+calluniqueid+'&source='+source+'&agent='+agent+'&duration='+durationText+'&agentdur='+agentdurText;
       
        //find user by agent
        User use=new User();
        if(pbxid=='2005')
        	use=new User(id='00541000002wy2T',Company__c='Toro');
        else{
        	if(pbxid=='2027')
        		use=new User(id='00541000002wy2x',Company__c='Green');
            else{
                if(pbxid=='2006')
                    use=new User(id='00541000004V4yt',Company__c='360TS');
                else
                    use=new User(id='00541000001544k',Company__c='360TS');
            }
        }
        
        List <User> uses=[select id,Company__c from User Where Pbx_Agent_id__c=:agent AND Pbx_id__c=:pbxid Limit 1];
        if(uses.size()>0)
            use=uses[0];

        //create task
        Task newtas=new Task();
        newtas.recordTypeid='012410000002go3';
        newtas.Subject = 'Inbound Non-Integrated Call';
        newtas.Tnfld_caller_id__c = callerid;
        newtas.Source__c = source;
        newtas.Call_Unique_Id__c = calluniqueid;
        newtas.ID_Number__c = ibnum;
        newtas.Duration__c = duration;
        newtas.Agent_Duration__c = agentdur;
        newtas.Ownerid=use.id;
        newtas.Status='Completed';
        newtas.AgentID__c=agent;
        newtas.PBX__c=pbxid;

        try{
            insert newtas;
        }
        catch (System.DmlException e) {
                message+=e.getDmlMessage(0)+', ';  
        }
        //get all data and new formulas from task we created
        Task tas = [select id, Call_Unique_Id__c, ID_Number__c, OwnerId, Source__c, Tnfld_caller_id__c, whatid, Work_Order__c,SourceKey__c, Reason__c from task where id =: newtas.id];
        
        //initialize
        RecordType rec = [select id, name from RecordType where name like: '%' + use.Company__c + '%' and SobjectType = 'Account'];
        List <Insurance_Account__c> insaccs = new List <Insurance_Account__c> ();
        List <Account> accs = new List <account> ();

        //initial Flow
        String tel = tas.Tnfld_caller_id__c;
        if (tas.Tnfld_caller_id__c == null || tas.Tnfld_caller_id__c == 'Restricted' || tas.Tnfld_caller_id__c == 'Anonymous' || tas.Tnfld_caller_id__c == 'anonymous') {
            //create
            Account acc = new Account(Name = 'tempname', ownerid = use.id, Call_Unique_Id__c = tas.Call_Unique_Id__c, ID_Number__c = tas.ID_Number__c, Operation__c = use.Company__c, Phone = tas.Tnfld_caller_id__c, RecordTypeid = rec.id, Route_To_Tech__c = 'yes', Source_Code__c = tas.Source__c);
            if (tas.Source__c != null && tas.Source__c.left(1) == 'T')
                acc.Services_Type__c = 'Towing';
            else{
                if(use.Company__c == 'Toro')
                    acc.Services_Type__c = 'Towing';
                else
                    acc.Services_Type__c = 'Locksmith';
            }
            try{
                insert acc;
            }
            catch (System.DmlException e) {     
                    message+=e.getDmlMessage(0)+', ';  
            }
            Account accn = [select id, Reference__c from account where id =: acc.id];
            tas.Work_Order__c = accn.id;
            tas.Reason__c = 'Restricted Or Anonymous';
            try {
                update tas;
            } 
            catch (System.DmlException e) {     
                    message+=e.getDmlMessage(0)+', ';  
            }
            if (use.Company__c == 'Green')
                accn.Name = 'GN#' + accn.Reference__c;
            if (use.Company__c == 'Toro')
                accn.Name = 'TO#' + accn.Reference__c;
            if (use.Company__c == '360TS')
                accn.Name = 'TS#' + accn.Reference__c;
            if (use.Company__c == 'Transporteam')
                accn.Name = 'TR#' + accn.Reference__c;
            try{
                update accn;
            }
            catch (System.DmlException e) {     
                    message+=e.getDmlMessage(0)+', ';  
            }
            //end create
        } 
        else {
            tel = tel.replaceAll(' ', '');
            tel = tel.replaceAll('-', '');
            tel = tel.replaceAll('[)]', '');
            tel = tel.replaceAll('[(]', '');
            String temptel = '%' + tel + '%';
            insaccs = [select id, Name from Insurance_Account__c where Callerid__c like: temptel and Operation__c =: use.Company__c limit 1];
        }
        //insurance account
        if (insaccs.size() > 0) {
            //create
            Account acc = new Account(Name = 'tempname', ownerid = use.id, Call_Unique_Id__c = tas.Call_Unique_Id__c, ID_Number__c = tas.ID_Number__c, Operation__c = use.Company__c, Phone = tas.Tnfld_caller_id__c, RecordTypeid = rec.id, Route_To_Tech__c = 'yes', Source_Code__c = tas.Source__c,Non_Integrated_Call__c=true,Status__c='Non-Integrated Call');
            if (tas.Source__c != null && tas.Source__c.left(1) == 'T')
                acc.Services_Type__c = 'Towing';
            else{
                if(use.Company__c == 'Toro')
                    acc.Services_Type__c = 'Towing';
                else
                    acc.Services_Type__c = 'Locksmith';
            }
            acc.Insurance_Account__c = insaccs[0].id;
            try{
                insert acc;
            }
            catch (System.DmlException e) {     
                    message+=e.getDmlMessage(0)+', ';  
            }
            Account accn = [select id, Reference__c from account where id =: acc.id];
            tas.Work_Order__c = accn.id;
            tas.Reason__c = 'Insurance Account';
            try {
                update tas;
            } 
            catch (System.DmlException e) {     
                    message+=e.getDmlMessage(0)+', ';  
            }
            if (use.Company__c == 'Green')
                accn.Name = 'GN#' + accn.Reference__c;
            if (use.Company__c == 'Toro')
                accn.Name = 'TO#' + accn.Reference__c;
            if (use.Company__c == '360TS')
                accn.Name = 'TS#' + accn.Reference__c;
            if (use.Company__c == 'Transporteam')
                accn.Name = 'TR#' + accn.Reference__c;
            try{
                update accn;
            }
            catch (System.DmlException e) {     
                    message+=e.getDmlMessage(0)+', ';  
            }
            //end create 
        }
        //not insurance account
        else {
            accs = [select id, Name, Last_name__c,AltPhoneDev__c, First_Name__c, CreatedDate, Status__c, Owner.Name, Source_Site__c, BillingStreet, BillingCity, BillingpostalCode, Flat_Rate__c, Service_Call__c, Minimum_Job_Rate__c, Maximum_Job_Rate__c, Mileage__c, Tech_ETA__c, Services_Type__c, Last_24_Hours__c, Last_72_Hours__c, Source_Code__c, SourceKey__c, Owner.CommunityNickname FROM Account WHERE (PhoneDev__c =: tel OR AltPhoneDev__c =: tel) AND Status__c != 'Cancel'
                /*AND Status__c != 'Don’t provide Services'*/
                /*AND Status__c != 'Irrelevant call'*/
                AND Operation__c =: use.Company__c
                order by CreatedDate DESC
            ];
            List < account > accfinish = new list < account > ();
            List < account > accreg = new list < account > ();
            List < account > accalt = new list < account > (); //added 9/10/17
            for (Account ac: accs)
                if(ac.AltPhoneDev__c==tel&&((ac.Services_Type__c == 'Towing' && ac.Last_24_Hours__c == TRUE) || (ac.Services_Type__c != 'Towing' && ac.Last_72_Hours__c == TRUE)))
                    accalt.add(ac);
                else
                    if (ac.Status__c == 'Finished' || ac.Status__c == 'Financial Hold')
                        accfinish.add(ac);
                    else 
                        if ((ac.Services_Type__c == 'Towing' && ac.Last_24_Hours__c == TRUE) || (ac.Services_Type__c != 'Towing' && ac.Last_72_Hours__c == TRUE))
                            accreg.add(ac);
            //altphone list
            if(accalt.size()>0){
                //connect to tas
                tas.Work_Order__c = accalt[0].id;
                tas.Reason__c = 'Alt Phone';
                try{
                    update tas;
                }
                catch (System.DmlException e) {     
                        message+=e.getDmlMessage(0)+', ';  
                }
            }
            else{
                //just finished account
                if (accfinish.size() > 0 && accreg.size() == 0) {
                    //connect to tas
                    tas.Work_Order__c = accfinish[0].id;
                    tas.Reason__c = 'Finished';
                    try{
                        update tas;
                    }
                    catch (System.DmlException e) {     
                            message+=e.getDmlMessage(0)+', ';  
                    }
                }
                else {
                    //no finished account and no account
                    if (accreg.size() == 0){
                        if (tas.Source__c!=null && !tas.Source__c.contains('Returning') && !tas.Source__c.contains('Call-Back') && !tas.Source__c.contains('Direct') && !tas.Source__c.contains('Return') && !tas.Source__c.contains('Tech')&& !tas.Source__c.contains('Multi-Calls')) {
                            //create
                            Account acc = new Account(Name = 'tempname', ownerid = use.id, Call_Unique_Id__c = tas.Call_Unique_Id__c, ID_Number__c = tas.ID_Number__c, Operation__c = use.Company__c, Phone = tas.Tnfld_caller_id__c, RecordTypeid = rec.id, Route_To_Tech__c = 'yes', Source_Code__c = tas.Source__c,Non_Integrated_Call__c=true,Status__c='Non-Integrated Call');
                            if (tas.Source__c != null && tas.Source__c.left(1) == 'T')
                                acc.Services_Type__c = 'Towing';
                            else{
                                if(use.Company__c == 'Toro')
                                    acc.Services_Type__c = 'Towing';
                                else
                                    acc.Services_Type__c = 'Locksmith';
                            }
                            Account accn = new Account();
                            try{
                                insert acc;
                                accn = [select id, Reference__c from account where id =: acc.id];
                            }
                            catch (System.DmlException e) {     
                                    message+=e.getDmlMessage(0)+', ';  
                            }
                            tas.Work_Order__c = accn.id;
                            tas.Reason__c = 'New Work Order';
                            try {
                                update tas;
                            } 
                            catch (System.DmlException e) {     
                                    message+=e.getDmlMessage(0)+', ';  
                            }
                            if (use.Company__c == 'Green')
                                accn.Name = 'GN#' + accn.Reference__c;
                            if (use.Company__c == 'Toro')
                                accn.Name = 'TO#' + accn.Reference__c;
                            if (use.Company__c == '360TS')
                                accn.Name = 'TS#' + accn.Reference__c;
                            if (use.Company__c == 'Transporteam')
                                accn.Name = 'TR#' + accn.Reference__c;
                            try{
                                update accn;
                            }
                            catch (System.DmlException e) {     
                                    message+=e.getDmlMessage(0)+', ';  
                            }
                            //end create 
                        }
                        else{
                            tas.Reason__c='Returning Without Work Order';
                            try {
                                update tas;
                            } 
                            catch (System.DmlException e) {     
                                    message+=e.getDmlMessage(0)+', ';  
                            }
                        }
                    } 
                    else {
                        if (tas.Source__c!=null&&(tas.Source__c.contains('Returning') || tas.Source__c.contains('Call-Back') || tas.Source__c.contains('Direct') || tas.Source__c.contains('Return') || tas.Source__c.contains('Tech') || tas.Source__c.contains('Multi-Calls'))) {
                            //connect to tas
                            tas.Work_Order__c = accreg[0].id;
                            tas.Reason__c = 'Returning with Work Order';
                            try{
                                update tas;
                            }
                            catch (System.DmlException e) {     
                                    message+=e.getDmlMessage(0)+', ';  
                            }
                        } 
                        else {
                            integer i=0;
                            for (account acc: accreg)
                                if (acc.SourceKey__c == tas.SourceKey__c) {
                                    //connect to tas
                                    tas.Work_Order__c = acc.id;
                                    tas.Reason__c = 'Same Source';
                                    try{
                                        update tas;
                                    }
                                    catch (System.DmlException e) {     
                                            message+=e.getDmlMessage(0)+', '; 
                                    }
                                    i++;
                                    break;
                                }
                            if(i==0){
                                //Copy Create
                                Account acc = [select id, Name, Salutation, FirstName, LastName, AccountNumber, RecordTypeid, Site, AccountSource, AnnualRevenue, PersonAssistantName, PersonAssistantPhone, BillingCity,BillingCountry,BillingState,BillingStreet,BillingPostalCode,ShippingCity,ShippingCountry,ShippingState,ShippingStreet,ShippingPostalCode, PersonBirthdate, Jigsaw, PersonDepartment, Description, PersonDoNotCall, PersonEmail, PersonHasOptedOutOfEmail, NumberOfEmployees, Fax, PersonHasOptedOutOfFax, PersonHomePhone, Industry, PersonLastCURequestDate, PersonLastCUUpdateDate, PersonLeadSource, PersonMailingAddress, PersonMobilePhone, PersonOtherAddress, PersonOtherPhone, Ownership, Parentid, Phone, Rating, Sic, SicDesc, TickerSymbol, PersonTitle, Type, Website, Access_Code2__c, Access_Code__c, Insurance_Account__c, Actual_Distance__c, Alt_name__c, Alt_Phone__c, Appointment_Date_Time__c, Notes4__c, Area__c, Billing_Address__c, Billing_Zip__c, Bill_To__c, Block_in_Trrafic__c, Business_Name__c, Call_Center_Notes_Green__c, Notes__c, Call_Center_Notes_Toro__c, Call_Tagging__c, Call_unique_id__c, Cancelled_By__c, Cancel_Reason__c, Card_Holder_Name__c, Card_Holder_Phone__c, Cash_Amount__c, CC_Approval_Code__c, Check_Amount__c, Cleaning_Fee__c, Color__c, Office_Credit__c, Company_Policy__c, Connivant_DropOff_Fee__c, Contact_Category__c, Contact_Info__c, Credit_Amount__c, Credit_Card__c, Credit_Card_Number__c, Customer_Email__c, Customer_Invoice_Num__c, Customer_Invoice__c, Customer_Invoice_Date__c, Customer_Type__c, Customer_Type_Green__c, Don_t_Provide_Service__c, ETA_Estimation__c, Expiration_Date__c, External_Fees__c, Extra_Labor__c, Extra_Milage__c, Facing_To__c, First_Name__c, Flat_Rate__c, Flat_Tire_Fee__c, Geolocation1__c, Geolocation2__c, GOA__c, Going_Into_Nutral_Gear_Position__c, Heigt__c, Hookup_Fee__c, HQ_Charged__c, HQ_Credit_Amount__c, HWY_PickUp_Fee__c, ID_Number__c, Income_compensation__c, Invoice_Hookup_Fee__c, Invoice_Mileage__c, Green_Invoice_Number__c, NG_Invoice_Number__c, Toro_Invoice_Number__c, Trio_Invoice_Number__c, X360TS_Invoice_Number__c, Invoice_Service_Call__c, Irrelevant_Services_Calls__c, Job_Type__c, Jump_Start_Fee__c, Key_Type__c, KM__c, Labor__c, Last_Four__c, Last_Name__c, Length__c, Lock_Out_Fee__c, Make__c, Car_Makers__c, Material__c, Material2__c, Maximum_Job_Rate__c, Company_Merchant_Fee__c, Merchant_Transaction_Fee__c, Mileage1__c, Mileage__c, Mileage_Actual__c, Minimum_Job_Rate__c, Missing_Info_24__c, Model__c, MODEL_trr__c, Name_Of_Location__c, No_CC_Details_24__c, Notes3__c, Not_The_Owner_24__c, Office_Cash_Check__c, Operation__c, Other_Invoice_Fees__c, Notes2__c, Payment_Type_Invoice__c, Plate__c, PO__c, Price_Notes__c, Priority__c, Project_Manager__c, Remarks__c, Required_Truck__c, Route_To_Tech__c, Final_Price__c, SAP_Account__c, Security_Code__c, Service_Call__c, Services_Type__c, Service_Technician__c, Sholder__c, Shoulder__c, Signature__c, Special_Equipment__c, Specific_Located_At__c, Status__c, Store_Front_24__c, Tech_ETA__c, Technician_Cash_Check__c, Technician_Credit__c, Direction__c, Use_Alt_Phone__c, Vehicle_In_Yard__c, Weight__c, Width__c, Year__c, YEARS__c, YEAR_VEHIC_TORO__c, Car_Details__c
                                    from account where id =: accreg[0].id
                                ];
                                Account accn = acc.Clone(false, true);
                                if (accn.Name.contains('(Copy')) {
                                    String[] spl = accn.Name.split('[(]');
                                    accn.Name = spl[0] + '(Copy ' + accs.size() + ')';
                                } 
                                else
                                    accn.Name += '(Copy ' + accs.size() + ')';
                                accn.Parentid = acc.id;
                                accn.Source_Code__c = tas.Source__c;
                                accn.Status__c='Non-Integrated Call';
                                accn.Non_Integrated_Call__c=true;
                                accn.ownerid = use.id;
                                try{
                                    insert accn;
                                }
                                catch (System.DmlException e) {     
                                        message+=e.getDmlMessage(0)+', '; 
                                }
                                tas.Work_Order__c = accn.id;
                                tas.Reason__c = 'Clone Work Order Diffrent Source';
                                try {
                                    update tas;
                                } 
                                catch (System.DmlException e) {     
                                        message+=e.getDmlMessage(0)+', ';  
                                }
                                //end Copy create 
                            }
                        }
                    }
                }
            }
        }
        system.debug(data+' : '+message);
        if(message!=''||Test.isRunningTest()){
            message='Task id :'+newtas.id+'  '+data+' : '+message;
            mail.setHtmlBody(message);
            if(!Test.isRunningTest())
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
        return 'Success';
    }
    public static void testing(){
        integer i=0;
        i--;
        i++;
        i--;
        i++;
        i--;
        i++;
        i--;
        i++;
        i--;
        i++;
        i--;
        i++;
        i--;
        i++;
        i--;
        i++;
        i--;
        i++;
        i--;
        i++;
        i--;
        i++;
        i--;
        i++;
        i--;
        i++;
        i--;
        i++;
        i--;
        i++;
    }
}
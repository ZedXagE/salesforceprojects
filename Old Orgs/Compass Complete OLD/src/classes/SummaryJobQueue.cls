public class SummaryJobQueue implements Queueable {
	public Invoice__c Invoice;
	public List <Account> Accounts;
    public List <Expense__c> Expenses;
	public SummaryJobQueue(Invoice__c inv,List <Account> accs,List <Expense__c> exps) {
		Invoice=inv;
		Accounts=accs;
        Expenses=exps;
	}
    public void execute(QueueableContext context) {
		upsert Invoice;
		for(Account acc:Accounts)
			acc.Invoice__c=Invoice.id;
		update Accounts;
        for(Expense__c exp:Expenses)
			exp.Job_Summary__c=Invoice.id;
		upsert Expenses;
    }
}
@isTest(seealldata=true)
private class Account_Action_Test {
		static TestMethod void test() {
		Account acc = [select id,Tech_ETA_to_Pick_Up__c,Status__c,Check_Amount__c,Cancel_Reason__c,Cancelled_By__c,Tech_ETA__c,Office_Credit__c,Technician_Cash_Check__c,
		CC_Approval_Code__c,Remarks__c,ShippingCity,ShippingCountry,ShippingPostalCode,ShippingState,ShippingStreet,Geolocation2__Latitude__s,Geolocation2__Longitude__s From Account
		Where Tech_ETA__c!=null And Geolocation2__Latitude__s!=null And Geolocation2__Longitude__s!=null And Technician_Cash_Check__c!=null And Office_Credit__c!=null limit 1];
		ApexPages.StandardController controller = new ApexPages.StandardController(acc);
		Account_Action_Controller sc = new Account_Action_Controller(controller);
		sc.En_Route_Id=acc.id;
    	sc.En_Route_Status_c=acc.Status__c;
		sc.En_Route_UpdateStatus();
		sc.Accept_Id=acc.id;
    	sc.Accept_Status_c=acc.Status__c;
		sc.Accept_Tech_ETA_to_Pick_Up_c=acc.Tech_ETA__c.Format();
		sc.Accept_UpdateStatus();
		sc.Accept_UpdateStatusNew();
		sc.Canceled_Id=acc.id;
    	sc.Canceled_Status_c=acc.Status__c;
		sc.Canceled_Cancelled_By_c=acc.Cancelled_By__c;
		sc.Canceled_Cancelled_By_c=acc.Cancelled_By__c;
		sc.Canceled_UpdateStatus();
		sc.Decline_Id=acc.id;
    	sc.Decline_Status_c=acc.Status__c;
		sc.Decline_UpdateStatus();
		sc.UpdateETA_Id=acc.id;
		sc.UpdateETA_Tech_ETA_c=acc.Tech_ETA__c.Format();
		sc.UpdateETA_UpdateStatus();
		sc.Finished_Id=acc.id;
		sc.Finished_Status_c=acc.Status__c;
		sc.Finished_Office_Credit_c=String.valueof(acc.Office_Credit__c);
        sc.Finished_Technician_Cash_Check_c=String.valueof(acc.Technician_Cash_Check__c);
        sc.Finished_Check_Amount_c=String.valueof(acc.Check_Amount__c);
        sc.Finished_CC_Approval_Code_c=acc.CC_Approval_Code__c;
        sc.Finished_Remarks_c=acc.Remarks__c;
		sc.Finished_UpdateStatus();
		sc.Loaded_Id=acc.id;
    	sc.Loaded_Status_c=acc.Status__c;
        sc.Loaded_Geolocation2_Latitude_s=String.valueOf(acc.Geolocation2__Latitude__s);
        sc.Loaded_Geolocation2_Longitude_s=String.valueOf(acc.Geolocation2__Longitude__s);
        sc.Loaded_Tech_ETA_c=acc.Tech_ETA__c.Format();
        sc.Loaded_ShippingStreet=acc.ShippingStreet;
        sc.Loaded_ShippingCity=acc.ShippingCity;
        sc.Loaded_ShippingState=acc.ShippingState;
        sc.Loaded_ShippingPostalCode=acc.ShippingPostalCode;
        sc.Loaded_ShippingCountry=acc.ShippingCountry;
		sc.Loaded_UpdateStatus();
		sc.Loaded_UpdateShipping();
	}
	
}
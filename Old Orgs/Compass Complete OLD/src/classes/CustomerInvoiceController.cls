public class CustomerInvoiceController {
    public String signatureData {set; get;}
    public String signaturepic {set; get;}
    public id Curid{get;set;}
    public account acc{get;set;}
    public Technician__c ser{get;set;}
    public String Credat{get;set;}
    public String Cusdat{get;set;}
    public String badd{get;set;}
    public String sadd{get;set;}
    public String Vec{get;set;}
    public String companyn{get;set;}
    public String companyt{get;set;}
    public String atid{get;set;}
    public Boolean sigin{get;set;}
    public Boolean noaccess{get;set;}
    public Invoice_Parameters__c inv{get;set;}
    public CustomerInvoiceController(ApexPages.StandardController controller)
    {
        atid='';
        companyn='';
        companyt='';
        noaccess=true;
        set <id> subsl = new set <id>();
        for(recordtype rec:[select id,name from recordtype])
            if(rec.name.contains('Sub'))
              subsl.add(rec.id);
        system.debug(Curid);
      sigin=false;
        Vec='';
        badd='';
        sadd='';
            Curid=ApexPages.currentPage().getParameters().get('id');
        acc=[select id,Signature__c,name,Operation__c,Total_Invoice_Amount__c,Total_Amount__c,Customer_Invoice_Date__c,Customer_Invoice__c,PO__c,First_Name__c,Billing_Address__c,Notes3__c
             ,Services_Type__c,CreatedDate,Reference__c,Service_Call__c,Year__c,Make__c,Model__c,Hookup_Fee__c,BillingCountry,BillingState,BillingCity,BillingPostalCode,BillingStreet
             ,Extra_Milage__c,ShippingCountry,ShippingState,ShippingCity,ShippingPostalCode,ShippingStreet,Extra_Labor__c,Special_Equipment__c,Lock_Out_Fee__c,Flat_Tire_Fee__c,Jump_Start_Fee__c
             ,Connivant_DropOff_Fee__c,HWY_PickUp_Fee__c,Cleaning_Fee__c,Material2__c,Sales_Tax__c,Invoice_Total__c,Service_Technician__c,Invoice_Hookup_Fee__c,Invoice_Service_Call__c,Invoice_Mileage__c,Car_Details__c from account where id=:Curid];
       list <Invoice_Parameters__c> invs=[select Logo_URL__c from Invoice_Parameters__c where Name=:acc.Operation__c limit 1];
        if(invs.size()>0)
          inv=invs[0];
        list <Technician__c> serl=[select id,First_Name__c,Last_Name__c,Company_Name__c,Phone__c,Email__c,Company_Phone__c,RecordTypeId from Technician__c where id=:acc.Service_Technician__c];
        if(serl.size()>0)
          ser=serl[0];
        else
            ser=new Technician__c();
        list <Technician__c> techcompname=[select id,Company_Name__c,Company_Phone__c from Technician__c where User__c=:UserInfo.getUserId() and recordTypeid in:subsl];
        if(techcompname.size()>0)
        {
            noaccess=false;
            companyn=techcompname[0].Company_Name__c;
            companyt=techcompname[0].Company_Phone__c;
            list <attachment> attach=[select id from attachment where ParentId=:ser.id and name=:'logo.jpg'];
            if(attach.size()>0)
            atid=attach[0].id;
        }
        Credat='';
        Cusdat='';
        if(acc.CreatedDate!=null)
        Credat=acc.CreatedDate.format();
        if(acc.Customer_Invoice_Date__c!=null)
        Cusdat=acc.Customer_Invoice_Date__c.format();
        if(acc.BillingStreet!=null)
            badd+=acc.BillingStreet+', ';
        if(acc.BillingCity!=null)
            badd+=acc.BillingCity+', ';
        if(acc.BillingState!=null)
            badd+=acc.BillingState;
        if(acc.BillingPostalCode!=null)
            badd+=acc.BillingPostalCode+' , ';
        else
        {
            if(acc.BillingPostalCode==null&&acc.BillingState!=null)
                badd+=', ';
        }
        if(acc.BillingCountry!=null)
            badd+=acc.BillingCountry;
        sadd='';
        if(acc.ShippingStreet!=null)
            sadd+=acc.ShippingStreet+', ';
        if(acc.ShippingCity!=null)
            sadd+=acc.ShippingCity+', ';
        if(acc.ShippingState!=null)
            sadd+=acc.ShippingState;
        if(acc.ShippingPostalCode!=null)
            sadd+=acc.ShippingPostalCode+' , ';
        else
        {
            if(acc.ShippingPostalCode==null&&acc.ShippingState!=null)
                sadd+=', ';
        }
        if(acc.ShippingCountry!=null)
            sadd+=acc.ShippingCountry;
        if(acc.Year__c!=null)
            Vec+=acc.Year__c;
        if(acc.Make__c!=null)
            Vec+='/'+acc.Make__c;
        if(acc.Model__c!=null)
            Vec+='/'+acc.Model__c;
    }
    public pagereference GeneratePDF(){
      sigin=false;
        List <string> b64 =new list <string>();
        String ur='/apex/CustomerInvoice1?id='+acc.id;
        if (signatureData != null && signatureData != '')
        {
            acc.Signature__c = '<img style="width: 100%;height:150px;" src="' + signatureData + '"/>';
            update acc;
            b64=signatureData.split(',');
            if(b64.size()>1)
            {
                Document doc = new Document();
                doc.Name=acc.name;
                doc.AuthorId = UserInfo.getUserId();
                doc.FolderId = UserInfo.getUserId();
                doc.ContentType = 'image/jpeg';
                doc.Type='jpg';
                doc.IsPublic=true;
                doc.Body =EncodingUtil.base64Decode(b64[1]);
                insert doc;
                ur='/apex/CustomerInvoice1?id='+acc.id+'&sigpic='+doc.id;
            }
            PageReference newPage = new PageReference(ur);
            newPage.setRedirect(true);
            return newPage;
        }
        else
        {
          sigin=true;
          return null;
        }
    }
}
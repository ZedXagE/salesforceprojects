@isTest
public class TestTrioAlerts {
	static testMethod void Test1() {
		RestRequest req = new RestRequest();
		req.params.put('pbxid', '2006');
		req.params.put('ibnum', '4083297334');
		req.params.put('callerid', '972508292891');
		req.params.put('calleruniqueid', '5432');
		req.params.put('source', 'L-SJ-PPC-vsfx/VSFXServices*Sunnyvale');
		req.params.put('agent', '100');
		req.params.put('duration', '321');
		req.params.put('agentdur', '308');
		RestResponse res = new RestResponse();
		RestContext.request = req;
		RestContext.response = res;
		Test.startTest();
		String s=TrioAlerts.Flow();
		req.params.put('callerid', 'Restricted');
		s=TrioAlerts.Flow();
		Test.stopTest();
	}
	static testMethod void Test2() {
		Account acc=new Account(name='test',Services_Type__c ='Towing',Operation__c='360TS',Phone='972508292891');
		insert acc;
		RestRequest req = new RestRequest();
		req.params.put('pbxid', '2006');
		req.params.put('ibnum', '4083297334');
		req.params.put('callerid', '972508292891');
		req.params.put('calleruniqueid', '5432');
		req.params.put('source', 'L-SJ-PPC-vsfx/VSFXServices*Sunnyvale');
		req.params.put('agent', '100');
		req.params.put('duration', '321');
		req.params.put('agentdur', '308');
		RestResponse res = new RestResponse();
		RestContext.request = req;
		RestContext.response = res;
		Test.startTest();
		String s=TrioAlerts.Flow();
		Test.stopTest();
	}
	static testMethod void Test3() {
		Account acc=new Account(name='test',Services_Type__c ='Towing');
		insert acc;
		RestRequest req = new RestRequest();
		req.params.put('pbxid', '2006');
		req.params.put('ibnum', '4083297334');
		req.params.put('callerid', '972508292891');
		req.params.put('calleruniqueid', '5432');
		req.params.put('source', 'L-SJ-PPC-vsfx/VSFXServices*Sunnyvale');
		req.params.put('agent', '100');
		req.params.put('duration', '321');
		req.params.put('agentdur', '308');
		RestResponse res = new RestResponse();
		RestContext.request = req;
		RestContext.response = res;
		Test.startTest();
		String s=TrioAlerts.Flow();
		Test.stopTest();
	}
	static testMethod void Test4() {
		Insurance_Account__c insacc=new Insurance_Account__c(Callerid__c='972508292891',Operation__c ='360TS');
		insert insacc;
		RestRequest req = new RestRequest();
		req.params.put('pbxid', '2006');
		req.params.put('ibnum', '4083297334');
		req.params.put('callerid', '972508292891');
		req.params.put('calleruniqueid', '5432');
		req.params.put('source', 'L-SJ-PPC-vsfx/VSFXServices*Sunnyvale');
		req.params.put('agent', '100');
		req.params.put('duration', '321');
		req.params.put('agentdur', '308');
		RestResponse res = new RestResponse();
		RestContext.request = req;
		RestContext.response = res;
		Test.startTest();
		String s=TrioAlerts.Flow();
		Test.stopTest();
	}
	static testMethod void Test5() {
		Account acc=new Account(name='test',Services_Type__c ='Towing',Operation__c='360TS',Alt_Phone__c='972508292891');
		insert acc;
		RestRequest req = new RestRequest();
		req.params.put('pbxid', '2006');
		req.params.put('ibnum', '4083297334');
		req.params.put('callerid', '972508292891');
		req.params.put('calleruniqueid', '5432');
		req.params.put('source', 'L-SJ-PPC-vsfx/VSFXServices*Sunnyvale');
		req.params.put('agent', '100');
		req.params.put('duration', '321');
		req.params.put('agentdur', '308');
		RestResponse res = new RestResponse();
		RestContext.request = req;
		RestContext.response = res;
		Test.startTest();
		String s=TrioAlerts.Flow();
		Test.stopTest();
	}
	static testMethod void Test6() {
		Account acc=new Account(name='test',Services_Type__c ='Towing',Operation__c='360TS',Phone='972508292891',Status__c='Finished');
		insert acc;
		RestRequest req = new RestRequest();
		req.params.put('pbxid', '2006');
		req.params.put('ibnum', '4083297334');
		req.params.put('callerid', '972508292891');
		req.params.put('calleruniqueid', '5432');
		req.params.put('source', 'L-SJ-PPC-vsfx/VSFXServices*Sunnyvale');
		req.params.put('agent', '100');
		req.params.put('duration', '321');
		req.params.put('agentdur', '308');
		RestResponse res = new RestResponse();
		RestContext.request = req;
		RestContext.response = res;
		Test.startTest();
		String s=TrioAlerts.Flow();
		Test.stopTest();
		TrioAlerts.Testing();
	}
    static testMethod void Test7() {
		RestRequest req = new RestRequest();
		req.params.put('pbxid', '2006');
		req.params.put('ibnum', '4083297334');
		req.params.put('callerid', '972508292891');
		req.params.put('calleruniqueid', '5432');
		req.params.put('source', 'ReturningL-SJ-PPC-vsfx/VSFXServices*Sunnyvale');
		req.params.put('agent', '100');
		req.params.put('duration', '321');
		req.params.put('agentdur', '308');
		RestResponse res = new RestResponse();
		RestContext.request = req;
		RestContext.response = res;
		Test.startTest();
		String s=TrioAlerts.Flow();
		req.params.put('callerid', 'Restricted');
		s=TrioAlerts.Flow();
		Test.stopTest();
	}
}
public with sharing class InitialFlowController {
    public String Curid;
    public Task tas {get;set;}
    public User use {get;set;}
    public RecordType rec {get;set;}
    public list < account > accs {get;set;}
    public list < Insurance_Account__c > insaccs {get;set;}
    public String Status {get;set;}
    public InitialFlowController() 
    {
        CurId = ApexPages.currentPage().getParameters().get('id');
        use = [select id, Name, Company__c, CommunityNickname from user where id =: UserInfo.getUserId()];
        rec = [select id, name from RecordType where name like: '%' + use.Company__c + '%'
            and SobjectType = 'Account'
        ];
        insaccs = new list < Insurance_Account__c > ();
        accs = new list < account > ();
        tas = new Task();
    }
    Public PageReference Start() 
    {
        if (CurId != null && Curid != '') 
        {
            tas = [select id, Call_Unique_Id__c, ID_Number__c, OwnerId, Source__c, Tnfld_caller_id__c, whatid, Work_Order__c,SourceKey__c from task where id =: Curid];
            String tel = tas.Tnfld_caller_id__c;
            if (tas.Tnfld_caller_id__c == null || tas.Tnfld_caller_id__c == 'Restricted' || tas.Tnfld_caller_id__c == 'Anonymous' || tas.Tnfld_caller_id__c == 'anonymous') 
            {
                PageReference pageRef = Create();
                pageref.setRedirect(true);
                return pageRef;
            } 
            else 
            {
                tel = tel.replaceAll(' ', '');
                tel = tel.replaceAll('-', '');
                tel = tel.replaceAll('[)]', '');
                tel = tel.replaceAll('[(]', '');
                String temptel = '%' + tel + '%';
                insaccs = [select id, Name from Insurance_Account__c where Callerid__c like: temptel and Operation__c =: use.Company__c limit 1];
            }
            //insurance account
            if (insaccs.size() > 0) 
            {
                accs = [select id, Name, Last_name__c, First_Name__c, Insurance_Account__r.Name, CreatedDate, Status__c, Owner.Name, Source_Site__c, BillingStreet, BillingCity, BillingpostalCode, Flat_Rate__c, Service_Call__c, Minimum_Job_Rate__c, Maximum_Job_Rate__c, Mileage__c, Tech_ETA__c, PO__c, Owner.CommunityNickname, Service_Technician__r.Name FROM Account WHERE Insurance_Account__c =: insaccs[0].id AND Last_12_Hours__c = TRUE AND Status__c != 'Financial Hold'
                    AND Status__c != 'Finished'
                    AND Status__c != 'Don’t provide Services'
                    /*AND Status__c != 'Irrelevant call'*/
                    order by CreatedDate ASC
                ];
                if (accs.size() == 0) 
                {
                    PageReference pageRef = Create();
                    pageref.setRedirect(true);
                    return pageRef;
                } 
                else 
                {
                    Status = 'Rule4';
                    return null;
                }
            }
            //not insurance account
            else 
            {
                accs = [select id, Name, Last_name__c,AltPhoneDev__c, First_Name__c, CreatedDate, Status__c, Owner.Name, Source_Site__c, BillingStreet, BillingCity, BillingpostalCode, Flat_Rate__c, Service_Call__c, Minimum_Job_Rate__c, Maximum_Job_Rate__c, Mileage__c, Tech_ETA__c, Services_Type__c, Last_24_Hours__c, Last_72_Hours__c, Source_Code__c, SourceKey__c, Owner.CommunityNickname FROM Account WHERE (PhoneDev__c =: tel OR AltPhoneDev__c =: tel) AND Status__c != 'Cancel'
                    /*AND Status__c != 'Don’t provide Services'*/
                    /*AND Status__c != 'Irrelevant call'*/
                    order by CreatedDate ASC
                ];
                List < account > accfinish = new list < account > ();
                List < account > accreg = new list < account > ();
                List < account > accalt = new list < account > (); //added 9/10/17
                for (Account ac: accs) 
                {
                    if(ac.AltPhoneDev__c==tel&&((ac.Services_Type__c == 'Towing' && ac.Last_24_Hours__c == TRUE) || (ac.Services_Type__c != 'Towing' && ac.Last_72_Hours__c == TRUE)))
                    {
                        accalt.add(ac);
                    }
                    else
                    {
                        if (ac.Status__c == 'Finished' || ac.Status__c == 'Financial Hold')
                            accfinish.add(ac);
                        else 
                        {
                            if ((ac.Services_Type__c == 'Towing' && ac.Last_24_Hours__c == TRUE) || (ac.Services_Type__c != 'Towing' && ac.Last_72_Hours__c == TRUE))
                                accreg.add(ac);
                        }
                    }
                }
                //altphone list
                if(accalt.size()>0)
                {
                    if(accalt.size()>1)
                    {
                        accs = accalt;
                        Status = 'Rule7';
                        return null;
                    }
                    else
                    {
                        PageReference pageRef = Redirect(accalt[0].id);
                        tas.Work_Order__c = accalt[0].id;
                        update tas;
                        return pageRef;
                    }
                }
                else
                    accs = accreg;
                //just finished account
                if (accfinish.size() > 0 && accs.size() == 0) 
                {
                    accs = accfinish;
                    Status = 'Rule6';
                    return null;
                }
                else 
                {
                    //no finished account and no account
                    if (accs.size() == 0)
                    {
                        PageReference pageRef = Create();
                        pageref.setRedirect(true);
                        return pageRef;
                    } 
                    else 
                    {
                        if (tas.Source__c!=null&&(tas.Source__c.contains('Returning') || tas.Source__c.contains('Call-Back') || tas.Source__c.contains('Direct') || tas.Source__c.contains('Return'))) {
                            if (accs.size() == 1) 
                            {
                                PageReference pageRef = Redirect(accs[0].id);
                                tas.Work_Order__c = accs[0].id;
                                update tas;
                                return pageRef;
                            } 
                            else 
                            {
                                Status = 'Rule5';
                                return null;
                            }
                        } 
                        else 
                        {
                            for (account acc: accs)
                                if (acc.SourceKey__c == tas.SourceKey__c) {
                                    PageReference pageRef = Redirect(acc.id);
                                    tas.Work_Order__c = acc.id;
                                    update tas;
                                    return pageRef;
                                }
                                else
                                    Status = 'Rule2';

                            for (account acc: accs)
                                if (acc.Status__c == 'Pending' || acc.Status__c == 'Appointment' || acc.Status__c == 'Assigned' || acc.Status__c == 'Confirmed' || acc.Status__c == 'On Route' || acc.Status__c == 'Loaded') {
                                    Status = 'Rule8';
                                    return null;
                                }
                                else
                                    Status = 'Rule2';
                            return null;
                        }
                    }
                }
            }
        } 
        else 
        {
            Status = 'error';
            return null;
        }
    }
    Public PageReference Create() 
    {
        Account acc = new Account(Name = 'tempname', ownerid = use.id, Call_Unique_Id__c = tas.Call_Unique_Id__c, ID_Number__c = tas.ID_Number__c, Operation__c = use.Company__c, Phone = tas.Tnfld_caller_id__c, RecordTypeid = rec.id, Route_To_Tech__c = 'yes', Source_Code__c = tas.Source__c);
        if (tas.Source__c != null && tas.Source__c.left(1) == 'T')
            acc.Services_Type__c = 'Towing';
        else
        {
            if(use.Company__c == 'Toro')
                acc.Services_Type__c = 'Towing';
            else
                acc.Services_Type__c = 'Locksmith';
        }
        if (insaccs.size() > 0)
            acc.Insurance_Account__c = insaccs[0].id;
        insert acc;
        Account accn = [select id, Reference__c from account where id =: acc.id];
        tas.Work_Order__c = accn.id;
        try 
        {
            update tas;
        } 
        catch (exception e) {}
        if (use.Company__c == 'Green')
            accn.Name = 'GN#' + accn.Reference__c;
        if (use.Company__c == 'Toro')
            accn.Name = 'TO#' + accn.Reference__c;
        if (use.Company__c == '360TS')
            accn.Name = 'TS#' + accn.Reference__c;
        if (use.Company__c == 'Transporteam')
            accn.Name = 'TR#' + accn.Reference__c;
        update accn;
        String ur = '/' + accn.id;
        PageReference pageRef = new PageReference(ur); //it works
        pageref.setRedirect(true);
        return pageRef;
    }
    Public PageReference Redirect(String reid) 
    {
        String ur = '/' + reid;
        PageReference pageRef = new PageReference(ur); //it works
        pageref.setRedirect(true);
        return pageRef;
    }
    Public PageReference CheckETA() 
    {
        String curaccid = System.currentPageReference().getParameters().get('curaccidcheck');
        Account acc = [select id, Call_Tagging__c from account where id =: curaccid];
        if (acc.Call_Tagging__c != null)
            acc.Call_Tagging__c = acc.Call_Tagging__c + ';Checking ETA';
        else
            acc.Call_Tagging__c = 'Checking ETA';
        update acc;
        tas.Work_Order__c = curaccid;
        try 
        {
            update tas;
        } 
        catch (exception e) {}
        return null;
    }
    Public PageReference rule4click() 
    {
        String curaccid = System.currentPageReference().getParameters().get('curaccidrule4');
        tas.Work_Order__c = curaccid;
        try 
        {
            update tas;
        } 
        catch (exception e) {}
        String ur = '/' + curaccid;
        PageReference pageRef = new PageReference(ur); //it works
        pageref.setRedirect(true);
        return pageRef;
    }
    Public PageReference rule2click() 
    {
        String curaccid = System.currentPageReference().getParameters().get('curaccidrule2');
        Account acc = [select id, Name, Salutation, FirstName, LastName, AccountNumber, RecordTypeid, Site, AccountSource, AnnualRevenue, PersonAssistantName, PersonAssistantPhone, BillingCity,BillingCountry,BillingState,BillingStreet,BillingPostalCode,ShippingCity,ShippingCountry,ShippingState,ShippingStreet,ShippingPostalCode, PersonBirthdate, Jigsaw, PersonDepartment, Description, PersonDoNotCall, PersonEmail, PersonHasOptedOutOfEmail, NumberOfEmployees, Fax, PersonHasOptedOutOfFax, PersonHomePhone, Industry, PersonLastCURequestDate, PersonLastCUUpdateDate, PersonLeadSource, PersonMailingAddress, PersonMobilePhone, PersonOtherAddress, PersonOtherPhone, Ownership, Parentid, Phone, Rating, Sic, SicDesc, TickerSymbol, PersonTitle, Type, Website, Access_Code2__c, Access_Code__c, Insurance_Account__c, Actual_Distance__c, Alt_name__c, Alt_Phone__c, Appointment_Date_Time__c, Notes4__c, Area__c, Billing_Address__c, Billing_Zip__c, Bill_To__c, Block_in_Trrafic__c, Business_Name__c, Call_Center_Notes_Green__c, Notes__c, Call_Center_Notes_Toro__c, Call_Tagging__c, Call_unique_id__c, Cancelled_By__c, Cancel_Reason__c, Card_Holder_Name__c, Card_Holder_Phone__c, Cash_Amount__c, CC_Approval_Code__c, Check_Amount__c, Cleaning_Fee__c, Color__c, Office_Credit__c, Company_Policy__c, Connivant_DropOff_Fee__c, Contact_Category__c, Contact_Info__c, Credit_Amount__c, Credit_Card__c, Credit_Card_Number__c, Customer_Email__c, Customer_Invoice_Num__c, Customer_Invoice__c, Customer_Invoice_Date__c, Customer_Type__c, Customer_Type_Green__c, Don_t_Provide_Service__c, ETA_Estimation__c, Expiration_Date__c, External_Fees__c, Extra_Labor__c, Extra_Milage__c, Facing_To__c, First_Name__c, Flat_Rate__c, Flat_Tire_Fee__c, Geolocation1__c, Geolocation2__c, GOA__c, Going_Into_Nutral_Gear_Position__c, Heigt__c, Hookup_Fee__c, HQ_Charged__c, HQ_Credit_Amount__c, HWY_PickUp_Fee__c, ID_Number__c, Income_compensation__c, Invoice_Hookup_Fee__c, Invoice_Mileage__c, Green_Invoice_Number__c, NG_Invoice_Number__c, Toro_Invoice_Number__c, Trio_Invoice_Number__c, X360TS_Invoice_Number__c, Invoice_Service_Call__c, Irrelevant_Services_Calls__c, Job_Type__c, Jump_Start_Fee__c, Key_Type__c, KM__c, Labor__c, Last_Four__c, Last_Name__c, Length__c, Lock_Out_Fee__c, Make__c, Car_Makers__c, Material__c, Material2__c, Maximum_Job_Rate__c, Company_Merchant_Fee__c, Merchant_Transaction_Fee__c, Mileage1__c, Mileage__c, Mileage_Actual__c, Minimum_Job_Rate__c, Missing_Info_24__c, Model__c, MODEL_trr__c, Name_Of_Location__c, No_CC_Details_24__c, Notes3__c, Not_The_Owner_24__c, Office_Cash_Check__c, Operation__c, Other_Invoice_Fees__c, Notes2__c, Payment_Type_Invoice__c, Plate__c, PO__c, Price_Notes__c, Priority__c, Project_Manager__c, Remarks__c, Required_Truck__c, Route_To_Tech__c, Final_Price__c, SAP_Account__c, Security_Code__c, Service_Call__c, Services_Type__c, Service_Technician__c, Sholder__c, Shoulder__c, Signature__c, Special_Equipment__c, Specific_Located_At__c, Status__c, Store_Front_24__c, Tech_ETA__c, Technician_Cash_Check__c, Technician_Credit__c, Direction__c, Use_Alt_Phone__c, Vehicle_In_Yard__c, Weight__c, Width__c, Year__c, YEARS__c, YEAR_VEHIC_TORO__c, Car_Details__c
            from account where id =: curaccid
        ];
        account accn = acc.Clone(false, true);
        if (accn.Name.contains('(Copy')) 
        {
            String[] spl = accn.Name.split('[(]');
            accn.Name = spl[0] + '(Copy ' + accs.size() + ')';
        } 
        else
            accn.Name += '(Copy ' + accs.size() + ')';
        
        accn.Parentid = acc.id;
        accn.Source_Code__c = tas.Source__c;
        insert accn;
        tas.Work_Order__c = accn.id;
        try 
        {
            update tas;
        } 
        catch (exception e) {}
        String ur = '/' + accn.id;
        PageReference pageRef = new PageReference(ur); //it works
        pageref.setRedirect(true);
        return pageRef;
    }
}
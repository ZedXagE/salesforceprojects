@isTest(seealldata=true)
public class CustomerInvoiceandEmailTest {
    static testMethod void Testci()
    {
        Account acc=new Account(name='testspec');
        insert acc;
        ApexPages.StandardController controller = new ApexPages.StandardController(acc);
        PageReference pgRef = Page.CustomerInvoice; //Create Page Reference - 'Appt_New' is the name of Page
		Test.setCurrentPage(pgRef); //Set the page for Test Method
		ApexPages.currentPage().getParameters().put('id', acc.id);//Pass Id to page
        CustomerInvoiceController sc = new CustomerInvoiceController(controller);
        sc.signatureData='12,13,14';
        sc.GeneratePDF();
    }
        static testMethod void Testcia()
    {
        Account acc=new Account(name='testspec');
        insert acc;
        Document doc = new Document();
                doc.Name='test1';
                doc.AuthorId = UserInfo.getUserId();
                doc.FolderId = UserInfo.getUserId();
                doc.ContentType = 'image/jpeg';
                doc.Type='jpg';
                doc.IsPublic=true;
                doc.Body =EncodingUtil.base64Decode('b64');
                insert doc;
        ApexPages.StandardController controller = new ApexPages.StandardController(acc);
        PageReference pgRef = Page.CustomerInvoice1; //Create Page Reference - 'Appt_New' is the name of Page
		Test.setCurrentPage(pgRef); //Set the page for Test Method
		ApexPages.currentPage().getParameters().put('id', acc.id);//Pass Id to page
    ApexPages.currentPage().getParameters().put('toro', '1');//Pass Id to page
        ApexPages.currentPage().getParameters().put('sigpic', doc.id);//Pass Id to page
        CustomerInvoiceControllerAccept sc = new CustomerInvoiceControllerAccept(controller);
        sc.signatureData='12,13,14';
        sc.GeneratePDF();
        sc.GenerateToroPDF();
        sc.GoToro();
        sc.back();
    }
    static testMethod void Testem()
    {
        Account acc=new Account(name='testspec',Customer_Invoice__c='test',Customer_Email__c='test@test.com');
        insert acc;
        attachment at = new Attachment();
        at.body=Blob.valueOf('Unit Test Attachment Body');
        at.OwnerId = UserInfo.getUserId();
        at.ParentId = acc.Id;
        at.Name=acc.Customer_Invoice__c+'.pdf';
        at.ContentType = 'application/pdf';
        insert at;
        ApexPages.StandardController controller = new ApexPages.StandardController(acc);
        PageReference pgRef = Page.InvoiceEmail; //Create Page Reference - 'Appt_New' is the name of Page
		Test.setCurrentPage(pgRef); //Set the page for Test Method
		ApexPages.currentPage().getParameters().put('id', acc.id);//Pass Id to page
        ApexPages.currentPage().getParameters().put('atid', at.id);//Pass Id to page
        InvoiceEmailController sc = new InvoiceEmailController(controller);
        sc.SendEmail();
    }
}